var country_code = {
    "items": [
        {
            "country": "Afghanistan",
            "code": "93"
        },
        {
            "country": "Albania",
            "code": "355"
        },
        {
            "country": "Algeria",
            "code": "213"
        },
        {
            "country": "AmericanSamoa",
            "code": "1684"
        },
        {
            "country": "Andorra",
            "code": "376"
        },
        {
            "country": "Angola",
            "code": "244"
        },
        {
            "country": "Anguilla",
            "code": "1264"
        },
        {
            "country": "Antarctica",
            "code": "672"
        },
        {
            "country": "AntiguaandBarbuda",
            "code": "1268"
        },
        {
            "country": "Argentina",
            "code": "54"
        },
        {
            "country": "Armenia",
            "code": "374"
        },
        {
            "country": "Aruba",
            "code": "297"
        },
        {
            "country": "Australia",
            "code": "61"
        },
        {
            "country": "Austria",
            "code": "43"
        },
        {
            "country": "Azerbaijan",
            "code": "994"
        },
        {
            "country": "Bahamas",
            "code": "1242"
        },
        {
            "country": "Bahrain",
            "code": "973"
        },
        {
            "country": "Bangladesh",
            "code": "880"
        },
        {
            "country": "Barbados",
            "code": "1246"
        },
        {
            "country": "Belarus",
            "code": "375"
        },
        {
            "country": "Belgium",
            "code": "32"
        },
        {
            "country": "Belize",
            "code": "501"
        },
        {
            "country": "Benin",
            "code": "229"
        },
        {
            "country": "Bermuda",
            "code": "1441"
        },
        {
            "country": "Bhutan",
            "code": "975"
        },
        {
            "country": "Bolivia",
            "code": "591"
        },
        {
            "country": "BosniaandHerzegovina",
            "code": "387"
        },
        {
            "country": "Botswana",
            "code": "267"
        },
        {
            "country": "Brazil",
            "code": "55"
        },
        {
            "country": "BritishVirginIslands",
            "code": "1284"
        },
        {
            "country": "Brunei",
            "code": "673"
        },
        {
            "country": "Bulgaria",
            "code": "359"
        },
        {
            "country": "BurkinaFaso",
            "code": "226"
        },
        {
            "country": "Burma(Myanmar",
            "code": "95"
        },
        {
            "country": "Burundi",
            "code": "257"
        },
        {
            "country": "Cambodia",
            "code": "855"
        },
        {
            "country": "Cameroon",
            "code": "237"
        },
        {
            "country": "Canada",
            "code": "1"
        },
        {
            "country": "CapeVerde",
            "code": "238"
        },
        {
            "country": "CaymanIslands",
            "code": "1345"
        },
        {
            "country": "CentralAfricanRepublic",
            "code": "236"
        },
        {
            "country": "Chad",
            "code": "235"
        },
        {
            "country": "Chile",
            "code": "56"
        },
        {
            "country": "China",
            "code": "86"
        },
        {
            "country": "ChristmasIsland",
            "code": "61"
        },
        {
            "country": "Cocos(KeelingIslands",
            "code": "61"
        },
        {
            "country": "Colombia",
            "code": "57"
        },
        {
            "country": "Comoros",
            "code": "269"
        },
        {
            "country": "CookIslands",
            "code": "682"
        },
        {
            "country": "CostaRica",
            "code": "506"
        },
        {
            "country": "Croatia",
            "code": "385"
        },
        {
            "country": "Cuba",
            "code": "53"
        },
        {
            "country": "Cyprus",
            "code": "357"
        },
        {
            "country": "CzechRepublic",
            "code": "420"
        },
        {
            "country": "DemocraticRepublicoftheCongo",
            "code": "243"
        },
        {
            "country": "Denmark",
            "code": "45"
        },
        {
            "country": "Djibouti",
            "code": "253"
        },
        {
            "country": "Dominica",
            "code": "1767"
        },
        {
            "country": "DominicanRepublic",
            "code": "1809"
        },
        {
            "country": "Ecuador",
            "code": "593"
        },
        {
            "country": "Egypt",
            "code": "20"
        },
        {
            "country": "ElSalvador",
            "code": "503"
        },
        {
            "country": "EquatorialGuinea",
            "code": "240"
        },
        {
            "country": "Eritrea",
            "code": "291"
        },
        {
            "country": "Estonia",
            "code": "372"
        },
        {
            "country": "Ethiopia",
            "code": "251"
        },
        {
            "country": "FalklandIslands",
            "code": "500"
        },
        {
            "country": "FaroeIslands",
            "code": "298"
        },
        {
            "country": "Fiji",
            "code": "679"
        },
        {
            "country": "Finland",
            "code": "358"
        },
        {
            "country": "France",
            "code": "33"
        },
        {
            "country": "FrenchPolynesia",
            "code": "689"
        },
        {
            "country": "Gabon",
            "code": "241"
        },
        {
            "country": "Gambia",
            "code": "220"
        },
        {
            "country": "GazaStrip",
            "code": "970"
        },
        {
            "country": "Georgia",
            "code": "995"
        },
        {
            "country": "Germany",
            "code": "49"
        },
        {
            "country": "Ghana",
            "code": "233"
        },
        {
            "country": "Gibraltar",
            "code": "350"
        },
        {
            "country": "Greece",
            "code": "30"
        },
        {
            "country": "Greenland",
            "code": "299"
        },
        {
            "country": "Grenada",
            "code": "1473"
        },
        {
            "country": "Guam",
            "code": "1671"
        },
        {
            "country": "Guatemala",
            "code": "502"
        },
        {
            "country": "Guinea",
            "code": "224"
        },
        {
            "country": "Guinea-Bissau",
            "code": "245"
        },
        {
            "country": "Guyana",
            "code": "592"
        },
        {
            "country": "Haiti",
            "code": "509"
        },
        {
            "country": "HolySee(VaticanCity",
            "code": "39"
        },
        {
            "country": "Honduras",
            "code": "504"
        },
        {
            "country": "HongKong",
            "code": "852"
        },
        {
            "country": "Hungary",
            "code": "36"
        },
        {
            "country": "Iceland",
            "code": "354"
        },
        {
            "country": "India",
            "code": "91"
        },
        {
            "country": "Indonesia",
            "code": "62"
        },
        {
            "country": "Iran",
            "code": "98"
        },
        {
            "country": "Iraq",
            "code": "964"
        },
        {
            "country": "Ireland",
            "code": "353"
        },
        {
            "country": "IsleofMan",
            "code": "44"
        },
        {
            "country": "Israel",
            "code": "972"
        },
        {
            "country": "Italy",
            "code": "39"
        },
        {
            "country": "IvoryCoast",
            "code": "225"
        },
        {
            "country": "Jamaica",
            "code": "1876"
        },
        {
            "country": "Japan",
            "code": "81"
        },
        {
            "country": "Jordan",
            "code": "962"
        },
        {
            "country": "Kazakhstan",
            "code": "7"
        },
        {
            "country": "Kenya",
            "code": "254"
        },
        {
            "country": "Kiribati",
            "code": "686"
        },
        {
            "country": "Kosovo",
            "code": "381"
        },
        {
            "country": "Kuwait",
            "code": "965"
        },
        {
            "country": "Kyrgyzstan",
            "code": "996"
        },
        {
            "country": "Laos",
            "code": "856"
        },
        {
            "country": "Latvia",
            "code": "371"
        },
        {
            "country": "Lebanon",
            "code": "961"
        },
        {
            "country": "Lesotho",
            "code": "266"
        },
        {
            "country": "Liberia",
            "code": "231"
        },
        {
            "country": "Libya",
            "code": "218"
        },
        {
            "country": "Liechtenstein",
            "code": "423"
        },
        {
            "country": "Lithuania",
            "code": "370"
        },
        {
            "country": "Luxembourg",
            "code": "352"
        },
        {
            "country": "Macau",
            "code": "853"
        },
        {
            "country": "Macedonia",
            "code": "389"
        },
        {
            "country": "Madagascar",
            "code": "261"
        },
        {
            "country": "Malawi",
            "code": "265"
        },
        {
            "country": "Malaysia",
            "code": "60"
        },
        {
            "country": "Maldives",
            "code": "960"
        },
        {
            "country": "Mali",
            "code": "223"
        },
        {
            "country": "Malta",
            "code": "356"
        },
        {
            "country": "MarshallIslands",
            "code": "692"
        },
        {
            "country": "Mauritania",
            "code": "222"
        },
        {
            "country": "Mauritius",
            "code": "230"
        },
        {
            "country": "Mayotte",
            "code": "262"
        },
        {
            "country": "Mexico",
            "code": "52"
        },
        {
            "country": "Micronesia",
            "code": "691"
        },
        {
            "country": "Moldova",
            "code": "373"
        },
        {
            "country": "Monaco",
            "code": "377"
        },
        {
            "country": "Mongolia",
            "code": "976"
        },
        {
            "country": "Montenegro",
            "code": "382"
        },
        {
            "country": "Montserrat",
            "code": "1664"
        },
        {
            "country": "Morocco",
            "code": "212"
        },
        {
            "country": "Mozambique",
            "code": "258"
        },
        {
            "country": "Namibia",
            "code": "264"
        },
        {
            "country": "Nauru",
            "code": "674"
        },
        {
            "country": "Nepal",
            "code": "977"
        },
        {
            "country": "Netherlands",
            "code": "31"
        },
        {
            "country": "NetherlandsAntilles",
            "code": "599"
        },
        {
            "country": "NewCaledonia",
            "code": "687"
        },
        {
            "country": "NewZealand",
            "code": "64"
        },
        {
            "country": "Nicaragua",
            "code": "505"
        },
        {
            "country": "Niger",
            "code": "227"
        },
        {
            "country": "Nigeria",
            "code": "234"
        },
        {
            "country": "Niue",
            "code": "683"
        },
        {
            "country": "NorfolkIsland",
            "code": "672"
        },
        {
            "country": "NorthKorea",
            "code": "850"
        },
        {
            "country": "NorthernMarianaIslands",
            "code": "1670"
        },
        {
            "country": "Norway",
            "code": "47"
        },
        {
            "country": "Oman",
            "code": "968"
        },
        {
            "country": "Pakistan",
            "code": "92"
        },
        {
            "country": "Palau",
            "code": "680"
        },
        {
            "country": "Panama",
            "code": "507"
        },
        {
            "country": "PapuaNewGuinea",
            "code": "675"
        },
        {
            "country": "Paraguay",
            "code": "595"
        },
        {
            "country": "Peru",
            "code": "51"
        },
        {
            "country": "Philippines",
            "code": "63"
        },
        {
            "country": "PitcairnIslands",
            "code": "870"
        },
        {
            "country": "Poland",
            "code": "48"
        },
        {
            "country": "Portugal",
            "code": "351"
        },
        {
            "country": "PuertoRico",
            "code": "1"
        },
        {
            "country": "Qatar",
            "code": "974"
        },
        {
            "country": "RepublicoftheCongo",
            "code": "242"
        },
        {
            "country": "Romania",
            "code": "40"
        },
        {
            "country": "Russia",
            "code": "7"
        },
        {
            "country": "Rwanda",
            "code": "250"
        },
        {
            "country": "SaintBarthelemy",
            "code": "590"
        },
        {
            "country": "SaintHelena",
            "code": "290"
        },
        {
            "country": "SaintKittsandNevis",
            "code": "1869"
        },
        {
            "country": "SaintLucia",
            "code": "1758"
        },
        {
            "country": "SaintMartin",
            "code": "1599"
        },
        {
            "country": "SaintPierreandMiquelon",
            "code": "508"
        },
        {
            "country": "SaintVincentandtheGrenadines",
            "code": "1784"
        },
        {
            "country": "Samoa",
            "code": "685"
        },
        {
            "country": "SanMarino",
            "code": "378"
        },
        {
            "country": "SaoTomeandPrincipe",
            "code": "239"
        },
        {
            "country": "SaudiArabia",
            "code": "966"
        },
        {
            "country": "Senegal",
            "code": "221"
        },
        {
            "country": "Serbia",
            "code": "381"
        },
        {
            "country": "Seychelles",
            "code": "248"
        },
        {
            "country": "SierraLeone",
            "code": "232"
        },
        {
            "country": "Singapore",
            "code": "65"
        },
        {
            "country": "Slovakia",
            "code": "421"
        },
        {
            "country": "Slovenia",
            "code": "386"
        },
        {
            "country": "SolomonIslands",
            "code": "677"
        },
        {
            "country": "Somalia",
            "code": "252"
        },
        {
            "country": "SouthAfrica",
            "code": "27"
        },
        {
            "country": "SouthKorea",
            "code": "82"
        },
        {
            "country": "Spain",
            "code": "34"
        },
        {
            "country": "SriLanka",
            "code": "94"
        },
        {
            "country": "Sudan",
            "code": "249"
        },
        {
            "country": "Suriname",
            "code": "597"
        },
        {
            "country": "Swaziland",
            "code": "268"
        },
        {
            "country": "Sweden",
            "code": "46"
        },
        {
            "country": "Switzerland",
            "code": "41"
        },
        {
            "country": "Syria",
            "code": "963"
        },
        {
            "country": "Taiwan",
            "code": "886"
        },
        {
            "country": "Tajikistan",
            "code": "992"
        },
        {
            "country": "Tanzania",
            "code": "255"
        },
        {
            "country": "Thailand",
            "code": "66"
        },
        {
            "country": "Timor-Leste",
            "code": "670"
        },
        {
            "country": "Togo",
            "code": "228"
        },
        {
            "country": "Tokelau",
            "code": "690"
        },
        {
            "country": "Tonga",
            "code": "676"
        },
        {
            "country": "TrinidadandTobago",
            "code": "1868"
        },
        {
            "country": "Tunisia",
            "code": "216"
        },
        {
            "country": "Turkey",
            "code": "90"
        },
        {
            "country": "Turkmenistan",
            "code": "993"
        },
        {
            "country": "TurksandCaicosIslands",
            "code": "1649"
        },
        {
            "country": "Tuvalu",
            "code": "688"
        },
        {
            "country": "Uganda",
            "code": "256"
        },
        {
            "country": "Ukraine",
            "code": "380"
        },
        {
            "country": "UnitedArabEmirates",
            "code": "971"
        },
        {
            "country": "UnitedKingdom",
            "code": "44"
        },
        {
            "country": "UnitedStates",
            "code": "1"
        },
        {
            "country": "Uruguay",
            "code": "598"
        },
        {
            "country": "USVirginIslands",
            "code": "1340"
        },
        {
            "country": "Uzbekistan",
            "code": "998"
        },
        {
            "country": "Vanuatu",
            "code": "678"
        },
        {
            "country": "Venezuela",
            "code": "58"
        },
        {
            "country": "Vietnam",
            "code": "84"
        },
        {
            "country": "WallisandFutuna",
            "code": "681"
        },
        {
            "country": "WestBank",
            "code": "970"
        },
        {
            "country": "Yemen",
            "code": "967"
        },
        {
            "country": "Zambia",
            "code": "260"
        },
        {
            "country": "Zimbabwe",
            "code": "263"
        }
    ]
}
