/*
 |-----------------------------------------------------------------
 |  routes.js
 |----------------------------------------------------------------
 |  @author:      Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:      2016-09-02
 |
 */

angular.module('hcncore')
.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider,$compileProvider){

  $ionicConfigProvider.tabs.position('bottom');

  // contacts.sort(cSort);

  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob|content):|data:image\//);

  var sp   =   $stateProvider;
  // setup an abstract state for the tabs directive
  sp.state('router',{
    url:'/router',
    views:{
      'root-view':{
        templateUrl:'router.html',
        controller:'RouteController',
        cache:false
      }
    }
  });


  //Route for the Login for Singup screen
  sp.state('credential',{
    url:'/credential',
    cache:false,
    // abstract:true,
    'views':{
      'root-view':{
        templateUrl:'credential.html',
        controller:'CredentialController',
        // cache:false
      }
    }
  });
  //Route for the Login for Singup screen
  sp.state('credential.login',{
    url:'/login',
    'views':{
      'credential-view':{
        templateUrl:'login.html',
        controller:'CredentialController'
      }
    }
  });

  //Route for the Login for Singup screen
  sp.state('credential.register',{
    url:'/register',
    'views':{
      'credential-view':{
        templateUrl:'signup.html',
        controller:'CredentialController'
      }
    }
  });

  //Route for the Update user info
  sp.state('user_info_med',{
    url:'/user_info_med',
    // cache:false,
    'views':{
      'root-view':{
        templateUrl:'user_profile_med.html',
        controller:'UserMedinfoController',
        // cache:false
      }
    }
  });


  //Route for the Update user info
  sp.state('med_dashboard',{
    url:'/med_dashboard',
    'views':{
      'root-view':{
        templateUrl:'med_dashboard.html',
        controller:'UserMedDashabordController',
      }
    }
  });

  sp.state('med_profile',{
    url:'/med_profile/:phone_number?:name',
    // cache:false,
    'views':{
      'root-view':{
        templateUrl:'med_profile.html',
        controller:'UserMedProfileController',
        // cache:false
      }
    }
  });

  sp.state('med_request',{
    url:'/med_request/:phone_number/:relation?:name',
    // cache:false,
    'views':{
      'root-view':{
        templateUrl:'med_request.html',
        controller:'UserMedRequestController',
        // cache:false
      }
    }
  });

  //Route for the Update user info
  sp.state('user_info',{
    url:'/user_info',
    cache:false,
    'views':{
      'root-view':{
        templateUrl:'user_info.html',
        controller:'UserinfoController',
        cache:false
      }
    }
  });

  //Route for the Update user info
  sp.state('user_goal',{
    url:'/user_goal',
    'views':{
      'root-view':{
        templateUrl:'user_goal.html',
        controller:'UserGoalController'
      }
    }
  });

  //Route for the Update user info
  sp.state('diabetes_type',{
    url:'/diabetes_type',
    'views':{
      'root-view':{
        templateUrl:'diabetes_type.html',
        controller:'DiabetesTypeController'
      }
    }
  });

  //Route for the Update user info
  sp.state('diabetes_range',{
    url:'/diabetes_range',
    'views':{
      'root-view':{
        templateUrl:'diabetes_range.html',
        controller:'DiabetesRangeController'
      }
    }
  });

  //Route for the Update user info
  sp.state('therephy_type',{
    url:'/therephy_type',
    'views':{
      'root-view':{
        templateUrl:'theraphy_type.html',
        controller:'DiabetesTherephyController'
      }
    }
  });

  //Route for the Update user info
  sp.state('hypertension',{
    url:'/hypertension',
    'views':{
      'root-view':{
        templateUrl:'blood_presure.html',
        controller:'HyperTenstionController'
      }
    }
  });

  sp.state('emergency_contact',{
    url:'/emergency_contact',
    'cache':false,
    'views':{
      'root-view':{
        templateUrl:'emergency_contact.html',
        'cache':false,
        controller:'EmergencyContactController'
      }
    }
  });

  sp.state('main_tabs',{
    url:'/main_tabs',
    abstract:true,
    'views':{
      'root-view':{
        templateUrl:'main_tabs.html',
        // controller:'EmergencyContactController'
      }
    }
  });

  sp.state('main_tabs.comming_soon',{
    url:'/comming_soon',
    'views':{
      'root-view':{
        templateUrl:'comming_soon.html',
        // controller:'EmergencyContactController'
      }
    }
  });

  // sp.state('main_tabs.log_widget',{
  //   url:'/log_widget',
  //   'views':{
  //     'log_widget':{
  //       templateUrl:'log_widget.html',
  //       controller:'LogWidgetController'
  //     }
  //   }
  // });

  sp.state('main_tabs.log_list_widget_tab',{
    url:'/log_list_widget_tab',
    abstract:true,
    'views':{
      'log_widget':{
        templateUrl:'log_list_widget_tab.html',
        // controller:'PhrContactController'
      }
    }
  });

  sp.state('main_tabs.log_widget',{
    url:'/log_widget',
    'views':{
      'log_widget':{
        // cache:false,
        templateUrl:'log_widget.html',
        controller:'LogWidgetController'
      }
    }
  });

  sp.state('main_tabs.insight',{
    url:'/insight',
    'views':{
      'insight_widget':{
        cache:false,
        templateUrl:'insights.html',
        controller:'InsightController'
      }
    }
  });

  sp.state('main_tabs.id',{
    url:'/insight/:id',
    'views':{
      'insight_widget':{
        cache:false,
        templateUrl:'insights_cards.html',
        controller:'InsightStateController'
      }
    }
  });

  sp.state('main_tabs.weight_log_widget',{
    url:'/weight_log_widget',
    'cache':false,
    'views':{
      'log_widget':{
        templateUrl:'weight_log_widget.html',
        'cache':false,
        controller:'LogWeightWidgetController'
      }
    }
  });

  sp.state('main_tabs.a1c_log_widget',{
    url:'/a1c_log_widget',
    'views':{
      'log_widget':{
        templateUrl:'a1c_log_widget.html',
        controller:'LogA1cWidgetController'
      }
    }
  });

  sp.state('main_tabs.bp_log_widget',{
    url:'/bp_log_widget',
    'views':{
      'log_widget':{
        templateUrl:'bp_log_widget.html',
        controller:'LogBPWidgetController'
      }
    }
  });

  sp.state('main_tabs.medicine_log_widget',{
    url:'/medicine_log_widget',
    'views':{
      'log_widget':{
        templateUrl:'medicine_log_widget.html',
        controller:'LogMedicineWidgetController'
      }
    }
  });


  sp.state('main_tabs.activity_log_widget',{
    url:'/activity_log_widget',
    'views':{
      'log_widget':{
        templateUrl:'activity_log_widget.html',
        controller:'LogActivityWidgetController'
      }
    }
  });

  sp.state('main_tabs.food_log_widget',{
    url:'/food_log_widget',
    'views':{
      'log_widget':{
        templateUrl:'food_log_widget.html',
        controller:'LogFoodWidgetController'
      }
    }
  });

  sp.state('main_tabs.bgl_log_widget',{
    url:'/bgl_log_widget',
    'views':{
      'log_widget':{
        templateUrl:'bgl_log_widget.html',
        controller:'LogBGLWidgetController'
      }
    }
  });

  sp.state('main_tabs.symptom_log_widget',{
    url:'/symptom_log_widget',
    'views':{
      'log_widget':{
        templateUrl:'symptoms_log_widget.html',
        controller:'LogSymptomsWidgetController'
      }
    }
  });

  sp.state('main_tabs.home_widget_tab',{
    url:'/home_widget_tab',
    abstract:true,
    'views':{
      'home_widget':{
        templateUrl:'home_widget_tab.html',
        // controller:'EmergencyContactController'
      }
    }
  });

  sp.state('main_tabs.home_widget_tab.dashboard',{
    url:'/dashboard',
    'cache':false,
    'views':{
      'home_dash_widget':{
        templateUrl:'home_dashboard_widget_tab.html',
        'cache':false,
        controller:'DashboardController'
      }
    }
  });

// added SOS Feature tab
sp.state('sos_feature_tab',{
  url:'/sos_feature_tab',
  abstract:true,
  'views':{
    'root-view':{
      templateUrl:'sos_feature_tab.html',
      controller:'SosFeatureController'
    }
  }
});

  sp.state('sos',{
    url:'/sos',
    'cache':false,
    'views':{
      'root-view':{
        templateUrl:'sos.html',
        'cache':false,
        controller:'SosFeatureController'
      }
    }
  });

  sp.state('main_tabs.home_widget_tab.timeline',{
    url:'/timeline',
    'cache':false,
    'views':{
      'home_timeline_widget':{
        templateUrl:'new_timeline_widget_tab.html',
        'cache':false,
        controller:'NewTimelineController'
        // controller:'EmergencyContactController'
      }
    }
  });

  sp.state('main_tabs.home_widget_tab.schedule_list',{
    url:'/schedule_list',
    cache:false,
    'views':{
      'home_reminder_widget':{
        cache:false,
        templateUrl:'schedule_list.html',
        controller:'ScheduleController'
      }
    }
  });


  sp.state('main_tabs.sch_medicine_list',{
    url:'/sch_medicine_list',
    cache:false,
    'views':{
      'home_widget':{
        cache:false,
        templateUrl:'sch_medicine_list.html',
        controller:'ScheduleController'
      }
    }
  });

  sp.state('main_tabs.sch_activity_list',{
    url:'/sch_activity_list',
    'views':{
      cache:false,
      'home_widget':{
        cache:false,
        templateUrl:'sch_activity_list.html',
        controller:'ScheduleController'
      }
    }
  });

  sp.state('main_tabs.sch_doc_list',{
    url:'/sch_doc_list',
    cache:false,
    'views':{
      'home_widget':{
        cache:false,
        templateUrl:'sch_doc_list.html',
        controller:'ScheduleController'
      }
    }
  });

  sp.state('main_tabs.sch_lab_list',{
    url:'/sch_lab_list',
    cache:false,
    'views':{
      'home_widget':{
        cache:false,
        templateUrl:'sch_lab_list.html',
        controller:'ScheduleController'
      }
    }
  });


  sp.state('main_tabs.schedule_main',{
    url:'/schedule',
    cache:false,
    'views':{
      'home_widget':{
        cache:false,
        templateUrl:'home_schedule_widget.html',
        controller:'ScheduleController'
      }
    }
  });

 // State for schedule medication
  sp.state('main_tabs.schedule_medication',{
    url:'/medication',
    'views':{
      'home_widget':{
        templateUrl:'sch_medication.html',
        controller:'ScheduleMedicationController'
      }
    }
  });

  // state for schedule doctor appointment
  sp.state('main_tabs.schedule_doctor',{
    url:'/doctor',
    'views':{
      'home_widget':{
        templateUrl:'sch_doc_appointment.html',
        controller:'ScheduleDoctorAppoController'
      }
    }
  });

  // state for schedule Lab Test
  sp.state('main_tabs.schedule_lab',{
    url:'/labtest',
    'views':{
      'home_widget':{
        templateUrl:'sch_lab_test.html',
        controller:'ScheduleLabTestController'
      }
    }
  });

  // state for schedule Walk Jog
  sp.state('main_tabs.schedule_walk_jog',{
    url:'/walkjog',
    'views':{
      'home_widget':{
        templateUrl:'sch_walk_jog.html',
        controller:'ScheduleWalkJogController'
      }
    }
  });

  // state for schedule Activity
  sp.state('main_tabs.schedule_activity',{
    url:'/activity',
    'views':{
      'home_widget':{
        templateUrl:'sch_activity.html',
        controller:'ScheduleActivityController'
      }
    }
  });

  sp.state('main_tabs.report_widget_tab',{
    url:'/report_widget_tab',
    abstract:true,
    'views':{
      'report_widget':{
        templateUrl:'report_widget_tab.html',
        // controller:'EmergencyContactController'
      }
    }
  });

  sp.state('main_tabs.report_widget_tab.generate',{
    url:'/generate',
    // cache:false,
    'views':{
      'report_generate_widget':{
        // cache:false,
        templateUrl:'report_generate.html',
        controller:'ReportController'
      }
    }
  });

  sp.state('main_tabs.report_widget_tab.archive',{
    url:'/archive',
    // cache:false,
    'views':{
      'report_archive_widget':{
        // cache:false,
        templateUrl:'report_archive.html',
        controller:'ReportController'
      }
    }
  });





  // State for PHR module

  // sp.state('main_tabs.phr_widget_tab',{
  //   url:'/phr_widget_tab',
  //   abstract:true,
  //   'views':{
  //     'phr_widget':{
  //       templateUrl:'phr_widget_tab.html',
  //       // controller:'PhrContactController'
  //     }
  //   }
  // });

  sp.state('main_tabs.phr_upload',{
    url:'/phr_upload',
      // cache:false,
    'views':{
      'phr_widget':{
        // cache:false,
        templateUrl:'phr_upload.html',
        controller:'PhrUploadController'
      }
    }
  });

  sp.state('main_tabs.phr_record',{
    url:'/phr_record',
      // cache:false,
    'views':{
      'phr_widget':{
        // cache:false,

        templateUrl:'phr_record.html',
        controller:'PhrRecordController'
      }
    }
  });

  sp.state('main_tabs.phr_search',{
    url:'/phr_search',
      // cache:false,
    'views':{
      'phr_widget':{
        // cache:false,

        templateUrl:'phr_search.html',
        controller:'PhrSearchController'
      }
    }
  });

  sp.state('main_tabs.phr_event',{
    url:'/phr_event/:event_name/:file_count',
    // cache:false,
    'views':{
      'phr_widget':{
        // cache:false,
        templateUrl:'phr_event_type_page.html',
        controller:'PhrEventTypeController'
      }
    }
  });

  sp.state('main_tabs.phr_event_type',{
    url:'/phr_event_type/:event_name/type/:type_name',
    'views':{
      'phr_widget':{
            // cache:false,
        templateUrl:'phr_event_page.html',
        controller:'PhrEventTypeRecordController'
      }
    }
  });

  //State for user profile

  sp.state('user_profile_tab',{
    url:'/user_profile_tab',
    abstract:true,
    'views':{
      'root-view':{
        templateUrl:'user_profile_tab.html',
        // controller:'EmergencyContactController'
      }
    }
  });


  sp.state('user_profile_tab.profile',{
    url:'/profile',
    // cache:false,
    'views':{
      'user_profile_widget':{
        // cache:false,
        templateUrl:'profile.html',
        controller:'UserProfileController'
      }
    }
  });

  sp.state('user_profile_tab.lifestyle',{
    url:'/lifestyle',
    // cache:false,
    'views':{
      'user_lifestyle_widget':{
        // cache:false,
        templateUrl:'lifestyle.html',
        controller:'UserProfileController'
      }
    }
  });

  sp.state('user_profile_tab.contact_info',{
    url:'/contact_info',
    // cache:false,
    'views':{
      'user_contact_widget':{
        // cache:false,
        templateUrl:'contact_info.html',
        controller:'UserProfileController'
      }
    }
  });


  // sp.state('main_tabs.user_profile_tab',{
  //   url:'/user_profile_tab',
  //   abstract:true,
  //   'views':{
  //     'home_widget':{
  //       templateUrl:'user_profile_tab.html',
  //       // controller:'EmergencyContactController'
  //     }
  //   }
  // });
  //State for static page

  //Route for the Update user info
  sp.state('about_us',{
    url:'/about_us',
    'views':{
      'root-view':{
        templateUrl:'aboutus.html',
        controller:'AboutUsController'
      }
    }
  });

  sp.state('about',{
    url:'/about',
    'views':{
      'root-view':{
        templateUrl:'static_about.html',
        controller:'AboutUsController'
      }
    }
  });

  sp.state('hcncarehelp',{
    url:'/hcncarehelp',
    'views':{
      'root-view':{
        templateUrl:'hcncare_help.html',
        controller:'HcnHelpController'
      }
    }
  });

  sp.state('version',{
    url:'/version',
    'views':{
      'root-view':{
        templateUrl:'hcncare_version.html',
        controller:'HcncareVersionController'
      }
    }
  });

  sp.state('share',{
    url:'/share',
    'views':{
      'root-view':{
        templateUrl:'shareapp.html',
        controller:'ShareAppController'
      }
    }
  });

  sp.state('notification',{
    url:'/notification',
    'views':{
      'root-view':{
        templateUrl:'notification.html',
        controller:'NotificationController'
      }
    }
  });

  sp.state('health',{
    url:'/health',
    'views':{
      'root-view':{
        templateUrl:'health_asses.html',
        controller:'HealthAssesController'
      }
    }
  });

  sp.state('settings',{
    url:'/settings',
    // cache:false,
    'views':{
      'root-view':{
        // cache:false,
        templateUrl:'settings.html',
        controller:'AccountSettingsController'
      }
    }
  });

  sp.state('privacy_policy',{
    url:'/privacy_policy',
    'views':{
      'root-view':{
        templateUrl:'privacy_policy.html',
      }
    }
  });

  sp.state('feedback',{
    url:'/feedback',
    'views':{
      'root-view':{
        templateUrl:'feedback.html',
        controller:'FeedbackController'
      }
    }
  });

  sp.state('menu_emergency_contact',{
    url:'/menu_emergency_contact',
    'cache':false,
    'views':{
      'root-view':{
        templateUrl:'menu_emergency_contact.html',
        'cache':false,
        controller:'EmergencyContactController'
      }
    }
  });

  sp.state('messennger',{
    url:'/messennger/:dest_id',
    // 'cache':false,
    'views':{
      'root-view':{
        templateUrl:'messanger.html',
        // 'cache':false,
        controller:'MessengerController'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/router');

});
