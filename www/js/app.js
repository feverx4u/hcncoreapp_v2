// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('hcncore', ['ionic', 'hcncore.controllers', 'hcncore.services','hcncore.filters','hcncore.directives',
'hcncore.templates','ngCordova','ngResource','angular-locker','ionic-datepicker','ionic-timepicker',
'ionic.contrib.drawer','jrCrop'])

.run(function($ionicPlatform,$rootScope,$state,$timeout,$log,Core,$cordovaCalendar,$cordovaGeolocation) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }

    if(window.StatusBar) {
      // StatusBar.styleDefault();
      StatusBar.backgroundColorByHexString("#1eaaf1");
    }

    $rootScope.device_height  =   {'height':((window.innerHeight > 0) ? window.innerHeight : screen.height)+'px'};

    $rootScope.device_height_header  =   {'height':(((window.innerHeight > 0) ? window.innerHeight : screen.height) - 80)+'px','position':'relative','overflow':'auto','padding-bottom':'100px'};
    $rootScope.device_height_header_tab  =   {'height':(((window.innerHeight > 0) ? window.innerHeight : screen.height) - 88)+'px','position':'relative','overflow':'auto'};
    $rootScope.device_height_header_tab_top_tab  = {'height':(((window.innerHeight > 0) ? window.innerHeight : screen.height) - (88))+'px','position':'relative','overflow':'auto','padding':'0px','padding-top':'55px','padding-bottom':'20px'};
    $rootScope.device_height_header_tab_top_tab2  = {'position':'relative','overflow':'auto','padding':'0px','padding-top':'55px','padding-bottom':'100px'};
    $rootScope.device_height_header_tab_top  = {'height':(((window.innerHeight > 0) ? window.innerHeight : screen.height) - (44))+'px','position':'relative','overflow':'auto','padding-top':'44px'};
    $rootScope.is_big_bar   =   false;
    // console.log($rootScope.device_height);
    // $cordovaStatusbar.styleHex('#1eaaf1');
    // $cordovaStatusbar.hide();

      Core.emergency_contact().get_contacts();
    console.log([
      $rootScope.device_height,
      $rootScope.device_height_header,
      $rootScope.device_height_header_tab,
      $rootScope.device_height_header_tab_top_tab,
      $rootScope.device_height_header_tab_top
    ]);
    $rootScope.sidemenu_show  =   true;

    var profile_type   =   Core.user_profile().getActiveProfileType();
    $rootScope.active_profile_flag = profile_type;
    $rootScope.hideInsight  =   true;

    if($rootScope.active_profile_flag == 'OWN_USER'){
      $rootScope.hideInsight  =   false;
    }
    $rootScope.showInsight  =   !$rootScope.hideInsight;

    console.log($rootScope.active_profile_flag);
    $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams, options){
      var state   =   toState.name;
      $log.info([$rootScope.sidemenu_show,toState]);
      if(state == 'intro' || state == 'credential' || state == 'credential.register'){
        $rootScope.sidemenu_show  =   false;
      } else {
        $rootScope.sidemenu_show  =   true;
      }
      var drawer  =   document.getElementById('drawer_back');
      $timeout(function() {
        var eng_elem   =  angular.element(drawer).triggerHandler('click');
        },0);


        var profile_type   =   Core.user_profile().getActiveProfileType();
        $rootScope.active_profile_flag = profile_type;
        $rootScope.hideInsight  =   true;

        if($rootScope.active_profile_flag == 'OWN_USER'){
          $rootScope.hideInsight  =   false;
        }
        $rootScope.showInsight  =   !$rootScope.hideInsight;
        console.log($rootScope.active_profile_flag);
        console.log([$rootScope.showInsight,11111]);
    });

    var state   =   $state.current.name;
    if(state == 'intro' || state == 'credential' || state == 'credential.register'){
      $rootScope.sidemenu_show  =   false;
    } else {
      $rootScope.sidemenu_show  =   true;
    }

    $log.info([$rootScope.sidemenu_show,$state]);
    // $log.info($rootScope.sidemenu_show);

    $rootScope.is_keyboard_open   = false;

    window.addEventListener('native.keyboardshow', keyboardShowHandler);

    function keyboardShowHandler(e){
        console.log(['show_handler',$rootScope.is_keyboard_open]);
        $rootScope.is_keyboard_open   = true;
        $rootScope.$digest();
    }

    window.addEventListener('native.keyboardhide', keyboardHideHandler);

    function keyboardHideHandler(e){
      console.log(['hide_handler',$rootScope.is_keyboard_open]);
        $rootScope.is_keyboard_open   = false;
        $rootScope.$digest();
    }
    // Core.phone_contacts().invalidatePhoneContact().then(function(data){
    //   console.log(data);
    // },function(data){
    //   console.log(data);
    // });

    // Core.calender().sync_events_to_calandar();
    // $cordovaCalendar.listEventsInRange(
    //   new Date('2016-12-20'),
    //   new Date('2016-12-30')
    // ).then(function (result) {
    //   console.log(result);
    // }, function (err) {
    //   // error
    //   // 'SU,MO,TU,WE,TH,FR,SA'
    //   console.log(err);
    // });

    if(cordova != undefined && cordova.plugins != undefined){
      cordova.plugins.diagnostic.requestRuntimePermissions(function(statuses){
          for (var permission in statuses){
              switch(statuses[permission]){
                  case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                      console.log("Permission granted to use "+permission);
                      break;
                  case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                      console.log("Permission to use "+permission+" has not been requested yet");
                      break;
                  case cordova.plugins.diagnostic.permissionStatus.DENIED:
                      console.log("Permission denied to use "+permission+" - ask again?");
                      break;
                  case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                      console.log("Permission permanently denied to use "+permission+" - guess we won't be using it then!");
                      break;
              }
          }

          //Get the Location for user
          var posOptions = {timeout: 70000, enableHighAccuracy: false};
          $cordovaGeolocation.getCurrentPosition(posOptions)

      }, function(error){
          console.error("The following error occurred: "+error);
      },[
          cordova.plugins.diagnostic.runtimePermission.CAMERA,
          cordova.plugins.diagnostic.runtimePermission.WRITE_EXTERNAL_STORAGE,
          cordova.plugins.diagnostic.runtimePermission.READ_EXTERNAL_STORAGE,
          cordova.plugins.diagnostic.runtimePermission.READ_CONTACTS,
          cordova.plugins.diagnostic.runtimePermission.READ_SMS,
          cordova.plugins.diagnostic.runtimePermission.CALL_PHONE,
          cordova.plugins.diagnostic.runtimePermission.SEND_SMS,
          cordova.plugins.diagnostic.runtimePermission.ACCESS_FINE_LOCATION,
          cordova.plugins.diagnostic.runtimePermission.ACCESS_COARSE_LOCATION
      ]);
    }


  });
})
