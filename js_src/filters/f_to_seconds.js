angular.module('hcncore.filters')
.filter('to_seconds',function(){
  return function(i){
    // console.log(i);
    if(i == undefined || i == null){
      return 0;
    }
    return parseInt(i/60);
  }
});
