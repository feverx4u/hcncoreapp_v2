angular.module('hcncore.filters')
.filter('timeline_time_format',function(){
  return function(time){
    return moment(time, "YYYY-MM-DD HH:mm:ss").format('hh:mm A');
  }
})
