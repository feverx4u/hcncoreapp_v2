angular.module('hcncore.filters')
.filter('format_time',function(){
  return function(time){
    var new_time  =   moment().format('YYYY-MM-DD')+' '+time;
    return moment(new_time, "YYYY-MM-DD HH:mm:ss").format('hh:mm A');
  }
})
