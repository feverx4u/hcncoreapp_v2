angular.module('hcncore.filters')
.filter('symptom_time_format',function(){
  return function(time){
    var new_time  =   moment().format('YYYY-MM-DD')+' '+time;
    return moment(new_time, "YYYY-MM-DD HH:mm").format('hh:mm A');
  }
})
