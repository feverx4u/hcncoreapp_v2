angular.module('hcncore.filters')
.filter('check_type',function(){
  return function(record,type){
       var extn = record.media.uri.split(".").pop();
       var _type = '';
       if(extn == 'pdf'){
         _type  =   'PDF';
       }else if(extn == 'jpg' || extn == 'png'){
         _type  =   'IMAGE';
       }else if(extn == 'doc' || extn == 'docx'){
         _type =  "DOC";
       }

       if(type  == _type){
         return true;
       } else {
         return false;
       }
  }
})
