/*
|------------------------------------------------------------------------------
|  FoodLog - Service to Log the Info from food
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('FoodLog',function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};

  function food_search(){
    var url   =   Network.api_url+'/food/:food_id';

    return  $resource(url, {},{
        'get': { method:'GET',params:{'food_id':''}},
        'show': { method:'GET',params:{}},
    });
  }

  function resource(){
    var url   =   Network.api_url+'/log/food';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  factory.food  =   function(food_id){
    var deferred  =   $q.defer();

    if(food_id == null || food_id.length <= 2){
      deferred.resolve({
        'staut':200,
        'message':'Food is not selected'
      });
    } else {
          food_search().show({
            'food_id':food_id
          },function(data){
            Log.info('FoodLog:food',data);
            deferred.resolve({
              'staut':200,
              'message':data.food
            });

          });
    }

    return deferred.promise;
  }

  factory.search  =   function(search_query){
      var deferred  =   $q.defer();

      if(search_query == null || search_query.length <= 2){
        deferred.resolve({
          'staut':200,
          'message':[]
        });
      } else {
            food_search().get({
              'sq':search_query
            },function(data){
              Log.info('ActivityLog:search',data.foods);
              if(data.foods != null && data.foods.food != undefined){
                if ( data.foods.food instanceof Array) {
                  deferred.resolve({
                    'staut':200,
                    'message':data.foods.food
                  });
                } else {
                  deferred.resolve({
                    'staut':200,
                    'message':[data.foods.food]
                  });
                }


              } else {
                deferred.resolve({
                  'staut':200,
                  'message':[]
                });
              }
            });
      }

      return deferred.promise;
  }

  factory.commit  =   function(data){

    var deferred  =   $q.defer();

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }

    var profile  =   UserProfile.getActiveProfile();
    if(profile ==  null){
      return null;
    }

    data.profile_id   =  profile.id;


    // if(data.activity_id == null || data.activity_id.length <= 0 ){
    //   deferred.reject({
    //     'staut':400,
    //     'message':'Please choose activity'
    //   });
    //   return deferred.promise;
    // }

    if(data.meal_name == null || data.meal_name.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please enter meal name'
      });
      return deferred.promise;
    }

    // if(data.doss == null || data.doss.length <= 0 ){
    //   deferred.reject({
    //     'staut':400,
    //     'message':'Please select doss'
    //   });
    //   return deferred.promise;
    // }

    // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
    var user_data   =   data;
    resource().add(data,function(data){
        Log.info('FoodLog:commit',data);
        deferred.resolve(data);
    },function(data){
        Log.error('FoodLog:commit',data);
        deferred.reject(error);
    });
    return deferred.promise;
  }
  return factory;
})
