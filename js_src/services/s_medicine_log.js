/*
|------------------------------------------------------------------------------
|  MedicineLog - Service to Log the Info from a1c
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('MedicineLog',function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};

  function resource(){


    var url   =   Network.api_url+'/log/medicine';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  factory.commit  =   function(data){

    var deferred  =   $q.defer();

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }

    var profile  =   UserProfile.getActiveProfile();
    if(profile ==  null){
      return null;
    }

    data.profile_id   =  profile.id;


    if(data.medicine_name == null || data.medicine_name.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please enter medicine_name'
      });
      return deferred.promise;
    }

    if(data.doss_type == null || data.doss_type.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please select doss type'
      });
      return deferred.promise;
    }

    if(data.doss == null || data.doss.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please select doss'
      });
      return deferred.promise;
    }

    // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
    var user_data   =   data;
    resource().add(data,function(data){
      Log.info('MedicineLog:commit',data);
      deferred.resolve(data);
    },function(data){
        Log.error('MedicineLog:commit',data);
        deferred.reject(error);
    });
    return deferred.promise;
  }
  return factory;
})
