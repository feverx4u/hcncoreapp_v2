/*
 |----------------------------------------------------------------------------
 |  FileOp - File Management for application
 |----------------------------------------------------------------------------
 |
 |  $author: Lokesh Kumawat<lokesh@hirarky.com>
 |  @date: 2016-09-24
 */
 angular.module('hcncore.services')
 .factory('FileOp',function($log,$cordovaFile,$cordovaFileTransfer){

   var factory  =   {};

   factory.checkDir =  function(directory){
     console.log(cordova.file.externalDataDirectory);
     return $cordovaFile.checkDir(cordova.file.externalDataDirectory,directory);
   }

   factory.checkFile  =   function(directory,file_name){
     return $cordovaFile.checkFile(cordova.file.externalDataDirectory+directory,file_name);
   }

   factory.createDir  =   function(directory){
     console.log([cordova.file.dataDirectory, directory]);
     return $cordovaFile.createDir(cordova.file.externalDataDirectory, directory, false);
   }

   factory.download_file    =   function(url, targetPath){
     return $cordovaFileTransfer.download(url, cordova.file.externalDataDirectory+targetPath, {}, true);
   }

   return factory;
 })
