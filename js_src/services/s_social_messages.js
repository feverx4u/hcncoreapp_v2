/*
|------------------------------------------------------------------------------
|  A1cLog - Service to Log the Info from a1c
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('SocialMessages',function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};

  factory.messages  =   function(){

    var url   =   Network.api_url+'/social/messages/:reader_id/:other_id';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'update': { method:'PUT',params:{'other_id':''}},
    });
  }
  return factory;
})
