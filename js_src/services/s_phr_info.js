/*
|------------------------------------------------------------------------------
|  PhrInfo - Service to Add Phr Info from app
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-11-04
*/
angular.module('hcncore.services')
.factory('PhrInfo',function(PhrUpload,PhrSearch,PhrEvent,PhrEventType,PhrEventTypeMedia){
  var factory   =   {};

  factory.phr_upload  =   function(){
      return PhrUpload;
  }

  factory.phr_search  =   function(){
      return PhrSearch;
  }

  factory.phr_event   =   function(){
    return PhrEvent;
  }

  factory.phr_event_type  =   function(){
    return PhrEventType;
  }

  factory.phr_event_type_media  =   function(){
    return PhrEventTypeMedia;
  }

  return factory;
})
