/*
 |  Method is used to get the analytics for the user
 |
 |
 */
 angular.module('hcncore.services')
 .factory('UserAnalytics',function(Network,$q,$resource,locker,User,Log,$timeout,UserProfile){
     var factory   =   {};

     function resource(){

       var user  =   User.logged_in_user();
       if(user == null){
         return null;
       }

       var profile  =   UserProfile.getActiveProfile();
       if(profile ==  null){
         return null;
       }
       var url   =   Network.api_url+'/profile/'+profile.id+'/analytics';

       return  $resource(url, {},{
           'get': { method:'GET',params:{}},
       });
     }

     factory.get_cached  =   function(){

        var user  =   User.logged_in_user();
        if(user == null){
           deferred.reject(null);
           return deferred.promise;
        }

        var profile  =   UserProfile.getActiveProfile();
        if(profile ==  null){
          return null;
        }

        var user_id  =   profile.id;

       return  locker.namespace('hcncore_'+profile.id).get('analytics',null);
     }

     factory.get  =   function(){
       var deferred  =   $q.defer();

       var user  =   User.logged_in_user();
       if(user == null){
          deferred.reject(null);
          return deferred.promise;
       }

       var profile  =   UserProfile.getActiveProfile();
       if(profile ==  null){
         return null;
       }

       var user_id  =   profile.id;

       var analytics = locker.namespace('hcncore_'+profile.id).get('analytics',null);
       if(analytics != null){
         $timeout(function(){
           Log.info('UserAnalytics:get notify',analytics);
           deferred.notify(analytics);
         },1);
       }
       $timeout(function(){
         resource().get({},function(data){
             Log.info('UserAnalytics:get',data);
             locker.namespace('hcncore_'+user_id).put('analytics',data);
             if(profile.is_init_flow == 0){
               User.resync_logged_in_user();
               UserProfile.resync_active_profile();
             }
             deferred.resolve(data);
         },function(data){
             Log.error('UserAnalytics:get',data);
             deferred.reject(data);
         });
       },100);

       return deferred.promise;
     }

     return factory;
 })
