/*
|------------------------------------------------------------------------------
|  EmergencyContact -Contact Management
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('EmergencyContact',function(Log,Network,$q,$resource,locker,User){
  var factory   = {};

  function resource(){

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }

    var url   =   Network.api_url+'/user/'+user.id+'/emergency_contact/:contact_id';

    return  $resource(url, {},{
        'get': { method:'GET',params:{'contact_id':''}},
        'add': { method:'POST',params:{'contact_id':''}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }
  factory.get_contacts_cached = function() {
    var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
    return  locker.namespace('hcncore_'+user_id).get('emergencycontact',null);
  }

  factory.get_contacts   =   function(){
    var deferred  =   $q.defer();
    var user  =   User.logged_in_user();
    if(user == null){
      deferred.reject({
        'staut':400,
        'message':'User not logged in'
      });
      return deferred.promise;
    }

    resource().get({},function(data){
      Log.info('EmergencyContact:get_contacts',data);
      angular.forEach(data.emergency_contacts,function(contact){
        contact.is_primary  =   parseInt(contact.is_primary);
        contact.is_primary = contact.is_primary==0?false:true;
      });
      var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
       locker.namespace('hcncore_'+user_id).put('emergencycontact',data);
      deferred.resolve(data);
    },function(error){
      Log.info('EmergencyContact:get_contacts',error);
      deferred.reject(error);
    });
    return deferred.promise;
  }

  factory.add_contact   =   function(data){
    var deferred  =   $q.defer();
    var user  =   User.logged_in_user();
    if(user == null){
      deferred.reject({
        'staut':400,
        'message':'User not logged in'
      });
      return deferred.promise;
    }

    if(data.name == null || data.name.length <= 0){
      deferred.reject({
        'staut':400,
        'message':'Please choose contact'
      });
      return deferred.promise;
    }

    if(data.phone_number == null || data.phone_number.length <= 0){
      deferred.reject({
        'staut':400,
        'message':'Please choose contact'
      });
      return deferred.promise;
    }

    if(data.type == null || data.type.length <= 0){
      deferred.reject({
        'staut':400,
        'message':'Please relation'
      });
      return deferred.promise;
    }

    data.is_primary  = data.is_primary?"TRUE":"FALSE";

    resource().add(data,function(data){
      Log.info('EmergencyContact:add_contact',data);
      deferred.resolve(data);
    },function(error){
      Log.info('EmergencyContact:add_contact',error);
      deferred.reject(error);
    });
    return deferred.promise;
  }

  factory.delete_contact  =   function(data){

    var deferred  =   $q.defer();
    var user  =   User.logged_in_user();
    if(user == null){
      deferred.reject({
        'staut':400,
        'message':'User not logged in'
      });
      return deferred.promise;
    }

    resource().delete({
      'contact_id':data.id
    },function(data){
      Log.info('EmergencyContact:delete_contact',data);
      deferred.resolve(data);
    },function(error){
      Log.info('EmergencyContact:delete_contact',error);
      deferred.reject(error);
    });
    return deferred.promise;
  }

  factory.update_contact  = function(data){
    var deferred  =   $q.defer();
    var user  =   User.logged_in_user();
    if(user == null){
      deferred.reject({
        'staut':400,
        'message':'User not logged in'
      });
      return deferred.promise;
    }

    if(data.name == null || data.name.length <= 0){
      deferred.reject({
        'staut':400,
        'message':'Please choose contact'
      });
      return deferred.promise;
    }

    if(data.phone_number == null || data.phone_number.length <= 0){
      deferred.reject({
        'staut':400,
        'message':'Please choose contact'
      });
      return deferred.promise;
    }

    if(data.type == null || data.type.length <= 0){
      deferred.reject({
        'staut':400,
        'message':'Please relation'
      });
      return deferred.promise;
    }

    data.is_primary  = data.is_primary?"TRUE":"FALSE";

    resource().update({
      'contact_id':data.id
    },data,function(data){
      Log.info('EmergencyContact:update_contact',data);
      deferred.resolve(data);
    },function(error){
      Log.info('EmergencyContact:update_contact',error);
      deferred.reject(error);
    });
    return deferred.promise;
  }
  return factory;
})
