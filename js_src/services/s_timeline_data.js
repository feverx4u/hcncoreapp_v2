/*
|------------------------------------------------------------------------------
|  TimelineData - Service to fetch timeline Info from server
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-11-16
*/
angular.module('hcncore.services')
.factory('TimelineData',function(Log,Network,$q,$resource,locker,User,UserProfile,$timeout){
  var factory   =   {};
  function resource(){

      var user  =   User.logged_in_user();
      if(user == null){
        return null;
      }

      var profile  =   UserProfile.getActiveProfile();
      if(profile ==  null){
        return null;
      }
      var url   =   Network.api_url+'/profile/'+profile.id+'/timeline';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
          'show': { method:'GET',params:{}},
      });
    }

    factory.get_cached  =   function(){

       var user  =   User.logged_in_user();
       if(user == null){
          deferred.reject(null);
          return deferred.promise;
       }

       var profile  =   UserProfile.getActiveProfile();
       if(profile ==  null){
         return null;
       }

       var user_id  =   profile.id;

      return  locker.namespace('hcncore_'+profile.id).get('timeline',null);
    }

    factory.get  =   function(start_date,lenght){
      var deferred  =   $q.defer();

      var user  =   User.logged_in_user();
      if(user == null){
         deferred.reject(null);
         return deferred.promise;
      }

      var profile  =   UserProfile.getActiveProfile();
      if(profile ==  null){
        return null;
      }

      var user_id  =   profile.id;

      var timeline = locker.namespace('hcncore_'+profile.id).get('timeline',null);
      if(timeline != null){
        $timeout(function(){
          Log.info('TimelineData:get notify',timeline);
          deferred.notify(timeline);
        },1);
      }

      $timeout(function(){
        resource().get({
          'start_date' :  start_date,
          'lenght'     :  lenght
        },function(data){
            Log.info('TimelineData:get',data);
            if(start_date == moment().format('YYYY-MM-DD')){
                locker.namespace('hcncore_'+profile.id).put('timeline',data);
            }
            deferred.resolve(data);
        },function(data){
            Log.error('TimelineData:get',data);
            deferred.reject(data);
        });
      },500);

      return deferred.promise;
    }

  return factory;
})
