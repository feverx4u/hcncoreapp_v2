/*
|------------------------------------------------------------------------------
|  ScheduleMedication - Service to Schedule the Medication Info from app
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-10-18
*/
angular.module('hcncore.services')
.factory('ScheduleMedication',function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};
  function resource(){


    var url   =   Network.api_url+'/schedule/medicine';

    return  $resource(url + '/:id', {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  factory.commit  =   function(data){

    var deferred  =   $q.defer();

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }

    var profile  =   UserProfile.getActiveProfile();
    if(profile ==  null){
      return null;
    }

    data.profile_id   =  profile.id;

    if(data.medicine_name == null || data.medicine_name.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please Enter Medicine Name'
      });
      return deferred.promise;
    }

    if(data.doss_type == null || data.doss_type.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please Select Medicine Type'
      });
      return deferred.promise;
    }

    if(data.doss == null || data.doss.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please Enter Dose Value'
      });
      return deferred.promise;
    }

    var user_data   =   data;
    console.log(data);
    resource().add({
      'profile_id':data.profile_id,
      'medicines':data.sch_medicine,
      'schedule_date':data.sch_date,
      'schedule_time':data.sch_time,
      'notes':data.notes,
      'repeat_on':data.repeat_on,
      'is_forever':data.is_forever,
      'event_count':data.event_count,
      'until':data.until,

    },function(data){
        Log.info('ScheduleMedication:commit',data);
        deferred.resolve(data);
    },function(data){
        Log.error('ScheduleMedication:commit',data);
        deferred.reject(error);
    });
    return deferred.promise;
  }

  factory.resource  = resource;

  return factory;
})
