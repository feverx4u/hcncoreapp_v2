/*
|------------------------------------------------------------------------------
|  Goal -Goal Management
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('Goal',function(Log,Network,$q,$resource,locker,User,UserProfile){
   var factory   =   {};

   function resource(){
     var url   =   Network.api_url+'/goal/:goal_id';

     return  $resource(url, {},{
         'get': { method:'GET',params:{'goal_id':''}},
         'show': { method:'GET',params:{}},
     });
   }

   factory.fetch_user_goal   =   function(){
       // var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
       var profile   = UserProfile.getActiveProfile();
       if(profile == null){
         return [];
       }

       var choosen_goal    =   locker.namespace('hcncore_'+profile.id).get('goals',null);
       return choosen_goal;
   }

   factory.set_user_goal   =   function(goals){
       // var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
       var profile   = UserProfile.getActiveProfile();
       if(profile == null){
         return;
       }

       locker.namespace('hcncore_'+profile.id).put('goals',goals);

   }

   //fethcing of Goals
   factory.fetch_goals   =   function(){
     var deferred  =   $q.defer();
     // var user_id   =   locker.namespace('hcncore').get('login_user_id',null);

     var choosen_goal    =   factory.fetch_user_goal();

     resource().get(function(data){
       Log.info('Goal:fetch_goals',data);
       var goals   =   data.goals;
       angular.forEach(goals,function(goal){
         goal.is_checked   =   false;
         if(choosen_goal != null && choosen_goal.indexOf(goal.slug) >= 0){
           goal.is_checked   =   true;
         }
         if(goal.slug == 'diabetes_mgmt'){
           goal.icon   =  './img/diabetes_management.png';
         } else if(goal.slug == 'hypertension'){
           goal.icon   =  './img/hypertension.png';
         } else if(goal.slug == 'general_health_mgmt'){
           goal.icon   =  './img/generalHealthManagement.png';
         } else if(goal.slug == 'weight_mgmt'){
           goal.icon   =  './img/weightManagement.png';
         } else if(goal.slug == 'epilepsy_mgmt'){
           goal.icon   =  './img/epilepsyManagement.png';
         }
       });
       deferred.resolve({
         'status':200,
         'message':goals
       });
     },function(error){
       Log.error('Goal:fetch_goals',error);
       deferred.reject({
         'status':400,
         'message':'Goal not able to fetch.'
       });
     });
     return deferred.promise;
   }

   factory.set_goal  =   function(goals){
     var deferred  =   $q.defer();

     var user  =   User.logged_in_user();
     if(user == null){
       Log.info('Goal:set_goal','User is not logged in');
       deferred.reject({
         'status':400,
         'message':'User is not logged in'
       });
       return deferred.promise;
     }

    //  if(goals.length <= 0){
    //    deferred.reject({
    //      'status':400,
    //      'message':'Please choose goal'
    //    });
    //    return deferred.promise;
    //  }
     factory.set_user_goal(goals);

     deferred.resolve({
       'status':200,
       'message':'Goal updated'
     });

     return deferred.promise;
   }

   return factory;
})
