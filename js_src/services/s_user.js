/*
|------------------------------------------------------------------------------
|  User -User Management
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-01
 */
 angular.module('hcncore.services')
 .factory('User',function(Log,Network,$q,$resource,locker,$ionicLoading){
   var factory  =   {};


   //Method to verify the phone number
   function phonenumber_verification(phoneNumber){
     var deferred  =   $q.defer();

     $ionicLoading.hide();
     if(window.plugins !== undefined){
       if(window.plugins !== undefined){
         window.plugins.digits.logout();
       }
       window.plugins.digits.authenticate({
         'phoneNumber':phoneNumber
       }, function(data){

         $ionicLoading.show({
             template: '<ion-spinner icon="android"></ion-spinner>'
         });
         Log.info('User:phonenumber_verification',data);
         deferred.resolve({
           'status':200,
           'error':'Phone verified successfully.'
         });
       }, function(error){
          Log.info('User:phonenumber_verification',"This is something funny");
          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });
         deferred.reject({
           'status':400,
           'error':'Failed to verify the phone number'
         });
       });
     } else {
       $ionicLoading.show({
           template: '<ion-spinner icon="android"></ion-spinner>'
       });
       deferred.resolve({
         'status':200,
         'error':'Phone verified successfully.'
       });
     }
     return deferred.promise;
   }

   //User Resource
   function resource(){
     var url   =   Network.api_url+'/user/:user_id';

     return  $resource(url, {},{
         'get': { method:'GET',params:{'user_id':''}},
         'add': { method:'POST',params:{'user_id':''}},
         'show': { method:'GET',params:{}},
         'update': { method:'PUT',params:{}},
         'delete': { method:'DELETE',params:{}}
     });
   }

   function auth(){
     var url   =   Network.api_url+'/user/authenticate';

     return  $resource(url, {},{
         'post': { method:'POST'},
     });
   }


   //Method is used to check whether user logged in on app
   factory.is_logged_in   =   function(){
     var user   =   locker.namespace('hcncore').get('login_user_id',null);
     if(user == null){
       return false;
     }
     return true;
   }

   //Method is used to get the current logged in user
   factory.logged_in_user   =   function(){
     if(!factory.is_logged_in()){
       return null;
     }
     //Code needed to be written to get the currently logged in User
     //TODO
     var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
    //  console.log('User is logged in');
     return locker.namespace('hcncore_'+user_id).get('user',null);
   }

   //Method is used to check whether user logged in on app
   factory.logout   =   function(){
     var user   =   locker.namespace('hcncore').put('login_user_id',"");
     return true;
   }

   factory.set_user_weight   =  function(weight,unit){
     var user_id   =   locker.namespace('hcncore').get('login_user_id',null);

     locker.namespace('hcncore_'+user_id).put('last_user_weight',{
       'weight':weight,
       'weight_unit':unit
     });
   }

   factory.get_user_weight   =  function(weight,unit){
     var user_id   =   locker.namespace('hcncore').get('login_user_id',null);

     return locker.namespace('hcncore_'+user_id).get('last_user_weight',null);
   }

   factory.update_user_med_info   =   function(user_data){
     var deferred  =   $q.defer();
     var user    =  factory.logged_in_user();
     if(user == null){
       Log.error(['User:update_user_med_info','No current user logged in.']);
       deferred.resolve({
         'status':500,
         'message':'User not logged in'
       });
       return deferred.promise;
     }

     if(user_data.name ==null || user_data.name.length <= 0){
       deferred.reject({
         'status':400,
         'message':'Name is empty.'
       });
       return deferred.promise;
     }

    //  dob    =   data.dob;
     resource().update({
       'user_id':user.id
     },user_data,function(data){
       locker.namespace('hcncore_'+data.user.id).put('user',data.user);
       Log.info('User:update_user_med_info',data);
       deferred.resolve({
         'status':200,
         'message':'User updated successfully'
       });
     },function(error){
       Log.error('User:update_user_med_info',error);
       deferred.reject({
         'status':500,
         'message':'Something went wrong'
       });
     });

     return deferred.promise;
   }
   //Method is used to update the information of currently logged in user
   factory.update_user_info   =   function(user_data){
     var deferred  =   $q.defer();
     var user    =  factory.logged_in_user();
     if(user == null){
       Log.error(['User:update_user_info','No current user logged in.']);
       deferred.resolve({
         'status':500,
         'message':'User not logged in'
       });
       return deferred.promise;
     }

     if(user_data.name ==null || user_data.name.length <= 0){
       deferred.reject({
         'status':400,
         'message':'Name is empty.'
       });
       return deferred.promise;
     }

     if(user_data.dob ==null || user_data.dob.length <= 0){
       deferred.reject({
         'status':400,
         'message':'Dob is empty.'
       });
       return deferred.promise;
     }

     if(user_data.gender ==null || user_data.gender.length <= 0){
       deferred.reject({
         'status':400,
         'message':'gender is empty.'
       });
       return deferred.promise;
     }

     if(user_data.height ==null || user_data.height.length <= 0 ){
       deferred.reject({
         'status':400,
         'message':'height is empty.'
       });
       return deferred.promise;
     } else if(isNaN(user_data.height)){
       deferred.reject({
         'status':400,
         'message':'Height should be numeric.'
       });
       return deferred.promise;
     }  else if((user_data.height < 1 || user_data.height > 10) && user_data.height_unit == 'FEET'){
       deferred.reject({
         'status':400,
         'message':'Please enter a valid height.'
       });
       return deferred.promise;
     } else if((user_data.height < 0.30 || user_data.height > 3.0) && user_data.height_unit == 'METER'){
       deferred.reject({
         'status':400,
         'message':'Please enter a valid height.'
       });
       return deferred.promise;
     }

     if(user_data.weight ==null || user_data.weight.length <= 0){
       deferred.reject({
         'status':400,
         'message':'weight is empty.'
       });
       return deferred.promise;
     } else if((user_data.weight < 20 || user_data.weight > 200) && user_data.weight_unit == 'KG'){
       deferred.reject({
         'status':400,
         'message':'Please enter a valid weight.'
       });
       return deferred.promise;
     } else if((user_data.weight < 44 || user_data.weight > 445) && user_data.weight_unit == 'LBS'){
       deferred.reject({
         'status':400,
         'message':'Please enter a valid weight.'
       });
       return deferred.promise;
     }
     if(user_data.email != undefined && user_data.email != null && user_data.email.length >0){
       var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       if(!re.test(user_data.email)){
         deferred.reject({
           'status':400,
           'message':'Invalid Email ID.'
         });
         return deferred.promise;
       }
     }

     user_data.dob   =   moment(user_data.dob,'Do MMM,YYYY').format('YYYY-MM-DD');
    //  dob    =   data.dob;
     resource().update({
       'user_id':user.id
     },user_data,function(data){

       locker.namespace('hcncore_'+data.user.id).put('user',data.user);

       factory.set_user_weight(user_data.weight,user_data.weight_unit);
       Log.info('User:update_user_info',data);
       Log.info('User:update_user_info',user_data.dob);
       user_data.dob   =   moment(user_data.dob,'YYYY-MM-DD').format('Do MMM,YYYY');
       Log.info('User:update_user_info',user_data.dob);
       deferred.resolve({
         'status':200,
         'message':'User updated successfully'
       });
     },function(error){
       user_data.dob   =   moment(user_data.dob,'YYYY-MM-DD').format('Do MMM,YYYY');
       Log.info('User:update_user_info',user_data.dob);
       Log.error('User:update_user_info',error);
       deferred.reject({
         'status':500,
         'message':'Something went wrong'
       });
     });

     return deferred.promise;
   }

   //Method is used to update the contact information of currently logged in user
   factory.update_user_other_info   =   function(user_data){
     var deferred  =   $q.defer();
     var user    =  factory.logged_in_user();
     if(user == null){
       Log.error(['User:update_user_other_info','No current user logged in.']);
       deferred.resolve({
         'status':500,
         'message':'User not logged in'
       });
       return deferred.promise;
     }

     resource().update({
       'user_id':user.id
     },user_data,function(data){

       locker.namespace('hcncore_'+data.user.id).put('user',data.user);

       Log.info('User:update_user_other_info',data);

       deferred.resolve({
         'status':200,
         'message':'User contact updated successfully'
       });
     },function(error){
       Log.error('User:update_user_other_info',error);
       deferred.reject({
         'status':500,
         'message':'Somevfthing went wrong'
       });
     });

     return deferred.promise;
   }


   //Method is used to authenticate the user
   factory.authenticate =   function(username,password,is_otp){
        var deferred  =   $q.defer();

        var _is_otp   = false;
        if(is_otp != undefined && is_otp == true){
          _is_otp   = true;
        }

        if(username.trim().length <= 0){
          deferred.reject({
            'status':400,
            'error':'Username should not be empty.'
          });
        }

        if(!is_otp && password.length <= 0){
          deferred.reject({
            'status':400,
            'error':'Password should not be empty.'
          });
        }

        if(!is_otp){
          auth().post({
            'phone_number':username,
            'password':password
          },function(data){
            locker.namespace('hcncore').put('login_user_id',data.user.id);
            locker.namespace('hcncore_'+data.user.id).put('user',data.user);
            deferred.resolve({
              'status':200,
              'message':'AUTHENTICATED',
              'user_data':data.user
            });
          },function(error){
            deferred.reject({
              'status':400,
              'message':'Invalid username/passsword'
            });
          });
        } else {
          console.log(1111111);
            deferred.notify({
              'message':'HIDE_LOADER'
            });
            phonenumber_verification(username).then(function(data){
              deferred.notify({
                'message':'SHOW_LOADER'
              });
              resource().show({
                'user_id':username
              },function(data){
                locker.namespace('hcncore').put('login_user_id',data.user.id);

                locker.namespace('hcncore_'+data.user.id).put('user',data.user);

                deferred.resolve({
                  'status':200,
                  'message':'AUTHENTICATED'
                });
              },function(error){
                deferred.reject({
                  'status':400,
                  'message':'User is not registered.'
                });
              });
            },function(error){
              deferred.reject({
                'status':400,
                'message':'Failed to verify phonenumber.'
              });
            });
        }
        // deferred.resolve({
        //   'status':200,
        //   'message':'AUTHENTICATED'
        // });

        return deferred.promise;
   }

   //Method is used to register the user.
   factory.register =   function(username,password){
        var deferred  =   $q.defer();

        var _username = username+'';
        console.log(_username);
        if(_username.trim().length <= 0){
          deferred.reject({
            'status':400,
            'error':'Username should not be empty.'
          });
        }

        if(password.length <= 0){
          deferred.reject({
            'status':400,
            'error':'Password should not be empty.'
          });
        }

        //Checking user exists with phonenumber
        resource().show({
          'user_id':username
        },function(data){
          //User Exists
          Log.info('User:register',data);
          deferred.reject({
            'status':400,
            'message':'User is already registered.'
          });

        },function(error){
          //User does not exists
          Log.error('User:register',error);

          phonenumber_verification(username).then(function(data){
            Log.info('User:register',data);
            resource().add({
              'phone_number':username,
              'password':password
            },function(data){
              locker.namespace('hcncore').put('login_user_id',data.user.id);
              locker.namespace('hcncore_'+data.user.id).put('user',data.user);

              deferred.resolve({
                'status':200,
                'message':'Successfully.'
              });
            },function(error){
              deferred.reject({
                'status':500,
                'message':'Something went wrong try again latter.'
              });
            });
          },function(error){
            Log.error('User:register',error);
            deferred.reject({
              'status':400,
              'message':'Failed to verify phonenumber.'
            });
          });
        });
        return deferred.promise;
   }


   //Method is used to resync the user.
   factory.resync_logged_in_user  =   function(){
     var deferred    = $q.defer();
     var user   =   factory.logged_in_user();
     if(user == null){
       return;
     }
     username    = user.id;
     resource().show({
       'user_id':username
     },function(data){
       locker.namespace('hcncore_'+data.user.id).put('user',data.user);

       deferred.resolve({
         'status':200,
         'error':''
       });

     },function(error){
       deferred.reject({
         'status':500,
         'error':''
       });
     });

     return deferred.promise;
   }

  //  factory.resync_logged_in_user  =   function(){
  //
  //  }

   return factory;
 })
