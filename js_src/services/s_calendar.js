/*
 |---------------------------------------------------------------
 |  Core - Single point access to access the all the application Feature
 |---------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-01
 */
 angular.module('hcncore.services')
.factory('Calender',function($rootScope,Log,Network,User,UserProfile,locker,$cordovaCalendar){
  var factory   =   {}

  function get_current_user(){
      var profile   =   UserProfile.getActiveProfile();
      if(profile == null){
        return null;
      }

      return profile;
  }

  factory.commit_event  =   function(evnt,type){
    if($rootScope.active_profile_flag == 'OTHER_USER'){
      return;
    }

    var profile = get_current_user();
    var calender  =   locker.namespace('hcncore_'+profile.id).get('calender',null);
    if(calender == null){
      calender   =  [];
    }
    evnt.type   =   type;
    evnt.user   =   null;
    evnt.synced   =   0;
    calender.push(evnt);
    locker.namespace('hcncore_'+profile.id).put('calender',calender);
    factory.sync_events_to_calandar();
  }

  factory.sync_events_to_calandar   =   function(){

    var user  = User.logged_in_user();
    if(user   == null){
      return;
    }

    var profiles  =user.profiles;
    var profile   = null;
    angular.forEach(profiles,function(item){
      profile   =   item;

      var calender  =   locker.namespace('hcncore_'+profile.id).get('calender',null);
      if(calender == null){
        return;
      }

      angular.forEach(calender,function(item,k){
        if(item.synced == 1){
          return;
        }

        if(item.type == 'MEDICINE'){
          var reminder_minutes  =   15;
          var title = [];
          angular.forEach(item.medicines, function(med){
            title.push(med.name+'('+med.doss+','+med.doss_type+')');
          });
          var cal_options   =   {
            title: title.join(' , '),
            location: 'Home',
            notes: item.notes,
            startDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').toDate(),
            endDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').add('hours',1).toDate(),
            firstReminderMinutes:reminder_minutes,
          };
          if(item.is_repeat == 1){
            cal_options.recurrence = 'weekly';
            cal_options.recurrenceInterval =  1;
            cal_options.recurrenceEndDate =  moment(item.until+' 23:59:59','YYYY-MM-DD HH:mm:ss').toDate();
            var   recurrenceByDay = [];
            // 'SU,MO,TU,WE,TH,FR,SA'
            if(item.sun == 1){
              recurrenceByDay.push('SU');
            }

            if(item.mon == 1){
              recurrenceByDay.push('MO');
            }

            if(item.tue == 1){
              recurrenceByDay.push('TU');
            }

            if(item.wed == 1){
              recurrenceByDay.push('WE');
            }

            if(item.thu == 1){
              recurrenceByDay.push('TH');
            }

            if(item.fri == 1){
              recurrenceByDay.push('FR');
            }

            if(item.sat == 1){
              recurrenceByDay.push('SA');
            }
            cal_options.recurrenceByDay   = recurrenceByDay.join(',');
          }
          cal_options.allday   =   'false';
          console.log(cal_options);
          $cordovaCalendar.createEventWithOptions(cal_options);
          item.synced   = 1;

        } else if(item.type == 'DOCTOR_APPOINTMENT'){

          var reminder_minutes  =   60;
          if(item.notify_before == 'Hour_1'){
            var reminder_minutes  =   60;
          } else if(item.notify_before == 'Hour_2'){
            var reminder_minutes  =   120;
          } else if(item.notify_before == 'Hour_4'){
            var reminder_minutes  =   240;
          }

          if(item.notes == null){
            item.notes   =   '';
          }


          // console.log({
          //   title: item.doctor_name,
          //   location: item.hospital_name,
          //   notes: item.notes,
          //   startDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').toDate(),
          //   endDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').add('hours',1).toDate(),
          //   firstReminderMinutes:reminder_minutes
          // });
          $cordovaCalendar.createEventWithOptions({
            title: item.doctor_name,
            location: item.hospital_name,
            notes: item.notes,
            startDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').toDate(),
            endDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').add('hours',1).toDate(),
            firstReminderMinutes:reminder_minutes
          });
          item.synced   = 1;
        } else if(item.type == 'LAB_TEST'){
          var reminder_minutes  =   60;
          if(item.notify_before == 'Hour_1'){
            var reminder_minutes  =   60;
          } else if(item.notify_before == 'Hour_2'){
            var reminder_minutes  =   120;
          } else if(item.notify_before == 'Hour_4'){
            var reminder_minutes  =   240;
          }
          // console.log({
          //   title: item.test_type,
          //   location: item.lab_name,
          //   notes: item.contact_detail,
          //   startDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').toDate(),
          //   endDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').add('hours',1).toDate(),
          //   firstReminderMinutes:reminder_minutes
          // });
          if(item.contact_detail == null){
            item.contact_detail   =   '';
          }
          $cordovaCalendar.createEventWithOptions({
            title: item.test_type,
            location: item.lab_name,
            notes: item.contact_detail,
            startDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').toDate(),
            endDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').add('hours',1).toDate(),
            firstReminderMinutes:reminder_minutes
          });
          item.synced   = 1;
        } else if(item.type == 'ACTIVITY'){
          var reminder_minutes  =   10;
          if(item.notify_before == '10_minutes'){
            var reminder_minutes  =   10;
          } else if(item.notify_before == '20_minutes'){
            var reminder_minutes  =   20;
          } else if(item.notify_before == '30_minutes'){
            var reminder_minutes  =   30;
          }

          var title = item.activity_name;
          if(item.duration != null){
              title += ' ('+item.duration.split('_').join(' ')+')';
          }

          var cal_options   =   {
            title: title,
            location: 'Home',
            notes: '',
            startDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').toDate(),
            endDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').add('hours',1).toDate(),
            firstReminderMinutes:reminder_minutes,
          };
          if(item.is_repeat == 1){
            cal_options.recurrence = 'weekly';
            cal_options.recurrenceInterval =  1;
            cal_options.recurrenceEndDate =  moment(item.until+' 23:59:59','YYYY-MM-DD HH:mm:ss').toDate();
            var   recurrenceByDay = [];
            // 'SU,MO,TU,WE,TH,FR,SA'
            if(item.sun == 1){
              recurrenceByDay.push('SU');
            }

            if(item.mon == 1){
              recurrenceByDay.push('MO');
            }

            if(item.tue == 1){
              recurrenceByDay.push('TU');
            }

            if(item.wed == 1){
              recurrenceByDay.push('WE');
            }

            if(item.thu == 1){
              recurrenceByDay.push('TH');
            }

            if(item.fri == 1){
              recurrenceByDay.push('FR');
            }

            if(item.sat == 1){
              recurrenceByDay.push('SA');
            }
            cal_options.recurrenceByDay   = recurrenceByDay.join(',');
          }
          cal_options.allday   =   'false';
          console.log(cal_options);
          $cordovaCalendar.createEventWithOptions(cal_options);
          item.synced   = 1;
        }
      });

      locker.namespace('hcncore_'+profile.id).put('calender',calender);
    });
  }

  factory.delete_calendar       =   function(evnt,type){

    if($rootScope.active_profile_flag == 'OTHER_USER'){
      return;
    }

    var profile  = UserProfile.getActiveProfile();
    var calender  =   locker.namespace('hcncore_'+profile.id).get('calender',null);
    if(calender == null){
      return -1;
    }

    angular.forEach(calender,function(item,k){
      // console.log(item);
      // console.log(item.id);
      // console.log(evnt.id);
      // console.log(item.type);
      // console.log(type);

        if(item.id  == evnt.id && item.type == type){

          if(item.type == "LAB_TEST"){
            factory.delete_labtes_event_from_calender(item);
          }else if(item.type == "ACTIVITY"){
            factory.delete_activity_event_from_calender(item);
          }else if(item.type == "DOCTOR_APPOINTMENT"){
            factory.delete_doctor_event_from_calender(item);
          }else if(item.type == "MEDICINE"){
            factory.delete_medicine_event_from_calender(item);
          }

          calender.splice(k,1);
          console.log(33333);
        }
    });

    locker.namespace('hcncore_'+profile.id).put('calender',calender);
  }

  factory.delete_labtes_event_from_calender = function(item){

    var reminder_minutes  =   60;
    if(item.notify_before == 'Hour_1'){
      var reminder_minutes  =   60;
    } else if(item.notify_before == 'Hour_2'){
      var reminder_minutes  =   120;
    } else if(item.notify_before == 'Hour_4'){
      var reminder_minutes  =   240;
    }

      $cordovaCalendar.deleteEvent({
        title: item.test_type,
        location: item.lab_name,
        notes: item.contact_detail,
        startDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').toDate(),
        endDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').add('hours',1).toDate(),
        firstReminderMinutes:reminder_minutes
    }).then(function (result) {
      console.log('successful delete');
    }, function (err) {
      console.log(err);
    });
  }

  factory.delete_activity_event_from_calender = function(item){

    var title = item.activity_name;
    if(item.duration != null){
        title += ' ('+item.duration.split('_').join(' ')+')';
    }

    var reminder_minutes  =   10;
    if(item.notify_before == '10_minutes'){
      var reminder_minutes  =   10;
    } else if(item.notify_before == '20_minutes'){
      var reminder_minutes  =   20;
    } else if(item.notify_before == '30_minutes'){
      var reminder_minutes  =   30;
    }

      $cordovaCalendar.deleteEvent({
        title: title,
        location: 'Home',
        notes: '',
        startDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').toDate(),
        endDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').add('hours',1).toDate(),
        firstReminderMinutes:reminder_minutes
    }).then(function (result) {
      console.log('successful delete');
    }, function (err) {
      console.log(err);
    });
  }

  factory.delete_doctor_event_from_calender = function(item){

    var reminder_minutes  =   60;
    if(item.notify_before == 'Hour_1'){
      var reminder_minutes  =   60;
    } else if(item.notify_before == 'Hour_2'){
      var reminder_minutes  =   120;
    } else if(item.notify_before == 'Hour_4'){
      var reminder_minutes  =   240;
    }

      $cordovaCalendar.deleteEvent({
        title: item.doctor_name,
        location: item.hospital_name,
        notes: item.notes,
        startDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').toDate(),
        endDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').add('hours',1).toDate(),
        firstReminderMinutes:reminder_minutes
    }).then(function (result) {
      console.log('successful delete');
    }, function (err) {
      console.log(err);
    });
  }

  factory.delete_medicine_event_from_calender = function(item){
    var reminder_minutes  =   15;
    var title = [];
    angular.forEach(item.medicines, function(med){
      title.push(med.name+'('+med.doss+','+med.doss_type+')');
    });

      $cordovaCalendar.deleteEvent({
        title: title.join(' , '),
        location: 'Home',
        notes: item.notes,
        startDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').toDate(),
        endDate: moment(item.schedule_date+' '+item.schedule_time,'YYYY-MM-DD HH:mm:ss').add('hours',1).toDate(),
        firstReminderMinutes:reminder_minutes,
    }).then(function (result) {
      console.log('successful delete');
    }, function (err) {
      console.log(err);
    });
  }


  return factory;
})
