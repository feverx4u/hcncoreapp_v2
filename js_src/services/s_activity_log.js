/*
|------------------------------------------------------------------------------
|  ActivityLog - Service to Log the Info from activity
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('ActivityLog',function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};

  function resource(){


    var url   =   Network.api_url+'/log/activity';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  function activity(){


    var url   =   Network.api_url+'/activities';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
    });
  }

  factory.search  =   function(search_query){
      var deferred  =   $q.defer();

      if(search_query == null || search_query.length <= 2){
        deferred.resolve({
          'staut':200,
          'message':[]
        });
      } else {
            activity().get({
              'sq':search_query
            },function(data){
              Log.info('ActivityLog:search',data);
              deferred.resolve({
                'staut':200,
                'message':data.data
              });
            });
      }

      return deferred.promise;
  }

  factory.commit  =   function(data){

    var deferred  =   $q.defer();

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }

    var profile  =   UserProfile.getActiveProfile();
    if(profile ==  null){
      return null;
    }

    data.profile_id   =  profile.id;

    if(data.activity_id == null || data.activity_id.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please choose activity'
      });
      return deferred.promise;
    }

    if(data.duration == null || data.duration.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please enter duration'
      });
      return deferred.promise;
    }

    // if(data.doss == null || data.doss.length <= 0 ){
    //   deferred.reject({
    //     'staut':400,
    //     'message':'Please select doss'
    //   });
    //   return deferred.promise;
    // }

    // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
    // data.duration   =   data.duration*60;
    var user_data   =   data;
    console.log(data);
    resource().add({
      'activity_id':data.activity_id,
      'profile_id':data.profile_id,
      'duration':data.duration*60,
    },function(data){
        Log.info('ActivityLog:commit',data);
        deferred.resolve(data);
    },function(data){
        Log.error('ActivityLog:commit',data);
        deferred.reject(error);
    });
    return deferred.promise;
  }
  return factory;
})
