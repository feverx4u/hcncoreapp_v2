/*
|------------------------------------------------------------------------------
|  ScheduleInfo - Service to Schedule the Info from app
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-10-18
*/
angular.module('hcncore.services')
.factory('ScheduleInfo',function(ScheduleMedication,ScheduleDocAppointment,ScheduleLabtest,ScheduleWalkJog,ScheduleActivity){
  var factory   =   {};

  factory.schedule_medication  =   function(){
      return ScheduleMedication;
  }

  factory.schedule_doc_appo   =   function(){
    return ScheduleDocAppointment;
  }

  factory.schedule_labtest  =   function(){
    return ScheduleLabtest;
  }

  factory.schedule_walkjog  =   function(){
    return ScheduleWalkJog;
  }

  factory.schedule_activity  =   function(){
    return ScheduleActivity;
  }
  return factory;
})
