/*
 |------------------------------------------------------------------------------
 |  Log - Loging of  the Log
 |------------------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-01
 |
 */
 angular.module('hcncore.services')
 .factory('Log',function($log){
    var factory   =   {};

    factory.log_level   =   'DEBUG';
    factory.app_env   =   'LOCAL'; //Possible values [LOCAL,TEST,PROD]

    //Funcation will set the log level for the application
    function set_log_level(){
      if(factory.app_env == 'LOCAL'){
        factory.log_level   =   'DEBUG';
      } else if(factory.app_env == 'TEST'){
        factory.log_level   =   'INFO';
      } else if(factory.app_env == 'PROD'){
        factory.log_level   =   'ERROR';
      }
    }

    //Method is used to get the log level
    function get_debug_level(){
      if(factory.log_level == 'DEBUG'){
        return 3;
      } else if(factory.log_level == 'INFO'){
        return 2;
      } else if(factory.log_level == 'ERROR'){
        return 1;
      }
    }

    //Method to log the debug log
    factory.debug   =   function(tag,obj){
      if(3 <= get_debug_level()){
        $log.debug({
          'tag':tag,
          'message':obj
        });
      }
    }

    //Method to log the warn log
    factory.warn   =   function(tag,obj){
      if(2 <= get_debug_level()){
        $log.warn({
          'tag':tag,
          'message':obj
        });
      }
    }

    //Method to log the info log
    factory.info   =   function(tag,obj){
      if(2 <= get_debug_level()){
        $log.info({
          'tag':tag,
          'message':obj
        });
      }
    }


      //Method to log the info log
      factory.error   =   function(tag,obj){
        if(1 <= get_debug_level()){
          $log.error({
            'tag':tag,
            'message':obj
          });
        }
      }

    return factory;
 })
