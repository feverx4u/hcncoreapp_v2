angular.module('hcncore.services')
.factory('MedSocial',function($log,$q,$ionicPlatform,$resource,Network,User){
  var factory   =   {};

  factory.social_profile = function(){

    var url   =   Network.api_url+'/social/profile/:phone_number';

    return  $resource(url, {},{
        'show': { method:'GET',params:{}},
    });

  }

  factory.network = function(){

    var user  =   User.logged_in_user();
    if(user == null){
       deferred.reject(null);
       return deferred.promise;
    }

    var url   =   Network.api_url+'/social/profile/'+user.id+'/network';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
    });

  }

  factory.sent_request   =  function(){
    var user  =   User.logged_in_user();
    if(user == null){
       deferred.reject(null);
       return deferred.promise;
    }

    var url   =   Network.api_url+'/social/profile/'+user.id+'/request/:dest_id';

    return  $resource(url, {},{
        'commit': { method:'POST',params:{}},
    });
  }

  factory.send_invite   =   function(){
    var user  =   User.logged_in_user();
    if(user == null){
       deferred.reject(null);
       return deferred.promise;
    }

    var url   =   Network.api_url+'/social/profile/'+user.id+'/invite/:dest_phone_number';

    return  $resource(url, {},{
        'commit': { method:'POST',params:{}},
    });
  }
  factory.status_request  =   function(){
    var user  =   User.logged_in_user();
    if(user == null){
       deferred.reject(null);
       return deferred.promise;
    }

    var profiles  =   user.profiles;
    var profile = {};
    angular.forEach(profiles,function(pro){
      if(parseInt(pro.is_virtual) == 0){
        profile   =   pro;
      }
    });
    var url   =   Network.api_url+'/social/profile/:src_id/request/'+profile.id;

    return  $resource(url, {},{
        'commit': { method:'POST',params:{}},
    });
  }
   return factory;
})
