/*
|  ScheduleLabtest - Service to Schedule the Lab Test from app
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-10-18
*/
angular.module('hcncore.services')
.factory('ScheduleLabtest',function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};
  function resource(){


    var url   =   Network.api_url+'/schedule/lab_test';

    return  $resource(url + '/:id', {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  factory.commit  =   function(data){

    var deferred  =   $q.defer();

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }

    var profile  =   UserProfile.getActiveProfile();
    if(profile ==  null){
      return null;
    }

    data.profile_id   =  profile.id;

    if(data.lab_name == null || data.lab_name.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please Enter Lab Name'
      });
      return deferred.promise;
    }

    if(data.test_type == null || data.test_type.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please Enter Test Type'
      });
      return deferred.promise;
    }

    if(data.notify_before == null || data.notify_before.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please choose Notify time'
      });
      return deferred.promise;
    }

    var user_data   =   data;
    console.log(data);
    console.log(data.lab_name);
    resource().add({
      'profile_id':data.profile_id,
      'lab_name':data.lab_name,
      'test_type':data.test_type,
      'schedule_date':data.labtest_date,
      'schedule_time':data.labtest_time,
      'notify_before':data.notify_before,
      'contact_detail':data.notes,

    },function(data){
        Log.info('ScheduleLabtest:commit',data);
        deferred.resolve(data);
    },function(data){
        Log.error('ScheduleLabtest:commit',data);
        deferred.reject(error);
    });
    return deferred.promise;
  }

  factory.resource  = resource;
  return factory;
})
