/*
|------------------------------------------------------------------------------
|  Diabetes -Diabetes Management
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('Diabetes',function(Log,Network,$q,$resource,locker,User,UserProfile){
    var factory   =   {};

    function diabetes_type(){
      var url   =   Network.api_url+'/diabetec_type';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
      });
    }

    function therephy_type(){
      var url   =   Network.api_url+'/diabetec_therepy';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
      });
    }

    function resource(){
      var url   =   Network.api_url+'/profile/:profile_id/goal/diabetes_mgmt';

      return  $resource(url, {},{
          'update': { method:'put',params:{}},
      });
    }

    function update_diabetes_goal(data){
      var deferred  =   $q.defer();
      var user  =   User.logged_in_user();
      if(user == null){
        deferred.reject({
          'staut':400,
          'message':'User not logged in'
        });
        return defer.promise;
      }

      var profile   =   UserProfile.getActiveProfile();
      if(profile == null){
        return null;
      }

      resource().update({
        'profile_id':profile.id
      },data,function(data){
        deferred.resolve({
          'status':200,
          'data':'Updated successfully'
        });
      },function(error){
        deferred.reject({
          'staut':400,
          'message':'Failed to update'
        });

      });
      return deferred.promise;
    }

    //fethcing of diabetes type
    factory.fetch_diabetes_type   =   function(){
      var deferred  =   $q.defer();

      var c_diabetes_type = factory.get_user_diabetes_type();
      // alert(c_diabetes_type);
      diabetes_type().get(function(data){
        Log.info('Diabetes:fetch_diabetes_type',data);
        var types   =   data.type;
        angular.forEach(types,function(type){
          type.is_checked   =   false;
          if(type.slug  == c_diabetes_type){
              type.is_checked   =   true;
          }
        });

        deferred.resolve({
          'status':200,
          'message':types
        });
      },function(error){
        Log.error('Diabetes:fetch_diabetes_type',error);
        deferred.reject({
          'status':400,
          'message':'Goal not able to fetch.'
        });
      });
      return deferred.promise;
    }

    //fethcing of Goals
    factory.fetch_therephy_type   =   function(){
      var deferred  =   $q.defer();

      var c_therephy_type = factory.get_user_diabetes_therephy_type();
      therephy_type().get(function(data){
        Log.info('Diabetes:therephy_type',data);
        var types   =   data.therepy;
        angular.forEach(types,function(type){
          type.is_checked   =   false;
          if(c_therephy_type != null && c_therephy_type.indexOf(type.id) >= 0){
            type.is_checked   =   true;
          }
        });

        deferred.resolve({
          'status':200,
          'message':types
        });
      },function(error){
        Log.error('Diabetes:therephy_type',error);
        deferred.reject({
          'status':400,
          'message':'Goal not able to fetch.'
        });
      });
      return deferred.promise;
    }

    factory.get_user_diabetes_type  =   function(){
      // var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
      var profile   =   UserProfile.getActiveProfile();
      if(profile == null){
        return null;
      }
        return locker.namespace('hcncore_'+profile.id).get('diabetes_type',null);
    }

    factory.update_diabeted_type  =   function(type){
      var profile   =   UserProfile.getActiveProfile();
      if(profile == null){
        return null;
      }
      locker.namespace('hcncore_'+profile.id).put('diabetes_type',type.slug);
      // console.log(type);
      return update_diabetes_goal({
        'diabetec_type':parseInt(type.id)
      });
    }

    factory.update_target_range   =   function(unit,start,end){
      var data  =   {
        'range_unit':unit,
        'start_range':start,
        'end_range':end
      };
      var profile   =   UserProfile.getActiveProfile();
      if(profile == null){
        return null;
      }
      locker.namespace('hcncore_'+profile.id).put('target_range',data);
      return update_diabetes_goal(data);
    }

    factory.get_target_range   =   function(){
      var profile   =   UserProfile.getActiveProfile();
      if(profile == null){
        return null;
      }
      return locker.namespace('hcncore_'+profile.id).get('target_range',null);
    }

    factory.get_user_diabetes_therephy_type  =   function(){
      var profile   =   UserProfile.getActiveProfile();
      if(profile == null){
        return null;
      }

      // var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
        return locker.namespace('hcncore_'+profile.id).get('diabetes_therephy_type',null);
    }

    factory.update_therephy_type  =   function(types){
      var profile   =   UserProfile.getActiveProfile();
      if(profile == null){
        return null;
      }
      locker.namespace('hcncore_'+profile.id).put('diabetes_therephy_type',types);
      return update_diabetes_goal({
        'therepy_types':types.join(',')
      });
    }


    return factory;
})
