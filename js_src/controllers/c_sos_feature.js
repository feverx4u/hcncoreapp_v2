/*
 |-------------------------------------------------------------------
 | SosFeatureController - Controller for SOS Feature
 |-------------------------------------------------------------------
 |  @author:  Shekh Rizwan<rizwan@hirarky.com>
 |  @date:  2016-10-12
 */
 angular.module('hcncore.controllers')
.controller('SosFeatureController',function($rootScope,$scope,$state,$cordovaSms,$cordovaGeolocation,$cordovaToast ,Core,$ionicLoading,$ionicHistory,$ionicPlatform){
    $scope.$state    =  $state;
    $scope.$history  =  $ionicHistory;

    $scope.contact_numbers  =   [];
    $scope.primary_contact  =   null;
    $scope.contacts   =   [];
    $scope.latitude   = null;
    $scope.longitude   = null;

    $scope.init   =   function(){

      $scope.contacts = Core.emergency_contact().get_contacts_cached();

      // Core.emergency_contact().get_contacts().then(function(){
      //   $scope.contacts = Core.emergency_contact().get_contacts_cached();
      //   $scope.$digest();
      // });

      // console.log($scope.contacts);
      $ionicPlatform.ready(function() {
        var watchOptions = {
          timeout : 3000,
          enableHighAccuracy: false // may cause errors if true
        };
        // console.log(1111111111111);
        var watch = $cordovaGeolocation.watchPosition(watchOptions);
        watch.then(
          null,
          function(err) {
          },
          function(position) {
            $scope.latitude  = position.coords.latitude;
            $scope.longitude = position.coords.longitude;
            console.log([$scope.latitude,$scope.longitude]);

        });

        var posOptions = {timeout: 70000, enableHighAccuracy: false};
        $cordovaGeolocation.getCurrentPosition(posOptions)
          .then(function (position) {
            $scope.latitude  = position.coords.latitude;
            $scope.longitude = position.coords.longitude;
            console.log([$scope.latitude,$scope.longitude]);
          }, function(err) {
            // error
            console.log(err);
          });
      });


      if($scope.contacts != null){
        angular.forEach($scope.contacts.emergency_contacts,function(contact){
          if(contact.is_primary){
            $scope.primary_contact  =   contact.phone_number;
          }
          $scope.contact_numbers.push(contact.phone_number);
        });
      }

    }

    // send SMS method
    $scope.sendSms = function(){
      $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
              });

      var options = {
      replaceLineBreaks: false, // true to replace \n by a new line, false by default
      android: {
        // intent: 'INTENT'  // send SMS with the default SMS app
        intent: ''        // send SMS without open any other app
      }
      };
      var count   =   0;
      var login_user  =   Core.user().logged_in_user();
      var profile   =   login_user.profiles[0];

      var latitude = $scope.latitude;
      var longitude = $scope.longitude;

      // console.log(profile);
      // var message    =  'EMERGENCY ! Attention, '+profile.name+' need immediate help. Call Now. Dial 102 or 108 for ambulance.';

      var message   =   'EMERGENCY ! '+profile.name+'need immediate help. Dial 102 or 108 for ambulance. ';

      if(latitude != null && longitude != null){
        message += 'Current location  http://www.google.com/maps/place/'+latitude+','+longitude;
      }

      angular.forEach($scope.contact_numbers,function(number){
        console.log($scope.contact_numbers);


        $cordovaSms.send(number, message, options)
              .then(function() {
                  // Success! SMS was sent
                  count++;
                  if(count == $scope.contact_numbers.length){
                    $ionicLoading.hide();
                  }
              $cordovaToast.show('Message Sent Successfully to '+number ,'short','center');

              }, function(error) {
              // An error occurred
              count++;
              console.log(error);
              if(count == $scope.contact_numbers.length){
                $ionicLoading.hide();
              }
              $cordovaToast.show('Message Not Sent','short','center');
            });//then
          });
        };

    // Phone Calling method
    $scope.makeCall = function(){
      function onSuccess(result){
          console.log("Success:"+result);
        }
      function onError(result) {
        console.log("Error:"+result);
        }
          window.plugins.CallNumber.callNumber(onSuccess, onError,$scope.primary_contact);
      };

      $scope.sos_cancel = function(){
        $state.go('main_tabs.home_widget_tab.dashboard');
      };
      $scope.init();
   })
