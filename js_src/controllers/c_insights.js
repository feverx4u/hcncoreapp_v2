angular.module('hcncore.controllers')
.controller('InsightController',function($rootScope,$scope,$state,$ionicPlatform,Core,$timeout,$ionicLoading){

  $scope.$state    =  $state;
  $scope.init   =   function(){
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
    }, 10);
  }

  $scope.goto_insights_card = function(_id){
    $state.go('main_tabs.id',{id:_id});
  }

  $scope.init();
})
