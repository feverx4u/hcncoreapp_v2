angular.module('hcncore.controllers')
.controller('ScheduleController',function($rootScope,$scope,$state,$ionicPopup,$window,$ionicLoading,$ionicHistory,Core){
  $scope.$state = $state;
  $scope.$history = $ionicHistory;
  $scope.current_state  =   '';
  $scope.profile_id   =   null;
  $scope.is_error   =   false;
  $scope.is_loading   =   false;
  $scope.init   =   function(){
    $scope.current_state  =   $state.current.name;
    var profile   =   Core.user_profile().getActiveProfile();
    if(profile != null){
      $scope.profile_id   =   profile.id;
    }

    // console.log($scope.current_state);
    if($scope.current_state == 'main_tabs.sch_lab_list'){
      $scope.lab_schedule  =   [];
      $scope.fetch_lab_schedules();
    } else if($scope.current_state == 'main_tabs.sch_medicine_list'){
      $scope.medicine_schedule  =   [];
      $scope.fetch_medicine_schedules();
    } else if($scope.current_state == 'main_tabs.sch_doc_list'){
      $scope.doc_schedule  =   [];
      $scope.fetch_doc_schedules();
    } else if($scope.current_state == 'main_tabs.sch_activity_list'){
      $scope.activity_schedule  =   [];
      $scope.fetch_activity_schedules();
    }

  }

  $scope.medicine_schedule  =   [];
  $scope.fetch_medicine_schedules   =   function(){
    $scope.is_error   =   false;
    $scope.is_loading   =   true;
    $scope.medicine_schedule  =   [];
    Core.scheduledata().schedule_medication().resource().get({
      'from':moment().format('YYYY-MM-DD'),
      'profile_id':$scope.profile_id
    },function(success){
      $scope.medicine_schedule  =   success.schedules;
      console.log(success);
      $scope.$broadcast('scroll.refreshComplete');
      $scope.is_loading   =   false;
    },function(error){
      $scope.is_error   =   true;
      $scope.is_loading   =   false;
    });
  }

  $scope.lab_schedule  =   [];
  $scope.fetch_lab_schedules   =   function(){
    $scope.is_error   =   false;
    $scope.is_loading   =   true;
    $scope.lab_schedule  =   [];
    Core.scheduledata().schedule_labtest().resource().get({
      'from':moment().format('YYYY-MM-DD'),
      'profile_id':$scope.profile_id
    },function(success){
      $scope.lab_schedule  =   success.schedules;
      console.log(success);
      $scope.$broadcast('scroll.refreshComplete');
      $scope.is_loading   =   false;
    },function(error){
      $scope.is_error   =   true;
      $scope.is_loading   =   false;
    });
  }

  $scope.doc_schedule  =   [];
  $scope.fetch_doc_schedules   =   function(){
    $scope.is_error   =   false;
    $scope.is_loading   =   true;
    $scope.doc_schedule  =   [];
    Core.scheduledata().schedule_doc_appo().resource().get({
      'from':moment().format('YYYY-MM-DD'),
      'profile_id':$scope.profile_id
    },function(success){
      $scope.doc_schedule  =   success.schedules;
      console.log(success);
      $scope.$broadcast('scroll.refreshComplete');
      $scope.is_loading   =   false;
    },function(error){
      $scope.is_error   =   true;
      $scope.is_loading   =   false;
    });
  }

  $scope.activity_schedule  =   [];
  $scope.fetch_activity_schedules   =   function(){
    $scope.is_error   =   false;
    $scope.is_loading   =   true;
    $scope.activity_schedule  =   [];
    Core.scheduledata().schedule_activity().resource().get({
      'from':moment().format('YYYY-MM-DD'),
      'profile_id':$scope.profile_id
    },function(success){
      $scope.activity_schedule  =   success.schedules;
      $scope.is_loading   =   false;
      console.log(success);
      $scope.$broadcast('scroll.refreshComplete');
    },function(error){
      $scope.is_error   =   true;
      $scope.is_loading   =   false;
    });
  }


  $scope.delete_activity_list_data = function(item_id ,item){

    var confirmPopup = $ionicPopup.confirm({
      title: 'Activity',
      template: 'Are you sure you want to delete this Activity?'
    });
    confirmPopup.then(function(res) {
      if(res) {

        $ionicLoading.show({
          template: '<div style="text-align:center;width:200px;" class="tf_loader" style="">Successfully deleted</div>'
        });

        Core.scheduledata().schedule_activity().resource().delete({
          'id':item_id
        },function(success){
          $ionicLoading.hide();
          $scope.fetch_activity_schedules();
          console.log(success);
        },function(error){
          $ionicLoading.hide();
          console.log(error);
        });

        Core.calender().delete_calendar(item,"ACTIVITY");
      } else {
        console.log('You are not sure');
      }
    });
  }

  $scope.delete_medicine_list_data = function(item_id,item){

    var confirmPopup = $ionicPopup.confirm({
      title: 'Medicine',
      template: 'Are you sure you want to delete this Medicine?'
    });
    confirmPopup.then(function(res) {
      if(res) {

        $ionicLoading.show({
          template: '<div style="text-align:center;width:200px;" class="tf_loader" style="">Successfully deleted</div>'
        });

        Core.scheduledata().schedule_medication().resource().delete({
          'id':item_id
        },function(success){
          $ionicLoading.hide();
          $scope.fetch_medicine_schedules();
          console.log(success);
        },function(error){
          $ionicLoading.hide();
          console.log(error);
        });

        Core.calender().delete_calendar(item,"MEDICINE");
      } else {
        console.log('You are not sure');
      }
    });
  }

  $scope.delete_labtest_list_data = function(item_id,item){

    var confirmPopup = $ionicPopup.confirm({
      title: 'LabTest',
      template: 'Are you sure you want to delete this LabTest Info?'
    });
    confirmPopup.then(function(res) {
      if(res) {

        $ionicLoading.show({
          template: '<div style="text-align:center;width:200px;" class="tf_loader" style="">Successfully deleted</div>'
        });

        Core.scheduledata().schedule_labtest().resource().delete({
          'id':item_id
        },function(success){
          $ionicLoading.hide();
          $scope.fetch_lab_schedules();
          console.log(success);
        },function(error){
          $ionicLoading.hide();
          console.log(error);
        });

        Core.calender().delete_calendar(item,"LAB_TEST");
      } else {
        console.log('You are not sure');
      }
    });
  }

  $scope.delete_doctor_list_data = function(item_id,item){

    var confirmPopup = $ionicPopup.confirm({
      title: 'Doctor Appointment',
      template: 'Are you sure you want to delete this Doctor Appointment Info?'
    });
    confirmPopup.then(function(res) {
      if(res) {

        $ionicLoading.show({
          template: '<div style="text-align:center;width:200px;" class="tf_loader" style="">Successfully deleted</div>'
        });

        Core.scheduledata().schedule_doc_appo().resource().delete({
          'id':item_id
        },function(success){
          $ionicLoading.hide();
          $scope.fetch_doc_schedules();
          console.log(success);
        },function(error){
          $ionicLoading.hide();
          console.log(error);
        });

        Core.calender().delete_calendar(item,"DOCTOR_APPOINTMENT");

      } else {
        console.log('You are not sure');
      }
    });
  }

  $scope.init();

  $rootScope.$on('$stateChangeSuccess',
    function(event, toState, toParams, fromState, fromParams, options){
      if(toState.name != $scope.current_state){
          $scope.init();
      }
    });
});
