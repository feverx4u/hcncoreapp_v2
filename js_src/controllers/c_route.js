/*
 |---------------------------------------------------------------
 |  RouteController - Controller is place where application is
 |                    is initialized and taken to proper state.
 |---------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-01
 */
 angular.module('hcncore.controllers')
 .controller('RouteController',function($scope,$state,$ionicLoading,$ionicPlatform,$timeout,Core,$ionicHistory){

   //Initializing the Route Controller
   $scope.init   =   function(){

     //Loader
     $ionicLoading.show({
         template: '<ion-spinner icon="android"></ion-spinner>'
     });


     //Hiding the splashscreen

         $state.go('main_tabs.home_widget_tab.dashboard');
         $timeout(function(){
            $scope.decide_next_route();
            $ionicPlatform.ready(function() {
              navigator.splashscreen.hide();
            });
         },2000);



   }

   $scope.focus_elem   =   function(id){
     document.getElementById(id).focus();
   }

   //Method will decide the next route and navigate to that
   $scope.decide_next_route  =   function(){
     $ionicLoading.hide();
     var user  =   Core.user().logged_in_user();
     if(user == null){
         $state.go('credential');
     } else {
       var profiles  =   user.profiles;
       if(profiles != null && profiles.length > 0){
          $state.go('med_dashboard');
       } else {
          $state.go('user_info_med');
       }
       // var initial_flow   =   parseInt(user.is_init_flow);
       // if(false && initial_flow !=  0){
       //    $state.go('main_tabs.home_widget_tab.dashboard');
       // } else {
       //
       // }
       $ionicHistory.clearHistory();
     }
   }

   $scope.init();
 })
