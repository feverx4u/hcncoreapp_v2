angular.module('hcncore.controllers')
.controller('UserMedProfileController',function($ionicLoading,$rootScope,$scope,$state,$stateParams,$ionicHistory,Core,$cordovaToast,$timeout,$ionicPopup,$cordovaSms){

  $scope.v   =   {};

  $scope.init  =   function(){
    $scope.v   =   {
      'name':'',
      'phone_number':'',
      'relation':'',
      'profile_image':null,
      'is_new':true,
      'profile_id':''
    };

    $scope.v.name  =   $stateParams.name;
    $scope.v.phone_number  =   $stateParams.phone_number;
    $scope.fetch_med_profile($scope.v.phone_number);
  }

  $scope.fetch_med_profile  =   function(phone_number){
    Core.med_social().social_profile().show({
      'phone_number':phone_number
    },function(data){
      $scope.v.is_new  =   false;
      if(data.profile != undefined && data.profile != null){
        $scope.v.profile_id  =   data.profile.id;
        if(data.profile.profile_media != null){
           $scope.v.profile_image   =   data.profile.profile_media.uri;
        }
      }
    },function(error){
      if(error.status == 404){
        $scope.v.is_new  =   true;
      }
    });
  }

  $scope.add_request   =   function(){
    if($scope.v.profile_id == null || $scope.v.profile_id.length <= 0){
      $cordovaToast.show('Profile does not exists with us, please invite.','short','center');
      return;
    }

    if($scope.v.relation == null || $scope.v.relation.length <= 0){
      $cordovaToast.show('Please choose relation with '+$scope.v.name+'.','short','center');
      return;
    }

    Core.med_social().sent_request().commit({
      'dest_id':$scope.v.profile_id
    },{
      'action':'ADD',
      'relation':$scope.v.relation,
    },function(data){
      $cordovaToast.show('Request to monitor is sent to user','short','center');
     //  console.log(data);
     $timeout(function(){
       $state.go('med_dashboard');
     },1000);
    },function(error){
      $cordovaToast.show('You have already sent Request to '+$scope.v.phone_number,'short','center');
      if(error.data.errors.already_request != undefined){
         $cordovaToast.show(error.data.errors.already_request,'short','center');
      }
    });
  }

  $scope.invite_request   =   function(){
    if($scope.v.phone_number == null || $scope.v.phone_number.length <= 0){
      $state.go('med_dashboard');
      return;
    }

    if($scope.v.relation == null || $scope.v.relation.length <= 0){
      $cordovaToast.show('Please choose relation with '+$scope.v.name+'.','short','center');
      return;
    }

    $ionicLoading.show({
               template: '<ion-spinner icon="android"></ion-spinner>'
     });


    Core.med_social().send_invite().commit({
      'dest_phone_number':$scope.v.phone_number
    },{
      'action':'ADD',
      'relation':$scope.v.relation,
    },function(data){
      console.log(data);
     $timeout(function(){
       $ionicLoading.hide();
       console.log('here2');

       $scope.user_invite_popup();
     },1000);
    },function(error){
      $ionicLoading.hide();
      $cordovaToast.show('You have already sent Invitation to '+$scope.v.phone_number,'short','center');
      $scope.user_invite_popup();
      if(error.data.errors.already_request != undefined){
        $cordovaToast.show(error.data.errors.already_request,'short','center');
        $cordovaToast.show('Already sent Invitation','short','center');
      }
    });
  }

  $scope.goBack  =   function(){
    $ionicHistory.goBack();
  }

  // popup for share HCNCare join invitation
  $scope.invite_popup  =   null;
  $scope.user_invite_popup  =   function(){
    $scope.invite_popup = $ionicPopup.show({
      templateUrl: 'user_invite_popup.html',
      title: 'HCNCare Invitation',
      scope: $scope,
      'cssClass':'user_invite_popup'
    });
  }

  $scope.send_msg_popup  =  function(){
    $scope.invite_popup.close();
    $timeout(function(){
     $scope.sendSms();
    },100);
  }

  $scope.share_msg_popup  =  function(){
    $scope.invite_popup.close();
    $timeout(function(){
     window.plugins.socialsharing.share('I Would like to invite you to user HCNCare', null, null, 'https://www.google.in/');
   },1000);
  }

  // send SMS method
  $scope.sendSms = function(){

    var options = {
    replaceLineBreaks: false, // true to replace \n by a new line, false by default
    android: {
      intent: 'INTENT'  // send SMS with the default SMS app
     //  intent: ''        // send SMS without open any other app
    }
    };
    var number = $scope.v.phone_number;
      $cordovaSms.send(number, 'I Would like to invite you to user HCNCare https://www.google.in/', options)
            .then(function() {
                // Success! SMS was sent
               console.log("Successfully sent message " +number);
            }, function(error) {
            // An error occurred
            console.log("Message Not Sent");
          });//then
      };

  $scope.init();
})
