angular.module('hcncore.controllers')
.controller('AboutUsController',function($scope,$rootScope,$ionicPopover,$state,$cordovaToast,
  $timeout,Core,$ionicLoading,$timeout,$ionicHistory){

  $scope.$state    =  $state;

  $scope.$history   =   $ionicHistory;

  $scope.open_in_app_browser  =   function(url){
    window.open(Core.network().web+encodeURI(url), '_blank', 'location=yes');
  }
})
