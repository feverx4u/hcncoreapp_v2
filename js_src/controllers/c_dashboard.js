angular.module('hcncore.controllers')
.controller('DashboardController',function($scope,$rootScope,$state,$timeout,Core,$ionicHistory,$ionicModal,$ionicLoading){
    $scope.$state   =   $state;

    $scope.analytics   =  {};

    // $scope.final_analytics  =   {};

    $scope.bgl_data   =   {};
    $scope.bp_data   =   {};
    $scope.food_data   =  {};
    $scope.a1c_data   =  {};
    $scope.weight_data  =   {};
    $scope.symptom_data   = {};
    $scope.medicine_data   = {};
    $scope.acticity_data   = {};
    $scope.profile_image    =  null;
    $scope.todays_date =  moment().format('YYYY-MM-DD');
    $scope.init   =   function(){

      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);

      var user = Core.user_profile().getActiveProfile();
      if(user == null){
        return;
      }
      var analytics   =   Core.user_analytics().get_cached();
      if(analytics != null){
        process_data(analytics);
        $timeout(function () {
          $scope.fetch_analytics();
        }, 5000);
      } else {
        $scope.fetch_analytics();
      }
      // $ionicHistory.clearHistory();

      if(user!=null){
        $scope.username = user.name;
        if($scope.username == null || $scope.username.length == 0){
          $scope.username = user.email;
          if($scope.username == null || $scope.username.length == 0){
            $scope.username = user.phonenumber;
          }
        }
        console.log(user.profile_media);
        if(user.profile_media != null){
          $scope.profile_image    =   user.profile_media.uri;
        }
      }
    }

    function process_data(data){
      console.log(data);
      if(data  ==  null){
        return;
      }
      $scope.analytics = data.analytics;
      console.log($scope.analytics);
      // return false;
      angular.forEach($scope.analytics,function(item){
        if(item.card_name == 'BGL_STAT'){
          $scope.bgl_data   =   item.state;
        }

        if(item.card_name == 'BP_STAT'){
          $scope.bp_data   =   item.state;
        }

        if(item.card_name == 'FOOD_STAT'){
          $scope.food_data   =   item.state;
        }

        if(item.card_name == 'A1C_STAT'){
          $scope.a1c_data   =   item.state;
          $scope.a1c_data.average   =   parseFloat($scope.a1c_data.average);
          $scope.a1c_data.log_count   =   parseFloat($scope.a1c_data.log_count);
        }

        if(item.card_name == 'SYMPTOM_STAT'){
          $scope.symptom_data   =   item.state;
        }

        if(item.card_name == 'MEDICINE_STAT'){
          $scope.medicine_data   =   item.state;
        }


        if(item.card_name == 'ACTIVITY_STAT'){
          $scope.activity_data   =   item.state;
          angular.forEach($scope.activity_data,function(d){
            // console.log(d.number);
            d.number   =  sprintf('%0.1f',parseFloat(d.number)/3600);
          });
          // console.log(item);
        }

        if(item.card_name == 'WEIGHT_FULL_HORIZONTAL'){
          $scope.weight_data   =   item.weight;
          $scope.weight_data.current   =   parseFloat($scope.weight_data.current);
          $scope.weight_data.target   =   parseFloat($scope.weight_data.target);
          if($scope.weight_data.target > $scope.weight_data.current){
            $scope.weight_data.away   =   $scope.weight_data.target - $scope.weight_data.current;
          } else {
            $scope.weight_data.away   =   $scope.weight_data.current - $scope.weight_data.target;
          }
          // $scope.weight_data.away   =   parseFloat($scope.a1c_data.target);
        }

        // $scope.final_analytics[item.card_name]    =   item;
        });
    }

    $rootScope.$on('data:loggged',function(){
      $scope.fetch_analytics();
    });

    $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams){
      console.log(toState);
      if(toState.name    == "main_tabs.home_widget_tab.dashboard"){
          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });
          $scope.fetch_analytics();
      }
    });

    $rootScope.$on('$stateChangeSuccess',
    function(event, toState, toParams, fromState, fromParams){
      if(toState.name    == "main_tabs.home_widget_tab.dashboard"){
        // $rootScope.$emit('lazyImg:refresh');
          $timeout(function () {

              $ionicLoading.hide();
          }, 1000);

      }
    });


    $scope.fetch_analytics  =   function(){

      Core.user_analytics().get().then(function(data){
        process_data(data);
        $scope.$broadcast('scroll.refreshComplete');
        // $ionicLoading.hide();
      },function(error){
        $scope.$broadcast('scroll.refreshComplete');
        // $ionicLoading.hide();
      },function(update){
        // $ionicLoading.hide();
        // console.log(112221);
        // console.log(update);
        // process_data(update);
      });
    }




    $scope.tip_modal  =   null;
    $scope.tip_data   =   null;
    $ionicModal.fromTemplateUrl('dashboard_tip_modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.tip_modal = modal;
    });

    //Method is used to open the tip modal
    $scope.open_tip_modal   =   function(data){
      $scope.tip_data   =   data;
      $scope.tip_modal.show();
    }

    $scope.go_to_action   =   function(){
      $scope.tip_modal.hide();
      $state.go($scope.tip_data.state);
    }

    $scope.move_to  =  function(url){
      window.open(url,'_system');
    }

    // Info modal
    $scope.info_modal  =   null;
    $scope.info_data   =   null;
    $ionicModal.fromTemplateUrl('dashboard_info_modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.info_modal = modal;
    });

    //Method is used to open the info modal
    $scope.open_info_modal   =   function(data){
      $scope.info_data   =   data;
      $scope.info_modal.show();
    }

    $scope.init();
})
