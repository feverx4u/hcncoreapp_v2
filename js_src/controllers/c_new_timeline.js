/*
|-------------------------------------------------------------------
| NewTimelineController - Controller for user Timeline
|-------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-11-16
*/
angular.module('hcncore.controllers')
.controller('NewTimelineController',function($scope,$rootScope,$ionicPopover,$state,$cordovaToast,$timeout,Core,$ionicLoading,$timeout){
 $scope.$state    =  $state;

 $scope.init   =   function(){
   $timeout(function () {
       $rootScope.is_big_bar   =   false;

       $scope.cached_timeline();

   }, 10);
 }

 $rootScope.$on('$stateChangeStart',
 function(event, toState, toParams, fromState, fromParams){
   console.log(toState);
   if(toState.name    == "main_tabs.home_widget_tab.timeline"){
     $scope.result_data = [];
     $scope.result_dates = [];
     $scope.today = new Date();
     $scope.start_date =  moment().format('YYYY-MM-DD');

     $scope.lenght = 5;
     $scope.last_date = null;
     $scope.noMoreItemsAvailable = false;
     $scope.yesterday = moment($scope.start_date).subtract(1,'days').format('YYYY-MM-DD');
     $scope.count = 0;
     $scope.is_data_1 = 'YES';
     $scope.is_data_2 = 'NO';

       $scope.loadMore();
   }
 });

 // API calling for getting user timeline
 $scope.result_data = [];
 $scope.result_dates = [];
 $scope.today = new Date();
 $scope.start_date =  moment().format('YYYY-MM-DD');

 $scope.lenght = 5;
 $scope.last_date = null;
 $scope.noMoreItemsAvailable = false;
 $scope.yesterday = moment($scope.start_date).subtract(1,'days').format('YYYY-MM-DD');
 $scope.count = 0;
 $scope.is_data_1 = 'YES';
 $scope.is_data_2 = 'NO';
 $scope.cached_timeline_data   =   null;
 $scope.cached_timeline   =   function(){

   $scope.todays_date =  moment().format('YYYY-MM-DD');
   console.lof($scope.todays_date);

   $scope.cached_timeline_data = Core.timeline().get_cached();
   console.log(['1111',_data]);
   if(_data ==  null){
     $scope.loadMore();
   } else {
     $scope.cached_timeline_data   =   [];
     angular.forEach(_data.result , function(result){
       $scope.cached_timeline_data.push(result);
     });
     console.log(['timeline_data',$scope.cached_timeline_data]);
   }
 }

 $scope.loadMore = function(){

   // $ionicLoading.show({
   //     template: '<ion-spinner icon="android"></ion-spinner>'
   // });

   if($scope.last_date   ==  null){
     $scope.last_date    =   moment().format('YYYY-MM-DD');
   }

   Core.timeline().get( $scope.last_date,  $scope.lenght ).then(function(data){
     angular.forEach(data.result , function(result){
       $scope.result_data.push(result);
       $scope.result_dates.push(result.date);
       $scope.count++;
       $scope.$broadcast('scroll.infiniteScrollComplete');
       console.log($scope.result_data);
     });

       if( $scope.count == 0 ){
          $scope.noMoreItemsAvailable = true;
        }

     $scope.last_date = moment($scope.result_dates[$scope.count-1]).subtract(1,'days').format('YYYY-MM-DD');
     $scope.result_dates = [];
     $scope.count = 0;

     if( $scope.result_data[0] == undefined  && $scope.result_data[0] == null){
       $scope.is_data_1 = 'NO';
       $scope.is_data_2 = 'YES';
     }else{
       $scope.is_data_1 = 'NO';
     }
     $ionicLoading.hide();
   },function(error){
     $ionicLoading.hide();
     console.log(error);
   },function(update){
     $ionicLoading.hide();
     console.log(update);
   });
 }

 // $scope.$on('$stateChangeSuccess', function() {
 // $scope.loadMore();
 // });

 $scope.init();
})
