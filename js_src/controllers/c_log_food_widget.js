/*
 |-------------------------------------------------------------------
 | LogFoodWidgetController - Controller for log of food
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
angular.module('hcncore.controllers')
.controller('LogFoodWidgetController',function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$ionicLoading,$ionicHistory){

  $scope.$state    =  $state;
  $scope.v  =   {};

  $scope.init   =   function(){
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
    }, 10);
    $scope.variables();


  }


  $scope.variables   =  function(){
      var v   =   $scope.v;
      v.activity_search_text  =   '';
      v.is_searching  =   false;
      v.search_results    =   [];

      v.is_check_enabled  =   false;
      v.food_log_input  =  {
          'activity_id':'',
          'meal_name':'',
          'foods':[],
          'log_date':'',
          'log_date_view':'',
          'log_time':'',
          'log_time_view':'',
          'log_time_time':'',
      };

      v.food_log_input.log_date     =   moment().format('YYYY-MM-DD');

      $scope.revalidate_date();

      var time      =   moment().format('YYYY-MM-DD hh:mm A');
      $scope.revalidate_time(time);
  }



  $scope.validate_for_check   =   function(){
    var v   =   $scope.v;
    console.log(111);
    if(v.food_log_input.foods.length == 0){
      v.is_check_enabled  =   false;
      return;
    }

    if(v.food_log_input.meal_name == null || v.food_log_input.meal_name.length == 0){
      v.is_check_enabled  =   false;
      return;
    }

    v.is_check_enabled  =   true;
  }

  $scope.revalidate_date =  function(){
      var v   =   $scope.v;
      v.food_log_input.log_date_view     =   moment(v.food_log_input.log_date).format('Do MMM,YYYY');
  }

  $scope.search_food  =   function(){
      var v   =   $scope.v;
      v.is_searching  =   true;
      Core.logdata().food_log().search(v.food_search_text).then(function(data){
           v.search_results   =   data.message;
           v.is_searching  =   false;
      },function(data){
          v.is_searching  =   false;
      });
  }

  $scope.revalidate_time =  function(time){
      var v   =   $scope.v;
      v.food_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
      // console.log(v.activity_log_input.log_time);
      v.food_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
      v.food_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
  }



  $scope.log_food   =   function(){

    $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
            });
    var v   =   $scope.v;
      Core.logdata().food_log().commit(v.food_log_input).then(function(data){
          $ionicLoading.hide();
        $scope.log_success_popup('Food Logged');
        // $timeout(function(){
        //   $state.go('main_tabs.log_widget');
        // },2000);
      },function(error){
          $ionicLoading.hide();
        console.log(error.message);
        $cordovaToast.show(error.message,'short','center');
      });
  }

  $scope.serving_popup  =   null;

  $scope.selected_food_servings  =   [];

  $scope.seleted_food_data  =   {
    'food_id':'',
    'food_name':'',
    'serving_unit_id':'',
    'serving':''
  };

  $scope.choose_food  =   function(food){
    // alert(1111);
    var v   =   $scope.v;

    $ionicLoading.show({
        template: '<ion-spinner icon="android"></ion-spinner>'
    });

    Core.logdata().food_log().food(food.food_id).then(function(data){
      var food  =   data.message;
      // $scope.selected_food  =   food;

      $scope.selected_food_servings   = [];
      $scope.seleted_food_data.food_id = food.food_id;
      $scope.seleted_food_data.food_name = food.food_name;

      if(food.servings.serving.serving_id != undefined){
        $scope.selected_food_servings.push(food.servings.serving);
        $scope.seleted_food_data.serving_unit_id   =   food.servings.serving.serving_id;
      } else {
        angular.forEach(food.servings.serving,function(serving,k){
          $scope.selected_food_servings.push(serving);
          if(k == 0){
            $scope.seleted_food_data.serving_unit_id   =   serving.serving_id;
          }
        });
      }


      $timeout(function(){
        $ionicLoading.hide();
        $scope.serving_popup = $ionicPopup.show({
          templateUrl: 'serving_popup.html',
          title: food.food_name,
          scope: $scope,
          'cssClass':'serving_popup'
        });
      },10);

       v.food_search_text  =   '';
       v.search_results   =   [];
    },function(data){
      $ionicLoading.hide();
        // v.is_searching  =   false;
        v.food_search_text  =   '';
        v.search_results   =   [];
    });
  }

  $scope.close_serving_popup  =   function(){
    var v   =   $scope.v;
    if($scope.seleted_food_data.serving_unit_id.length <= 0){
      $cordovaToast.show('Please select the unit','short','center');
      return;
    }

    if($scope.seleted_food_data.serving.length <= 0){
      $cordovaToast.show('Please enter quantity','short','center');
      return;
    }
    $scope.serving_popup.close();
    v.food_log_input.foods.push($scope.seleted_food_data);
    $scope.seleted_food_data  =   {
      'food_id':'',
      'serving_unit_id':'',
      'serving':''
    };
    $scope.validate_for_check();
  }

  $scope.open_datepicker  =   function(){
    // alert(111);
    var v   =   $scope.v;
    ionicDatePicker.openDatePicker({
      callback:function(val){

        v.food_log_input.log_date   =   moment(val).format('YYYY-MM-DD');
        $scope.revalidate_date();
        // console.log(val)
      },
      templateType: 'popup',
      to:new Date( moment().format('YYYY-MM-DD')),
      inputDate:new Date(moment())
    });
  }

  $scope.open_timepicker  =   function(){
    var v   =   $scope.v;
    var ipObj1 = {
      callback: function (val) {      //Mandatory
        if (typeof (val) === 'undefined') {
          console.log('Time not selected');
        } else {
          var selectedTime = new Date(val * 1000);
          $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
        }
      },
      inputTime: v.food_log_input.log_time_time,   //Optional
      format: 12,         //Optional
      step: 1,           //Optional
      setLabel: 'Set'    //Optional
    };

    ionicTimePicker.openTimePicker(ipObj1);
  }

  $scope.log_pop_message  =   null;
  $scope.log_popup  =   null;
  $scope.log_success_popup  =   function(message){
    $scope.log_pop_message  =   message;
    $scope.log_popup = $ionicPopup.show({
      templateUrl: 'log_sucess_popup.html',
      title: 'Successfully Logged',
      scope: $scope,
      'cssClass':'log_success_popup'
    });
  }

  $scope.close_log_success_popup  =  function(notes){
    $scope.log_popup.close();
    $timeout(function(){
      $state.go('main_tabs.log_widget');
      $rootScope.$broadcast('data:loggged');
    },100);
  }

  // Go back confirmation popup
   $scope.showConfirm = function() {
     var v   =   $scope.v;
     if(!v.is_check_enabled){
       $ionicHistory.goBack();
     }else {
       var confirmPopup = $ionicPopup.confirm({
         title: 'Log Food',
         template: 'Are you sure you want to go back?'
       });
       confirmPopup.then(function(res) {
         if(res) {
           v.is_check_enabled  =   false;
           $state.go("main_tabs.log_widget");
         } else {
           console.log('You are not sure');
         }
       });
     }
   };

  $scope.init();
})
