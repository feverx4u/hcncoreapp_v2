/*
 |-------------------------------------------------------------------
 | ScheduleLabTestController - Controller for Schedule Lab Test
 |-------------------------------------------------------------------
 |  @author:  Shekh Rizwan<rizwan@hirarky.com>
 |  @date:  2016-10-07
 */
 angular.module('hcncore.controllers')
.controller('ScheduleLabTestController',function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$ionicLoading,$cordovaCalendar,$ionicHistory){
    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();
    }

    $scope.variables   =  function(){
        var v   =   $scope.v;
        v.is_check_enabled  =   false;
        v.labtest_sch_input  =  {
            'lab_name':'',
            'test_type':'',
            'labtest_date':'',
            'labtest_date_view':'',
            'labtest_time':'',
            'labtest_time_view':'',
            'labtest_time_time':'',
            'notify_before':'Hour_1',
            'notes':'',
            'years':'',
            'months':'',
            'days':'',
            'hours':'',
            'minutes':''
        };

        v.labtest_sch_input.labtest_date     =   moment().format('YYYY-MM-DD');

        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');
        $scope.revalidate_time(time);
    }

    $scope.open_datepicker  =   function(){
      // alert(111);
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.labtest_sch_input.labtest_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
          // console.log(val)
        },
        templateType: 'popup',
        from:new Date(),
        to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
        inputDate:new Date(moment())
      });
    }

    $scope.open_timepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
          }
        },
        inputTime: v.labtest_sch_input.labtest_time_time,   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };

      ionicTimePicker.openTimePicker(ipObj1);
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.labtest_sch_input.labtest_date_view     =   moment(v.labtest_sch_input.labtest_date).format('Do MMM,YYYY');
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.labtest_sch_input.labtest_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        // console.log(v.a1c_log_input.log_time);
        v.labtest_sch_input.labtest_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.labtest_sch_input.labtest_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
    }
    $scope.notes_popup  =   null;
    $scope.notes  =   '';
    $scope.open_note_popup  =   function(){
      $scope.notes_popup = $ionicPopup.show({
        templateUrl: 'note_popup.html',
        title: 'Notes',
        scope: $scope,
        'cssClass':'note_popup'
      });
    }

    $scope.close_notes_popup  =  function(notes){
      $scope.v.labtest_sch_input.notes =  notes;
      $scope.notes_popup.close();
    }
    $scope.cancel_notes_popup  =  function(){
      $scope.notes_popup.close();
    }

      // Schedule LabTest API Calling Method
    $scope.sch_labtest   =   function(){

      $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
              });
      var v   =   $scope.v;
        Core.scheduledata().schedule_labtest().commit(v.labtest_sch_input).then(function(data){
          $ionicLoading.hide();
          Core.calender().commit_event(data.schedule,'LAB_TEST');
          $cordovaToast.show("Lab Test Details scheduled",'long','center');
          $timeout(function(){
            $state.go('main_tabs.schedule_main');
            // $scope.createEvent(); // add Clender Events
          },2000);
        },function(error){
          $ionicLoading.hide();
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }

    // add  Schedule LabTest event to the Calender
    $scope.createEvent = function() {
      var v   =   $scope.v;
      $scope.formatDate(v.labtest_sch_input.labtest_date);
      $scope.formatTime(v.labtest_sch_input.labtest_time);
        $cordovaCalendar.createEvent({
            title: 'Lab test scheduled at '+v.labtest_sch_input.lab_name,
            location: v.labtest_sch_input.lab_name,
            notes: v.labtest_sch_input.notes,
            startDate: new Date(v.labtest_sch_input.years, v.labtest_sch_input.months-1, v.labtest_sch_input.days, v.labtest_sch_input.hours, v.labtest_sch_input.minutes, 0, 0, 0),
            endDate: new Date(moment(new Date(v.labtest_sch_input.years, v.labtest_sch_input.months-1, v.labtest_sch_input.days, v.labtest_sch_input.hours, v.labtest_sch_input.minutes, 0, 0, 0)).add(60, 'minutes').toDate())
        }).then(function (result) {
            $cordovaToast.show("Schedule LabTest Event Created",'long','center');
            console.log("Event created successfully " +result);
        }, function (err) {
            console.error("There was an error: " + err);
        });
    }

    $scope.formatDate = function(date) {
      var v   =   $scope.v;
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

          if (month.length < 2) month = '0' + month;
          if (day.length < 2) day = '0' + day;

          _month = parseInt(month);
          _day = parseInt(day);
          _year = parseInt(year);

          v.labtest_sch_input.years = _year;
          v.labtest_sch_input.months = _month;
          v.labtest_sch_input.days = _day;
      }

        $scope.formatTime = function(time) {
            var v   =   $scope.v;
            var parts = time.split(':');
            v.labtest_sch_input.hours = parts[0];
            v.labtest_sch_input.minutes = parts[1];
        }

        // Go back confirmation popup
         $scope.showConfirm = function() {
           var v   =   $scope.v;
           if(v.labtest_sch_input.lab_name == null || v.labtest_sch_input.lab_name.length <= 0){
             $ionicHistory.goBack();
           }else {
             var confirmPopup = $ionicPopup.confirm({
               title: 'Lab Test',
               template: 'Are you sure you want to go back?'
             });
             confirmPopup.then(function(res) {
               if(res) {
                 $ionicHistory.goBack();
               } else {
                 console.log('You are not sure');
               }
             });
           }
         };

    $scope.init();
})
