/*
 |-------------------------------------------------------------------
 | DiabetesTypeController - Controller for user goal Diabetes
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
 angular.module('hcncore.controllers')
 .controller('DiabetesTypeController',function($rootScope,$scope,$state,$ionicPlatform,Core,
   $timeout,$ionicLoading,$cordovaToast,ionicDatePicker,$ionicHistory){

     $scope.v   =   {};
      //Method to initialize the controller
      $scope.init   =   function(){
        $scope.variable();
        $scope.fetch_type();
      }

      //variable
      $scope.variable   = function(){
        var v   =   $scope.v;
        $scope.v.diabetes_type    =   [];
      }

      //fetching the goal
      $scope.fetch_type  =   function(){
        // alert(111);
        Core.diabetes().fetch_diabetes_type().then(function(data){
          $scope.v.diabetes_type  =   data.message;
          // console.log(data);
        });
      }

      $scope.go_back  =   function(){
        $ionicHistory.goBack();
      }

      //Method is used to choose the goal
      $scope.choose_diabetec_type  =   function(type){
        // console.log(type);
        angular.forEach($scope.v.diabetes_type,function(i_type){
            if(i_type.id == type.id){
              i_type.is_checked   = true;
            } else {
              i_type.is_checked   = false;
            }
        });

      }

      //Method is used to select the goal
      $scope.select_type  =   function(){
        var diabetes_type   =   $scope.v.diabetes_type;
        var choosen_type  = null;
        angular.forEach(diabetes_type,function(type){
            if(type.is_checked){
              choosen_type = type;
            }
        });

        if(choosen_type==null){
          $state.go('diabetes_range');
          // $cordovaToast.show('Please choose the diabetes type.','short','center');
          return;
        }

        Core.diabetes().update_diabeted_type(choosen_type).then(function(){
          // $cordovaToast.show('Updated','short','bottom');
          $state.go('diabetes_range');
        },function(error){
          $cordovaToast.show(error.message,'short','center');
        });
      }

      $scope.init();
  })
