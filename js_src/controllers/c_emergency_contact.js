/*
 |-------------------------------------------------------------------
 | EmergencyContactController - Controller for contact
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
 angular.module('hcncore.controllers')
 .controller('EmergencyContactController',function($rootScope,$scope,$state,$ionicPlatform,Core,
 $timeout,$ionicLoading,$cordovaToast,$ionicPopup,$ionicModal,$cordovaContacts,$ionicHistory){

   $scope.v  =   {};

   $scope.v.is_overlay   =   false;
   $scope.$history  =   $ionicHistory;

   $scope.init   =   function(){
     $scope.is_top_menu_show  =   true;
     $timeout(function () {
       if($state.current.name != 'menu_emergency_contact'){
           $rootScope.is_big_bar   =   true;
           $scope.is_top_menu_show  =   true;
       } else {
         $scope.is_top_menu_show  =   false;
       }
     }, 10);
     $scope.variables();
     // $scope.fetch_contacts();
     $scope.fetch_contacts();
   }

   $scope.fetch_contacts     =   function(){
       Core.emergency_contact().get_contacts().then(function(data){
         $scope.v.contacts   =   data.emergency_contacts;
       });
   }
   $scope.variables  =   function(){
     var v   =   $scope.v;
     $scope.v.contacts   =   [];
     v.is_add  =   false;

     v.add_contact_data     =   {
       'name':'',
       'phone_number':'',
       'type':'',
       'is_primary':false
     };
   }

   $scope.why_add  =   null;
   $ionicModal.fromTemplateUrl('why_emergency_add_modal.html', {
     scope: $scope,
     animation: 'slide-in-up'
   }).then(function(modal) {
     $scope.why_add = modal;
   });

   $scope.why_should_i_add     =   function(){
     $scope.why_add.show();
   }



   $scope.open_contact_modal   =   function(i_contact){
     $ionicPlatform.ready(function(){
       $cordovaContacts.pickContact().then(function (contactPicked) {
         // $scope.contact = contactPicked;
         var v   =   $scope.v;
         if(contactPicked.phoneNumbers != null && contactPicked.phoneNumbers.length > 0){
             var contactnum  =   contactPicked.phoneNumbers[0].value;
             if(contactnum != null && contactnum != undefined){
               i_contact.name   = contactPicked.displayName;
               i_contact.phone_number  = contactnum;
             } else {
               $cordovaToast.show('No Phone number associated to this contact, choose another contact.','short','center');
             }
         } else {
           $cordovaToast.show('No Phone number associated to this contact, choose another contact.','short','center');
         }

         // console.log($scope.contact);
       });
     });
   }

   $scope.add_contact  =   function(){
     var v   =   $scope.v;
     Core.emergency_contact().add_contact(v.add_contact_data).then(function(data){
       $scope.fetch_contacts();
       v.is_add  = false;
       v.add_contact_data     =   {
         'name':'',
         'phone_number':'',
         'type':'',
         'is_primary':false
       };
     },function(error){
       console.log(error.message);
       $cordovaToast.show(error.message,'short','center');
     });
   }

   $scope.update_contact   =   function(contact){
     var v   =   $scope.v;
     Core.emergency_contact().update_contact(contact).then(function(data){
       $scope.fetch_contacts();
       v.is_add  = false;
       $scope.v.is_overlay   =   false;
     },function(error){
       console.log(error.message);
       $cordovaToast.show(error.message,'short','center');
       $scope.v.is_overlay   =   false;
     });
   }

   $scope.remove_contact   =   function(contact){
     var v   =   $scope.v;

     var confirmPopup = $ionicPopup.confirm({
      title: 'Remove Contact',
      template: 'Are you sure you want to remove Emergency Contact?'
    });

    confirmPopup.then(function(res) {
      if(res) {
        Core.emergency_contact().delete_contact(contact).then(function(data){
          $scope.fetch_contacts();
          v.is_add  = false;
        },function(error){
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
      } else {
        console.log('You are not sure');
      }
    });


   }

   $scope.go_back  =   function(){
    //  alert(1111);
     $ionicHistory.goBack();
     // var user_goal   =   Core.goal().fetch_user_goal();
     // if(user_goal.indexOf('hypertension') >= 0 && user_goal.indexOf('diabetes_management') >= 0){
     //   $state.go('hypertension');
     // } else if(user_goal.indexOf('hypertension') >= 0){
     //   $state.go('hypertension');
     // } else if(user_goal.indexOf('diabetes_management') >= 0){
     //   $state.go('therephy_type');
     // } else {
     //    $state.go('user_goal');
     // }
   }

   $scope.dahsboard  =   function(){
     $state.go('main_tabs.home_widget_tab.dashboard');
   }
   $scope.init();
 })
