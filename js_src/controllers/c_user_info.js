/*
 |-------------------------------------------------------------------
 | UserinfoController - Controller for userinfomation
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
 angular.module('hcncore.controllers')
 .controller('UserinfoController',function($rootScope,$scope,$state,$ionicPlatform,Core,
   $timeout,$ionicLoading,$cordovaToast,ionicDatePicker,$ionicNavBarDelegate,$ionicHistory){

     $scope.v    =   {};

    //  $scope.
     //Method to initialize the variable
     $scope.variable   =   function(){
       var v   =   $scope.v;
       v.user_info   =   {
         'name':'',
         'dob':'',
         'gender':'',
         'gender_view':'',
         'height':'',
         'weight':'',
         'height_unit':'FEET',
         'weight_unit':'KG'
       };
       v.gender_options   =   {
         'MALE':'Male',
         'FEMALE':'Female'
       };
     }

     $scope.$watch('v.user_info.height_unit',function(new_val,current_val){
         var v   =   $scope.v;
       if(new_val != current_val){
         if(new_val == 'METER'){
           v.user_info.height    =   parseFloat(sprintf('%.2f',parseFloat(v.user_info.height)*0.30));
         } else if(new_val == 'FEET'){
           v.user_info.height    =   parseFloat(sprintf('%.2f',parseFloat(v.user_info.height)/0.30));
         }
       }

     });

     $scope.$watch('v.user_info.weight_unit',function(new_val,current_val){
         var v   =   $scope.v;
       if(new_val != current_val){
         if(new_val == 'LBS'){
           v.user_info.weight    =   parseFloat(sprintf('%.2f',parseFloat(v.user_info.weight)*2.2));
         } else if(new_val == 'KG'){
           v.user_info.weight    =   parseFloat(sprintf('%.2f',parseFloat(v.user_info.weight)/2.2));
         }
       }
     });

     $scope.$watch('v.user_info.gender',function(){
       if($scope.v.user_info.gender == 'MALE'){
         $scope.v.user_info.gender_view  =   'Male';
       } else if($scope.v.user_info.gender == 'FEMALE'){
         $scope.v.user_info.gender_view  =   'Female';
       } else {
         $scope.v.user_info.gender_view  =   '';
       }
     });
     $scope.trigger_click  =   function(id){
       var event = new MouseEvent('mousedown', {
          // 'view': window,
          // 'bubbles': true,
          'cancelable': true
      });
     //  alert(id);
      var cb = document.getElementById(id);
     //  console.log(document.getElementById(id));
      // cb.size=4;
      cb.dispatchEvent(event);
     }

     $scope.open_date_picker    =  function(){
       // alert(111);
       var v   =   $scope.v;
       var default_date  = '';
       if(v.user_info.dob != ''){
           default_date  =   moment(v.user_info.dob, 'Do MMM,YYYY');
       } else  {
         default_date  =   moment('1991-01-01');
       }


       ionicDatePicker.openDatePicker({
         callback:function(val){
           v.user_info.dob   =   moment(val).format('Do MMM,YYYY');
         },
         templateType: 'popup',
         to:new Date( moment().subtract(3,'years').format('YYYY-MM-DD')),
         inputDate:new Date(default_date)
       });
     }

     //Fecthing the loged in user
     $scope.fetch_login_user   =   function(){
       var v   =   $scope.v;
       var user  = Core.user_profile().getActiveProfile();
       if(user == null){
         return;
       }
       v.user_info.name  = user.name;
       if(user.dob != null && user.dob != undefined && user.dob != ''){
           v.user_info.dob    = moment(user.dob).format( 'Do MMM,YYYY');
       }
       if(user.gender != null && user.gender != undefined){
         v.user_info.gender   = user.gender;
       } else {
         // v.user_info.gender   = 'MALE';
         v.user_info.gender   = '';
       }
       if(user.height != null && user.height != undefined){
         v.user_info.height   = parseFloat(user.height);
       }

       if(user.weight != null && user.weight != undefined){
         v.user_info.weight   = parseFloat(user.weight);
       }
       if(user.height_unit != null && user.height_unit != undefined){
         v.user_info.height_unit   = user.height_unit;
       }
       if(user.weight_unit != null && user.weight_unit != undefined){
         v.user_info.weight_unit   = user.weight_unit;
       }
     }

     //Initialisig the init
     $scope.init    =  function(){
       // $ionicNavBarDelegate.showBar(false);
       $timeout(function () {
           $rootScope.is_big_bar   =   true;
       }, 10);
       // document.querySelect
       $scope.variable();
       $scope.fetch_login_user();
       // $ionicHistory.clearHistory();
     }

     //Inititalizing the updated user
     $scope.update_user_info   =   function(){
       var user_info   =   $scope.v.user_info;

       Core.user_profile().update_user_info(user_info).then(function(data){
         // $cordovaToast.show(error.message,'short','bottom');
         // $cordovaToast.show('User updated','short','bottom');

         $state.go('user_goal')
       },function(error){
         // login_data.in_prog   =   false;
         console.log(error.message);
         $cordovaToast.show(error.message,'short','center');
       });
     }

     $scope.go_back  =   function(){
         $ionicHistory.goBack();
     }
     $scope.init();
 })
