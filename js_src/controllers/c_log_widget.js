/*
 |-------------------------------------------------------------------
 | LogWidgetController - Controller for log
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
angular.module('hcncore.controllers')
.controller('LogWidgetController',function($rootScope,$scope,$state,$ionicPlatform,Core,$timeout){

  $scope.$state    =  $state;
  $scope.init   =   function(){
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
    }, 10);
  }

  $scope.init();
})
