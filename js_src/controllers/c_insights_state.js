angular.module('hcncore.controllers')
.controller('InsightStateController',function($rootScope,$scope,$state,$ionicPlatform,Core,$timeout,$ionicLoading,$stateParams,$ionicHistory){

  $scope.$state    =  $state;
  $scope.$history    =  $history;
  $scope.init   =   function(){
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
        $scope.get_insights($stateParams.id);
        console.log($stateParams.id);
    }, 10);
  }

  $scope.blogs  = [];
  $scope.get_insights = function(_id){

    $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
            });

    Core.insights().get(_id).then(function(data){
      // console.log(data);
      angular.forEach(data ,function(_data){
        var featured_media_id   = _data.featured_media;
          Core.insights().media_resource().get({
            'id':featured_media_id
          },function(it){
            _data.media_url =  it.source_url;
          });
        $scope.blogs.push(_data);
      });
      // console.log($scope.blogs.lenght);
      $ionicLoading.hide();

    },function(error){
      $ionicLoading.hide();
      console.log(error);
    });
  }

  $scope.goto_browser = function(url){
     window.open(url, '_blank', 'location=no');
  }

  $scope.init();
})
