/*
|-------------------------------------------------------------------
| AccountSettingsController - Controller for user account setting
|-------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-11-10
*/
angular.module('hcncore.controllers')
.controller('AccountSettingsController',function($scope,$rootScope,$ionicPopover,$state,$cordovaToast,$timeout,Core,$ionicLoading,$timeout,$ionicHistory){
 $scope.$state    =  $state;

 $scope.init   =   function(){
   $timeout(function () {
       $rootScope.is_big_bar   =   false;
   }, 10);
 }

 $scope.logout   =   function(){
   Core.user().logout();
   $ionicLoading.show({
       template: '<ion-spinner icon="android"></ion-spinner>'
   });

   $timeout(function(){
     $ionicLoading.hide();
     $timeout(function(){
       $state.go('credential');
     },10);
   },1000);
 };

 $scope.$history   =   $ionicHistory;

 $scope.open_in_app_browser  =   function(url){
   window.open(Core.network().web+encodeURI(url), '_blank', 'location=yes');
 }
 $scope.init();
})
