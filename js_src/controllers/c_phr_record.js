/*
 |-------------------------------------------------------------------
 | PhrRecordController - Controller for PHR Record Feature
 |-------------------------------------------------------------------
 |  @author:  Shekh Rizwan<rizwan@hirarky.com>
 |  @date:  2016-11-01
 */
 angular.module('hcncore.controllers')
.controller('PhrRecordController',function($rootScope,$scope,$stateParams,$ionicPopup,$cordovaFileOpener2,$timeout,$state,$cordovaToast ,Core,$ionicLoading,$ionicHistory){

  $scope.$state    =  $state;
  $scope.$history    =  $ionicHistory;
  $scope.v  =   {};
  var v   =   $scope.v;

  $scope.init   =   function(){
    if($state.current.name  == 'main_tabs.phr_widget_tab.event'){
      console.log($stateParams.event_name);
    } else {

    }
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
        $scope.phr_show_event();
    }, 10);
    $scope.variables();
  }
  $scope.variables   =  function(){
      var v   =   $scope.v;
      v.phr_record_input  =  {
        'head1':'',
        'head2':'',
        'file_path':''
      };
  }
  // Phr show API Calling Method for getting Event
  $scope.event_array =[];
   $scope.phr_show_event   =   function(){
     $ionicLoading.show({
         template: '<ion-spinner icon="android"></ion-spinner>'
     });

     Core.phrdata().phr_event().get().then(function(data){
       angular.forEach(data.data,function(data){
               $scope.event_array.push(data);
       });
       $ionicLoading.hide();

     },function(error){
       console.log("error "+error);
       $ionicLoading.hide();
     },function(update){
       console.log("update "+update);
       $ionicLoading.hide();
     });
   }

   $scope.navi_toEtype = function(para1 , para2){
     $state.go('main_tabs.phr_event',{'event_name':para1,'file_count':para2});

     v.phr_record_input.head1 = para1;
     v.phr_record_input.head2 = para2;
     console.log(v.phr_record_input.head1);
     console.log(v.phr_record_input.head2);
   }

  $scope.init();
 })
