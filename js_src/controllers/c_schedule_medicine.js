/*
 |-------------------------------------------------------------------
 | ScheduleMedicationController - Controller for Schedule Medication
 |-------------------------------------------------------------------
 |  @author:  Shekh Rizwan<rizwan@hirarky.com>
 |  @date:  2016-10-07
 */
 angular.module('hcncore.controllers')
.controller('ScheduleMedicationController',function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$ionicLoading,$cordovaCalendar,DateTimePickerServices,$ionicHistory){
    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();
    }

    $scope.variables   =  function(){
        var v   =   $scope.v;
        v.is_check_enabled  =   false;
        v.med_sch_input  =  {

            'sch_medicine':[],
            'medicine_name':'',
            'doss_type':'',
            'doss':'',
            'sch_date':'',
            'sch_date_view':'',
            'sch_time':'',
            'sch_time_view':'',
            'sch_time_time':'',
            'sch_repeat':'',
            'notes':'',
            'repeat_on':'',
            'is_forever':'',
            'until':'',
            'event_count':'',
            'years':'',
            'months':'',
            'days':'',
            'hours':'',
            'minutes':'',
            'togle_on_off':false,
            'toggle_disable':false
        };

        v.repeat  =   {
          'sun':false,
          'mon':false,
          'tue':false,
          'wed':false,
          'thu':false,
          'fri':false,
          'sat':false,
          'is_forever':'1',
          'popup_date_view':'',
          'until_date':'',
          'num_event':''
        };

        v.med_sch_input.sch_date     =   moment().format('YYYY-MM-DD');
        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');
        $scope.revalidate_time(time);
        $scope.$watchGroup([
          'v.repeat.sun',
          'v.repeat.mon',
          'v.repeat.tue',
          'v.repeat.wed',
          'v.repeat.thu',
          'v.repeat.fri',
          'v.repeat.sat',
        ],function(){
          var v   =   $scope.v;
          var until   = (new Date());
          console.log(v.repeat.sun);
          if(v.repeat.sun == true){
            var temp  = get_next_date(0);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }

          if(v.repeat.mon  == true){
            var temp  = get_next_date(1);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }

          if(v.repeat.tue  == true){
            var temp  = get_next_date(2);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }

          if(v.repeat.wed  == true){
            var temp  = get_next_date(3);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }

          if(v.repeat.thu  == true){
            var temp  = get_next_date(4);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }

          if(v.repeat.fri  == true){
            var temp  = get_next_date(5);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }

          if(v.repeat.sat  == true){
            var temp  = get_next_date(6);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }
          v.repeat.until_date   = moment(until).format('YYYY-MM-DD');
          v.repeat.popup_date_view   = moment(until).format('Do MMM,YYYY');
          console.log(v.repeat.until_date);
        });
    }

    function get_next_date(index){
      var today_date  =   new Date();
      var current_week_day  = today_date.getDay();

      var different_days  =   index - current_week_day;
      if(different_days <= 0){
        different_days = 7 + different_days;
      }

      return moment(today_date).add('days',different_days).toDate();
    }
    $scope.open_datepicker  =   function(){

      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.med_sch_input.sch_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
          // console.log(val)
        },
        templateType: 'popup',
        from:new Date(),
        // to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
        inputDate:new Date(moment(v.med_sch_input.sch_date))
      });
    }

    $scope.open_untildatepicker  =   function(){

      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.repeat.until_date   =   moment(val).format('YYYY-MM-DD');
          v.repeat.popup_date_view     =   moment(v.repeat.until_date).format('Do MMM,YYYY');
          // console.log(val)
        },
        templateType: 'popup',
        from:new Date(),
        // to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
        inputDate:new Date(moment(v.repeat.until_date,'YYYY-MM-DD').format('YYYY-MM-DD'))
      });
    }

    $scope.open_timepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
          }
        },
        inputTime: v.med_sch_input.sch_time_time,   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };

      ionicTimePicker.openTimePicker(ipObj1);
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.med_sch_input.sch_date_view     =   moment(v.med_sch_input.sch_date).format('Do MMM,YYYY');
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.med_sch_input.sch_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        v.med_sch_input.sch_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.med_sch_input.sch_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
    }
    $scope.notes_popup  =   null;
    $scope.notes  =   '';
    $scope.open_note_popup  =   function(){
      $scope.notes_popup = $ionicPopup.show({
        templateUrl: 'note_popup.html',
        title: 'Notes',
        scope: $scope,
        'cssClass':'note_popup'
      });
    }

    $scope.close_notes_popup  =  function(notes){
      $scope.v.med_sch_input.notes =  notes;
      $scope.notes_popup.close();
    }
    $scope.cancel_notes_popup  =  function(){
      $scope.notes_popup.close();
    }


    // display repeat days popup
    $scope.repeat_days_popup  =   null;
    $scope.sch_repeat  =   '';
    $scope.open_repeat_days_popup  =   function(){
      var v   =   $scope.v;
      v.med_sch_input.togle_on_off = true;
      v.med_sch_input.toggle_disable = true;
      $scope.repeat_days_popup = $ionicPopup.show({
        templateUrl: 'repeat_days_popup.html',
        title: 'Repeat',
        scope: $scope,
        'cssClass':'repeat_days_popup'
      });
    }

    $scope.save_repeat_days_popup  =  function(){
      var v   =   $scope.v;
      if((v.repeat.sun === false)&&(v.repeat.mon === false)&&(v.repeat.tue === false)&&(v.repeat.wed === false)
        &&(v.repeat.thu === false)&&(v.repeat.fri === false)&&(v.repeat.sat === false)){
        $cordovaToast.show("Please Select Atleast One day",'short','center');
      }
      else{

      var sun;
      var mon;
      var tue;
      var wed;
      var thu;
      var fri;
      var sat;
      var week_array  =   [];
      if(v.repeat.sun === true){
        sun = "Sun";
        week_array.push(sun);
      }else {
        sun = "";
      }
      if(v.repeat.mon === true){
        mon = "Mon";
        week_array.push(mon);
      }else {
        mon = "";
      }
      if(v.repeat.tue === true){
        tue = "Tue";
        week_array.push(tue);
      }else {
        tue = "";
      }
      if(v.repeat.wed === true){
        wed = "Wed";
        week_array.push(wed);
      }else {
        wed = "";
      }if(v.repeat.thu === true){
        thu = "Thu";
        week_array.push(thu);
      }else {
        thu = "";
      }
      if(v.repeat.fri === true){
        fri = "Fri";
        week_array.push(fri);
      }else {
        fri = "";
      }if(v.repeat.sat === true){
        sat = "Sat";
        week_array.push(sat);
      }else {
        sat = "";
      }
      v.med_sch_input.is_forever = false;
      v.med_sch_input.until = null;
      v.med_sch_input.event_count = null;
      if(v.repeat.is_forever == 0){
        v.med_sch_input.is_forever = true;
      }

      if(v.repeat.is_forever == 1){
        v.med_sch_input.is_forever = false;
        v.med_sch_input.until = v.repeat.until_date;
      }

      if(v.repeat.is_forever == 2){
        v.med_sch_input.is_forever = false;
        v.med_sch_input.event_count = v.repeat.num_event;
      }

      $scope.repeat_days_popup.close();
      v.med_sch_input.toggle_disable = false;
      v.med_sch_input.repeat_on = week_array.join(', ');
      v.med_sch_input.repeat_on    =   v.med_sch_input.repeat_on.trim();
      console.log(v.med_sch_input.repeat_on);

      if(v.med_sch_input.repeat_on != null && v.med_sch_input.repeat_on != ""){
          v.med_sch_input.togle_on_off = true;
      }else{
        v.med_sch_input.togle_on_off = false;
      }
    }
  }

    $scope.cancel_repeat_days_popup  =  function(){
      var v   =   $scope.v;
      v.med_sch_input.toggle_disable = false;
      $scope.repeat_days_popup.close();
      if(v.med_sch_input.repeat_on != null && v.med_sch_input.repeat_on != ""){
          v.med_sch_input.togle_on_off = true;
      }else{
        v.med_sch_input.togle_on_off = false;
      }
    }

    // display medicine select popup
    $scope.select_medicine_popup  =   null;
    $scope.open_medicine_popup  =   function(){
      $scope.select_medicine_popup = $ionicPopup.show({
        templateUrl: 'select_medicine_popup.html',
        title: 'Medicine 1',
        scope: $scope,
        'cssClass':'repeat_days_popup'
      });
    }

    $scope.save_medicine_popup  =  function(){
      $scope.select_medicine_popup.close();
    }

    $scope.cancel_medicine_popup  =  function(){
      $scope.select_medicine_popup.close();
    }


    // API Calling Method for Schedule Medicine
    $scope.sch_medication  =   function(){
      $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
              });
      var v   =   $scope.v;
      v.med_sch_input.sch_medicine = [];
      v.med_sch_input.sch_medicine.push({
        'name':v.med_sch_input.medicine_name,
        'doss_type': v.med_sch_input.doss_type,
        'doss': v.med_sch_input.doss,
      })
        Core.scheduledata().schedule_medication().commit(v.med_sch_input).then(function(data){
          $ionicLoading.hide();
          Core.calender().commit_event(data.schedule,'MEDICINE');
          $cordovaToast.show("Medication Scheduled",'long','center');
          $timeout(function(){
            $state.go('main_tabs.schedule_main');
            // $scope.createEvent(); // add Clender Events
          },2000);
        },function(error){
          $ionicLoading.hide();
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });

    }


    // add  Schedule Medicine event to the Calender
    $scope.createEvent = function() {
        var v   =   $scope.v;

        $scope.formatDate(v.med_sch_input.sch_date);
        $scope.formatTime(v.med_sch_input.sch_time);

        $cordovaCalendar.createEvent({
            title: 'Medicine time '+v.med_sch_input.medicine_name+'('+v.med_sch_input.doss_type+','+v.med_sch_input.doss+')',
            location: '',
            notes: v.med_sch_input.notes,
            startDate: new Date(v.med_sch_input.years, v.med_sch_input.months-1, v.med_sch_input.days, v.med_sch_input.hours, v.med_sch_input.minutes, 0, 0, 0),
            endDate: new Date(moment(new Date(v.med_sch_input.years, v.med_sch_input.months-1, v.med_sch_input.days, v.med_sch_input.hours, v.med_sch_input.minutes, 0, 0, 0)).add(15, 'minutes').toDate())
        }).then(function (result) {
            $cordovaToast.show("Schedule Medication Event Created",'long','center');
            console.log("Event created successfully " +result);
        }, function (err) {
            console.error("There was an error: " + err);
        });
    }

    $scope.formatDate = function(date) {
      var v   =   $scope.v;
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

          if (month.length < 2) month = '0' + month;
          if (day.length < 2) day = '0' + day;

          _month = parseInt(month);
          _day = parseInt(day);
          _year = parseInt(year);

          v.med_sch_input.years = _year;
          v.med_sch_input.months = _month;
          v.med_sch_input.days = _day;
      }

        $scope.formatTime = function(time) {
            var v   =   $scope.v;
            var parts = time.split(':');
            v.med_sch_input.hours = parts[0];
            v.med_sch_input.minutes = parts[1];
        }

        // Go back confirmation popup
         $scope.showConfirm = function() {
           var v   =   $scope.v;
           if(v.med_sch_input.medicine_name == null || v.med_sch_input.medicine_name.length <= 0){
             $ionicHistory.goBack();
           }else {
             var confirmPopup = $ionicPopup.confirm({
               title: 'Medication',
               template: 'Are you sure you want to go back?'
             });
             confirmPopup.then(function(res) {
               if(res) {
                 $ionicHistory.goBack();
               } else {
                 console.log('You are not sure');
               }
             });
           }
         };

    $scope.init();
})
