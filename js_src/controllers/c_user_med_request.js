angular.module('hcncore.controllers')
.controller('UserMedRequestController',function($rootScope,$scope,$state,$stateParams,$ionicHistory,Core,$cordovaToast,$timeout){

  $scope.v   =   {};

  $scope.init  =   function(){
    $scope.v   =   {
      'name':'',
      'phone_number':'',
      'relation':'',
      'profile_image':null,
      'is_new':true,
      'profile_id':'',
      'profile_user_id':'',
    };

    $scope.v.name  =   $stateParams.name;
    $scope.v.phone_number  =   $stateParams.phone_number;
    $scope.v.relation  =   $stateParams.relation;
    $scope.fetch_med_profile($scope.v.phone_number);
  }

  $scope.fetch_med_profile  =   function(phone_number){
    Core.med_social().social_profile().show({
      'phone_number':phone_number
    },function(data){
      $scope.v.is_new  =   false;
      if(data.profile != undefined && data.profile != null){
        $scope.v.profile_id  =   data.profile.id;
        $scope.v.profile_user_id  =   data.profile.user_id;
        if(data.profile.profile_media != null){
           $scope.v.profile_image   =   data.profile.profile_media.uri;
           console.log($scope.v.profile_image);
        }
      }
    },function(error){
      if(error.status == 404){
        $scope.v.is_new  =   true;
      }
    });
  }

  $scope.status_update   =   function(status){
    if($scope.v.profile_id == null || $scope.v.profile_id.length <= 0){
      $cordovaToast.show('Profile does not exists with us, please invite.','short','center');
      return;
    }

    Core.med_social().status_request().commit({
      'src_id':$scope.v.profile_user_id
    },{
      'action':'STATUS_UPDATE',
      'status':status,
    },function(data){
      if(status == 'ACCEPT'){
         $cordovaToast.show($scope.v.name+' is now in your Med network','short','center');
      }
     //  console.log(data);
     $timeout(function(){
       $state.go('med_dashboard');
     },1000);
    },function(error){
      console.log("request fail");
      if(error.data.errors.already_request != undefined){
         $cordovaToast.show(error.data.errors.already_request,'short','center');
      }
    });
  }

  $scope.goBack  =   function(){
    $ionicHistory.goBack();
  }

  $scope.init();
})
