/*
 |-------------------------------------------------------------------
 | PhrEventTypeController - Controller for PHR get Event Type Feature
 |-------------------------------------------------------------------
 |  @author:  Shekh Rizwan<rizwan@hirarky.com>
 |  @date:  2016-11-01
 */
 angular.module('hcncore.controllers')
 .controller('PhrEventTypeController',function($rootScope,$scope,$state,$stateParams,$timeout,$state,Core,$ionicLoading,$ionicHistory){
       $scope.$state    =  $state;
       $scope.v  =   {};

       $scope.init   =   function(){
         var v   =   $scope.v;

         $timeout(function () {
             $rootScope.is_big_bar   =   false;
             v.event_name =  $stateParams.event_name;
             v.count =  $stateParams.file_count;
              $scope.phr_show_event_type();
         }, 10);
       }

       $scope.go_back = function(){
         $ionicHistory.goBack();
       }
       $scope.eType_array =[];
        $scope.phr_show_event_type   =   function(){
          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });

          Core.phrdata().phr_event_type().get($scope.v.event_name).then(function(data){
            angular.forEach(data.data,function(data){
                    $scope.eType_array.push(data);
                    console.log($scope.eType_array);
                    $ionicLoading.hide();
            });
          },function(error){
            console.log("error "+error);
            $ionicLoading.hide();
          },function(update){
            console.log("update "+update);
            $ionicLoading.hide();
          });
        }

        $scope.navigate_to_type  =   function(type){
          $state.go('main_tabs.phr_event_type',{'event_name':$scope.v.event_name,'type_name':type.type});
        }
       $scope.init();

     })
