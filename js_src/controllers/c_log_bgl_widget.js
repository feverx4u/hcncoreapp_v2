/*
 |-------------------------------------------------------------------
 | LogBGLWidgetController - Controller for log of food
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
angular.module('hcncore.controllers')
.controller('LogBGLWidgetController',function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$ionicLoading){

  $scope.$state    =  $state;
  $scope.v  =   {};

  $scope.init   =   function(){
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
    }, 10);
    $scope.variables();
    $scope.fetch_events();

  }


  $scope.variables   =  function(){
      var v   =   $scope.v;
      v.activity_search_text  =   '';
      v.is_searching  =   false;
      v.search_results    =   [];
      v.search_results1    =   [];

      v.is_check_enabled  =   false;
      v.bgl_log_input  =  {
        'event_id':'',
        'event_name':'',
          'subevent_name':'',
          'blood_glucose_level':'',
          'unit':'mg_dl',
          'foods':[],
          'log_date':'',
          'log_date_view':'',
          'log_time':'',
          'log_time_view':'',
          'log_time_time':'',
          'place':'',
          'notes':'',
          'mood':''
      };

      v.events  =   {};
      v.bgl_log_input.log_date     =   moment().format('YYYY-MM-DD');

      var target_range = Core.diabetes().get_target_range();
      if(target_range != null){
        v.bgl_log_input.unit  =   target_range.range_unit;
        // console.log(target_range);
      }

      $scope.revalidate_date();

      var time      =   moment().format('YYYY-MM-DD hh:mm A');
      $scope.revalidate_time(time);

      $scope.$watch('v.bgl_log_input.unit',function(new_val,old_val){
        // console.log([new_val,old_val]);
        if(old_val == 'mg_dl'){
          if(new_val == 'mmol_l'){
            console.log($scope.v.bgl_log_input.blood_glucose_level);
            if($scope.v.bgl_log_input.blood_glucose_level != null){
              $scope.v.bgl_log_input.blood_glucose_level    =   parseFloat(sprintf('%0.1f',parseFloat($scope.v.bgl_log_input.blood_glucose_level )/18));
            }
          }
        } else{
          if(new_val == 'mg_dl'){
            console.log($scope.v.bgl_log_input.blood_glucose_level);
            if($scope.v.bgl_log_input.blood_glucose_level != null){
              $scope.v.bgl_log_input.blood_glucose_level    =   parseInt(parseFloat($scope.v.bgl_log_input.blood_glucose_level )*18);
              if(  $scope.v.bgl_log_input.blood_glucose_level >  999){
                $scope.v.bgl_log_input.blood_glucose_level  =   999;
              }
            }
          }
        }
      });
  }

  $scope.fetch_events    =  function(){
    var v   =   $scope.v;
    Core.logdata().bgl_log().event().then(function(data){
      angular.forEach(data.message,function(evnt){
          if(evnt.parent_id == null){
            if(v.events[evnt.id] == undefined){
              evnt.child   =   [];
              if(evnt.event_name == 'Morning'){
                evnt.icon   =   './img/bgl_morning.png';
              } else if(evnt.event_name == 'Afternoon'){
                evnt.icon   =   './img/bgl_afternoon.png';
              } else if(evnt.event_name == 'Evening'){
                evnt.icon   =   './img/bgl_evening.png';
              } else if(evnt.event_name == 'Night'){
                evnt.icon   =   './img/bgl_night.png';
              }
              v.events[evnt.id]   =   evnt;

            }
              // console.log(v.events);
          } else {
            evnt.icon   = '';
            // console.log(v.events[evnt.parent_id]);
            var parent_event  = v.events[evnt.parent_id];
            if(parent_event != undefined){
              parent_event.child.push(evnt);
            }
              // console.log(v.events);
          }
      });
      console.log(v.events);
    });
  }


  $scope.validate_for_check   =   function(){
    var v   =   $scope.v;
    console.log(111);
    if(v.bgl_log_input.event_id.length == 0){
      v.is_check_enabled  =   false;
      return;
    }

    if(v.bgl_log_input.blood_glucose_level == null || v.bgl_log_input.blood_glucose_level.length == 0){
      v.is_check_enabled  =   false;
      return;
    }

    if(v.bgl_log_input.unit == null || v.bgl_log_input.unit.length == 0){
      v.is_check_enabled  =   false;
      return;
    }

    v.is_check_enabled  =   true;
  }

  $scope.revalidate_date =  function(){
      var v   =   $scope.v;
      v.bgl_log_input.log_date_view     =   moment(v.bgl_log_input.log_date).format('Do MMM,YYYY');
  }

  $scope.search_food  =   function(){
      var v   =   $scope.v;
      v.is_searching  =   true;
      Core.logdata().food_log().search(v.food_search_text).then(function(data){
        v.search_results   =   data.message;
          v.search_results1   =   data.message;
           v.is_searching  =   false;
      },function(data){
          v.is_searching  =   false;
      });
  }

  $scope.revalidate_time =  function(time){
      var v   =   $scope.v;
      v.bgl_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
      // console.log(v.activity_log_input.log_time);
      v.bgl_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
      v.bgl_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
  }



  $scope.log_bgl   =   function(){

    $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
            });
    var v   =   $scope.v;
      Core.logdata().bgl_log().commit(v.bgl_log_input).then(function(data){
          $ionicLoading.hide();
        $scope.log_success_popup('Blood glucose Level Logged');
        // $cordovaToast.show("Blood glucose Level Logged",'long','center');
        // $timeout(function(){
        //   $state.go('main_tabs.log_widget');
        // },2000);
          v.search_results1   =   [];
          $scope.variables();
      },function(error){
        $ionicLoading.hide();
        v.search_results1   =   [];
        console.log(error.message);
        $cordovaToast.show(error.message,'short','center');
      });
  }


  $scope.log_pop_message  =   null;
  $scope.log_popup  =   null;
  $scope.log_success_popup  =   function(message){
    $scope.log_pop_message  =   message;
    $scope.log_popup = $ionicPopup.show({
      templateUrl: 'log_sucess_popup.html',
      title: 'Successfully Logged',
      scope: $scope,
      'cssClass':'log_success_popup'
    });
  }

  $scope.close_log_success_popup  =  function(notes){
    $scope.log_popup.close();
    $timeout(function(){
      $state.go('main_tabs.log_widget');
      $rootScope.$broadcast('data:loggged');
    },100);
  }

  $scope.serving_popup  =   null;

  $scope.selected_food_servings  =   [];

  $scope.seleted_food_data  =   {
    'food_id':'',
    'food_name':'',
    'serving_unit_id':'',
    'serving':''
  };

  $scope.choose_food  =   function(food){
    // alert(1111);
    var v   =   $scope.v;

    $ionicLoading.show({
        template: '<ion-spinner icon="android"></ion-spinner>'
    });

    Core.logdata().food_log().food(food.food_id).then(function(data){
      var food  =   data.message;
      // $scope.selected_food  =   food;

      $scope.selected_food_servings   = [];
      $scope.seleted_food_data.food_id = food.food_id;
      $scope.seleted_food_data.food_name = food.food_name;

      if(food.servings.serving.serving_id != undefined){
        $scope.selected_food_servings.push(food.servings.serving);
        $scope.seleted_food_data.serving_unit_id   =   food.servings.serving.serving_id;
      } else {
        angular.forEach(food.servings.serving,function(serving,k){
          $scope.selected_food_servings.push(serving);
          if(k == 0){
            $scope.seleted_food_data.serving_unit_id   =   serving.serving_id;
          }
        });
      }

      // angular.forEach(food.servings.serving,function(serving,k){
      //   $scope.selected_food_servings.push(serving);
      //   if(k == 0){
      //     $scope.seleted_food_data.serving_unit_id   =   serving.serving_id;
      //   }
      // });

      $timeout(function(){
        $ionicLoading.hide();
        $scope.serving_popup = $ionicPopup.show({
          templateUrl: 'serving_popup.html',
          title: food.food_name,
          scope: $scope,
          'cssClass':'serving_popup'
        });
      },10);

       v.food_search_text  =   '';
       v.search_results   =   [];
    },function(data){
      $ionicLoading.hide();
        // v.is_searching  =   false;
        v.food_search_text  =   '';
        v.search_results   =   [];
    });
  }

  $scope.close_serving_popup  =   function(){
    var v   =   $scope.v;
    if($scope.seleted_food_data.serving_unit_id.length <= 0){
      $cordovaToast.show('Please select the unit','short','center');
      return;
    }

    if($scope.seleted_food_data.serving.length <= 0){
      $cordovaToast.show('Please enter quantity','short','center');
      return;
    }
    $scope.serving_popup.close();
    v.bgl_log_input.foods.push($scope.seleted_food_data);
    $scope.seleted_food_data  =   {
      'food_id':'',
      'serving_unit_id':'',
      'serving':''
    };
    $scope.validate_for_check();
  }

  $scope.open_datepicker  =   function(){
    // alert(111);
    var v   =   $scope.v;
    ionicDatePicker.openDatePicker({
      callback:function(val){

        v.bgl_log_input.log_date   =   moment(val).format('YYYY-MM-DD');
        $scope.revalidate_date();
        // console.log(val)
      },
      templateType: 'popup',
      to:new Date( moment().format('YYYY-MM-DD')),
      inputDate:new Date(moment())
    });
  }

  $scope.open_timepicker  =   function(){
    var v   =   $scope.v;
    var ipObj1 = {
      callback: function (val) {      //Mandatory
        if (typeof (val) === 'undefined') {
          console.log('Time not selected');
        } else {
          var selectedTime = new Date(val * 1000);
          $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
        }
      },
      inputTime: v.bgl_log_input.log_time_time,   //Optional
      format: 12,         //Optional
      step: 1,           //Optional
      setLabel: 'Set'    //Optional
    };

    ionicTimePicker.openTimePicker(ipObj1);
  }

  $scope.notes_popup  =   null;
  $scope.notes  =   '';
  $scope.open_note_popup  =   function(){
    $scope.notes_popup = $ionicPopup.show({
      templateUrl: 'note_popup.html',
      title: 'Notes',
      scope: $scope,
      'cssClass':'note_popup'
    });
  }

  $scope.close_notes_popup  =  function(notes){
    $scope.v.bgl_log_input.notes =  notes;
    $scope.notes_popup.close();
  }
  $scope.cancel_notes_popup  =  function(){
      $scope.notes_popup.close();
    }

  $scope.events_popup  =   null;
  $scope.events  =   '';
  $scope.title  = '';

  $scope.open_event_popup  =   function(){
    $scope.title  = 'Event';
    $scope.v.selected_main_event_id    =   null;
    $scope.events_popup = $ionicPopup.show({
      templateUrl: 'event_popup.html',
      title: $scope.title,
      scope: $scope,
      'cssClass':'event_popup'
    });
  }

  $scope.v.selected_main_event_id    =   null;
  $scope.sub_events   =   [];
  $scope.select_event   =   function(evnt){
    // alert(111);
    $scope.v.selected_main_event_id   =  evnt.id;
    $scope.sub_events   =  evnt.child;
    $scope.v.bgl_log_input.event_name   =   evnt.event_name;
    // $scope.$digest();
    // console.log($scope.selected_main_event_id);
  }

  $scope.close_and_restart  =   function(){
    $scope.v.selected_main_event_id    =   null;
    $scope.events_popup.close();
      $scope.open_event_popup();
  }

  $scope.choose_final_event  =  function(evnt){
    // $scope.v.bgl_log_input.notes =  notes;
    $scope.v.bgl_log_input.event_id   =   evnt.id;
    $scope.v.bgl_log_input.subevent_name   =   evnt.event_name;
    $scope.events_popup.close();
    $scope.validate_for_check();
  }

  $scope.close_event_popup  =  function(){
    // $scope.v.bgl_log_input.notes =  notes;
    $scope.events_popup.close();
  }

  // Go back confirmation popup
   $scope.showConfirm = function() {
     var v   =   $scope.v;
     if(v.bgl_log_input.blood_glucose_level == null || v.bgl_log_input.blood_glucose_level.length <= 0){
       $state.go("main_tabs.log_widget");
     }else {
       var confirmPopup = $ionicPopup.confirm({
         title: 'Blood Glucose Level',
         template: 'Are you sure you want to go back?'
       });
       confirmPopup.then(function(res) {
         if(res) {
           $state.go("main_tabs.log_widget");
         } else {
           console.log('You are not sure');
         }
       });
     }
   };

  $scope.init();
})
