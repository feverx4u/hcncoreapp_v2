angular.module('hcncore.directives')
.directive('numberval', function ($timeout) {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs,ngModel) {
      var max   =   parseInt(attrs.maxval);
      var fractionlen   = parseInt(attrs.fractionlen);
    // console.log(ngModel.$modelValue);
      element.bind("keyup",function(e){
        var current_val   =   ngModel.$viewValue;
        current_val   =   current_val+'';
        if(current_val.indexOf('.') >= 0){
          var indexOf = current_val.indexOf('.');
          if(indexOf < current_val.length-2){
              current_val   =   current_val.substr(0,indexOf+2);
              current_val   =   parseFloat(current_val);
              ngModel.$setViewValue(current_val);
              ngModel.$render();
              scope.$apply();
          }
        }
        if(current_val > max){
          current_val   =   current_val+'';
          current_val   =   current_val.substr(0,current_val.length-1);
          current_val   =   parseFloat(current_val);
          ngModel.$setViewValue(current_val);
          ngModel.$render();
          scope.$apply();
        }


      });
    }
  };
})
