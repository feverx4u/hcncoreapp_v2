angular.module('hcncore.filters',[])
.filter('format_date',function(){
  return function(date,format,format_to){
    return moment(date, format).format(format_to);
  }
})
.filter('php_date_format',function(){
  return function(date,format){
    return moment(date, "YYYY-MM-DD").format(format);
  }
})
.filter('php_datetime_format',function(){
  return function(date,format){
    return moment(date, "YYYY-MM-DD HH:mm:ss").format(format);
  }
})
.filter('weight_string',function(){
  return function(str){
    // console.log(str);
    if(str == 'KG'){
      return 'Kgs';
    } else {
      return 'Lbs';
    }
    // return moment(date, "YYYY-MM-DD HH:mm:ss").format(format);
  }
})

.filter('symptom_date_format',function(){
  return function(date){
    return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
  }
})

.filter('symptom_time_format',function(){
  return function(time){
    var new_time  =   moment().format('YYYY-MM-DD')+' '+time;
    return moment(new_time, "YYYY-MM-DD HH:mm").format('hh:mm A');
  }
})

.filter('timeline_time_format',function(){
  return function(time){
    return moment(time, "YYYY-MM-DD HH:mm:ss").format('hh:mm A');
  }
})


.filter('duration_format',function(){
  return function(duration,format){

    //Considerint duration in seconds;
    if(format == 'h'){
      return parseInt(duration/3600);
    } else if(format == 'm'){
      var hour  =   parseInt(duration/3600);

      return parseInt((duration - hour*3600)/60)
    }

    console.log([duration,format]);
    return duration;
  }
})

.filter('to_int',function(){
  return function(i){
    // console.log(i);
    return parseInt(i);
  }
})

.filter('ago',function(){
    return function(date){
      var now = moment();
      var then = moment(date,'YYYY-MM-DD HH:mm:ss');
      return moment.duration(now.diff(then))._data.days;
    }
})
.filter('check_type',function(){
  return function(record,type){
       var extn = record.media.uri.split(".").pop();
       var _type = '';
       if(extn == 'pdf'){
         _type  =   'PDF';
       }else if(extn == 'jpg' || extn == 'png'){
         _type  =   'IMAGE';
       }else if(extn == 'doc' || extn == 'docx'){
         _type =  "DOC";
       }

       if(type  == _type){
         return true;
       } else {
         return false;
       }
  }
})
;
