/*
 |-----------------------------------------------------------------
 |  services.js
 |----------------------------------------------------------------
 |  @author:      Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:      2016-09-01
 |
 */


angular.module('hcncore.services',[])

/*
 |---------------------------------------------------------------
 |  Core - Single point access to access the all the application Feature
 |---------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-01
 */
.factory('Core',function(Log,Network,User,Goal,Diabetes,Hypertenstion,EmergencyContact,LogInfo,ScheduleInfo,
  UserAnalytics,FileOp,UserReports,UserProfile,PhoneContacts,MedSocial,PhrInfo,TimelineData){
  var factory   =   {}

  //Method will return the user object to handle all user
  //related funcationality
  factory.user  =  function(){
    return User;
  }

  factory.user_profile  =   function(){
    return UserProfile;
  }

  //Method will return the goal object to handle all goal
  //related funcationality
  factory.goal  =  function(){
    return Goal;
  }

  //Method will return the goal object to handle all goal
  //related funcationality
  factory.diabetes  =  function(){
    return Diabetes;
  }

  factory.hypertension  =   function(){
    return Hypertenstion;
  }

  factory.emergency_contact  =   function(){
    return EmergencyContact;
  }

  factory.logdata   =   function(){
    return LogInfo;
  }

  factory.phrdata   =   function(){
    return PhrInfo;
  }

  factory.scheduledata   =   function(){
    return ScheduleInfo;
  }
  //Method will return the log object to handle the loging the log
  //For the application.
  factory.log   =   function(){
    return Log;
  }

  factory.schedule   =   function(){
    return Schedule;
  }

  //Method will return the log object to handle the network status call
  factory.network   =   function(){
    return Network;
  }

  //Method will return the log object to handle the network status call
  factory.user_analytics   =   function(){
    return UserAnalytics;
  }

  factory.file  =   function(){
    return FileOp;
  }

  factory.user_report  =   function(){
    return UserReports;
  }


  factory.phone_contacts  =   function(){
    return PhoneContacts;
  }

  factory.med_social  =   function(){
    return MedSocial;
  }

  factory.timeline = function(){
    return TimelineData;
  }
  return factory;
})


/*
 |------------------------------------------------------------------------------
 |  Log - Loging of  the Log
 |------------------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-01
 |
 */
 .factory('Log',function($log){
    var factory   =   {};

    factory.log_level   =   'DEBUG';
    factory.app_env   =   'LOCAL'; //Possible values [LOCAL,TEST,PROD]

    //Funcation will set the log level for the application
    function set_log_level(){
      if(factory.app_env == 'LOCAL'){
        factory.log_level   =   'DEBUG';
      } else if(factory.app_env == 'TEST'){
        factory.log_level   =   'INFO';
      } else if(factory.app_env == 'PROD'){
        factory.log_level   =   'ERROR';
      }
    }

    //Method is used to get the log level
    function get_debug_level(){
      if(factory.log_level == 'DEBUG'){
        return 3;
      } else if(factory.log_level == 'INFO'){
        return 2;
      } else if(factory.log_level == 'ERROR'){
        return 1;
      }
    }

    //Method to log the debug log
    factory.debug   =   function(tag,obj){
      if(3 <= get_debug_level()){
        $log.debug({
          'tag':tag,
          'message':obj
        });
      }
    }

    //Method to log the warn log
    factory.warn   =   function(tag,obj){
      if(2 <= get_debug_level()){
        $log.warn({
          'tag':tag,
          'message':obj
        });
      }
    }

    //Method to log the info log
    factory.info   =   function(tag,obj){
      if(2 <= get_debug_level()){
        $log.info({
          'tag':tag,
          'message':obj
        });
      }
    }


      //Method to log the info log
      factory.error   =   function(tag,obj){
        if(1 <= get_debug_level()){
          $log.error({
            'tag':tag,
            'message':obj
          });
        }
      }

    return factory;
 })

 /*
  |------------------------------------------------------------------------------
  |  Network - Loging of  the Log
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-01
  |
  */
  .factory('Network',function($log,$cordovaNetwork){
    var factory   =   {};

    factory.api_url   =   'http://hcncore.hirarky.com/api/v1';

    // factory.api_url   =   'http://192.168.0.6:8000/api/v1';
    // factory.api_url   =   'http://localhost:8000/api/v1';

    //Method is used to get network state
    factory.getNetwork  =   function(){
      /*
       |
       | Connection Type	Description
       | Connection.UNKNOWN	Unknown connection
       | Connection.ETHERNET	Ethernet connection
       | Connection.WIFI	WiFi connection
       | Connection.CELL_2G	Cell 2G connection
       | Connection.CELL_3G	Cell 3G connection
       | Connection.CELL_4G	Cell 4G connection
       | Connection.CELL	Cell generic connection
      */
      return $cordovaNetwork.getNetwork();
    }

    //Method is used to check whether network is online or not
    factory.isOnline  =   function(){
      return $cordovaNetwork.isOnline();
    }

    //Method is used to check whether network is online or not
    factory.isOffline  =   function(){
      return $cordovaNetwork.isOffline();
    }

    return factory;
  })

  /*
   |----------------------------------------------------------------------------
   |  FileOp - File Management for application
   |----------------------------------------------------------------------------
   |
   |  $author: Lokesh Kumawat<lokesh@hirarky.com>
   |  @date: 2016-09-24
   */
   .factory('FileOp',function($log,$cordovaFile,$cordovaFileTransfer){

     var factory  =   {};

     factory.checkDir =  function(directory){
       console.log(cordova.file.externalDataDirectory);
       return $cordovaFile.checkDir(cordova.file.externalDataDirectory,directory);
     }

     factory.checkFile  =   function(directory,file_name){
       return $cordovaFile.checkFile(cordova.file.externalDataDirectory+directory,file_name);
     }

     factory.createDir  =   function(directory){
       console.log([cordova.file.dataDirectory, directory]);
       return $cordovaFile.createDir(cordova.file.externalDataDirectory, directory, false);
     }

     factory.download_file    =   function(url, targetPath){
       return $cordovaFileTransfer.download(url, cordova.file.externalDataDirectory+targetPath, {}, true);
     }

     return factory;
   })

   /*
    |
    |
    |
    |
    |
    */
  .factory('Media',function($log,Network){
      var factory   =   {};

      factory.upload  =   function(file,type,success,failed,abort,progress){
        var fd = new FormData()
        fd.append("file", file)
        fd.append("type", type)

        var xhr = new XMLHttpRequest()
        xhr.upload.addEventListener("progress", progress, false)
        xhr.addEventListener("load", success, false)
        xhr.addEventListener("error", failed, false)
        xhr.addEventListener("abort", abort, false)
        xhr.open("POST", Network.api_url+"/media")
        xhr.send(fd)
      }

      factory.validate_for_upload   =   function(oInput,ext){
          if(ext == undefined){
            ext   =   [
              'JPG','JPEG','PDF','PPT','PPTX','DOC','DOCX','PNG'
            ];
          }
          if (oInput.type == "file") {
            var sFileName = oInput.value;
             if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < ext.length; j++) {
                    var sCurExtension = ext[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }

                if (!blnValid) {
                    oInput.value = "";
                    return false;
                }
            }
        }
        return true;
      }

      return factory;
  })
  /*
  |------------------------------------------------------------------------------
  |  User -User Management
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-01
   */
   .factory('User',function(Log,Network,$q,$resource,locker){
     var factory  =   {};


     //Method to verify the phone number
     function phonenumber_verification(phoneNumber){
       var deferred  =   $q.defer();

       if(window.plugins !== undefined){
         if(window.plugins !== undefined){
           window.plugins.digits.logout();
         }
         window.plugins.digits.authenticate({
           'phoneNumber':phoneNumber
         }, function(data){
           Log.info('User:phonenumber_verification',data);
           deferred.resolve({
             'status':200,
             'error':'Phone verified successfully.'
           });
         }, function(error){
           deferred.resolve({
             'status':400,
             'error':'Failed to verify the phone number'
           });
         });
       } else {
         deferred.resolve({
           'status':200,
           'error':'Phone verified successfully.'
         });
       }
       return deferred.promise;
     }

     //User Resource
     function resource(){
       var url   =   Network.api_url+'/user/:user_id';

       return  $resource(url, {},{
           'get': { method:'GET',params:{'user_id':''}},
           'add': { method:'POST',params:{'user_id':''}},
           'show': { method:'GET',params:{}},
           'update': { method:'PUT',params:{}},
           'delete': { method:'DELETE',params:{}}
       });
     }

     function auth(){
       var url   =   Network.api_url+'/user/authenticate';

       return  $resource(url, {},{
           'post': { method:'POST'},
       });
     }


     //Method is used to check whether user logged in on app
     factory.is_logged_in   =   function(){
       var user   =   locker.namespace('hcncore').get('login_user_id',null);
       if(user == null){
         return false;
       }
       return true;
     }

     //Method is used to get the current logged in user
     factory.logged_in_user   =   function(){
       if(!factory.is_logged_in()){
         return null;
       }
       //Code needed to be written to get the currently logged in User
       //TODO
       var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
       return locker.namespace('hcncore_'+user_id).get('user',null);
     }

     //Method is used to check whether user logged in on app
     factory.logout   =   function(){
       var user   =   locker.namespace('hcncore').put('login_user_id',"");
       return true;
     }

     factory.set_user_weight   =  function(weight,unit){
       var user_id   =   locker.namespace('hcncore').get('login_user_id',null);

       locker.namespace('hcncore_'+user_id).put('last_user_weight',{
         'weight':weight,
         'weight_unit':unit
       });
     }

     factory.get_user_weight   =  function(weight,unit){
       var user_id   =   locker.namespace('hcncore').get('login_user_id',null);

       return locker.namespace('hcncore_'+user_id).get('last_user_weight',null);
     }

     factory.update_user_med_info   =   function(user_data){
       var deferred  =   $q.defer();
       var user    =  factory.logged_in_user();
       if(user == null){
         Log.error(['User:update_user_med_info','No current user logged in.']);
         deferred.resolve({
           'status':500,
           'message':'User not logged in'
         });
         return deferred.promise;
       }

       if(user_data.name ==null || user_data.name.length <= 0){
         deferred.reject({
           'status':400,
           'message':'Name is empty.'
         });
         return deferred.promise;
       }

      //  dob    =   data.dob;
       resource().update({
         'user_id':user.id
       },user_data,function(data){
         locker.namespace('hcncore_'+data.user.id).put('user',data.user);
         Log.info('User:update_user_med_info',data);
         deferred.resolve({
           'status':200,
           'message':'User updated successfully'
         });
       },function(error){
         Log.error('User:update_user_med_info',error);
         deferred.reject({
           'status':500,
           'message':'Something went wrong'
         });
       });

       return deferred.promise;
     }
     //Method is used to update the information of currently logged in user
     factory.update_user_info   =   function(user_data){
       var deferred  =   $q.defer();
       var user    =  factory.logged_in_user();
       if(user == null){
         Log.error(['User:update_user_info','No current user logged in.']);
         deferred.resolve({
           'status':500,
           'message':'User not logged in'
         });
         return deferred.promise;
       }

       if(user_data.name ==null || user_data.name.length <= 0){
         deferred.reject({
           'status':400,
           'message':'Name is empty.'
         });
         return deferred.promise;
       }

       if(user_data.dob ==null || user_data.dob.length <= 0){
         deferred.reject({
           'status':400,
           'message':'Dob is empty.'
         });
         return deferred.promise;
       }

       if(user_data.gender ==null || user_data.gender.length <= 0){
         deferred.reject({
           'status':400,
           'message':'gender is empty.'
         });
         return deferred.promise;
       }

       if(user_data.height ==null || user_data.height.length <= 0 ){
         deferred.reject({
           'status':400,
           'message':'height is empty.'
         });
         return deferred.promise;
       } else if(isNaN(user_data.height)){
         deferred.reject({
           'status':400,
           'message':'Height should be numeric.'
         });
         return deferred.promise;
       }  else if((user_data.height < 1 || user_data.height > 10) && user_data.height_unit == 'FEET'){
         deferred.reject({
           'status':400,
           'message':'Please enter a valid height.'
         });
         return deferred.promise;
       } else if((user_data.height < 0.30 || user_data.height > 3.0) && user_data.height_unit == 'METER'){
         deferred.reject({
           'status':400,
           'message':'Please enter a valid height.'
         });
         return deferred.promise;
       }

       if(user_data.weight ==null || user_data.weight.length <= 0){
         deferred.reject({
           'status':400,
           'message':'weight is empty.'
         });
         return deferred.promise;
       } else if((user_data.weight < 20 || user_data.weight > 200) && user_data.weight_unit == 'KG'){
         deferred.reject({
           'status':400,
           'message':'Please enter a valid weight.'
         });
         return deferred.promise;
       } else if((user_data.weight < 44 || user_data.weight > 445) && user_data.weight_unit == 'LBS'){
         deferred.reject({
           'status':400,
           'message':'Please enter a valid weight.'
         });
         return deferred.promise;
       }
       if(user_data.email != undefined && user_data.email != null && user_data.email.length >0){
         var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
         if(!re.test(user_data.email)){
           deferred.reject({
             'status':400,
             'message':'Invalid Email ID.'
           });
           return deferred.promise;
         }
       }

       user_data.dob   =   moment(user_data.dob,'Do MMM,YYYY').format('YYYY-MM-DD');
      //  dob    =   data.dob;
       resource().update({
         'user_id':user.id
       },user_data,function(data){

         locker.namespace('hcncore_'+data.user.id).put('user',data.user);

         factory.set_user_weight(user_data.weight,user_data.weight_unit);
         Log.info('User:update_user_info',data);
         Log.info('User:update_user_info',user_data.dob);
         user_data.dob   =   moment(user_data.dob,'YYYY-MM-DD').format('Do MMM,YYYY');
         Log.info('User:update_user_info',user_data.dob);
         deferred.resolve({
           'status':200,
           'message':'User updated successfully'
         });
       },function(error){
         user_data.dob   =   moment(user_data.dob,'YYYY-MM-DD').format('Do MMM,YYYY');
         Log.info('User:update_user_info',user_data.dob);
         Log.error('User:update_user_info',error);
         deferred.reject({
           'status':500,
           'message':'Something went wrong'
         });
       });

       return deferred.promise;
     }

     //Method is used to update the contact information of currently logged in user
     factory.update_user_other_info   =   function(user_data){
       var deferred  =   $q.defer();
       var user    =  factory.logged_in_user();
       if(user == null){
         Log.error(['User:update_user_other_info','No current user logged in.']);
         deferred.resolve({
           'status':500,
           'message':'User not logged in'
         });
         return deferred.promise;
       }

       resource().update({
         'user_id':user.id
       },user_data,function(data){

         locker.namespace('hcncore_'+data.user.id).put('user',data.user);

         Log.info('User:update_user_other_info',data);

         deferred.resolve({
           'status':200,
           'message':'User contact updated successfully'
         });
       },function(error){
         Log.error('User:update_user_other_info',error);
         deferred.reject({
           'status':500,
           'message':'Somevfthing went wrong'
         });
       });

       return deferred.promise;
     }


     //Method is used to authenticate the user
     factory.authenticate =   function(username,password,is_otp){
          var deferred  =   $q.defer();

          var _is_otp   = false;
          if(is_otp != undefined && is_otp == true){
            _is_otp   = true;
          }

          if(username.trim().length <= 0){
            deferred.reject({
              'status':400,
              'error':'Username should not be empty.'
            });
          }

          if(!is_otp && password.length <= 0){
            deferred.reject({
              'status':400,
              'error':'Password should not be empty.'
            });
          }

          if(!is_otp){
            auth().post({
              'phone_number':username,
              'password':password
            },function(data){
              locker.namespace('hcncore').put('login_user_id',data.user.id);
              locker.namespace('hcncore_'+data.user.id).put('user',data.user);
              deferred.resolve({
                'status':200,
                'message':'AUTHENTICATED',
                'user_data':data.user
              });
            },function(error){
              deferred.reject({
                'status':400,
                'message':'Invalid username/passsword'
              });
            });
          } else {
              phonenumber_verification(username).then(function(data){
                resource().show({
                  'user_id':username
                },function(data){
                  locker.namespace('hcncore').put('login_user_id',data.user.id);

                  locker.namespace('hcncore_'+data.user.id).put('user',data.user);

                  deferred.resolve({
                    'status':200,
                    'message':'AUTHENTICATED'
                  });
                },function(error){
                  deferred.reject({
                    'status':400,
                    'message':'User is not registered.'
                  });
                });
              },function(error){
                deferred.reject({
                  'status':400,
                  'message':'Failed to verify phonenumber.'
                });
              });
          }
          // deferred.resolve({
          //   'status':200,
          //   'message':'AUTHENTICATED'
          // });

          return deferred.promise;
     }

     //Method is used to register the user.
     factory.register =   function(username,password){
          var deferred  =   $q.defer();

          var _username = username+'';
          console.log(_username);
          if(_username.trim().length <= 0){
            deferred.reject({
              'status':400,
              'error':'Username should not be empty.'
            });
          }

          if(password.length <= 0){
            deferred.reject({
              'status':400,
              'error':'Password should not be empty.'
            });
          }

          //Checking user exists with phonenumber
          resource().show({
            'user_id':username
          },function(data){
            //User Exists
            Log.info('User:register',data);
            deferred.reject({
              'status':400,
              'message':'User is already registered.'
            });

          },function(error){
            //User does not exists
            Log.error('User:register',error);

            phonenumber_verification(username).then(function(data){
              Log.info('User:register',data);
              resource().add({
                'phone_number':username,
                'password':password
              },function(data){
                locker.namespace('hcncore').put('login_user_id',data.user.id);
                locker.namespace('hcncore_'+data.user.id).put('user',data.user);

                deferred.resolve({
                  'status':200,
                  'message':'Successfully.'
                });
              },function(error){
                deferred.reject({
                  'status':500,
                  'message':'Something went wrong try again latter.'
                });
              });
            },function(error){
              Log.error('User:register',error);
              deferred.reject({
                'status':400,
                'message':'Failed to verify phonenumber.'
              });
            });
          });
          return deferred.promise;
     }


     //Method is used to resync the user.
     factory.resync_logged_in_user  =   function(){
       var deferred    = $q.defer();
       var user   =   factory.logged_in_user();
       if(user == null){
         return;
       }
       username    = user.id;
       resource().show({
         'user_id':username
       },function(data){
         locker.namespace('hcncore_'+data.user.id).put('user',data.user);

         deferred.resolve({
           'status':200,
           'error':''
         });

       },function(error){
         deferred.reject({
           'status':500,
           'error':''
         });
       });

       return deferred.promise;
     }


     return factory;
   })

   .factory('UserProfile',function(Log,Network,$q,$resource,locker,User,$state){

     var factory  =   {};

     factory.user_id  =   null;

     function resource(){
       getLoggedinUser();

       var url   =   Network.api_url+'/user/'+factory.user_id+'/profile/:profile_id';

       return  $resource(url, {},{
           'get': { method:'GET',params:{'profile_id':''}},
           'add': { method:'POST',params:{'profile_id':''}},
           'show': { method:'GET',params:{}},
           'update': { method:'PUT',params:{}},
           'delete': { method:'DELETE',params:{}}
       });
     }


     function getLoggedinUser(){
       var user   =   User.logged_in_user();
       if(user == null){
         $state.go('credential');
         return;
       }
       console.log(user);
       factory.user_id   = user.id;
     }



     factory.get_profiles   =   function(){

     }

      factory.resync_active_profile  =   function(){
        var user   =   User.logged_in_user();
        if(user == null){
          return;
        }

        var profile = factory.getActiveProfile();
        if(profile == null){
          return;
        }

        var profile_id  =   profile.id;
        angular.forEach(user.profiles,function(p){
            if(p.id == profile_id){
              factory.setActiveProfile(p);
            }
        });
      }
     factory.setActiveProfile   =   function(profile){
       if(profile == null){
         locker.namespace('hcncore_'+factory.user_id).pull('active_profile',null);
       } else {
          locker.namespace('hcncore_'+factory.user_id).put('active_profile',profile);
       }
     }

     factory.getActiveProfile   =   function(){
       return locker.namespace('hcncore_'+factory.user_id).get('active_profile',null);
     }

     factory.add_profile  =   function(data){
       var deferred  =   $q.defer();
       if(data.name == undefined || data.name == null || data.name.length == 0){
         deferred.reject({
           'status':500,
           'message':'Something went wrong try again latter.'
         });
       }

       resource().add(data,function(data){
           Log.info('UserProfile:add_profile',data);
           User.resync_logged_in_user().then(function(){
             deferred.resolve({
               'status':200,
               'message':'Successfully.'
             });
           },function(){
             deferred.resolve({
               'status':200,
               'message':'Successfully.'
             });
           });


       },function(error){
         Log.error('UserProfile:add_profile',error);
         deferred.reject({
           'status':500,
           'message':'Something went wrong.'
         });
       });

       return deferred.promise;
     }

     factory.update_profile   =   function(profile_id,data){
       var deferred  =   $q.defer();
       if(data.name == undefined || data.name == null){
         deferred.reject({
           'status':500,
           'message':'Something went wrong try again latter.'
         });
       }

       resource().update({
         'profile_id':profile_id
       },data,function(data){
           Log.info('UserProfile:update_profile',data);
           User.resync_logged_in_user().then(function(){
             deferred.resolve({
               'status':200,
               'message':'Successfully.'
             });
           },function(){
             deferred.resolve({
               'status':200,
               'message':'Successfully.'
             });
           });
       },function(error){
         Log.error('UserProfile:update_profile',error);
         deferred.reject({
           'status':500,
           'message':'Something went wrong.'
         });
       });

       return deferred.promise;
     }

     factory.delete_profile   =   function(){

     }

     factory.set_user_weight   =  function(weight,unit){
       var profile   =   factory.getActiveProfile();
       if(profile == null){
         return;
       }
       locker.namespace('hcncore_'+profile.id).put('last_user_weight',{
         'weight':weight,
         'weight_unit':unit
       });
     }

     factory.get_user_weight   =  function(weight,unit){
       var profile   =   factory.getActiveProfile();
       if(profile == null){
         return;
       }
       return locker.namespace('hcncore_'+profile.id).get('last_user_weight',null);
     }


     //Method is used to update the information of currently logged in user
     factory.update_user_info   =   function(user_data){
       var deferred  =   $q.defer();
       var user    =  factory.getActiveProfile();
       if(user == null){
         Log.error(['UserProfile:update_user_info','No current active profile.']);
         deferred.resolve({
           'status':500,
           'message':'User not logged in'
         });
         return deferred.promise;
       }

       if(user_data.name ==null || user_data.name.length <= 0){
         deferred.reject({
           'status':400,
           'message':'Name is empty.'
         });
         return deferred.promise;
       }

       if(user_data.dob ==null || user_data.dob.length <= 0){
         deferred.reject({
           'status':400,
           'message':'Dob is empty.'
         });
         return deferred.promise;
       }

       if(user_data.gender ==null || user_data.gender.length <= 0){
         deferred.reject({
           'status':400,
           'message':'gender is empty.'
         });
         return deferred.promise;
       }

       if(user_data.height ==null || user_data.height.length <= 0 ){
         deferred.reject({
           'status':400,
           'message':'height is empty.'
         });
         return deferred.promise;
       } else if(isNaN(user_data.height)){
         deferred.reject({
           'status':400,
           'message':'Height should be numeric.'
         });
         return deferred.promise;
       }  else if((user_data.height < 1 || user_data.height > 10) && user_data.height_unit == 'FEET'){
         deferred.reject({
           'status':400,
           'message':'Please enter a valid height.'
         });
         return deferred.promise;
       } else if((user_data.height < 0.30 || user_data.height > 3.0) && user_data.height_unit == 'METER'){
         deferred.reject({
           'status':400,
           'message':'Please enter a valid height.'
         });
         return deferred.promise;
       }

       if(user_data.weight ==null || user_data.weight.length <= 0){
         deferred.reject({
           'status':400,
           'message':'weight is empty.'
         });
         return deferred.promise;
       } else if((user_data.weight < 20 || user_data.weight > 200) && user_data.weight_unit == 'KG'){
         deferred.reject({
           'status':400,
           'message':'Please enter a valid weight.'
         });
         return deferred.promise;
       } else if((user_data.weight < 44 || user_data.weight > 445) && user_data.weight_unit == 'LBS'){
         deferred.reject({
           'status':400,
           'message':'Please enter a valid weight.'
         });
         return deferred.promise;
       }
       if(user_data.email != undefined && user_data.email != null && user_data.email.length >0){
         var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
         if(!re.test(user_data.email)){
           deferred.reject({
             'status':400,
             'message':'Invalid Email ID.'
           });
           return deferred.promise;
         }
       }

       user_data.dob   =   moment(user_data.dob,'Do MMM,YYYY').format('YYYY-MM-DD');
      //  dob    =   data.dob;
       resource().update({
         'profile_id':user.id
       },user_data,function(data){
         factory.setActiveProfile(data.profile);
        //  locker.namespace('hcncore_'+data.user.id).put('user',data.user);

         factory.set_user_weight(user_data.weight,user_data.weight_unit);
         Log.info('UserProfile:update_user_info',data);
         user_data.dob   =   moment(user_data.dob,'YYYY-MM-DD').format('Do MMM,YYYY');
         deferred.resolve({
           'status':200,
           'message':'User updated successfully'
         });
       },function(error){
         user_data.dob   =   moment(user_data.dob,'YYYY-MM-DD').format('Do MMM,YYYY');
         Log.info('UserProfile:update_user_info',user_data.dob);
         Log.error('UserProfile:update_user_info',error);
         deferred.reject({
           'status':500,
           'message':'Something went wrong'
         });
       });

       return deferred.promise;
     }

     return factory;
   })
   /*
   |------------------------------------------------------------------------------
   |  Goal -Goal Management
   |------------------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */
  .factory('Goal',function(Log,Network,$q,$resource,locker,User,UserProfile){
      var factory   =   {};

      function resource(){
        var url   =   Network.api_url+'/goal/:goal_id';

        return  $resource(url, {},{
            'get': { method:'GET',params:{'goal_id':''}},
            'show': { method:'GET',params:{}},
        });
      }

      factory.fetch_user_goal   =   function(){
          // var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
          var profile   = UserProfile.getActiveProfile();
          if(profile == null){
            return [];
          }

          var choosen_goal    =   locker.namespace('hcncore_'+profile.id).get('goals',null);
          return choosen_goal;
      }

      factory.set_user_goal   =   function(goals){
          // var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
          var profile   = UserProfile.getActiveProfile();
          if(profile == null){
            return;
          }

          locker.namespace('hcncore_'+profile.id).put('goals',goals);

      }

      //fethcing of Goals
      factory.fetch_goals   =   function(){
        var deferred  =   $q.defer();
        // var user_id   =   locker.namespace('hcncore').get('login_user_id',null);

        var choosen_goal    =   factory.fetch_user_goal();

        resource().get(function(data){
          Log.info('Goal:fetch_goals',data);
          var goals   =   data.goals;
          angular.forEach(goals,function(goal){
            goal.is_checked   =   false;
            if(choosen_goal != null && choosen_goal.indexOf(goal.slug) >= 0){
              goal.is_checked   =   true;
            }
            if(goal.slug == 'diabetes_mgmt'){
              goal.icon   =  './img/diabetes_management.png';
            } else if(goal.slug == 'hypertension'){
              goal.icon   =  './img/hypertension.png';
            } else if(goal.slug == 'general_health_mgmt'){
              goal.icon   =  './img/generalHealthManagement.png';
            } else if(goal.slug == 'weight_mgmt'){
              goal.icon   =  './img/weightManagement.png';
            } else if(goal.slug == 'epilepsy_mgmt'){
              goal.icon   =  './img/epilepsyManagement.png';
            }
          });
          deferred.resolve({
            'status':200,
            'message':goals
          });
        },function(error){
          Log.error('Goal:fetch_goals',error);
          deferred.reject({
            'status':400,
            'message':'Goal not able to fetch.'
          });
        });
        return deferred.promise;
      }

      factory.set_goal  =   function(goals){
        var deferred  =   $q.defer();

        var user  =   User.logged_in_user();
        if(user == null){
          Log.info('Goal:set_goal','User is not logged in');
          deferred.reject({
            'status':400,
            'message':'User is not logged in'
          });
          return deferred.promise;
        }

        if(goals.length <= 0){
          deferred.reject({
            'status':400,
            'message':'Please choose goal'
          });
          return deferred.promise;
        }
        factory.set_user_goal(goals);

        deferred.resolve({
          'status':200,
          'message':'Goal updated'
        });

        return deferred.promise;
      }

      return factory;
  })

  /*
  |------------------------------------------------------------------------------
  |  Diabetes -Diabetes Management
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .factory('Diabetes',function(Log,Network,$q,$resource,locker,User,UserProfile){
      var factory   =   {};

      function diabetes_type(){
        var url   =   Network.api_url+'/diabetec_type';

        return  $resource(url, {},{
            'get': { method:'GET',params:{}},
        });
      }

      function therephy_type(){
        var url   =   Network.api_url+'/diabetec_therepy';

        return  $resource(url, {},{
            'get': { method:'GET',params:{}},
        });
      }

      function resource(){
        var url   =   Network.api_url+'/profile/:profile_id/goal/diabetes_mgmt';

        return  $resource(url, {},{
            'update': { method:'put',params:{}},
        });
      }

      function update_diabetes_goal(data){
        var deferred  =   $q.defer();
        var user  =   User.logged_in_user();
        if(user == null){
          deferred.reject({
            'staut':400,
            'message':'User not logged in'
          });
          return defer.promise;
        }

        var profile   =   UserProfile.getActiveProfile();
        if(profile == null){
          return null;
        }

        resource().update({
          'profile_id':profile.id
        },data,function(data){
          deferred.resolve({
            'status':200,
            'data':'Updated successfully'
          });
        },function(error){
          deferred.reject({
            'staut':400,
            'message':'Failed to update'
          });

        });
        return deferred.promise;
      }

      //fethcing of diabetes type
      factory.fetch_diabetes_type   =   function(){
        var deferred  =   $q.defer();

        var c_diabetes_type = factory.get_user_diabetes_type();
        // alert(c_diabetes_type);
        diabetes_type().get(function(data){
          Log.info('Diabetes:fetch_diabetes_type',data);
          var types   =   data.type;
          angular.forEach(types,function(type){
            type.is_checked   =   false;
            if(type.slug  == c_diabetes_type){
                type.is_checked   =   true;
            }
          });

          deferred.resolve({
            'status':200,
            'message':types
          });
        },function(error){
          Log.error('Diabetes:fetch_diabetes_type',error);
          deferred.reject({
            'status':400,
            'message':'Goal not able to fetch.'
          });
        });
        return deferred.promise;
      }

      //fethcing of Goals
      factory.fetch_therephy_type   =   function(){
        var deferred  =   $q.defer();

        var c_therephy_type = factory.get_user_diabetes_therephy_type();
        therephy_type().get(function(data){
          Log.info('Diabetes:therephy_type',data);
          var types   =   data.therepy;
          angular.forEach(types,function(type){
            type.is_checked   =   false;
            if(c_therephy_type != null && c_therephy_type.indexOf(type.id) >= 0){
              type.is_checked   =   true;
            }
          });

          deferred.resolve({
            'status':200,
            'message':types
          });
        },function(error){
          Log.error('Diabetes:therephy_type',error);
          deferred.reject({
            'status':400,
            'message':'Goal not able to fetch.'
          });
        });
        return deferred.promise;
      }

      factory.get_user_diabetes_type  =   function(){
        // var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
        var profile   =   UserProfile.getActiveProfile();
        if(profile == null){
          return null;
        }
          return locker.namespace('hcncore_'+profile.id).get('diabetes_type',null);
      }

      factory.update_diabeted_type  =   function(type){
        var profile   =   UserProfile.getActiveProfile();
        if(profile == null){
          return null;
        }
        locker.namespace('hcncore_'+profile.id).put('diabetes_type',type.slug);
        // console.log(type);
        return update_diabetes_goal({
          'diabetec_type':parseInt(type.id)
        });
      }

      factory.update_target_range   =   function(unit,start,end){
        var data  =   {
          'range_unit':unit,
          'start_range':start,
          'end_range':end
        };
        var profile   =   UserProfile.getActiveProfile();
        if(profile == null){
          return null;
        }
        locker.namespace('hcncore_'+profile.id).put('target_range',data);
        return update_diabetes_goal(data);
      }

      factory.get_target_range   =   function(){
        var profile   =   UserProfile.getActiveProfile();
        if(profile == null){
          return null;
        }
        return locker.namespace('hcncore_'+profile.id).get('target_range',null);
      }

      factory.get_user_diabetes_therephy_type  =   function(){
        var profile   =   UserProfile.getActiveProfile();
        if(profile == null){
          return null;
        }

        // var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
          return locker.namespace('hcncore_'+profile.id).get('diabetes_therephy_type',null);
      }

      factory.update_therephy_type  =   function(types){
        var profile   =   UserProfile.getActiveProfile();
        if(profile == null){
          return null;
        }
        locker.namespace('hcncore_'+profile.id).put('diabetes_therephy_type',types);
        return update_diabetes_goal({
          'therepy_types':types.join(',')
        });
      }


      return factory;
  })

  .factory('Hypertenstion',function(Log,Network,$q,$resource,locker,User,UserProfile){

      var factory = {};
      function resource(){
        var url   =   Network.api_url+'/profile/:profile_id/goal/hypertension';

        return  $resource(url, {},{
            'update': { method:'put',params:{}},
        });
      }

      factory.update_hypertension = function(data){
        var deferred  =   $q.defer();
        var user  =   User.logged_in_user();
        if(user == null){
          deferred.reject({
            'staut':400,
            'message':'User not logged in'
          });
          return deferred.promise;
        }

        if(data.systolic.length <= 0){
          deferred.reject({
            'staut':400,
            'message':'Please type systolic'
          });
          return deferred.promise;
        }

        if(data.diastolic.length <= 0){
          deferred.reject({
            'staut':400,
            'message':'Please type diastolic'
          });
          return deferred.promise;
        }

        // if(data.pulse.length <= 0){
        //   deferred.reject({
        //     'staut':400,
        //     'message':'Please type pulse'
        //   });
        //   return deferred.promise;
        // }

        data.weight   =   user.weight;
        data.weight_unit   =   user.weight_unit;

        var profile   =   UserProfile.getActiveProfile();
        if(profile == null){
          return null;
        }

        resource().update({
          'profile_id':profile.id
        },data,function(data){
          deferred.resolve({
            'status':200,
            'data':'Updated successfully'
          });
        },function(error){
          deferred.reject({
            'staut':400,
            'message':'Failed to update'
          });
        });
        return deferred.promise;
      }

      return factory;
  })

  /*
  |------------------------------------------------------------------------------
  |  EmergencyContact -Contact Management
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .factory('EmergencyContact',function(Log,Network,$q,$resource,locker,User){
    var factory   = {};

    function resource(){

      var user  =   User.logged_in_user();
      if(user == null){
        return null;
      }

      var url   =   Network.api_url+'/user/'+user.id+'/emergency_contact/:contact_id';

      return  $resource(url, {},{
          'get': { method:'GET',params:{'contact_id':''}},
          'add': { method:'POST',params:{'contact_id':''}},
          'show': { method:'GET',params:{}},
          'update': { method:'PUT',params:{}},
          'delete': { method:'DELETE',params:{}}
      });
    }
    factory.get_contacts_cached = function() {
      var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
      return  locker.namespace('hcncore_'+user_id).get('emergencycontact',null);
    }

    factory.get_contacts   =   function(){
      var deferred  =   $q.defer();
      var user  =   User.logged_in_user();
      if(user == null){
        deferred.reject({
          'staut':400,
          'message':'User not logged in'
        });
        return deferred.promise;
      }

      resource().get({},function(data){
        Log.info('EmergencyContact:get_contacts',data);
        angular.forEach(data.emergency_contacts,function(contact){
          contact.is_primary  =   parseInt(contact.is_primary);
          contact.is_primary = contact.is_primary==0?false:true;
        });
        var user_id   =   locker.namespace('hcncore').get('login_user_id',null);
         locker.namespace('hcncore_'+user_id).put('emergencycontact',data);
        deferred.resolve(data);
      },function(error){
        Log.info('EmergencyContact:get_contacts',error);
        deferred.reject(error);
      });
      return deferred.promise;
    }

    factory.add_contact   =   function(data){
      var deferred  =   $q.defer();
      var user  =   User.logged_in_user();
      if(user == null){
        deferred.reject({
          'staut':400,
          'message':'User not logged in'
        });
        return deferred.promise;
      }

      if(data.name == null || data.name.length <= 0){
        deferred.reject({
          'staut':400,
          'message':'Please choose contact'
        });
        return deferred.promise;
      }

      if(data.phone_number == null || data.phone_number.length <= 0){
        deferred.reject({
          'staut':400,
          'message':'Please choose contact'
        });
        return deferred.promise;
      }

      if(data.type == null || data.type.length <= 0){
        deferred.reject({
          'staut':400,
          'message':'Please relation'
        });
        return deferred.promise;
      }

      data.is_primary  = data.is_primary?"TRUE":"FALSE";

      resource().add(data,function(data){
        Log.info('EmergencyContact:add_contact',data);
        deferred.resolve(data);
      },function(error){
        Log.info('EmergencyContact:add_contact',error);
        deferred.reject(error);
      });
      return deferred.promise;
    }

    factory.delete_contact  =   function(data){

      var deferred  =   $q.defer();
      var user  =   User.logged_in_user();
      if(user == null){
        deferred.reject({
          'staut':400,
          'message':'User not logged in'
        });
        return deferred.promise;
      }

      resource().delete({
        'contact_id':data.id
      },function(data){
        Log.info('EmergencyContact:delete_contact',data);
        deferred.resolve(data);
      },function(error){
        Log.info('EmergencyContact:delete_contact',error);
        deferred.reject(error);
      });
      return deferred.promise;
    }

    factory.update_contact  = function(data){
      var deferred  =   $q.defer();
      var user  =   User.logged_in_user();
      if(user == null){
        deferred.reject({
          'staut':400,
          'message':'User not logged in'
        });
        return deferred.promise;
      }

      if(data.name == null || data.name.length <= 0){
        deferred.reject({
          'staut':400,
          'message':'Please choose contact'
        });
        return deferred.promise;
      }

      if(data.phone_number == null || data.phone_number.length <= 0){
        deferred.reject({
          'staut':400,
          'message':'Please choose contact'
        });
        return deferred.promise;
      }

      if(data.type == null || data.type.length <= 0){
        deferred.reject({
          'staut':400,
          'message':'Please relation'
        });
        return deferred.promise;
      }

      data.is_primary  = data.is_primary?"TRUE":"FALSE";

      resource().update({
        'contact_id':data.id
      },data,function(data){
        Log.info('EmergencyContact:update_contact',data);
        deferred.resolve(data);
      },function(error){
        Log.info('EmergencyContact:update_contact',error);
        deferred.reject(error);
      });
      return deferred.promise;
    }
    return factory;
  })

  /*
  |------------------------------------------------------------------------------
  |  LogInfo - Service to Log the Info from app
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .factory('LogInfo',function(WeightLog,A1cLog,BpLog,MedicineLog,ActivityLog,FoodLog,BGLLog,SymptomsLog){
    var factory   =   {};

    factory.weight_log  =   function(){
        return WeightLog;
    }

    factory.a1c_log   =   function(){
      return A1cLog;
    }

    factory.bp_log  =   function(){
      return BpLog;
    }

    factory.medicine_log  =   function(){
      return MedicineLog;
    }

    factory.activity_log  =   function(){
      return ActivityLog;
    }

    factory.food_log  =   function(){
      return FoodLog;
    }

    factory.bgl_log   =   function(){
      return BGLLog;
    }
    factory.symptoms_log  =   function(){
      return SymptomsLog;
    }

    return factory;
  })

  /*
  |------------------------------------------------------------------------------
  |  WeightLog - Service to Log the Info from weight
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .factory('WeightLog',function(Log,Network,$q,$resource,locker,User,UserProfile){
    var factory   =   {};

    function resource(){


      var url   =   Network.api_url+'/log/weight';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
          'add': { method:'POST',params:{}},
          'show': { method:'GET',params:{}},
          'update': { method:'PUT',params:{}},
          'delete': { method:'DELETE',params:{}}
      });
    }

    factory.commit  =   function(data){

      var deferred  =   $q.defer();

      var user  =   User.logged_in_user();
      if(user == null){
        return null;
      }

      var profile  =   UserProfile.getActiveProfile();
      if(profile ==  null){
        return null;
      }

      data.profile_id   =  profile.id;


      if(data.weight_unit == null || data.weight_unit.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please pass weight unit'
        });
        return deferred.promise;
      }

      if(data.weight == null || data.weight.length <= 0 || isNaN(data.weight)){
        deferred.reject({
          'staut':400,
          'message':'Please enter weight'
        });
        return deferred.promise;
      }

      if(data.log_date == null || data.log_date.length <= 0){
        deferred.reject({
          'staut':400,
          'message':'Please enter date'
        });
        return deferred.promise;
      }

      if(data.log_time == null || data.log_time.length <= 0){
        deferred.reject({
          'staut':400,
          'message':'Please enter time'
        });
        return deferred.promise;
      }

      // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
      var user_data   =   data;
      resource().add(data,function(data){
        Log.info('WeightLog:commit',data);
        UserProfile.set_user_weight(user_data.weight,user_data.weight_unit);
        deferred.resolve(data);
      },function(data){
          Log.error('WeightLog:commit',data);
          deferred.reject(error);
      });
      return deferred.promise;
    }
    return factory;
  })

  /*
  |------------------------------------------------------------------------------
  |  A1cLog - Service to Log the Info from a1c
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .factory('A1cLog',function(Log,Network,$q,$resource,locker,User,UserProfile){
    var factory   =   {};

    function resource(){


      var url   =   Network.api_url+'/log/a1c';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
          'add': { method:'POST',params:{}},
          'show': { method:'GET',params:{}},
          'update': { method:'PUT',params:{}},
          'delete': { method:'DELETE',params:{}}
      });
    }

    factory.commit  =   function(data){

      var deferred  =   $q.defer();

      var user  =   User.logged_in_user();
      if(user == null){
        return null;
      }

      var profile  =   UserProfile.getActiveProfile();
      if(profile ==  null){
        return null;
      }

      data.profile_id   =  profile.id;


      if(data.a1c == null || data.a1c.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please enter a1c'
        });
        return deferred.promise;
      }
      data.a1c   =  parseFloat(data.a1c);
      if(data.a1c > 20){
        deferred.reject({
          'staut':400,
          'message':'A1c should be less than 20'
        });
        return deferred.promise;
      }

      // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
      var user_data   =   data;
      resource().add(data,function(data){
        Log.info('A1cLog:commit',data);
        deferred.resolve(data);
      },function(data){
          Log.error('A1cLog:commit',data);
          deferred.reject(error);
      });
      return deferred.promise;
    }
    return factory;
  })

  /*
  |------------------------------------------------------------------------------
  |  BpLog - Service to Log the Info from a1c
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .factory('BpLog',function(Log,Network,$q,$resource,locker,User,UserProfile){
    var factory   =   {};

    function resource(){


      var url   =   Network.api_url+'/log/bp';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
          'add': { method:'POST',params:{}},
          'show': { method:'GET',params:{}},
          'update': { method:'PUT',params:{}},
          'delete': { method:'DELETE',params:{}}
      });
    }

    factory.commit  =   function(data){

      var deferred  =   $q.defer();

      var user  =   User.logged_in_user();
      if(user == null){
        return null;
      }

      var profile  =   UserProfile.getActiveProfile();
      if(profile ==  null){
        return null;
      }

      data.profile_id   =  profile.id;


      if(data.systolic == null || data.systolic.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please enter systolic'
        });
        return deferred.promise;
      }

      if(data.diastolic == null || data.diastolic.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please enter diastolic'
        });
        return deferred.promise;
      }

      // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
      var user_data   =   data;
      resource().add(data,function(data){
        Log.info('BpLog:commit',data);
        deferred.resolve(data);
      },function(data){
          Log.error('BpLog:commit',data);
          deferred.reject(error);
      });
      return deferred.promise;
    }
    return factory;
  })


  /*
  |------------------------------------------------------------------------------
  |  MedicineLog - Service to Log the Info from a1c
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .factory('MedicineLog',function(Log,Network,$q,$resource,locker,User,UserProfile){
    var factory   =   {};

    function resource(){


      var url   =   Network.api_url+'/log/medicine';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
          'add': { method:'POST',params:{}},
          'show': { method:'GET',params:{}},
          'update': { method:'PUT',params:{}},
          'delete': { method:'DELETE',params:{}}
      });
    }

    factory.commit  =   function(data){

      var deferred  =   $q.defer();

      var user  =   User.logged_in_user();
      if(user == null){
        return null;
      }

      var profile  =   UserProfile.getActiveProfile();
      if(profile ==  null){
        return null;
      }

      data.profile_id   =  profile.id;


      if(data.medicine_name == null || data.medicine_name.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please enter medicine_name'
        });
        return deferred.promise;
      }

      if(data.doss_type == null || data.doss_type.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please select doss type'
        });
        return deferred.promise;
      }

      if(data.doss == null || data.doss.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please select doss'
        });
        return deferred.promise;
      }

      // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
      var user_data   =   data;
      resource().add(data,function(data){
        Log.info('MedicineLog:commit',data);
        deferred.resolve(data);
      },function(data){
          Log.error('MedicineLog:commit',data);
          deferred.reject(error);
      });
      return deferred.promise;
    }
    return factory;
  })

  /*
  |------------------------------------------------------------------------------
  |  ActivityLog - Service to Log the Info from activity
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .factory('ActivityLog',function(Log,Network,$q,$resource,locker,User,UserProfile){
    var factory   =   {};

    function resource(){


      var url   =   Network.api_url+'/log/activity';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
          'add': { method:'POST',params:{}},
          'show': { method:'GET',params:{}},
          'update': { method:'PUT',params:{}},
          'delete': { method:'DELETE',params:{}}
      });
    }

    function activity(){


      var url   =   Network.api_url+'/activities';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
      });
    }

    factory.search  =   function(search_query){
        var deferred  =   $q.defer();

        if(search_query == null || search_query.length <= 2){
          deferred.resolve({
            'staut':200,
            'message':[]
          });
        } else {
              activity().get({
                'sq':search_query
              },function(data){
                Log.info('ActivityLog:search',data);
                deferred.resolve({
                  'staut':200,
                  'message':data.data
                });
              });
        }

        return deferred.promise;
    }

    factory.commit  =   function(data){

      var deferred  =   $q.defer();

      var user  =   User.logged_in_user();
      if(user == null){
        return null;
      }

      var profile  =   UserProfile.getActiveProfile();
      if(profile ==  null){
        return null;
      }

      data.profile_id   =  profile.id;

      if(data.activity_id == null || data.activity_id.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please choose activity'
        });
        return deferred.promise;
      }

      if(data.duration == null || data.duration.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please enter duration'
        });
        return deferred.promise;
      }

      // if(data.doss == null || data.doss.length <= 0 ){
      //   deferred.reject({
      //     'staut':400,
      //     'message':'Please select doss'
      //   });
      //   return deferred.promise;
      // }

      // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
      // data.duration   =   data.duration*60;
      var user_data   =   data;
      console.log(data);
      resource().add({
        'activity_id':data.activity_id,
        'profile_id':data.profile_id,
        'duration':data.duration*60,
      },function(data){
          Log.info('ActivityLog:commit',data);
          deferred.resolve(data);
      },function(data){
          Log.error('ActivityLog:commit',data);
          deferred.reject(error);
      });
      return deferred.promise;
    }
    return factory;
  })

  /*
  |------------------------------------------------------------------------------
  |  FoodLog - Service to Log the Info from food
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .factory('FoodLog',function(Log,Network,$q,$resource,locker,User,UserProfile){
    var factory   =   {};

    function food_search(){
      var url   =   Network.api_url+'/food/:food_id';

      return  $resource(url, {},{
          'get': { method:'GET',params:{'food_id':''}},
          'show': { method:'GET',params:{}},
      });
    }

    function resource(){
      var url   =   Network.api_url+'/log/food';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
          'add': { method:'POST',params:{}},
          'show': { method:'GET',params:{}},
          'update': { method:'PUT',params:{}},
          'delete': { method:'DELETE',params:{}}
      });
    }

    factory.food  =   function(food_id){
      var deferred  =   $q.defer();

      if(food_id == null || food_id.length <= 2){
        deferred.resolve({
          'staut':200,
          'message':'Food is not selected'
        });
      } else {
            food_search().show({
              'food_id':food_id
            },function(data){
              Log.info('FoodLog:food',data);
              deferred.resolve({
                'staut':200,
                'message':data.food
              });

            });
      }

      return deferred.promise;
    }

    factory.search  =   function(search_query){
        var deferred  =   $q.defer();

        if(search_query == null || search_query.length <= 2){
          deferred.resolve({
            'staut':200,
            'message':[]
          });
        } else {
              food_search().get({
                'sq':search_query
              },function(data){
                Log.info('ActivityLog:search',data.foods);
                if(data.foods != null && data.foods.food != undefined){
                  deferred.resolve({
                    'staut':200,
                    'message':data.foods.food
                  });
                } else {
                  deferred.resolve({
                    'staut':200,
                    'message':[]
                  });
                }
              });
        }

        return deferred.promise;
    }

    factory.commit  =   function(data){

      var deferred  =   $q.defer();

      var user  =   User.logged_in_user();
      if(user == null){
        return null;
      }

      var profile  =   UserProfile.getActiveProfile();
      if(profile ==  null){
        return null;
      }

      data.profile_id   =  profile.id;


      // if(data.activity_id == null || data.activity_id.length <= 0 ){
      //   deferred.reject({
      //     'staut':400,
      //     'message':'Please choose activity'
      //   });
      //   return deferred.promise;
      // }

      if(data.meal_name == null || data.meal_name.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please enter meal name'
        });
        return deferred.promise;
      }

      // if(data.doss == null || data.doss.length <= 0 ){
      //   deferred.reject({
      //     'staut':400,
      //     'message':'Please select doss'
      //   });
      //   return deferred.promise;
      // }

      // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
      var user_data   =   data;
      resource().add(data,function(data){
          Log.info('FoodLog:commit',data);
          deferred.resolve(data);
      },function(data){
          Log.error('FoodLog:commit',data);
          deferred.reject(error);
      });
      return deferred.promise;
    }
    return factory;
  })
  /*
  |------------------------------------------------------------------------------
  |  BGLLog - Service to Log the Info from food
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .factory('BGLLog',function(Log,Network,$q,$resource,locker,User,UserProfile){
    var factory   =   {};

    function resource(){
      var url   =   Network.api_url+'/log/blood_glucose';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
          'add': { method:'POST',params:{}},
          'show': { method:'GET',params:{}},
          'update': { method:'PUT',params:{}},
          'delete': { method:'DELETE',params:{}}
      });
    }

    function events(){
      var url   =   Network.api_url+'/day_event';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
      });
    }

    factory.event  =   function(){
        var deferred  =   $q.defer();
              events().get({
              },function(data){
                Log.info('BGLLog:event',data.events);
                  deferred.resolve({
                    'staut':200,
                    'message':data.events
                  });
                // if(data.foods != null && data.foods.food != undefined){
                //   deferred.resolve({
                //     'staut':200,
                //     'message':data.foods.food
                //   });
                // } else {
                //   deferred.resolve({
                //     'staut':200,
                //     'message':[]
                //   });
                // }
              });

        return deferred.promise;
    }


    factory.commit  =   function(data){

      var deferred  =   $q.defer();

      var user  =   User.logged_in_user();
      if(user == null){
        return null;
      }

      var profile  =   UserProfile.getActiveProfile();
      if(profile ==  null){
        return null;
      }

      data.profile_id   =  profile.id;


      // if(data.activity_id == null || data.activity_id.length <= 0 ){
      //   deferred.reject({
      //     'staut':400,
      //     'message':'Please choose activity'
      //   });
      //   return deferred.promise;
      // }
      if(data.blood_glucose_level == null || data.blood_glucose_level.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please enter blood glucose level'
        });
        return deferred.promise;
      }
      var bgl_val   =   parseFloat(data.blood_glucose_level);
      if(data.unit  ==  'mg_dl'){
        if(bgl_val > 999 || bgl_val < 0){
          deferred.reject({
            'staut':400,
            'message':'Glucose level should be less than 999'
          });
          return deferred.promise;
        }
      } else {
        if(bgl_val > 99 || bgl_val < 0){
          deferred.reject({
            'staut':400,
            'message':'Glucose level should be less than 99'
          });
          return deferred.promise;
        }
      }

      if(data.event_id == null || data.event_id.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please enter event'
        });
        return deferred.promise;
      }

      var user_data   =   data;
      resource().add(data,function(data){
          Log.info('BGLLog:commit',data);
          deferred.resolve(data);
      },function(data){
          Log.error('BGLLog:commit',data);
          deferred.reject(error);
      });
      return deferred.promise;
    }
    return factory;
  })

  /*
  |------------------------------------------------------------------------------
  |  SymptomsLog - Service to Log the Info from Symptoms
  |------------------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .factory('SymptomsLog',function(Log,Network,$q,$resource,locker,User,UserProfile){
    var factory   =   {};

    function resource(){
      var url   =   Network.api_url+'/log/symptom_log';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
          'add': { method:'POST',params:{}},
          'show': { method:'GET',params:{}},
          'update': { method:'PUT',params:{}},
          'delete': { method:'DELETE',params:{}}
      });
    }

    function symptom(){
      var url   =   Network.api_url+'/symptom';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
      });
    }

    function locations(){
      var url   =   Network.api_url+'/symptom_location';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
      });
    }

    function nature_pain(){
      var url   =   Network.api_url+'/symptom_pain_nature';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
      });
    }

    function provoking_factors(){
      var url   =   Network.api_url+'/symptom_provoking';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
      });
    }

    function releving_factors(){
      var url   =   Network.api_url+'/symptom_relieving';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
      });
    }

    function enviorments(){
      var url   =   Network.api_url+'/enviorment';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
      });
    }

    factory.symptoms_location   = function(){
      var deferred  =   $q.defer();

            locations().get({
            },function(data){
              Log.info('SymptomsLog:symptoms_location',data.symptom_locations);
                deferred.resolve({
                  'staut':200,
                  'message':data.symptom_locations
                });
            });
      return deferred.promise;
    }

    factory.symptoms   = function(){
      var deferred  =   $q.defer();

            symptom().get({
            },function(data){
              Log.info('SymptomsLog:symptom',data.symptoms);
                deferred.resolve({
                  'staut':200,
                  'message':data.symptoms
                });
            });
      return deferred.promise;
    }

    factory.nature_pain   = function(){
      var deferred  =   $q.defer();

            nature_pain().get({
            },function(data){
              Log.info('SymptomsLog:nature_pain',data.symptom_pain_natures);
                deferred.resolve({
                  'staut':200,
                  'message':data.symptom_pain_natures
                });
            });
      return deferred.promise;
    }

    factory.provoking_factors   = function(){
      var deferred  =   $q.defer();

            provoking_factors().get({
            },function(data){
              Log.info('SymptomsLog:provoking_factors',data.symptom_provokings);
                deferred.resolve({
                  'staut':200,
                  'message':data.symptom_provokings
                });
            });
      return deferred.promise;
    }


    factory.releving_factors   = function(){
      var deferred  =   $q.defer();

            releving_factors().get({
            },function(data){
              Log.info('SymptomsLog:releving_factors',data.symptom_relieving);
                deferred.resolve({
                  'staut':200,
                  'message':data.symptom_relieving
                });
            });
      return deferred.promise;
    }

    factory.enviorment   = function(){
      var deferred  =   $q.defer();

            enviorments().get({
            },function(data){
              Log.info('SymptomsLog:enviorment',data.enviorments);
                deferred.resolve({
                  'staut':200,
                  'message':data.enviorments
                });
            });
      return deferred.promise;
    }


    factory.symptoms_search  =   function(search_query){
        var deferred  =   $q.defer();
            if(search_query.length <= 2){
              deferred.resolve({
                'staut':200,
                'message':[]
              });
            } else {
              symptom().get({
                'sq':search_query
              },function(data){

                Log.info('SymptomsLog:event',data.symptoms);
                  deferred.resolve({
                    'staut':200,
                    'message':data.symptoms
                  });
              });
            }
        return deferred.promise;
    }


    factory.commit  =   function(data){

      var deferred  =   $q.defer();

      var user  =   User.logged_in_user();
      if(user == null){
        return null;
      }

      var profile  =   UserProfile.getActiveProfile();
      if(profile ==  null){
        return null;
      }

      data.profile_id   =  profile.id;


      // if(data.activity_id == null || data.activity_id.length <= 0 ){
      //   deferred.reject({
      //     'staut':400,
      //     'message':'Please choose activity'
      //   });
      //   return deferred.promise;
      // }
      if(data.symptom_id == null || data.symptom_id.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please select symptom'
        });
        return deferred.promise;
      }


      if(data.severity == null || data.severity.length <= 0 ){
        deferred.reject({
          'staut':400,
          'message':'Please select severity'
        });
        return deferred.promise;
      }

      if(data.location_ids != null){
          data.location_ids   =   data.location_ids.join(',');
      }

      if(data.pain_nature_ids != null){
        data.pain_nature_ids   =   data.pain_nature_ids.join(',');
      }

      if(data.provoking_ids != null){
        data.provoking_ids   =   data.provoking_ids.join(',');
      }

      if(data.related_symptom_ids != null){
        data.related_symptom_ids   =   data.related_symptom_ids.join(',');
      }

      if(data.relieving_ids != null){
        data.relieving_ids   =   data.relieving_ids.join(',');
      }

      var user_data   =   data;
      resource().add(data,function(data){
          Log.info('SymptomsLog:commit',data);
          deferred.resolve(data);
      },function(data){
          Log.error('SymptomsLog:commit',data);
          deferred.reject(error);
      });
      return deferred.promise;
    }
    return factory;
  })
  /*
   |  Method is used to get the analytics for the user
   |
   |
   */
   .factory('UserAnalytics',function(Network,$q,$resource,locker,User,Log,$timeout,UserProfile){
       var factory   =   {};

       function resource(){

         var user  =   User.logged_in_user();
         if(user == null){
           return null;
         }

         var profile  =   UserProfile.getActiveProfile();
         if(profile ==  null){
           return null;
         }
         var url   =   Network.api_url+'/profile/'+profile.id+'/analytics';

         return  $resource(url, {},{
             'get': { method:'GET',params:{}},
         });
       }

       factory.get  =   function(){
         var deferred  =   $q.defer();

         var user  =   User.logged_in_user();
         if(user == null){
            deferred.reject(null);
            return deferred.promise;
         }

         var profile  =   UserProfile.getActiveProfile();
         if(profile ==  null){
           return null;
         }

         var user_id  =   profile.id;

         var analytics = locker.namespace('hcncore_'+profile.id).get('analytics',null);
         if(analytics != null){
           $timeout(function(){
             Log.info('UserAnalytics:get notify',analytics);
             deferred.notify(analytics);
           },1);
         }
         $timeout(function(){
           resource().get({},function(data){
               Log.info('UserAnalytics:get',data);
               locker.namespace('hcncore_'+user_id).put('analytics',data);
               if(profile.is_init_flow == 0){
                 User.resync_logged_in_user();
                 UserProfile.resync_active_profile();
               }
               deferred.resolve(data);
           },function(data){
               Log.error('UserAnalytics:get',data);
               deferred.reject(data);
           });
         },500);

         return deferred.promise;
       }

       return factory;
   })

   /*
    | Factory to manage the Reports for User
    |
    |
    */
    .factory('UserReports',function(Network,$q,$resource,locker,User,Log,$timeout,FileOp,UserProfile){

      var factory   =   {};

      function resource(){

        var user  =   User.logged_in_user();
        if(user == null){
          return null;
        }

        var profile  =   UserProfile.getActiveProfile();
        if(profile ==  null){
          return null;
        }

        var url   =   Network.api_url+'/profile/'+profile.id+'/report';

        return  $resource(url, {},{
            'get': { method:'POST',params:{}},
        });
      }

      factory.generate_report  =   function(data){
        var deferred  =   $q.defer();

        var user  =   User.logged_in_user();
        if(user == null){
           deferred.reject(null);
           return deferred.promise;
        }
        var user_id  =   user.id;

        var profile  =   UserProfile.getActiveProfile();
        if(profile ==  null){
          return null;
        }


        resource().get(data,function(data){
            Log.info('UserReports:generate_report',data);

            // FileOp.createDir('hcncore');
            FileOp.checkDir('reports').then(function(){

            },function(){
              FileOp.createDir('reports').then(function(data){
                console.log(data);
              },function(data){
                console.log(data);
              });
            });

            FileOp.download_file(data.report.path,'reports/HCNCORE_'+moment().format("YYYY-MM-DD_HHmmss")+'.pdf').then(function(data){
              var _reports = locker.namespace('hcncore_'+profile.id).get('rerports',[]);
              _reports.push(data);
              locker.namespace('hcncore_'+profile.id).put('rerports',_reports);
              deferred.resolve(data);
            },function(error){
              deferred.reject(error);
            },function(progress){
              deferred.notify(progress);
            });
        },function(data){
            Log.error('UserReports:generate_report',data);
            deferred.reject(data);
        });

        return deferred.promise;
      }

      factory.get_archive_reports    = function(){
        var user  =   User.logged_in_user();
        if(user == null){
           deferred.reject(null);
           return deferred.promise;
        }

        var profile  =   UserProfile.getActiveProfile();
        if(profile ==  null){
          return null;
        }


        var user_id  =   user.id;
        console.log(locker.namespace('hcncore_'+profile.id).get('rerports',[]));
        return locker.namespace('hcncore_'+profile.id).get('rerports',[]);
      }

      factory.delete_report   =   function(i){

        var profile  =   UserProfile.getActiveProfile();
        if(profile ==  null){
          return null;
        }

        var reports   =   locker.namespace('hcncore_'+profile.id).get('rerports',[]);

        reports.splice(i,1);
        cosole.log(reports);
        locker.namespace('hcncore_'+profile.id).put('rerports',reports);
      }
      return factory;
    })

    .factory('PhoneContacts',function($log,$q,$ionicPlatform,$cordovaContacts,$cordovaSQLite){
      var factory   =   {};



      factory.invalidatePhoneContact  =   function(){
        //Funcation is used to fetch the contact number from decie and strore it to sqllite
        var defer     =   $q.defer();
        $ionicPlatform.ready(function(){
            var db = $cordovaSQLite.openDB({ name: "hcncare_manual.db", location: 'default' });
            var query   =   'CREATE TABLE IF NOT EXISTS hcncontact_table (contact_num, name,image,is_medsocial)';
            $cordovaSQLite.execute(db, query, []).then(function(res) {
            }, function (err) {
            });

            $cordovaContacts.find({}).then(function(allContacts) { //omitting parameter to .find() causes all contacts to be returned
              var contact_num_array   =   [];
              angular.forEach(allContacts,function(contact){
                if(contact.phoneNumbers != null && contact.phoneNumbers.length > 0){
                    var displayName    = contact.displayName;
                    var photo   =   null;
                    if(contact.photos != null){
                      photo   =   '';
                    }
                    // console.log(photo);
                    // console.log(contact);
                    angular.forEach(contact.phoneNumbers,function(phoneNumber){
                      var phone_number  =   phoneNumber.value;
                      phone_number = phone_number.replace(/\s/g, '');

                      contact_num_array.push(phone_number);
                      var query   =   "Select * from hcncontact_table where contact_num=?";
                      $cordovaSQLite.execute(db, query, [phone_number]).then(function(res) {
                        if(res.rows.length > 0){
                          var up_query  =   "Update hcncontact_table set name=?,image=? where contact_num=?";

                          $cordovaSQLite.execute(db, up_query, [displayName,photo,phone_number]).then(function(res) {
                            // console.log(res);
                          },function(err){
                            // console.log(error);
                          });
                        } else {
                          var query   = 'INSERT INTO hcncontact_table VALUES (?,?,?,?)';
                          $cordovaSQLite.execute(db, query, [phone_number,displayName,photo,'NO']).then(function(res) {
                          }, function (err) {
                          });
                        }
                      }, function (err) {
                        console.log(err);
                      });
                    });
                    factory.invalidateMedsocial(contact_num_array);
                }
              });
            });
        });

        return defer.promise
      }

      factory.invalidateMedsocial   =   function(phoneNumbers){
        //Funcation is used to check whether phone numbers are medsocial avaible or not based on that it will set the flog
      }

      factory.searchContact   = function(searc_query){
        //Will search the data from sqlite and return the contact
        var defer     =   $q.defer();
        var db = $cordovaSQLite.openDB({ name: "hcncare_manual.db", location: 'default' });
        var query   =   '';
        if(searc_query.trim().length > 0){
            query   =   "Select * from hcncontact_table where name like '%"+searc_query.trim()+"%' order by name asc";
        } else{
          query   =   "Select * from hcncontact_table order by name asc";
        }

        $cordovaSQLite.execute(db, query, []).then(function(res) {
          var details   =   [];
          for(var i=0;i<res.rows.length;i++){
            details.push(res.rows.item(i));
          }
          defer.resolve(details);
        },function(des){
          defer.reject({});
        });
        return defer.promise
      }

      return factory;
    })

    .factory('MedSocial',function($log,$q,$ionicPlatform,$resource,Network,User){
      var factory   =   {};

      factory.social_profile = function(){

        var url   =   Network.api_url+'/social/profile/:phone_number';

        return  $resource(url, {},{
            'show': { method:'GET',params:{}},
        });

      }

      factory.network = function(){

        var user  =   User.logged_in_user();
        if(user == null){
           deferred.reject(null);
           return deferred.promise;
        }

        var url   =   Network.api_url+'/social/profile/'+user.id+'/network';

        return  $resource(url, {},{
            'get': { method:'GET',params:{}},
        });

      }

      factory.sent_request   =  function(){
        var user  =   User.logged_in_user();
        if(user == null){
           deferred.reject(null);
           return deferred.promise;
        }

        var url   =   Network.api_url+'/social/profile/'+user.id+'/request/:dest_id';

        return  $resource(url, {},{
            'commit': { method:'POST',params:{}},
        });
      }

      factory.send_invite   =   function(){
        var user  =   User.logged_in_user();
        if(user == null){
           deferred.reject(null);
           return deferred.promise;
        }

        var url   =   Network.api_url+'/social/profile/'+user.id+'/invite/:dest_phone_number';

        return  $resource(url, {},{
            'commit': { method:'POST',params:{}},
        });
      }
      factory.status_request  =   function(){
        var user  =   User.logged_in_user();
        if(user == null){
           deferred.reject(null);
           return deferred.promise;
        }

        var profiles  =   user.profiles;
        var profile = {};
        angular.forEach(profiles,function(pro){
          if(parseInt(pro.is_virtual) == 0){
            profile   =   pro;
          }
        });
        var url   =   Network.api_url+'/social/profile/:src_id/request/'+profile.id;

        return  $resource(url, {},{
            'commit': { method:'POST',params:{}},
        });
      }
       return factory;
    })



    /*
    |------------------------------------------------------------------------------
    |  ScheduleInfo - Service to Schedule the Info from app
    |------------------------------------------------------------------------------
    |  @author:  Shekh Rizwan<rizwan@hirarky.com>
    |  @date:  2016-10-18
    */
    .factory('ScheduleInfo',function(ScheduleMedication,ScheduleDocAppointment,ScheduleLabtest,ScheduleWalkJog,ScheduleActivity){
      var factory   =   {};

      factory.schedule_medication  =   function(){
          return ScheduleMedication;
      }

      factory.schedule_doc_appo   =   function(){
        return ScheduleDocAppointment;
      }

      factory.schedule_labtest  =   function(){
        return ScheduleLabtest;
      }

      factory.schedule_walkjog  =   function(){
        return ScheduleWalkJog;
      }

      factory.schedule_activity  =   function(){
        return ScheduleActivity;
      }
      return factory;
    })

    /*
    |------------------------------------------------------------------------------
    |  ScheduleMedication - Service to Schedule the Medication Info from app
    |------------------------------------------------------------------------------
    |  @author:  Shekh Rizwan<rizwan@hirarky.com>
    |  @date:  2016-10-18
    */
    .factory('ScheduleMedication',function(Log,Network,$q,$resource,locker,User,UserProfile){
      var factory   =   {};
      function resource(){


        var url   =   Network.api_url+'/schedule/medicine';

        return  $resource(url, {},{
            'get': { method:'GET',params:{}},
            'add': { method:'POST',params:{}},
            'show': { method:'GET',params:{}},
            'update': { method:'PUT',params:{}},
            'delete': { method:'DELETE',params:{}}
        });
      }

      factory.commit  =   function(data){

        var deferred  =   $q.defer();

        var user  =   User.logged_in_user();
        if(user == null){
          return null;
        }

        var profile  =   UserProfile.getActiveProfile();
        if(profile ==  null){
          return null;
        }

        data.profile_id   =  profile.id;

        if(data.medicine_name == null || data.medicine_name.length <= 0 ){
          deferred.reject({
            'staut':400,
            'message':'Please Enter Medicine Name'
          });
          return deferred.promise;
        }

        if(data.doss_type == null || data.doss_type.length <= 0 ){
          deferred.reject({
            'staut':400,
            'message':'Please Select Medicine Type'
          });
          return deferred.promise;
        }

        if(data.doss == null || data.doss.length <= 0 ){
          deferred.reject({
            'staut':400,
            'message':'Please Enter Doss Value'
          });
          return deferred.promise;
        }

        var user_data   =   data;
        console.log(data);
        resource().add({
          'profile_id':data.profile_id,
          'medicines':data.sch_medicine,
          'schedule_date':data.sch_date,
          'schedule_time':data.sch_time,
          'notes':data.notes,
          'repeat_on':data.repeat_on,
          'is_forever':data.is_forever,
          'event_count':data.event_count,
          'until':data.until,

        },function(data){
            Log.info('ScheduleMedication:commit',data);
            deferred.resolve(data);
        },function(data){
            Log.error('ScheduleMedication:commit',data);
            deferred.reject(error);
        });
        return deferred.promise;
      }
      return factory;
    })

    /*
    |  ScheduleDocAppointment - Service to Schedule the Doctor Appointment Info from app
    |------------------------------------------------------------------------------
    |  @author:  Shekh Rizwan<rizwan@hirarky.com>
    |  @date:  2016-10-18
    */
    .factory('ScheduleDocAppointment',function(Log,Network,$q,$resource,locker,User,UserProfile){
          var factory   =   {};
          function resource(){


            var url   =   Network.api_url+'/schedule/doctor';

            return  $resource(url, {},{
                'get': { method:'GET',params:{}},
                'add': { method:'POST',params:{}},
                'show': { method:'GET',params:{}},
                'update': { method:'PUT',params:{}},
                'delete': { method:'DELETE',params:{}}
            });
          }

          factory.commit  =   function(data){

            var deferred  =   $q.defer();

            var user  =   User.logged_in_user();
            if(user == null){
              return null;
            }

            var profile  =   UserProfile.getActiveProfile();
            if(profile ==  null){
              return null;
            }

            data.profile_id   =  profile.id;

            if(data.hospital_name == null || data.hospital_name.length <= 0 ){
              deferred.reject({
                'staut':400,
                'message':'Please Enter Hospital Name'
              });
              return deferred.promise;
            }

            if(data.doctor_name == null || data.doctor_name.length <= 0 ){
              deferred.reject({
                'staut':400,
                'message':'Please Enter Doctor Name'
              });
              return deferred.promise;
            }

            if(data.notify_me == null || data.notify_me.length <= 0 ){
              deferred.reject({
                'staut':400,
                'message':'Please choose Notify time'
              });
              return deferred.promise;
            }

            var user_data   =   data;
            console.log(data);
            resource().add({
              'profile_id':data.profile_id,
              'hospital_name':data.hospital_name,
              'doctor_name':data.doctor_name,
              'schedule_date':data.doc_date,
              'schedule_time':data.doc_time,
              'notify_before':data.notify_me,
              'notes':data.notes,
              'follow_ups':data.follow_ups,

            },function(data){
                Log.info('ScheduleDocAppointment:commit',data);
                deferred.resolve(data);
            },function(data){
                Log.error('ScheduleDocAppointment:commit',data);
                deferred.reject(error);
            });
            return deferred.promise;
          }
          return factory;
        })

            /*
            |  ScheduleLabtest - Service to Schedule the Lab Test from app
            |------------------------------------------------------------------------------
            |  @author:  Shekh Rizwan<rizwan@hirarky.com>
            |  @date:  2016-10-18
            */
            .factory('ScheduleLabtest',function(Log,Network,$q,$resource,locker,User,UserProfile){
              var factory   =   {};
              function resource(){


                var url   =   Network.api_url+'/schedule/lab_test';

                return  $resource(url, {},{
                    'get': { method:'GET',params:{}},
                    'add': { method:'POST',params:{}},
                    'show': { method:'GET',params:{}},
                    'update': { method:'PUT',params:{}},
                    'delete': { method:'DELETE',params:{}}
                });
              }

              factory.commit  =   function(data){

                var deferred  =   $q.defer();

                var user  =   User.logged_in_user();
                if(user == null){
                  return null;
                }

                var profile  =   UserProfile.getActiveProfile();
                if(profile ==  null){
                  return null;
                }

                data.profile_id   =  profile.id;

                if(data.lab_name == null || data.lab_name.length <= 0 ){
                  deferred.reject({
                    'staut':400,
                    'message':'Please Enter Lab Name'
                  });
                  return deferred.promise;
                }

                if(data.test_type == null || data.test_type.length <= 0 ){
                  deferred.reject({
                    'staut':400,
                    'message':'Please Enter Test Type'
                  });
                  return deferred.promise;
                }

                if(data.notify_before == null || data.notify_before.length <= 0 ){
                  deferred.reject({
                    'staut':400,
                    'message':'Please choose Notify time'
                  });
                  return deferred.promise;
                }

                var user_data   =   data;
                console.log(data);
                console.log(data.lab_name);
                resource().add({
                  'profile_id':data.profile_id,
                  'lab_name':data.lab_name,
                  'test_type':data.test_type,
                  'schedule_date':data.labtest_date,
                  'schedule_time':data.labtest_time,
                  'notify_before':data.notify_before,
                  'contact_detail':data.notes,

                },function(data){
                    Log.info('ScheduleLabtest:commit',data);
                    deferred.resolve(data);
                },function(data){
                    Log.error('ScheduleLabtest:commit',data);
                    deferred.reject(error);
                });
                return deferred.promise;
              }
              return factory;
            })

            /*
            |------------------------------------------------------------------------------
            |  Schedule Walking/Jogging - Service to Schedule the Info from Walking/Jogging
            |------------------------------------------------------------------------------
            |  @author:  Shekh Rizwan<rizwan@hirarky.com>
            |  @date:  2016-10-17
            */
            .factory('ScheduleWalkJog',function(Log,Network,$q,$resource,locker,User,UserProfile){
              var factory   =   {};
              function resource(){


                var url   =   Network.api_url+'/schedule/activity';

                return  $resource(url, {},{
                    'get': { method:'GET',params:{}},
                    'add': { method:'POST',params:{}},
                    'show': { method:'GET',params:{}},
                    'update': { method:'PUT',params:{}},
                    'delete': { method:'DELETE',params:{}}
                });
              }

              factory.commit  =   function(data){

                var deferred  =   $q.defer();

                var user  =   User.logged_in_user();
                if(user == null){
                  return null;
                }

                var profile  =   UserProfile.getActiveProfile();
                if(profile ==  null){
                  return null;
                }

                data.profile_id   =  profile.id;

                if(data.notify_before == null || data.notify_before.length <= 0 ){
                  deferred.reject({
                    'staut':400,
                    'message':'Please choose Notify time'
                  });
                  return deferred.promise;
                }

                var user_data   =   data;
                console.log(data);
                resource().add({
                  'profile_id':data.profile_id,
                  'activity_name':data.activity_name,
                  'schedule_date':data.walkjog_date,
                  'schedule_time':data.walkjog_time,
                  'notify_before':data.notify_before,
                  'repeat_on':data.repeat_on,
                  'is_forever':data.is_forever,
                  'event_count':data.event_count,
                  'until':data.until,

                },function(data){
                    Log.info('ScheduleActivity:commit',data);
                    deferred.resolve(data);
                },function(data){
                    Log.error('ScheduleActivity:commit',data);
                    deferred.reject(error);
                });
                return deferred.promise;
              }
              return factory;
            })

      /*
      |------------------------------------------------------------------------------
      |  Schedule Activity - Service to Schedule the Info from activity
      |------------------------------------------------------------------------------
      |  @author:  Shekh Rizwan<rizwan@hirarky.com>
      |  @date:  2016-09-05
      */
      .factory('ScheduleActivity',function(Log,Network,$q,$resource,locker,User,UserProfile){
        var factory   =   {};

        function resource(){


          var url   =   Network.api_url+'/schedule/activity';

          return  $resource(url, {},{
              'get': { method:'GET',params:{}},
              'add': { method:'POST',params:{}},
              'show': { method:'GET',params:{}},
              'update': { method:'PUT',params:{}},
              'delete': { method:'DELETE',params:{}}
          });
        }

        function activity(){


          var url   =   Network.api_url+'/activities';

          return  $resource(url, {},{
              'get': { method:'GET',params:{}},
          });
        }

        factory.search  =   function(search_query){
            var deferred  =   $q.defer();

            if(search_query == null || search_query.length <= 2){
              deferred.resolve({
                'staut':200,
                'message':[]
              });
            } else {
                  activity().get({
                    'sq':search_query
                  },function(data){
                    Log.info('ScheduleActivity:search',data);
                    deferred.resolve({
                      'staut':200,
                      'message':data.data
                    });
                  });
            }

            return deferred.promise;
        }

        factory.commit  =   function(data){

          var deferred  =   $q.defer();

          var user  =   User.logged_in_user();
          if(user == null){
            return null;
          }

                var profile  =   UserProfile.getActiveProfile();
                if(profile ==  null){
                  return null;
                }

                data.profile_id   =  profile.id;

                if(data.activity_id == null || data.activity_id.length <= 0 ){
                  deferred.reject({
                    'staut':400,
                    'message':'Please choose activity'
                  });
                  return deferred.promise;
                }

                if(data.notify_before == null || data.notify_before.length <= 0 ){
                  deferred.reject({
                    'staut':400,
                    'message':'Please choose Notify time'
                  });
                  return deferred.promise;
                }

                var user_data   =   data;
                console.log(data);
                console.log(data.profile_id);
                resource().add({
                  'profile_id':data.profile_id,
                  'activity_name':data.activity_name,
                  'schedule_date':data.activity_date,
                  'schedule_time':data.activity_time,
                  'notify_before':data.notify_before,
                  'repeat_on':data.repeat_on,
                  'is_forever':data.is_forever,
                  'event_count':data.event_count,
                  'until':data.until,
                },function(data){
                    Log.info('ScheduleActivity:commit',data);
                    deferred.resolve(data);
                },function(data){
                    Log.error('ScheduleActivity:commit',data);
                    deferred.reject(error);
                });
                return deferred.promise;
              }
              return factory;
            })

        /*
        |------------------------------------------------------------------------------
        |  DateTimePickerServices - Common Service for Schedule and DateTimePicker
        |------------------------------------------------------------------------------
        |  @author:  Shekh Rizwan<rizwan@hirarky.com>
        |  @date:  2016-10-20
        */
        .factory('DateTimePickerServices',function(){
           var date = '';
           var time = '';
           return {
               getDate: function () {
                   return date;
               },
               setDate: function(datevalue) {
                   date = datevalue;
               },

               getTime: function () {
                   return time;
               },
               setTime: function(timevalue) {
                   time = timevalue;
               }
           };
        })

    /*
    |------------------------------------------------------------------------------
    |  PhrInfo - Service to Add Phr Info from app
    |------------------------------------------------------------------------------
    |  @author:  Shekh Rizwan<rizwan@hirarky.com>
    |  @date:  2016-11-04
    */
    .factory('PhrInfo',function(PhrUpload,PhrSearch,PhrEvent,PhrEventType,PhrEventTypeMedia){
      var factory   =   {};

      factory.phr_upload  =   function(){
          return PhrUpload;
      }

      factory.phr_search  =   function(){
          return PhrSearch;
      }

      factory.phr_event   =   function(){
        return PhrEvent;
      }

      factory.phr_event_type  =   function(){
        return PhrEventType;
      }

      factory.phr_event_type_media  =   function(){
        return PhrEventTypeMedia;
      }

      return factory;
    })

    /*
    |------------------------------------------------------------------------------
    |  PhrUpload - Service to upload/add Phr Info from app
    |------------------------------------------------------------------------------
    |  @author:  Shekh Rizwan<rizwan@hirarky.com>
    |  @date:  2016-11-04
    */
    .factory('PhrUpload',function(Log,Network,$q,$resource,locker,User,UserProfile){
      var factory   =   {};

      function resource(){

        var user  =   User.logged_in_user();
        if(user == null){
          return null;
        }
        var profile  =   UserProfile.getActiveProfile();
        if(profile ==  null){
          return null;
        }


        var url   =   Network.api_url+'/profile/'+profile.id+'/phr';
        console.log(user.id);

        return  $resource(url, {},{
            'get': { method:'GET',params:{}},
            'add': { method:'POST',params:{}},
            'show': { method:'GET',params:{}},
            'update': { method:'PUT',params:{}},
            'delete': { method:'DELETE',params:{}}
        });
      }

      factory.commit  =   function(data){

        var deferred  =   $q.defer();

        var user  =   User.logged_in_user();
        if(user == null){
          return null;
        }

        var profile  =   UserProfile.getActiveProfile();
        if(profile ==  null){
          return null;
        }

        data.profile_id   =  profile.id;

        if(data.media_id == null || data.media_id.length <= 0){
          deferred.reject({
            'staut':400,
            'message':'Please Attach Media'
          });
          return deferred.promise;
        }

        if(data.event_name == null || data.event_name.length <= 0 ){
          deferred.reject({
            'staut':400,
            'message':'Please Select Event'
          });
          return deferred.promise;
        }

        if(data.event_type == null || data.event_type.length <= 0){
          deferred.reject({
            'staut':400,
            'message':'Please Select Type'
          });
          return deferred.promise;
        }

        if(data.phr_date == null || data.phr_date.length <= 0){
          deferred.reject({
            'staut':400,
            'message':'Please enter date'
          });
          return deferred.promise;
        }

        if(data.phr_time == null || data.phr_time.length <= 0){
          deferred.reject({
            'staut':400,
            'message':'Please enter time'
          });
          return deferred.promise;
        }

        // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
        var user_data   =   data;
        console.log(data);
        resource().add({
          'event_name':data.event_name,
          'type':data.event_type,
          'phr_date':data.phr_date,
          'phr_time':data.phr_time,
          'notes':data.notes,
          'media_id':data.media_id
        },function(data){
            Log.info('PhrUpload:commit',data);
            deferred.resolve(data);
        },function(data){
            Log.error('PhrUpload:commit',data);
            deferred.reject(error);
        });
        return deferred.promise;
      }
      return factory;
    })

    /*
    |------------------------------------------------------------------------------
    |  PhrSearch - Service to Search Phr Record Info from app
    |------------------------------------------------------------------------------
    |  @author:  Shekh Rizwan<rizwan@hirarky.com>
    |  @date:  2016-11-04
    */
    .factory('PhrSearch',function(Log,Network,$q,$resource,locker,User,UserProfile,$timeout){
      var factory   =   {};

      function resource(){

          var user  =   User.logged_in_user();
          if(user == null){
            return null;
          }

          var  profile =   UserProfile.getActiveProfile();
          if(profile ==  null){
            return null;
          }

          var url   =   Network.api_url+'/profile/'+profile.id+'/phr';

          return  $resource(url, {},{
              'get': { method:'GET',params:{}},
              'show': { method:'GET',params:{}},
          });
        }

        factory.get  =   function(data){
          var deferred  =   $q.defer();

          var user  =   User.logged_in_user();
          if(user == null){
             deferred.reject(null);
             return deferred.promise;
          }

          var profile  =   UserProfile.getActiveProfile();
          if(profile ==  null){
            return null;
          }

          data.profile_id   =   profile.id;
          $timeout(function(){
            resource().get(data,function(data){
                Log.info('PhrSearch:get',data);
                deferred.resolve(data);
            },function(data){
                Log.error('PhrSearch:get',data);
                deferred.reject(data);
            });
          },500);

          return deferred.promise;
        }

      return factory;
    })

    /*
    |------------------------------------------------------------------------------
    |  PhrEvent - Service to fetch Phr Info from app
    |------------------------------------------------------------------------------
    |  @author:  Shekh Rizwan<rizwan@hirarky.com>
    |  @date:  2016-11-04
    */
    .factory('PhrEvent',function(Log,Network,$q,$resource,locker,User,UserProfile,$timeout){
      var factory   =   {};
      function resource(){

          var user  =   User.logged_in_user();
          if(user == null){
            return null;
          }

          var profile  =   UserProfile.getActiveProfile();
          if(profile ==  null){
            return null;
          }
          var url   =   Network.api_url+'/profile/'+profile.id+'/phr_event';

          return  $resource(url, {},{
              'get': { method:'GET',params:{}},
              'show': { method:'GET',params:{}},
          });
        }

        factory.get  =   function(){
          var deferred  =   $q.defer();

          var user  =   User.logged_in_user();
          if(user == null){
             deferred.reject(null);
             return deferred.promise;
          }

          var profile  =   UserProfile.getActiveProfile();
          if(profile ==  null){
            return null;
          }

          $timeout(function(){
            resource().get({
            },function(data){
                Log.info('PhrEvent:get',data);
                deferred.resolve(data);
            },function(data){
                Log.error('PhrEvent:get',data);
                deferred.reject(data);
            });
          },500);

          return deferred.promise;
        }
        return factory;
   })


    /*
    |------------------------------------------------------------------------------
    |  PhrEventType - Service to Add Phr event type Info from app
    |------------------------------------------------------------------------------
    |  @author:  Shekh Rizwan<rizwan@hirarky.com>
    |  @date:  2016-11-04
    */
    .factory('PhrEventType',function(Log,Network,$q,$resource,locker,User,UserProfile,$timeout){
      var factory   =   {};
      function resource(){

          var user  =   User.logged_in_user();
          if(user == null){
            return null;
          }

          var profile  =   UserProfile.getActiveProfile();
          if(profile ==  null){
            return null;
          }
          var url   =   Network.api_url+'/profile/'+profile.id+'/phr_event/:event_name/type';

          return  $resource(url, {},{
              'get': { method:'GET',params:{}},
              'show': { method:'GET',params:{}},
          });
        }

        factory.get  =   function(event_name){
          var deferred  =   $q.defer();

          var user  =   User.logged_in_user();
          if(user == null){
             deferred.reject(null);
             return deferred.promise;
          }

          var profile  =   UserProfile.getActiveProfile();
          if(profile ==  null){
            return null;
          }

          var user_id  =   profile.id;
          $timeout(function(){
            resource().get({
              'event_name' : event_name
            },function(data){
                Log.info('PhrEventType:get',data);
                deferred.resolve(data);
            },function(data){
                Log.error('PhrEventType:get',data);
                deferred.reject(data);
            });
          },500);

          return deferred.promise;
        }

      return factory;
    })

    /*
    |------------------------------------------------------------------------------
    |  PhrEventTypeMedia - Service to Add Phr event type Media Info from app
    |------------------------------------------------------------------------------
    |  @author:  Shekh Rizwan<rizwan@hirarky.com>
    |  @date:  2016-11-04
    */
    .factory('PhrEventTypeMedia',function(Log,Network,$q,$resource,locker,User,UserProfile,$timeout){
      var factory   =   {};
      function resource(){

          var user  =   User.logged_in_user();
          if(user == null){
            return null;
          }

          var profile  =   UserProfile.getActiveProfile();
          if(profile ==  null){
            return null;
          }
          var url   =   Network.api_url+'/profile/'+profile.id+'/phr_event/:event_name/type/:type_name';

          return  $resource(url, {},{
              'get': { method:'GET',params:{}},
              'show': { method:'GET',params:{}},
          });
        }

        factory.get  =   function(event_name,type_name){
          var deferred  =   $q.defer();

          var user  =   User.logged_in_user();
          if(user == null){
             deferred.reject(null);
             return deferred.promise;
          }

          var profile  =   UserProfile.getActiveProfile();
          if(profile ==  null){
            return null;
          }

          var user_id  =   profile.id;
          $timeout(function(){
            resource().get({
              'event_name' : event_name,
              'type_name' : type_name
            },function(data){
                Log.info('PhrEventTypeMedia:get',data);
                deferred.resolve(data);
            },function(data){
                Log.error('PhrEventTypeMedia:get',data);
                deferred.reject(data);
            });
          },500);

          return deferred.promise;
        }

      return factory;
    })

    /*
    |------------------------------------------------------------------------------
    |  TimelineData - Service to fetch timeline Info from server
    |------------------------------------------------------------------------------
    |  @author:  Shekh Rizwan<rizwan@hirarky.com>
    |  @date:  2016-11-16
    */
    .factory('TimelineData',function(Log,Network,$q,$resource,locker,User,UserProfile,$timeout){
      var factory   =   {};
      function resource(){

          var user  =   User.logged_in_user();
          if(user == null){
            return null;
          }

          var profile  =   UserProfile.getActiveProfile();
          if(profile ==  null){
            return null;
          }
          var url   =   Network.api_url+'/profile/'+profile.id+'/timeline';

          return  $resource(url, {},{
              'get': { method:'GET',params:{}},
              'show': { method:'GET',params:{}},
          });
        }

        factory.get  =   function(start_date,lenght){
          var deferred  =   $q.defer();

          var user  =   User.logged_in_user();
          if(user == null){
             deferred.reject(null);
             return deferred.promise;
          }

          var profile  =   UserProfile.getActiveProfile();
          if(profile ==  null){
            return null;
          }

          var user_id  =   profile.id;
          $timeout(function(){
            resource().get({
              'start_date' :  start_date,
              'lenght'     :  lenght
            },function(data){
                Log.info('TimelineData:get',data);
                deferred.resolve(data);
            },function(data){
                Log.error('TimelineData:get',data);
                deferred.reject(data);
            });
          },500);

          return deferred.promise;
        }

      return factory;
    })
  ;
