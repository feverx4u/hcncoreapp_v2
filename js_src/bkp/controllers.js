/*
 |-----------------------------------------------------------------
 |  controllers.js
 |----------------------------------------------------------------
 |  @author:      Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:      2016-09-01
 |
 */


angular.module('hcncore.controllers',[])

/*
 |---------------------------------------------------------------
 |  RouteController - Controller is place where application is
 |                    is initialized and taken to proper state.
 |---------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-01
 */
.controller('SideMenuController',function($scope,$state,$rootScope,$ionicHistory,Core,$timeout,$ionicLoading){
  $scope.setting_status   =   false;

  $scope.setting_style  =   {};
  $scope.$state   =   $state;

  $scope.is_show  =   true;
  $scope.username = '';
  $scope.profile_image    = null  ;

  $scope.init   =   function(){
    if($state.current.name     ==  'credential'){
        $scope.is_show  =   false;
    }
    var user = Core.user_profile().getActiveProfile();
    if(user!=null){
      $scope.username = user.name;
      if($scope.username == null || $scope.username.length == 0){
        $scope.username = user.email;
        if($scope.username == null || $scope.username.length == 0){
          $scope.username = user.phonenumber;
        }
      }
      console.log(user.profile_media);
      if(user.profile_media != null){
        $scope.profile_image    =   user.profile_media.uri;
      }
    }

  }
  $scope.init();
  $scope.toggle_setting   =   function(){
      $scope.setting_status   =   !$scope.setting_status;
      if($scope.setting_status === true){
          $scope.setting_style  =   {'height':'252px'};
      } else {
        $scope.setting_style  =   {'height':'0'};
      }
  };

  $scope.go   =   function(state){
    $state.go(state);
  };

  $scope.logout   =   function(){
    Core.user().logout();
    $ionicLoading.show({
        template: '<ion-spinner icon="android"></ion-spinner>'
    });

    $timeout(function(){
      $ionicLoading.hide();
      $timeout(function(){
        $state.go('credential');
      },10);
    },1000);
    // User.logout().then(function(){
    //   $state.go('intro');
    // });
  };

})
/*
 |---------------------------------------------------------------
 |  RouteController - Controller is place where application is
 |                    is initialized and taken to proper state.
 |---------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-01
 */
.controller('RouteController',function($scope,$state,$ionicLoading,$ionicPlatform,$timeout,Core){

  //Initializing the Route Controller
  $scope.init   =   function(){

    //Loader
    $ionicLoading.show({
        template: '<ion-spinner icon="android"></ion-spinner>'
    });


    //Hiding the splashscreen
    $ionicPlatform.ready(function() {
        $timeout(function() {
            navigator.splashscreen.hide();
        }, 1000);
     });
     $scope.decide_next_route();
  }

  $scope.focus_elem   =   function(id){
    document.getElementById(id).focus();
  }

  //Method will decide the next route and navigate to that
  $scope.decide_next_route  =   function(){
    $ionicLoading.hide();
    var user  =   Core.user().logged_in_user();
    if(user == null){
        $state.go('credential');
    } else {
      var profiles  =   user.profiles;
      if(profiles != null && profiles.length > 0){
         $state.go('med_dashboard');
      } else {
         $state.go('user_info_med');
      }
      // var initial_flow   =   parseInt(user.is_init_flow);
      // if(false && initial_flow !=  0){
      //    $state.go('main_tabs.home_widget_tab.dashboard');
      // } else {
      //
      // }
    }
  }

  $scope.init();
})

/*
 |---------------------------------------------------------------
 |  CredentialController - Controller is used to manage the credential view
 |---------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-02
 */
 .controller('CredentialController',function($rootScope,$scope,$state,$ionicPlatform,Core,$timeout,
   $ionicLoading,$cordovaToast,$ionicNavBarDelegate,$ionicHistory,$ionicScrollDelegate){

   $scope.v   =   {};

   //Initializing class
   $scope.init   =  function(){
     $ionicNavBarDelegate.showBar(false);
     $ionicHistory.clearHistory();
     $scope.variables();
     $scope.set_active_tab_handler();
   }

   $scope.take_screen_to_top  =   function(id){
     var elem   =   document.getElementById(id);
     var top =  elem.getBoundingClientRect().top - 10;
     console.log(top);
      $ionicScrollDelegate.$getByHandle('mainScroll').scrollTo(0,top,true);
      console.log($ionicScrollDelegate.$getByHandle('mainScroll').getScrollPosition());
   }


   $scope.variables =   function(){
     var v  =   $scope.v;

    //  if($state.current.name == 'credential.register'){
       v.active_tab   =   'REGISTER';
    //  } else {
      //  v.active_tab   =   'LOGIN';
    //  }

     v.login_data   =   {
       'username':'',
       'username_err':'',
       'password':'',
       'password_err':'',
       'is_pass_visible':false,
       'is_otp':false,
       'country_code':'91',
       'country_name':'India',
       'in_prog':false
     };

     v.register_data   =   {
       'username':'',
       'username_err':'',
       'password':'',
       'password_err':'',
       'is_pass_visible':false,
       'repeat_password':'',
       'password_con_err':'',
       'is_pass_con_visible':false,
       'is_otp':false,
       'country_code':'91',
       'country_name':'India',
       'in_prog':false
     };

     v.country_code   = country_code.items;
    //  console.log(v.country_code);
   }

   $scope.$watch('v.login_data.country_code',function(){
     var code = $scope.v.login_data.country_code;
     angular.forEach($scope.v.country_code,function(item){
       if(item.code   == code){
         $scope.v.login_data.country_name   = item.country;
       }
     });
   });
   $scope.open_country_list    =  function(id){

     var event = new MouseEvent('mousedown', {
        // 'view': window,
        // 'bubbles': true,
        'cancelable': true
    });
    var cb;
    if(id == undefined){
        cb = document.getElementById('country_list');
    } else {
      cb = document.getElementById(id);
    }

    // cb.size=4;
    cb.dispatchEvent(event);

    //  document.getElementById('country_list').click();
   }
   //Method is used to change the tab
   $scope.change_tab  =   function(tab_name){
      var v  =   $scope.v;
      if(tab_name == 'LOGIN'){
        // $state.go('credential.login');
        v.active_tab   =   'LOGIN';
      } else if(tab_name == 'REGISTER'){
        // $state.go('credential.register');
        v.active_tab   =   'REGISTER';
      }
   }

   //Set the Active Tab handler
   $scope.set_active_tab_handler  =   function(){
     var v  =   $scope.v;
     $rootScope.$on('$stateChangeSuccess',
     function(event, toState, toParams, fromState, fromParams){
       if(toState.name == 'credential.register'){
         v.active_tab   =   'REGISTER';
       } else if(toState.name == 'credential.login'){
         v.active_tab   =   'LOGIN';
       }
     });
   }


   //Authenticating the User.
   $scope.authenticate_user   =   function(){
     var login_data  =   $scope.v.login_data;
     login_data.username_err = '';
     login_data.password_err = '';
     var login_data_username = login_data.username+'';
     if(login_data.username.length == 0){
       login_data.username_err = 'Phone Number should not be empty.';
       return;
     }

     if(login_data_username.length < 10){
       login_data.username_err = 'Phone Number should be of 10 digits.';
       return;
     }

     if(!login_data.is_otp){
       if(login_data.password.length < 6){
         login_data.password_err = 'Password should be of 6 charactes.';
         return;
       }
     }

     login_data.in_prog   =   true;

     Core.user().authenticate('+'+login_data.country_code+''+login_data.username,login_data.password,login_data.is_otp).then(function(data){
       $ionicLoading.show({
           template: '<ion-spinner icon="android"></ion-spinner>'
       });

       $timeout(function () {
         login_data.in_prog   =   false;
         var user_data  =   data.user_data;
        //  var initial_flow   =   parseInt(data.is_init_flow);
         if(user_data.profiles != null && user_data.profiles.length > 0){
           $state.go('med_dashboard');
         } else {
           $state.go('user_info_med');
         }
         $ionicLoading.hide();

       }, 2000);
     },function(error){
       login_data.in_prog   =   false;
       $cordovaToast.show(error.message,'short','center');
     });

   }

   $scope.register_user    =  function(){
     var register_data  =   $scope.v.register_data;
     register_data.username_err = '';
     register_data.password_err = '';
     register_data.password_con_err = '';
     var register_data_username = register_data.username+'';
     if(register_data.username.length == 0){
       register_data.username_err = 'Phonenumber should not be empty.';
       return;
     }

     if(register_data_username.length < 10){
       register_data.username_err = 'Phonenumber should be of 10 digits.';
       return;
     }

     if(register_data.password.length < 6){
       register_data.password_err = 'Password should be of 6 charactes.';
       return;
     }

     if(register_data.password != register_data.repeat_password){
       register_data.password_con_err = 'Password does not match.';
       return;
     }

     register_data.in_prog   =   true;

     $ionicLoading.show({
         template: '<ion-spinner icon="android"></ion-spinner>'
     });

     Core.user().register('+'+register_data.country_code+''+register_data.username,register_data.password).then(function(data){
       $timeout(function () {
         register_data.in_prog   =   false;
         $ionicLoading.hide();
        //  $ionicLoading.show({
        //      template: 'User Registration Successfully Finished.'
        //  });
        //  $timeout(function () {
        //    $ionicLoading.hide();
        //  },2000);
        $state.go('user_info_med');

       }, 2000);
     },function(error){
       $cordovaToast.show(error.message,'short','center');
       register_data.in_prog   =   false;
       $ionicLoading.hide();
     });


   }

   $scope.init();
 })

 .controller('UserMedinfoController',function($rootScope,$scope,$state,$timeout,Core,$ionicPopup,
   $cordovaImagePicker,$jrCrop,$cordovaCamera,Media,$cordovaToast,$ionicLoading,$ionicHistory){

     $scope.init    =  function(){
       // $ionicNavBarDelegate.showBar(false);
       $timeout(function () {
           $rootScope.is_big_bar   =   false;
       }, 10);
       // document.querySelect
       $scope.variable();

        $scope.fetch_login_user();
        $ionicHistory.clearHistory();
     }


     $scope.v     =   {};

     $scope.user  =   null;
     $scope.variable  =   function(){
        var v   =   $scope.v;
        v.name  =   '';
        v.profile_image  =   null;
        v.image_changed   =   false;

        v.profile_id    = null;
     }

     //Fecthing the loged in user
     $scope.fetch_login_user   =   function(){
       var v   =   $scope.v;
       var user  = Core.user().logged_in_user();
       console.log(user);
       if(user == null){
         return;
       }

       $scope.user    =   user;

       if(user.profiles != null){
         angular.forEach(user.profiles,function(profile){
           if(profile.is_virtual == 0){
             v.name  = profile.name;
             v.profile_id   =   profile.id;
             if(profile.profile_media != null){
                v.profile_image  = profile.profile_media.uri;
             }
           }
         });
       }

     }

     $scope.update_user_info   =   function(){
       var v   =   $scope.v;

       if(v.profile_id == null){
         if(v.image_changed && v.profile_image != null){
           $ionicLoading.show({
               template: '<ion-spinner icon="android"></ion-spinner>'
           });

           Media.upload(v.profile_image,'BASE_64_IMAGE',function(data){
             console.log(data);
             var response  =   JSON.parse(data.currentTarget.response);
             var media   =   response.media;
             Core.user_profile().add_profile({
                'name':v.name,
                'profile_pic_media':media.id,
                'is_virtual':'FALSE',
                'phone_number':$scope.user.phone_number
             }).then(function(data){
               $state.go('med_dashboard');
               console.log(data);
             },function(error){
               console.log(error);
               $cordovaToast.show(error.message,'short','center');
             });

             $ionicLoading.hide();
           },function(data){
             console.log(data);
             $cordovaToast.show('Failed uploading image.','short','center');
              $ionicLoading.hide();

           },function(data){
             console.log(data);
             $cordovaToast.show('Image upload aborted','short','center');
              $ionicLoading.hide();
           },function(data){
             console.log(data);
           });

         } else {
           var data    =  {
             'name':v.name,
             'is_virtual':'FALSE',
             'phone_number':$scope.user.phone_number
           };

           if(v.profile_image == null){
             data['profile_pic_media']    = 'null';
           }

           $ionicLoading.show({
               template: '<ion-spinner icon="android"></ion-spinner>'
           });

           Core.user_profile().add_profile(data).then(function(data){
             $state.go('med_dashboard');
             $ionicLoading.hide();

           },function(error){
             $cordovaToast.show(error.message,'short','center');
           });
         }
       } else {
         if(v.image_changed && v.profile_image != null){
           $ionicLoading.show({
               template: '<ion-spinner icon="android"></ion-spinner>'
           });

           Media.upload(v.profile_image,'BASE_64_IMAGE',function(data){
             console.log(data);
             var response  =   JSON.parse(data.currentTarget.response);
             var media   =   response.media;
             Core.user_profile().update_profile(v.profile_id,{
                'name':v.name,
                'profile_pic_media':media.id,
                'is_virtual':'FALSE'
             }).then(function(data){
               $state.go('med_dashboard');
               console.log(data);
             },function(error){
               console.log(error);
               $cordovaToast.show(error.message,'short','center');
             });

             $ionicLoading.hide();
           },function(data){
             console.log(data);
             $cordovaToast.show('Failed uploading image.','short','center');
              $ionicLoading.hide();

           },function(data){
             console.log(data);
             $cordovaToast.show('Image upload aborted','short','center');
              $ionicLoading.hide();
           },function(data){
             console.log(data);
           });

         } else{
           var data    =  {
             'name':v.name,
             'is_virtual':'FALSE'
           };

           if(v.profile_image == null){
             data['profile_pic_media']    = 'null';
           }

           $ionicLoading.show({
               template: '<ion-spinner icon="android"></ion-spinner>'
           });

           Core.user_profile().update_profile(v.profile_id,data).then(function(data){
             $state.go('med_dashboard');
             $ionicLoading.hide();

           },function(error){
             $cordovaToast.show(error.message,'short','center');
           });
         }
       }


     }

     $scope.upload_popup  =   null;
     $scope.upload_options   =   function(){
       $scope.upload_popup = $ionicPopup.show({
         templateUrl: 'file_upload_popup.html',
         title: 'Upload',
         scope: $scope,
         'cssClass':'report_popup'
       });
     }

     $scope.close_upload_popup    =   function(){
       $scope.upload_popup.close();
     }

     function take_me_to_croper(uri){
       $jrCrop.crop({
            url: uri,
            width: 200,
            height: 200,
            // title: 'Move and Scale'
        }).then(function(canvas) {
            // success!
            var image = canvas.toDataURL();
            $scope.v.profile_image   = image;
            $scope.v.image_changed   = true;
            // console.log(image);
        }, function(error) {
            console.log(error);
            // User canceled or couldn't load image.
        });
     }


     $scope.open_image_picker   =   function(){
       var options = {
         maximumImagesCount: 1,
         width: 800,
         height: 800,
        };

        $scope.close_upload_popup();
       $cordovaImagePicker.getPictures(options)
       .then(function (results) {
          for (var i = 0; i < results.length; i++) {
            console.log('Image URI: ' + results[i]);
             take_me_to_croper(results[i]);
          }
        }, function(error) {
          // error getting photos
        });
     }

     $scope.open_camera_for_image   =   function(){
          $scope.close_upload_popup();
           var options = {
             quality: 50,
             destinationType: Camera.DestinationType.FILE_URI,
             sourceType: Camera.PictureSourceType.CAMERA,
             encodingType: Camera.EncodingType.PNG,
             targetWidth:800,
             popoverOptions: CameraPopoverOptions,
             saveToPhotoAlbum: false,
             correctOrientation:true
          };

          $cordovaCamera.getPicture(options).then(function(imageURI) {
            take_me_to_croper(imageURI);
          }, function(err) {
            // error
          });
     }

     $scope.remove_image  =   function(){
        $scope.v.profile_image   = null;
        $scope.close_upload_popup();
        $scope.v.image_changed  = true;
     }

     $scope.init();
 })

 .controller('UserMedDashabordController',function($rootScope,$scope,$state,$timeout,Core,$ionicPopup,
 $cordovaToast,$ionicLoading,$ionicHistory){

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
        $ionicHistory.clearHistory();
        $scope.fetch_login_user();
          Core.user_profile().setActiveProfile(null);
          $scope.fetch_network();
          $timeout(function(){
              Core.phone_contacts().searchContact('').then(function(data){
                $scope.contacts   =   data;
              });
          },1000);

          // ionicMaterialInk.displayEffect();
    }

    $scope.profiles     =   [];
    $scope.contacts   =   [];

    $scope.v  =   {};
    $scope.fetch_login_user   =   function(){
      var v   =   $scope.v;
      v.user_name   =   '';
      v.is_search   =   '';
      v.is_search_contact   =   false;

      var user  = Core.user().logged_in_user();
      if(user == null){
        return;
      }

      if(user.profiles != null){
        $scope.profiles = user.profiles;
      }

    }

    $scope.network  = {};
    $scope.fetch_network  =   function(){
      Core.med_social().network().get({},function(data){
        $scope.network  =   data;
      },function(error){

      });
    }

    $scope.take_to_med_request  =   function(person){
      $state.go('med_request',{'phone_number':person.source_profile.phone_number,'relation':person.relation,'name':person.source_profile.name});
    }

    $scope.take_to_med_profile  =   function(contact){
      $state.go('med_profile',{'phone_number':contact.contact_num,'name':contact.name});
    }
    $scope.search_contacts  =   function(){
      var v   =   $scope.v;
      v.is_search_contact = true;
      Core.phone_contacts().searchContact($scope.v.user_name).then(function(data){
        $scope.contacts   =   data;
        console.log(data);
        v.is_search_contact = false;
      });
    }

    $scope.take_to_internal_profile  =   function(profile){
      Core.user_profile().setActiveProfile(profile);
      if(profile.is_init_flow == 0){
        $state.go('user_info');
      } else {
        $state.go('main_tabs.home_widget_tab.dashboard');
      }
    }
    $scope.init();
 })
 /*
  |-------------------------------------------------------------------
  | UserinfoController - Controller for userinfomation
  |-------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .controller('UserinfoController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,$ionicLoading,$cordovaToast,ionicDatePicker,$ionicNavBarDelegate,$ionicHistory){

      $scope.v    =   {};

      //Method to initialize the variable
      $scope.variable   =   function(){
        var v   =   $scope.v;
        v.user_info   =   {
          'name':'',
          'dob':'',
          'gender':'',
          'gender_view':'',
          'height':'',
          'weight':'',
          'height_unit':'FEET',
          'weight_unit':'KG'
        };
      }

      $scope.$watch('v.user_info.height_unit',function(new_val,current_val){
          var v   =   $scope.v;
        if(new_val != current_val){
          if(new_val == 'METER'){
            v.user_info.height    =   parseFloat(sprintf('%.2f',parseFloat(v.user_info.height)*0.30));
          } else if(new_val == 'FEET'){
            v.user_info.height    =   parseFloat(sprintf('%.2f',parseFloat(v.user_info.height)/0.30));
          }
        }

      });

      $scope.$watch('v.user_info.weight_unit',function(new_val,current_val){
          var v   =   $scope.v;
        if(new_val != current_val){
          if(new_val == 'LBS'){
            v.user_info.weight    =   parseFloat(sprintf('%.2f',parseFloat(v.user_info.weight)*2.2));
          } else if(new_val == 'KG'){
            v.user_info.weight    =   parseFloat(sprintf('%.2f',parseFloat(v.user_info.weight)/2.2));
          }
        }
      });

      $scope.$watch('v.user_info.gender',function(){
        if($scope.v.user_info.gender == 'MALE'){
          $scope.v.user_info.gender_view  =   'Male';
        } else if($scope.v.user_info.gender == 'FEMALE'){
          $scope.v.user_info.gender_view  =   'Female';
        } else {
          $scope.v.user_info.gender_view  =   '';
        }
      });
      $scope.trigger_click  =   function(id){
        var event = new MouseEvent('mousedown', {
           // 'view': window,
           // 'bubbles': true,
           'cancelable': true
       });
      //  alert(id);
       var cb = document.getElementById(id);
      //  console.log(document.getElementById(id));
       // cb.size=4;
       cb.dispatchEvent(event);
      }

      $scope.open_date_picker    =  function(){
        // alert(111);
        var v   =   $scope.v;
        var default_date  = '';
        if(v.user_info.dob != ''){
            default_date  =   moment(v.user_info.dob, 'Do MMM,YYYY');
        } else  {
          default_date  =   moment('1991-01-01');
        }


        ionicDatePicker.openDatePicker({
          callback:function(val){
            v.user_info.dob   =   moment(val).format('Do MMM,YYYY');
          },
          templateType: 'popup',
          to:new Date( moment().subtract(3,'years').format('YYYY-MM-DD')),
          inputDate:new Date(default_date)
        });
      }

      //Fecthing the loged in user
      $scope.fetch_login_user   =   function(){
        var v   =   $scope.v;
        var user  = Core.user_profile().getActiveProfile();
        if(user == null){
          return;
        }
        v.user_info.name  = user.name;
        if(user.dob != null && user.dob != undefined && user.dob != ''){
            v.user_info.dob    = moment(user.dob).format( 'Do MMM,YYYY');
        }
        if(user.gender != null && user.gender != undefined){
          v.user_info.gender   = user.gender;
        } else {
          // v.user_info.gender   = 'MALE';
          v.user_info.gender   = '';
        }
        if(user.height != null && user.height != undefined){
          v.user_info.height   = parseFloat(user.height);
        }

        if(user.weight != null && user.weight != undefined){
          v.user_info.weight   = parseFloat(user.weight);
        }
        if(user.height_unit != null && user.height_unit != undefined){
          v.user_info.height_unit   = user.height_unit;
        }
        if(user.weight_unit != null && user.weight_unit != undefined){
          v.user_info.weight_unit   = user.weight_unit;
        }
      }

      //Initialisig the init
      $scope.init    =  function(){
        // $ionicNavBarDelegate.showBar(false);
        $timeout(function () {
            $rootScope.is_big_bar   =   true;
        }, 10);
        // document.querySelect
        $scope.variable();
        $scope.fetch_login_user();
        // $ionicHistory.clearHistory();
      }

      //Inititalizing the updated user
      $scope.update_user_info   =   function(){
        var user_info   =   $scope.v.user_info;

        Core.user_profile().update_user_info(user_info).then(function(data){
          // $cordovaToast.show(error.message,'short','bottom');
          // $cordovaToast.show('User updated','short','bottom');

          $state.go('user_goal')
        },function(error){
          // login_data.in_prog   =   false;
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
      }

      $scope.go_back  =   function(){
          $ionicHistory.goBack();
      }
      $scope.init();
  })

  /*
   |-------------------------------------------------------------------
   | UserGoalController - Controller for user goal
   |-------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */
   .controller('UserGoalController',function($rootScope,$scope,$state,$ionicPlatform,Core,
     $timeout,$ionicLoading,$cordovaToast,ionicDatePicker,$ionicHistory){

       $scope.v   =   {};
        //Method to initialize the controller
        $scope.init   =   function(){
          $timeout(function () {
              $rootScope.is_big_bar   =   true;
          }, 10);
          $scope.variable();
          $scope.fetch_goal();
        }

        //variable
        $scope.variable   = function(){
          var v   =   $scope.v;
          $scope.v.goals    =   [];
        }

        //fetching the goal
        $scope.fetch_goal  =   function(){
          // alert(111);
          Core.goal().fetch_goals().then(function(data){
            $scope.v.goals  =   data.message;
            // console.log(data);
          });
        }

        $scope.go_back  =   function(){
          $ionicHistory.goBack();
          // $state.go('user_info');
        }

        //Method is used to choose the goal
        $scope.choose_goal  =   function(goal){
          if(goal.status == 'COMING_SOON'){
              $cordovaToast.show('Goal yet to be released','short','center');
              return;
          }
          goal.is_checked   = !goal.is_checked;

        }

        //Method is used to select the goal
        $scope.select_goal  =   function(){
          var goals   =   $scope.v.goals;
          var choosen_goal  = [];
          angular.forEach(goals,function(goal){
              if(goal.is_checked){
                choosen_goal.push(goal.slug);
              }
          });

          if(choosen_goal.length <= 0){
            $cordovaToast.show('Please choose the goal.','short','center');
            return;
          }
          console.log(choosen_goal);
          Core.goal().set_goal(choosen_goal).then(function(){
            if(choosen_goal.indexOf('diabetes_mgmt') >= 0){
              $state.go('diabetes_type');
            } else if(choosen_goal.indexOf('hypertension') >= 0){
              $state.go('hypertension');
            } else {
              $state.go('emergency_contact');
            }
            // $cordovaToast.show('Goal updated','short','bottom');
          },function(error){
            $cordovaToast.show(error.message,'short','center');
          });
        }

        $scope.init();
    })

    /*
     |-------------------------------------------------------------------
     | DiabetesTypeController - Controller for user goal Diabetes
     |-------------------------------------------------------------------
     |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
     |  @date:  2016-09-05
     */
     .controller('DiabetesTypeController',function($rootScope,$scope,$state,$ionicPlatform,Core,
       $timeout,$ionicLoading,$cordovaToast,ionicDatePicker,$ionicHistory){

         $scope.v   =   {};
          //Method to initialize the controller
          $scope.init   =   function(){
            $scope.variable();
            $scope.fetch_type();
          }

          //variable
          $scope.variable   = function(){
            var v   =   $scope.v;
            $scope.v.diabetes_type    =   [];
          }

          //fetching the goal
          $scope.fetch_type  =   function(){
            // alert(111);
            Core.diabetes().fetch_diabetes_type().then(function(data){
              $scope.v.diabetes_type  =   data.message;
              // console.log(data);
            });
          }

          $scope.go_back  =   function(){
            $ionicHistory.goBack();
          }

          //Method is used to choose the goal
          $scope.choose_diabetec_type  =   function(type){
            // console.log(type);
            angular.forEach($scope.v.diabetes_type,function(i_type){
                if(i_type.id == type.id){
                  i_type.is_checked   = true;
                } else {
                  i_type.is_checked   = false;
                }
            });

          }

          //Method is used to select the goal
          $scope.select_type  =   function(){
            var diabetes_type   =   $scope.v.diabetes_type;
            var choosen_type  = null;
            angular.forEach(diabetes_type,function(type){
                if(type.is_checked){
                  choosen_type = type;
                }
            });

            if(choosen_type==null){
              $cordovaToast.show('Please choose the diabetes type.','short','center');
              return;
            }

            Core.diabetes().update_diabeted_type(choosen_type).then(function(){
              // $cordovaToast.show('Updated','short','bottom');
              $state.go('diabetes_range');
            },function(error){
              $cordovaToast.show(error.message,'short','center');
            });
          }

          $scope.init();
      })

  /*
   |-------------------------------------------------------------------
   | DiabetesRangeController - Controller for user diabater range
   |-------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */
   .controller('DiabetesRangeController',function($rootScope,$scope,$state,$ionicPlatform,Core,
     $timeout,$ionicLoading,$cordovaToast,ionicDatePicker,$ionicHistory){

       $scope.v   =   {};
        //Method to initialize the controller
        $scope.init   =   function(){
          $timeout(function () {
              $rootScope.is_big_bar   =   true;
          }, 10);
          $scope.variable();
          $scope.get_diabates_type();
        }

        $scope.set_range = function(is_set){
          var v   =   $scope.v;
          v.range_start_ar = [];
          v.range_end_ar = [];

          if(v.range_unit == 'mg_dl'){
            if(is_set == undefined){
              v.choosen_start   =   parseInt(sprintf('%0.1f',parseFloat(v.choosen_start)*18))+'';
              v.choosen_end   =   parseInt(sprintf('%0.1f',parseFloat(v.choosen_end)*18))+'';
            }


            for(var i=v.range_start;i<parseInt(v.choosen_end);i++){
                v.range_start_ar.push(i);
            }

            for(var i=parseInt(v.choosen_start);i<=v.range_end;i++){
                v.range_end_ar.push(i);
            }
          } else {
            v.choosen_start   =   sprintf('%0.1f',parseFloat(v.choosen_start)/18)+'';
            v.choosen_end   =   sprintf('%0.1f',parseFloat(v.choosen_end)/18)+'';

            for(var i=v.range_start/18;i<parseFloat(v.choosen_end);i = i+0.1){
                v.range_start_ar.push(sprintf('%0.1f',i));
            }

            for(var i=parseFloat(v.choosen_start);i<=v.range_end/18;i = i+0.1){
                v.range_end_ar.push(sprintf('%0.1f',i));
            }
            //
          }

          console.log([v.range_start_ar,v.range_end_ar]);
        }

        $scope.refactor_range = function(is_set){
          var v   =   $scope.v;
          v.range_start_ar = [];
          v.range_end_ar = [];

          if(v.range_unit == 'mg_dl'){
            if(is_set == undefined){
              v.choosen_start   =   parseInt(sprintf('%0.1f',parseFloat(v.choosen_start)))+'';
              v.choosen_end   =   parseInt(sprintf('%0.1f',parseFloat(v.choosen_end)))+'';
            }


            for(var i=v.range_start;i<parseInt(v.choosen_end);i++){
                v.range_start_ar.push(i);
            }

            for(var i=parseInt(v.choosen_start)+1;i<=v.range_end;i++){
                v.range_end_ar.push(i);
            }
          } else {
            v.choosen_start   =   sprintf('%0.1f',parseFloat(v.choosen_start))+'';
            v.choosen_end   =   sprintf('%0.1f',parseFloat(v.choosen_end))+'';

            for(var i=v.range_start/18;i<parseFloat(v.choosen_end);i = i+0.1){
                v.range_start_ar.push(sprintf('%0.1f',i));
            }

            for(var i=(parseFloat(v.choosen_start)+0.1);i<=v.range_end/18;i = i+0.1){
                v.range_end_ar.push(sprintf('%0.1f',i));
            }
            //
          }

          console.log([v.range_start_ar,v.range_end_ar]);
        }

        //variable
        $scope.variable   = function(){
          var v   =   $scope.v;
          // $scope.v.diabetes_type    =   [];
          v.range_start       =   60;
          v.range_end       =   220;

          v.choosen_start   =   '82';
          v.choosen_end   =   '109';
          v.range_start_ar = [];
          v.range_end_ar = [];
          v.range_unit  =   'mg_dl';
        }



        $scope.get_diabates_type  =   function(){
          var v   =   $scope.v;
          var diabetes_type  = Core.diabetes().get_user_diabetes_type();
          if(diabetes_type == 'gestational_diabetes'){
            v.range_start       =   80;
            v.range_end       =   140;

            v.choosen_start   =   '82';
            v.choosen_end   =   '109';
            $scope.set_range(true);
          } else {
            $scope.set_range(true);
          }
        }


        $scope.set_target_range =   function(){
          var v   =   $scope.v;

          Core.diabetes().update_target_range(v.range_unit,v.range_start,v.range_end).then(function(){
            // $cordovaToast.show('Updated','short','bottom');
            $state.go('therephy_type');
          },function(error){
            $cordovaToast.show(error.message,'short','center');
          });
        }

        $scope.go_back  =   function(){
          $ionicHistory.goBack();
          // $state.go('diabetes_type');
        }



        $scope.init();
    })

    /*
     |-------------------------------------------------------------------
     | DiabetesTherephyController - Controller for user goal
     |-------------------------------------------------------------------
     |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
     |  @date:  2016-09-05
     */
     .controller('DiabetesTherephyController',function($rootScope,$scope,$state,$ionicPlatform,Core,
       $timeout,$ionicLoading,$cordovaToast,ionicDatePicker,$ionicHistory){

         $scope.v   =   {};
          //Method to initialize the controller
          $scope.init   =   function(){
            $scope.variable();
            $scope.fetch_therephy();
          }

          //variable
          $scope.variable   = function(){
            var v   =   $scope.v;
            $scope.v.therephy_type    =   [];
          }

          //fetching the goal
          $scope.fetch_therephy  =   function(){
            // alert(111);
            Core.diabetes().fetch_therephy_type().then(function(data){
              $scope.v.therephy_type  =   data.message;
              // console.log(data);
            });
          }

          $scope.go_back  =   function(){
            $ionicHistory.goBack();
            // $state.go('user_goal');
          }


          //Method is used to select the goal
          $scope.select_type  =   function(){
            var therephy_type   =   $scope.v.therephy_type;
            var choosen_types  = [];
            angular.forEach(therephy_type,function(type){
                if(type.is_checked){
                  choosen_types.push(type.id);
                }
            });

            if(choosen_types.length <= 0){
              $cordovaToast.show('Please choose the therephy type.','short','center');
              return;
            }

            Core.diabetes().update_therephy_type(choosen_types).then(function(){
              // $cordovaToast.show('Updated','short','bottom');

              var user_goal   =   Core.goal().fetch_user_goal();
              if(user_goal.indexOf('hypertension') >= 0){
                $state.go('hypertension');
              } else {
                $state.go('emergency_contact');
              }
              // $state.go('diabetes_range');
            },function(error){
              $cordovaToast.show(error.message,'short','center');
            });
          }

          $scope.init();
      })

/*
 |-------------------------------------------------------------------
 | HyperTenstionController - Controller for hypertension
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
 .controller('HyperTenstionController',function($rootScope,$scope,$state,$ionicPlatform,Core,
   $timeout,$ionicLoading,$cordovaToast,ionicDatePicker,$ionicHistory){

     $scope.v    =   {};

     //Method to initialize the variable
     $scope.variable   =   function(){
       var v   =   $scope.v;
       v.info   =   {
         'systolic_unit':'MMHG',
         'systolic':'',
         'diastolic_unit':'MMHG',
         'diastolic':'',
         'pulse_unit':'BPM',
         'pulse':''
       };
     }



     //Fecthing the loged in user
     $scope.fetch_user_details   =   function(){
       var v   =   $scope.v;
       var user  = Core.user().logged_in_user();
       if(user == null){
         return;
       }
      //  v.user_info.name  = user.name;
      //  v.user_info.dob    = moment(user.dob).format( 'Do MMM,YYYY');
      //  v.user_info.gender   = user.gender;
      //  v.user_info.height   = user.height;
      //  v.user_info.weight   = user.weight;
      //  v.user_info.height_unit   = user.height_unit;
      //  v.user_info.weight_unit   = user.weight_unit;
     }

     //Initialisig the init
     $scope.init    =  function(){
       $timeout(function () {
           $rootScope.is_big_bar   =   true;
       }, 10);
       $scope.variable();
       $scope.fetch_user_details();
     }

     //Inititalizing the updated user
     $scope.update_info   =   function(){
       var info   =   $scope.v.info;

       Core.hypertension().update_hypertension(info).then(function(data){
         // $cordovaToast.show(error.message,'short','bottom');
         // $cordovaToast.show('User updated','short','bottom');

         $state.go('emergency_contact');

       },function(error){
         // login_data.in_prog   =   false;
         console.log(error.message);
         $cordovaToast.show(error.message,'short','center');
       });
     }

     $scope.go_back  =   function(){
       $ionicHistory.goBack();
      //  var user_goal   =   Core.goal().fetch_user_goal();
      //  if(user_goal.indexOf('diabetes_mgmt') >= 0){
      //    $state.go('therephy_type');
      //  } else {
      //     $state.go('user_goal');
      //  }

     }

     $scope.init();
 })

 /*
  |-------------------------------------------------------------------
  | EmergencyContactController - Controller for contact
  |-------------------------------------------------------------------
  |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
  |  @date:  2016-09-05
  */
  .controller('EmergencyContactController',function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,$ionicLoading,$cordovaToast,$ionicPopup,$ionicModal,$cordovaContacts,$ionicHistory){

    $scope.v  =   {};

    $scope.v.is_overlay   =   false;
    $scope.init   =   function(){
      $timeout(function () {
        if($state.current.name != 'menu_emergency_contact'){
            $rootScope.is_big_bar   =   true;
        }
      }, 10);
      $scope.variables();
      // $scope.fetch_contacts();
      $scope.fetch_contacts();
    }

    $scope.fetch_contacts     =   function(){
        Core.emergency_contact().get_contacts().then(function(data){
          $scope.v.contacts   =   data.emergency_contacts;
        });
    }
    $scope.variables  =   function(){
      var v   =   $scope.v;
      $scope.v.contacts   =   [];
      v.is_add  =   false;

      v.add_contact_data     =   {
        'name':'',
        'phone_number':'',
        'type':'',
        'is_primary':false
      };
    }

    $scope.why_should_i_add     =   function(){
      var template  =   '<p>The purpose of complete and accurate patient record documentation is to foster quality and continuity of patient care. At HCN Core we ask for your health records and medical history to create a means of communication between you, your family members & closed ones and health service providers about health status, preventive health services, treatment, planning, and delivery of care.</p> '+
      '<p>Medical records document the history of examination, diagnosis and treatment of a patient. This information is vital for all providers involved in a patient’s care and for any subsequent new provider who assumes responsibility for the patient. Complete and accurate medical records can justify (or refute) the need for a particular treatment. </p> '+
      '<p>Medical records are the single most important evidence for the provider whenever a malpractice claim, or other inquiry, arises concerning patient care. In multi-specialty care, and in ever changing health care networks, it often required for patients to transfer to different providers, and the need for comprehensive and accurate medical records are vital to the whole treatment process.</p> ';
      var alertPopup = $ionicPopup.alert({
         title: 'Why should I add?',
         template:template
       });
    }



    $scope.open_contact_modal   =   function(i_contact){
      $ionicPlatform.ready(function(){
        $cordovaContacts.pickContact().then(function (contactPicked) {
          // $scope.contact = contactPicked;
          var v   =   $scope.v;
          if(contactPicked.phoneNumbers != null && contactPicked.phoneNumbers.length > 0){
              var contactnum  =   contactPicked.phoneNumbers[0].value;
              if(contactnum != null && contactnum != undefined){
                i_contact.name   = contactPicked.displayName;
                i_contact.phone_number  = contactnum;
              } else {
                $cordovaToast.show('No Phone number associated to this contact, choose another contact.','short','center');
              }
          } else {
            $cordovaToast.show('No Phone number associated to this contact, choose another contact.','short','center');
          }

          // console.log($scope.contact);
        });
      });
    }

    $scope.add_contact  =   function(){
      var v   =   $scope.v;
      Core.emergency_contact().add_contact(v.add_contact_data).then(function(data){
        $scope.fetch_contacts();
        v.is_add  = false;
        v.add_contact_data     =   {
          'name':'',
          'phone_number':'',
          'type':'',
          'is_primary':false
        };
      },function(error){
        console.log(error.message);
        $cordovaToast.show(error.message,'short','center');
      });
    }

    $scope.update_contact   =   function(contact){
      var v   =   $scope.v;
      Core.emergency_contact().update_contact(contact).then(function(data){
        $scope.fetch_contacts();
        v.is_add  = false;
        $scope.v.is_overlay   =   false;
      },function(error){
        console.log(error.message);
        $cordovaToast.show(error.message,'short','center');
        $scope.v.is_overlay   =   false;
      });
    }

    $scope.remove_contact   =   function(contact){
      var v   =   $scope.v;

      var confirmPopup = $ionicPopup.confirm({
       title: 'Remove Contact',
       template: 'Are you sure you want to remove Emergency Contact?'
     });

     confirmPopup.then(function(res) {
       if(res) {
         Core.emergency_contact().delete_contact(contact).then(function(data){
           $scope.fetch_contacts();
           v.is_add  = false;
         },function(error){
           console.log(error.message);
           $cordovaToast.show(error.message,'short','center');
         });
       } else {
         console.log('You are not sure');
       }
     });


    }

    $scope.go_back  =   function(){
      $ionicHistory.goBack();
      // var user_goal   =   Core.goal().fetch_user_goal();
      // if(user_goal.indexOf('hypertension') >= 0 && user_goal.indexOf('diabetes_management') >= 0){
      //   $state.go('hypertension');
      // } else if(user_goal.indexOf('hypertension') >= 0){
      //   $state.go('hypertension');
      // } else if(user_goal.indexOf('diabetes_management') >= 0){
      //   $state.go('therephy_type');
      // } else {
      //    $state.go('user_goal');
      // }
    }

    $scope.dahsboard  =   function(){
      $state.go('main_tabs.home_widget_tab.dashboard');
    }
    $scope.init();
  })

  /*
   |-------------------------------------------------------------------
   | LogWidgetController - Controller for log
   |-------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */

  .controller('LogWidgetController',function($rootScope,$scope,$state,$ionicPlatform,Core,$timeout){

    $scope.$state    =  $state;
    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
    }

    $scope.init();
  })

  /*
   |-------------------------------------------------------------------
   | LogWeightWidgetController - Controller for log
   |-------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */

  .controller('LogWeightWidgetController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup){

    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();


    }

    $scope.variables   =  function(){
        var v   =   $scope.v;
        v.is_check_enabled  =   false;
        v.weight_log_input  =  {
            'existing_weight':'',
            'weight':'',
            'weight_unit':'KG',
            'log_date':'',
            'log_date_view':'',
            'log_time':'',
            'log_time_view':'',
        };

        v.weight_log_input.log_date     =   moment().format('YYYY-MM-DD');

        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');
        $scope.revalidate_time(time);

        var existing_weight   = Core.user_profile().get_user_weight();
        // cons
        if(existing_weight != null){
          v.weight_log_input.existing_weight   =   existing_weight.weight;
          v.weight_log_input.weight_unit   =   existing_weight.weight_unit;
        }

    }

    $scope.validate_for_check   =   function(){
      var v   =   $scope.v;
      if(v.weight_log_input.weight == null || v.weight_log_input.weight.length == 0){
        v.is_check_enabled  =   false;
        return;
      }
      v.is_check_enabled  =   true;
    }

    $scope.open_datepicker  =   function(){
      // alert(111);
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){
          v.weight_log_input.log_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
          // console.log(val)
        },
        templateType: 'popup',
        to:new Date( moment().format('YYYY-MM-DD')),
        inputDate:new Date(moment())
      });
    }

    $scope.open_timepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
          }
        },
        inputTime: v.weight_log_input.log_time_time,   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };

      ionicTimePicker.openTimePicker(ipObj1);
    }
    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.weight_log_input.log_date_view     =   moment(v.weight_log_input.log_date).format('Do MMM,YYYY');
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.weight_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        console.log(v.weight_log_input.log_time);
        v.weight_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.weight_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
    }

    $scope.log_weight   =   function(){
      var v   =   $scope.v;
        Core.logdata().weight_log().commit(v.weight_log_input).then(function(data){
          $scope.log_success_popup('Weight Logged');
          // $cordovaToast.show("Weight Logged",'long','center');
        },function(error){
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }

    $scope.log_pop_message  =   null;
    $scope.log_popup  =   null;
    $scope.log_success_popup  =   function(message){
      $scope.log_pop_message  =   message;
      $scope.log_popup = $ionicPopup.show({
        templateUrl: 'log_sucess_popup.html',
        title: 'Successfully Logged',
        scope: $scope,
        'cssClass':'log_success_popup'
      });
    }

    $scope.close_log_success_popup  =  function(notes){
      $scope.log_popup.close();
      $timeout(function(){
        $state.go('main_tabs.log_list_widget_tab.log_widget');
      },100);
    }

    $scope.init();
  })

  /*
   |-------------------------------------------------------------------
   | LogA1cWidgetController - Controller for log of a1c Unit
   |-------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */

  .controller('LogA1cWidgetController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup){

    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();
    }

    $scope.variables   =  function(){
        var v   =   $scope.v;
        v.is_check_enabled  =   false;
        v.a1c_log_input  =  {
            'a1c':'',
            'log_date':'',
            'log_date_view':'',
            'log_time':'',
            'log_time_view':'',
            'place':'',
            'mood':'',
            'notes':''
        };

        v.a1c_log_input.log_date     =   moment().format('YYYY-MM-DD');

        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');
        $scope.revalidate_time(time);

        // var existing_weight   = Core.user().get_user_weight();
        // // cons
        // v.a1c_log_input.existing_weight   =   existing_weight.weight;
        // v.a1c_log_input.weight_unit   =   existing_weight.weight_unit;
    }

    $scope.validate_for_check   =   function(){
      var v   =   $scope.v;
      // console.log(111);
      if(v.a1c_log_input.a1c == null || v.a1c_log_input.a1c.length == 0){
        v.is_check_enabled  =   false;
        return;
      }
      v.is_check_enabled  =   true;
    }

    $scope.open_datepicker  =   function(){
      // alert(111);
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.a1c_log_input.log_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
          // console.log(val)
        },
        templateType: 'popup',
        to:new Date( moment().format('YYYY-MM-DD')),
        inputDate:new Date(moment())
      });
    }

    $scope.open_timepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
          }
        },
        inputTime: v.a1c_log_input.log_time_time,   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };

      ionicTimePicker.openTimePicker(ipObj1);
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.a1c_log_input.log_date_view     =   moment(v.a1c_log_input.log_date).format('Do MMM,YYYY');
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.a1c_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        console.log(v.a1c_log_input.log_time);
        v.a1c_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.a1c_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
    }

    $scope.log_a1c   =   function(){
      var v   =   $scope.v;
        Core.logdata().a1c_log().commit(v.a1c_log_input).then(function(data){

          $scope.log_success_popup("A1c Logged");
          // $cordovaToast.show("A1c Logged",'long','center');
          // $timeout(function(){
          //   $state.go('main_tabs.log_widget');
          // },2000);
        },function(error){
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }
    $scope.notes_popup  =   null;
    $scope.notes  =   '';
    $scope.open_note_popup  =   function(){
      $scope.notes_popup = $ionicPopup.show({
        templateUrl: 'note_popup.html',
        title: 'Notes',
        scope: $scope,
        'cssClass':'note_popup'
      });
    }

    $scope.close_notes_popup  =  function(notes){
      $scope.v.a1c_log_input.notes =  notes;
      $scope.notes_popup.close();
    }
    $scope.cancel_notes_popup  =  function(){
      $scope.notes_popup.close();
    }

    $scope.log_pop_message  =   null;
    $scope.log_popup  =   null;
    $scope.log_success_popup  =   function(message){
      $scope.log_pop_message  =   message;
      $scope.log_popup = $ionicPopup.show({
        templateUrl: 'log_sucess_popup.html',
        title: 'Successfully Logged',
        scope: $scope,
        'cssClass':'log_success_popup'
      });
    }

    $scope.close_log_success_popup  =  function(notes){
      $scope.log_popup.close();
      $timeout(function(){
        $state.go('main_tabs.log_list_widget_tab.log_widget');
      },100);
    }

    $scope.init();
  })


  /*
   |-------------------------------------------------------------------
   | LogBPWidgetController - Controller for log of BP Unit
   |-------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */

  .controller('LogBPWidgetController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup){

    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();


    }


    $scope.variables   =  function(){
        var v   =   $scope.v;
        v.is_check_enabled  =   false;
        v.bp_log_input  =  {
            'systolic_unit':'MMHG',
            'systolic':'',
            'diastolic_unit':'MMHG',
            'diastolic':'',
            'log_date':'',
            'log_date_view':'',
            'log_time':'',
            'log_time_view':'',
            'weight_unit':'',
            'weight':'',
            'pulse':'',
            'pulse_unit':'BPM',
            'mood':'',
            'notes':''
        };

        v.bp_log_input.log_date     =   moment().format('YYYY-MM-DD');

        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');
        $scope.revalidate_time(time);

        var existing_weight   = Core.user_profile().get_user_weight();
        // cons
        // v.a1c_log_input.existing_weight   =   existing_weight.weight;
        v.bp_log_input.weight_unit   =   existing_weight.weight_unit;
    }

    $scope.validate_for_check   =   function(){
      var v   =   $scope.v;
      // console.log(111);
      if(v.bp_log_input.systolic == null || v.bp_log_input.systolic.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      if(v.bp_log_input.diastolic == null || v.bp_log_input.diastolic.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      v.is_check_enabled  =   true;
    }

    $scope.open_datepicker  =   function(){
      // alert(111);
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.bp_log_input.log_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
          // console.log(val)
        },
        templateType: 'popup',
        to:new Date( moment().format('YYYY-MM-DD')),
        inputDate:new Date(moment())
      });
    }

    $scope.open_timepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
          }
        },
        inputTime: v.bp_log_input.log_time_time,   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };

      ionicTimePicker.openTimePicker(ipObj1);
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.bp_log_input.log_date_view     =   moment(v.bp_log_input.log_date).format('Do MMM,YYYY');
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.bp_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        console.log(v.bp_log_input.log_time);
        v.bp_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.bp_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
    }

    $scope.log_bp   =   function(){
      var v   =   $scope.v;
        Core.logdata().bp_log().commit(v.bp_log_input).then(function(data){
          $scope.log_success_popup('Blood Pressure Logged');
          // $cordovaToast.show("Blood Pressure Logged",'long','center');
          // $timeout(function(){
          //   $state.go('main_tabs.log_widget');
          // },2000);
        },function(error){
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }

    $scope.notes_popup  =   null;

    $scope.notes  =   '';
    $scope.open_note_popup  =   function(){
      $scope.notes_popup = $ionicPopup.show({
        templateUrl: 'note_popup.html',
        title: 'Notes',
        scope: $scope,
        'cssClass':'note_popup'
      });
    }

    $scope.close_notes_popup  =  function(notes){
      $scope.v.bp_log_input.notes =  notes;
      console.log(notes);
      $scope.notes_popup.close();
    }


    $scope.log_pop_message  =   null;
    $scope.log_popup  =   null;
    $scope.log_success_popup  =   function(message){
      $scope.log_pop_message  =   message;
      $scope.log_popup = $ionicPopup.show({
        templateUrl: 'log_sucess_popup.html',
        title: 'Successfully Logged',
        scope: $scope,
        'cssClass':'log_success_popup'
      });
    }

    $scope.close_log_success_popup  =  function(notes){
      $scope.log_popup.close();
      $timeout(function(){
        $state.go('main_tabs.log_list_widget_tab.log_widget');
      },100);
    }

    $scope.cancel_notes_popup  =  function(){
        $scope.notes_popup.close();
      }
    $scope.init();
  })


  /*
   |-------------------------------------------------------------------
   | LogMedicineWidgetController - Controller for log of Medicine
   |-------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */

  .controller('LogMedicineWidgetController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup){

    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();


    }


    $scope.variables   =  function(){
        var v   =   $scope.v;
        v.is_check_enabled  =   false;
        v.medicine_log_input  =  {
            'medicine_name':'',
            'doss_type':'',
            'doss':'',
            'log_date':'',
            'log_date_view':'',
            'log_time':'',
            'log_time_view':'',
            'mood':'',
            'notes':''
        };

        v.medicine_log_input.log_date     =   moment().format('YYYY-MM-DD');

        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');
        $scope.revalidate_time(time);

        // var existing_weight   = Core.user().get_user_weight();
        // cons
        // v.a1c_log_input.existing_weight   =   existing_weight.weight;
        // v.bp_log_input.weight_unit   =   existing_weight.weight_unit;
    }



    $scope.validate_for_check   =   function(){
      var v   =   $scope.v;
      // console.log(111);
      if(v.medicine_log_input.medicine_name == null || v.medicine_log_input.medicine_name.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      if(v.medicine_log_input.doss_type == null || v.medicine_log_input.doss_type.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      if(v.medicine_log_input.doss == null || v.medicine_log_input.doss.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      v.is_check_enabled  =   true;
    }

    $scope.open_datepicker  =   function(){
      // alert(111);
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.medicine_log_input.log_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
          // console.log(val)
        },
        templateType: 'popup',
        to:new Date( moment().format('YYYY-MM-DD')),
        inputDate:new Date(moment())
      });
    }

    $scope.open_timepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
          }
        },
        inputTime: v.medicine_log_input.log_time_time,   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };

      ionicTimePicker.openTimePicker(ipObj1);
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.medicine_log_input.log_date_view     =   moment(v.medicine_log_input.log_date).format('Do MMM,YYYY');
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.medicine_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        console.log(v.medicine_log_input.log_time);
        v.medicine_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.medicine_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
    }

    $scope.log_medicine   =   function(){
      var v   =   $scope.v;
        Core.logdata().medicine_log().commit(v.medicine_log_input).then(function(data){
          $scope.log_success_popup("Medication Logged");
          // $cordovaToast.show("Medication Logged",'long','center');
          // $timeout(function(){
          //   $state.go('main_tabs.log_widget');
          // },2000);
        },function(error){
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }

    $scope.notes_popup  =   null;

    $scope.notes  =   '';
    $scope.open_note_popup  =   function(){
      $scope.notes_popup = $ionicPopup.show({
        templateUrl: 'note_popup.html',
        title: 'Notes',
        scope: $scope,
        'cssClass':'note_popup'
      });
    }

    $scope.close_notes_popup  =  function(notes){
      $scope.v.medicine_log_input.notes =  notes;
      console.log(notes);
      $scope.notes_popup.close();
    }

    $scope.log_pop_message  =   null;
    $scope.log_popup  =   null;
    $scope.log_success_popup  =   function(message){
      $scope.log_pop_message  =   message;
      $scope.log_popup = $ionicPopup.show({
        templateUrl: 'log_sucess_popup.html',
        title: 'Successfully Logged',
        scope: $scope,
        'cssClass':'log_success_popup'
      });
    }

    $scope.close_log_success_popup  =  function(notes){
      $scope.log_popup.close();
      $timeout(function(){
        $state.go('main_tabs.log_list_widget_tab.log_widget');
      },100);
    }

    $scope.cancel_notes_popup  =  function(){
        $scope.notes_popup.close();
      }
    $scope.init();
  })

  /*
   |-------------------------------------------------------------------
   | LogActivityWidgetController - Controller for log of activity
   |-------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */

  .controller('LogActivityWidgetController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup){

    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();


    }


    $scope.variables   =  function(){
        var v   =   $scope.v;
        v.activity_search_text  =   '';
        v.is_searching  =   false;
        v.search_results    =   [];

        v.is_check_enabled  =   false;
        v.activity_log_input  =  {
            'activity_id':'',
            'activity_name':'',
            'duration':'',
            'log_date':'',
            'log_date_view':'',
            'log_time':'',
            'log_time_view':'',
        };

        v.activity_log_input.log_date     =   moment().format('YYYY-MM-DD');

        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');
        $scope.revalidate_time(time);
    }



    $scope.validate_for_check   =   function(){
      var v   =   $scope.v;
      // console.log(111);
      if(v.activity_log_input.duration == null || v.activity_log_input.duration.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      if(v.activity_log_input.activity_id == null || v.activity_log_input.activity_id.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      v.is_check_enabled  =   true;
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.activity_log_input.log_date_view     =   moment(v.activity_log_input.log_date).format('Do MMM,YYYY');
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.activity_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        console.log(v.activity_log_input.log_time);
        v.activity_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.activity_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
    }

    $scope.search_activity  =   function(){
        var v   =   $scope.v;
        v.is_searching  =   true;
        Core.logdata().activity_log().search(v.activity_search_text).then(function(data){
             v.search_results   =   data.message;
             v.is_searching  =   false;
        },function(data){
            v.is_searching  =   false;
        });
    }

    $scope.log_activity   =   function(){
      var v   =   $scope.v;
        Core.logdata().activity_log().commit(v.activity_log_input).then(function(data){
          $scope.log_success_popup("Activity Logged");
          // $cordovaToast.show("Activity Logged",'long','center');
          // $timeout(function(){
          //   $state.go('main_tabs.log_widget');
          // },2000);
        },function(error){
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }

    $scope.choose_activity  =   function(item){
      // alert(1111);
      var v   =   $scope.v;
      v.activity_log_input.activity_id   = item.id+'';
      v.activity_log_input.activity_name   = item.activity_name;
      v.activity_search_text  =   '';
      v.search_results   =   [];
      // console.log(v.activity_log_input.activity_id);
      // console.log(v.is_searching);
    }

    $scope.log_pop_message  =   null;
    $scope.log_popup  =   null;
    $scope.log_success_popup  =   function(message){
      $scope.log_pop_message  =   message;
      $scope.log_popup = $ionicPopup.show({
        templateUrl: 'log_sucess_popup.html',
        title: 'Successfully Logged',
        scope: $scope,
        'cssClass':'log_success_popup'
      });
    }

    $scope.close_log_success_popup  =  function(notes){
      $scope.log_popup.close();
      $timeout(function(){
        $state.go('main_tabs.log_list_widget_tab.log_widget');
      },100);
    }

    $scope.init();
  })

  /*
   |-------------------------------------------------------------------
   | LogFoodWidgetController - Controller for log of food
   |-------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */

  .controller('LogFoodWidgetController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$ionicLoading){

    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();


    }


    $scope.variables   =  function(){
        var v   =   $scope.v;
        v.activity_search_text  =   '';
        v.is_searching  =   false;
        v.search_results    =   [];

        v.is_check_enabled  =   false;
        v.food_log_input  =  {
            'activity_id':'',
            'meal_name':'',
            'foods':[],
            'log_date':'',
            'log_date_view':'',
            'log_time':'',
            'log_time_view':'',
            'log_time_time':'',
        };

        v.food_log_input.log_date     =   moment().format('YYYY-MM-DD');

        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');
        $scope.revalidate_time(time);
    }



    $scope.validate_for_check   =   function(){
      var v   =   $scope.v;
      console.log(111);
      if(v.food_log_input.foods.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      if(v.food_log_input.meal_name == null || v.food_log_input.meal_name.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      v.is_check_enabled  =   true;
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.food_log_input.log_date_view     =   moment(v.food_log_input.log_date).format('Do MMM,YYYY');
    }

    $scope.search_food  =   function(){
        var v   =   $scope.v;
        v.is_searching  =   true;
        Core.logdata().food_log().search(v.food_search_text).then(function(data){
             v.search_results   =   data.message;
             v.is_searching  =   false;
        },function(data){
            v.is_searching  =   false;
        });
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.food_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        // console.log(v.activity_log_input.log_time);
        v.food_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.food_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
    }



    $scope.log_food   =   function(){
      var v   =   $scope.v;
        Core.logdata().food_log().commit(v.food_log_input).then(function(data){
          // $cordovaToast.show("Food Logged",'long','center');
          $scope.log_success_popup('Food Logged');
          // $timeout(function(){
          //   $state.go('main_tabs.log_widget');
          // },2000);
        },function(error){
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }

    $scope.serving_popup  =   null;

    $scope.selected_food_servings  =   [];

    $scope.seleted_food_data  =   {
      'food_id':'',
      'food_name':'',
      'serving_unit_id':'',
      'serving':''
    };

    $scope.choose_food  =   function(food){
      // alert(1111);
      var v   =   $scope.v;

      $ionicLoading.show({
          template: '<ion-spinner icon="android"></ion-spinner>'
      });

      Core.logdata().food_log().food(food.food_id).then(function(data){
        var food  =   data.message;
        // $scope.selected_food  =   food;

        $scope.selected_food_servings   = [];
        $scope.seleted_food_data.food_id = food.food_id;
        $scope.seleted_food_data.food_name = food.food_name;

        if(food.servings.serving.serving_id != undefined){
          $scope.selected_food_servings.push(food.servings.serving);
          $scope.seleted_food_data.serving_unit_id   =   food.servings.serving.serving_id;
        } else {
          angular.forEach(food.servings.serving,function(serving,k){
            $scope.selected_food_servings.push(serving);
            if(k == 0){
              $scope.seleted_food_data.serving_unit_id   =   serving.serving_id;
            }
          });
        }


        $timeout(function(){
          $ionicLoading.hide();
          $scope.serving_popup = $ionicPopup.show({
            templateUrl: 'serving_popup.html',
            title: food.food_name,
            scope: $scope,
            'cssClass':'serving_popup'
          });
        },10);

         v.food_search_text  =   '';
         v.search_results   =   [];
      },function(data){
        $ionicLoading.hide();
          // v.is_searching  =   false;
          v.food_search_text  =   '';
          v.search_results   =   [];
      });
    }

    $scope.close_serving_popup  =   function(){
      var v   =   $scope.v;
      if($scope.seleted_food_data.serving_unit_id.length <= 0){
        $cordovaToast.show('Please select the unit','short','center');
        return;
      }

      if($scope.seleted_food_data.serving.length <= 0){
        $cordovaToast.show('Please enter quantity','short','center');
        return;
      }
      $scope.serving_popup.close();
      v.food_log_input.foods.push($scope.seleted_food_data);
      $scope.seleted_food_data  =   {
        'food_id':'',
        'serving_unit_id':'',
        'serving':''
      };
      $scope.validate_for_check();
    }

    $scope.open_datepicker  =   function(){
      // alert(111);
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.food_log_input.log_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
          // console.log(val)
        },
        templateType: 'popup',
        to:new Date( moment().format('YYYY-MM-DD')),
        inputDate:new Date(moment())
      });
    }

    $scope.open_timepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
          }
        },
        inputTime: v.food_log_input.log_time_time,   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };

      ionicTimePicker.openTimePicker(ipObj1);
    }

    $scope.log_pop_message  =   null;
    $scope.log_popup  =   null;
    $scope.log_success_popup  =   function(message){
      $scope.log_pop_message  =   message;
      $scope.log_popup = $ionicPopup.show({
        templateUrl: 'log_sucess_popup.html',
        title: 'Successfully Logged',
        scope: $scope,
        'cssClass':'log_success_popup'
      });
    }

    $scope.close_log_success_popup  =  function(notes){
      $scope.log_popup.close();
      $timeout(function(){
        $state.go('main_tabs.log_list_widget_tab.log_widget');
      },100);
    }

    $scope.init();
  })


  /*
   |-------------------------------------------------------------------
   | LogBGLWidgetController - Controller for log of food
   |-------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */

  .controller('LogBGLWidgetController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$ionicLoading){

    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();
      $scope.fetch_events();

    }


    $scope.variables   =  function(){
        var v   =   $scope.v;
        v.activity_search_text  =   '';
        v.is_searching  =   false;
        v.search_results    =   [];

        v.is_check_enabled  =   false;
        v.bgl_log_input  =  {
          'event_id':'',
          'event_name':'',
            'subevent_name':'',
            'blood_glucose_level':'',
            'unit':'mg_dl',
            'foods':[],
            'log_date':'',
            'log_date_view':'',
            'log_time':'',
            'log_time_view':'',
            'log_time_time':'',
            'place':'',
            'notes':'',
            'mood':''
        };

        v.events  =   {};
        v.bgl_log_input.log_date     =   moment().format('YYYY-MM-DD');

        var target_range = Core.diabetes().get_target_range();
        if(target_range != null){
          v.bgl_log_input.unit  =   target_range.range_unit;
          // console.log(target_range);
        }

        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');
        $scope.revalidate_time(time);

        $scope.$watch('v.bgl_log_input.unit',function(new_val,old_val){
          // console.log([new_val,old_val]);
          if(old_val == 'mg_dl'){
            if(new_val == 'mmol_l'){
              console.log($scope.v.bgl_log_input.blood_glucose_level);
              if($scope.v.bgl_log_input.blood_glucose_level != null){
                $scope.v.bgl_log_input.blood_glucose_level    =   parseFloat(sprintf('%0.1f',parseFloat($scope.v.bgl_log_input.blood_glucose_level )/18));
              }
            }
          } else{
            if(new_val == 'mg_dl'){
              console.log($scope.v.bgl_log_input.blood_glucose_level);
              if($scope.v.bgl_log_input.blood_glucose_level != null){
                $scope.v.bgl_log_input.blood_glucose_level    =   parseInt(parseFloat($scope.v.bgl_log_input.blood_glucose_level )*18);
              }
            }
          }
        });
    }

    $scope.fetch_events    =  function(){
      var v   =   $scope.v;
      Core.logdata().bgl_log().event().then(function(data){
        angular.forEach(data.message,function(evnt){
            if(evnt.parent_id == null){
              if(v.events[evnt.id] == undefined){
                evnt.child   =   [];
                if(evnt.event_name == 'Morning'){
                  evnt.icon   =   './img/bgl_morning.png';
                } else if(evnt.event_name == 'Afternoon'){
                  evnt.icon   =   './img/bgl_afternoon.png';
                } else if(evnt.event_name == 'Evening'){
                  evnt.icon   =   './img/bgl_evening.png';
                } else if(evnt.event_name == 'Night'){
                  evnt.icon   =   './img/bgl_night.png';
                }
                v.events[evnt.id]   =   evnt;

              }
                // console.log(v.events);
            } else {
              evnt.icon   = '';
              // console.log(v.events[evnt.parent_id]);
              var parent_event  = v.events[evnt.parent_id];
              if(parent_event != undefined){
                parent_event.child.push(evnt);
              }
                // console.log(v.events);
            }
        });
        console.log(v.events);
      });
    }


    $scope.validate_for_check   =   function(){
      var v   =   $scope.v;
      console.log(111);
      if(v.bgl_log_input.event_id.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      if(v.bgl_log_input.blood_glucose_level == null || v.bgl_log_input.blood_glucose_level.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      if(v.bgl_log_input.unit == null || v.bgl_log_input.unit.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      v.is_check_enabled  =   true;
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.bgl_log_input.log_date_view     =   moment(v.bgl_log_input.log_date).format('Do MMM,YYYY');
    }

    $scope.search_food  =   function(){
        var v   =   $scope.v;
        v.is_searching  =   true;
        Core.logdata().food_log().search(v.food_search_text).then(function(data){
             v.search_results   =   data.message;
             v.is_searching  =   false;
        },function(data){
            v.is_searching  =   false;
        });
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.bgl_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        // console.log(v.activity_log_input.log_time);
        v.bgl_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.bgl_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
    }



    $scope.log_bgl   =   function(){
      var v   =   $scope.v;
        Core.logdata().bgl_log().commit(v.bgl_log_input).then(function(data){
          $scope.log_success_popup('Blood glucose Level Logged');
          // $cordovaToast.show("Blood glucose Level Logged",'long','center');
          // $timeout(function(){
          //   $state.go('main_tabs.log_widget');
          // },2000);
        },function(error){
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }


    $scope.log_pop_message  =   null;
    $scope.log_popup  =   null;
    $scope.log_success_popup  =   function(message){
      $scope.log_pop_message  =   message;
      $scope.log_popup = $ionicPopup.show({
        templateUrl: 'log_sucess_popup.html',
        title: 'Successfully Logged',
        scope: $scope,
        'cssClass':'log_success_popup'
      });
    }

    $scope.close_log_success_popup  =  function(notes){
      $scope.log_popup.close();
      $timeout(function(){
        $state.go('main_tabs.log_list_widget_tab.log_widget');
      },100);
    }

    $scope.serving_popup  =   null;

    $scope.selected_food_servings  =   [];

    $scope.seleted_food_data  =   {
      'food_id':'',
      'food_name':'',
      'serving_unit_id':'',
      'serving':''
    };

    $scope.choose_food  =   function(food){
      // alert(1111);
      var v   =   $scope.v;

      $ionicLoading.show({
          template: '<ion-spinner icon="android"></ion-spinner>'
      });

      Core.logdata().food_log().food(food.food_id).then(function(data){
        var food  =   data.message;
        // $scope.selected_food  =   food;

        $scope.selected_food_servings   = [];
        $scope.seleted_food_data.food_id = food.food_id;
        $scope.seleted_food_data.food_name = food.food_name;

        if(food.servings.serving.serving_id != undefined){
          $scope.selected_food_servings.push(food.servings.serving);
          $scope.seleted_food_data.serving_unit_id   =   food.servings.serving.serving_id;
        } else {
          angular.forEach(food.servings.serving,function(serving,k){
            $scope.selected_food_servings.push(serving);
            if(k == 0){
              $scope.seleted_food_data.serving_unit_id   =   serving.serving_id;
            }
          });
        }

        // angular.forEach(food.servings.serving,function(serving,k){
        //   $scope.selected_food_servings.push(serving);
        //   if(k == 0){
        //     $scope.seleted_food_data.serving_unit_id   =   serving.serving_id;
        //   }
        // });

        $timeout(function(){
          $ionicLoading.hide();
          $scope.serving_popup = $ionicPopup.show({
            templateUrl: 'serving_popup.html',
            title: food.food_name,
            scope: $scope,
            'cssClass':'serving_popup'
          });
        },10);

         v.food_search_text  =   '';
         v.search_results   =   [];
      },function(data){
        $ionicLoading.hide();
          // v.is_searching  =   false;
          v.food_search_text  =   '';
          v.search_results   =   [];
      });
    }

    $scope.close_serving_popup  =   function(){
      var v   =   $scope.v;
      if($scope.seleted_food_data.serving_unit_id.length <= 0){
        $cordovaToast.show('Please select the unit','short','center');
        return;
      }

      if($scope.seleted_food_data.serving.length <= 0){
        $cordovaToast.show('Please enter quantity','short','center');
        return;
      }
      $scope.serving_popup.close();
      v.bgl_log_input.foods.push($scope.seleted_food_data);
      $scope.seleted_food_data  =   {
        'food_id':'',
        'serving_unit_id':'',
        'serving':''
      };
      $scope.validate_for_check();
    }

    $scope.open_datepicker  =   function(){
      // alert(111);
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.bgl_log_input.log_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
          // console.log(val)
        },
        templateType: 'popup',
        to:new Date( moment().format('YYYY-MM-DD')),
        inputDate:new Date(moment())
      });
    }

    $scope.open_timepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
          }
        },
        inputTime: v.bgl_log_input.log_time_time,   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };

      ionicTimePicker.openTimePicker(ipObj1);
    }

    $scope.notes_popup  =   null;
    $scope.notes  =   '';
    $scope.open_note_popup  =   function(){
      $scope.notes_popup = $ionicPopup.show({
        templateUrl: 'note_popup.html',
        title: 'Notes',
        scope: $scope,
        'cssClass':'note_popup'
      });
    }

    $scope.close_notes_popup  =  function(notes){
      $scope.v.bgl_log_input.notes =  notes;
      $scope.notes_popup.close();
    }
    $scope.cancel_notes_popup  =  function(){
        $scope.notes_popup.close();
      }

    $scope.events_popup  =   null;
    $scope.events  =   '';
    $scope.title  = '';

    $scope.open_event_popup  =   function(){
      $scope.title  = 'Event';
      $scope.v.selected_main_event_id    =   null;
      $scope.events_popup = $ionicPopup.show({
        templateUrl: 'event_popup.html',
        title: 'Event',
        scope: $scope,
        'cssClass':'event_popup'
      });
    }

    $scope.v.selected_main_event_id    =   null;
    $scope.sub_events   =   [];
    $scope.select_event   =   function(evnt){
      // alert(111);
      $scope.v.selected_main_event_id   =  evnt.id;
      $scope.sub_events   =  evnt.child;
      $scope.v.bgl_log_input.event_name   =   evnt.event_name;
      // $scope.$digest();
      // console.log($scope.selected_main_event_id);
    }

    $scope.close_and_restart  =   function(){
      $scope.v.selected_main_event_id    =   null;
      $scope.events_popup.close();
        $scope.open_event_popup();
    }

    $scope.choose_final_event  =  function(evnt){
      // $scope.v.bgl_log_input.notes =  notes;
      $scope.v.bgl_log_input.event_id   =   evnt.id;
      $scope.v.bgl_log_input.subevent_name   =   evnt.event_name;
      $scope.events_popup.close();
      $scope.validate_for_check();
    }

    $scope.close_event_popup  =  function(){
      // $scope.v.bgl_log_input.notes =  notes;
      $scope.events_popup.close();
    }

    $scope.init();
  })

  /*
   |-------------------------------------------------------------------
   | LogSymptomsWidgetController - Controller for log of symptoms
   |-------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */

  .controller('LogSymptomsWidgetController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup){

    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();
    }

    $scope.is_main  =  true;
    $scope.is_location  =  false;
    $scope.is_symptoms  =   false;
    $scope.is_nature_symptoms   =   false;
    $scope.is_provoking_factors   =   false;
    $scope.is_relieving_factors   =   false;
    $scope.is_timing   =   false;
    $scope.is_enviorment   =   false;

    $scope.variables   =  function(){
        var v   =   $scope.v;

        v.symptom_search_text  =   '';
        v.is_searching  =   false;
        v.search_results    =   [];

        v.is_check_enabled  =   false;
        v.symptoms_log_input  =  {
            'symptom_id':'',
            'severity':5,
            'location_ids':[],
            'related_symptom_ids':[],
            'pain_nature_ids':[],
            'provoking_ids':[],
            'relieving_ids':[],
            'log_date':'',
            'log_date_view':'',
            'log_time':'',
            'log_time_view':'',
            'start_date':'',
            'start_time':'',
            'end_date':'',
            'end_time':'',
            'notes':'',
            'enviorment_id':'',
            'frequency':'',
            'duration':''
        };

        v.symptom = {
          'location_string':'',
          'symptom_string':'',
          'provoking_string':'',
          'enviorment_string':'',
          'nature_string':'',
          'releving_string':''
        };

        v.locations     =   [];
        v.symptoms    =   [];
        v.pain_natures   =   [];
        v.provoking_factors   =   [];
        v.releving_factors   =   [];
        v.enviorments   =   [];
        v.symptoms_timing_saved   = false;
        v.symptoms_log_input.log_date     =   moment().format('YYYY-MM-DD');

        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');

        v.symptoms_log_input.start_date     =   moment().format('YYYY-MM-DD');
        v.symptoms_log_input.start_time     =   moment().format('HH:mm');
        v.symptoms_log_input.end_date     =   moment().format('YYYY-MM-DD');
        v.symptoms_log_input.end_time     =   moment().format('HH:mm');


        $scope.revalidate_time(time);
        $scope.fetch_locations();
        $scope.fetch_symptoms();
        $scope.fetch_pain_nature();
        $scope.fetch_provoking_factors();
        $scope.fetch_releving_factors();
        $scope.fetch_enviorments();

        // $scope.init_handler();
    }

    // $scope.init_handler    =  function(){
    //   $scope.$watch('v.locations',function(){
    //     console.log($scope.v.locations);
    //   });
    // }

    // $scope.$watch('is_main',function(){
    //   console.log($scope.is_main?"Hello":"name");
    // });

    $scope.fetch_locations  =   function(){
      Core.logdata().symptoms_log().symptoms_location().then(function(data){
           $scope.v.locations   =   data.message;
          //  v.is_searching  =   false;
      },function(data){
          // v.is_searching  =   false;
      });
    }

    $scope.fetch_symptoms  =   function(){
      Core.logdata().symptoms_log().symptoms().then(function(data){
           $scope.v.symptoms   =   data.message;
          //  v.is_searching  =   false;
      },function(data){
          // v.is_searching  =   false;
      });
    }

    $scope.fetch_pain_nature  =   function(){
      Core.logdata().symptoms_log().nature_pain().then(function(data){
           $scope.v.pain_natures   =   data.message;
          //  v.is_searching  =   false;
      },function(data){
          // v.is_searching  =   false;
      });
    }

    $scope.fetch_provoking_factors  =   function(){
      Core.logdata().symptoms_log().provoking_factors().then(function(data){
           $scope.v.provoking_factors   =   data.message;
          //  v.is_searching  =   false;
      },function(data){
          // v.is_searching  =   false;
      });
    }

    $scope.fetch_releving_factors  =   function(){
      Core.logdata().symptoms_log().releving_factors().then(function(data){
           $scope.v.releving_factors   =   data.message;
          //  v.is_searching  =   false;
      },function(data){
          // v.is_searching  =   false;
      });
    }

    $scope.fetch_enviorments  =   function(){
      Core.logdata().symptoms_log().enviorment().then(function(data){
           $scope.v.enviorments   =   data.message;
          //  v.is_searching  =   false;
      },function(data){
          // v.is_searching  =   false;
      });
    }

    $scope.validate_for_check   =   function(){
      var v   =   $scope.v;
      // console.log(111);
      if(v.symptoms_log_input.symptom_id == null || v.symptoms_log_input.symptom_id.length == 0){
        v.is_check_enabled  =   false;
        return;
      }

      v.is_check_enabled  =   true;
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.symptoms_log_input.log_date_view     =   moment(v.symptoms_log_input.log_date).format('Do MMM,YYYY');
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.symptoms_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        console.log(v.symptoms_log_input.log_time);
        v.symptoms_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.symptoms_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
    }

    $scope.get_e_time   = function(time){
      var time_split    =   time.split(':');
      // console.log(time_split);
      // console.log(parseInt(time_split[0])*3600 + parseInt(time_split[1])*60);
      return parseInt(time_split[0])*3600 + parseInt(time_split[1])*60;

    }

    $scope.search_symptoms  =   function(){
        var v   =   $scope.v;
        v.is_searching  =   true;
        v.symptoms_log_input.symptom_id = '';
        Core.logdata().symptoms_log().symptoms_search(v.symptom_search_text).then(function(data){
             v.search_results   =   data.message;
             angular.forEach(data.message,function(result){
               result.name =  result.name.charAt(0).toUpperCase() + result.name.slice(1);
             });
             v.is_searching  =   false;
        },function(data){
            v.is_searching  =   false;
        });
        $scope.validate_for_check();
    }

    $scope.symptoms  =   function(){
        var v   =   $scope.v;
        v.is_searching  =   true;
        v.symptoms_log_input.symptom_id = '';
        Core.logdata().symptoms_log().symptoms_search(v.symptom_search_text).then(function(data){
             v.search_results   =   data.message;
             v.is_searching  =   false;
        },function(data){
            v.is_searching  =   false;
        });
        $scope.validate_for_check();
    }

    $scope.log_symptoms   =   function(){
      var v   =   $scope.v;
        Core.logdata().symptoms_log().commit(v.symptoms_log_input).then(function(data){
          $scope.log_success_popup("Symptoms Logged");
          // $cordovaToast.show("Symptoms Logged",'long','center');
          // $timeout(function(){
          //   $state.go('main_tabs.log_widget');
          // },2000);
        },function(error){
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }

    $scope.log_pop_message  =   null;
    $scope.log_popup  =   null;
    $scope.log_success_popup  =   function(message){
      $scope.log_pop_message  =   message;
      $scope.log_popup = $ionicPopup.show({
        templateUrl: 'log_sucess_popup.html',
        title: 'Successfully Logged',
        scope: $scope,
        'cssClass':'log_success_popup'
      });
    }

    $scope.close_log_success_popup  =  function(notes){
      $scope.log_popup.close();
      $timeout(function(){
        $state.go('main_tabs.log_list_widget_tab.log_widget');
      },100);
    }

    $scope.choose_symptom  =   function(item){
      // alert(1111);
      var v   =   $scope.v;
      v.symptoms_log_input.symptom_id   = item.id;
      v.symptom_search_text   = item.name;
      // v.activity_search_text  =   '';
      v.search_results   =   [];
      $scope.validate_for_check();
    }

    $scope.show_location    =   function(status,final){
      if(status){
          $scope.is_main = false;
          $scope.is_location = true;
      } else {
        $scope.is_main = true;
        $scope.is_location = false;
        if(final != undefined){
          var location_array   =  [];
          $scope.v.symptoms_log_input.location_ids  =   [];
          angular.forEach($scope.v.locations,function(item){
            if(item.is_checked){
              $scope.v.symptoms_log_input.location_ids.push(parseInt(item.id));
              location_array.push(item.name);
            }
          });
          $scope.v.symptom.location_string  =   location_array.join(', ');
          // console.log($scope.v.symptom.location_string);
        } else {
          console.log($scope.v.symptoms_log_input.location_ids);
          angular.forEach($scope.v.locations,function(item){
            if(item.is_checked && $scope.v.symptoms_log_input.location_ids.indexOf(parseInt(item.id)) == -1){
              item.is_checked = false;
            }
          });
        }

      }
    }

    $scope.show_symptoms   =  function(status,final){
      if(status){
          $scope.is_main = false;
          $scope.is_symptoms = true;
      } else {
        $scope.is_main = true;
        $scope.is_symptoms = false;
        if(final != undefined){
          var symptom_array   =  [];
          $scope.v.symptoms_log_input.related_symptom_ids  =   [];
          angular.forEach($scope.v.symptoms,function(item){
            if(item.is_checked){
              $scope.v.symptoms_log_input.related_symptom_ids.push(parseInt(item.id));
              symptom_array.push(item.name);
            }
          });
          $scope.v.symptom.symptom_string  =   symptom_array.join(', ');
        } else {
          // console.log($scope.v.symptoms_log_input.related_symptom_ids);
          angular.forEach($scope.v.symptoms,function(item){
            if(item.is_checked && $scope.v.symptoms_log_input.related_symptom_ids.indexOf(parseInt(item.id)) == -1){
              item.is_checked = false;
            }
          });
        }
      }
    }

    $scope.show_nature_symptoms   =   function(status,final){
      if(status){
          $scope.is_main = false;
          $scope.is_nature_symptoms = true;
      } else {
        $scope.is_main = true;
        $scope.is_nature_symptoms = false;

        if(final != undefined){
          var nature_array   =  [];
          $scope.v.symptoms_log_input.pain_nature_ids  =   [];
          angular.forEach($scope.v.pain_natures,function(item){
            if(item.is_checked){
              $scope.v.symptoms_log_input.pain_nature_ids.push(parseInt(item.id));
              nature_array.push(item.name);
            }
          });
          $scope.v.symptom.nature_string  =   nature_array.join(', ');
        } else {
          // console.log($scope.v.symptoms_log_input.related_symptom_ids);
          angular.forEach($scope.v.pain_natures,function(item){
            if(item.is_checked && $scope.v.symptoms_log_input.pain_nature_ids.indexOf(parseInt(item.id)) == -1){
              item.is_checked = false;
            }
          });
        }

      }
    }

    $scope.show_provoking_factors   =   function(status,final){
      if(status){
          $scope.is_main = false;
          $scope.is_provoking_factors = true;
      } else {
        $scope.is_main = true;
        $scope.is_provoking_factors = false;

        if(final != undefined){
          var provoking_array   =  [];
          $scope.v.symptoms_log_input.provoking_ids  =   [];
          angular.forEach($scope.v.provoking_factors,function(item){
            if(item.is_checked){
              $scope.v.symptoms_log_input.provoking_ids.push(parseInt(item.id));
              provoking_array.push(item.name);
            }
          });
          $scope.v.symptom.provoking_string  =   provoking_array.join(', ');
        } else {
          // console.log($scope.v.symptoms_log_input.related_symptom_ids);
          angular.forEach($scope.v.provoking_factors,function(item){
            if(item.is_checked && $scope.v.symptoms_log_input.provoking_ids.indexOf(parseInt(item.id)) == -1){
              item.is_checked = false;
            }
          });
        }
      }
    }

    $scope.show_relieving_factors   =   function(status,final){
      if(status){
          $scope.is_main = false;
          $scope.is_relieving_factors = true;
      } else {
        $scope.is_main = true;
        $scope.is_relieving_factors = false;

        if(final != undefined){
          var releving_array   =  [];
          $scope.v.symptoms_log_input.relieving_ids  =   [];
          angular.forEach($scope.v.releving_factors,function(item){
            if(item.is_checked){
              $scope.v.symptoms_log_input.relieving_ids.push(parseInt(item.id));
              releving_array.push(item.name);
            }
          });
          $scope.v.symptom.releving_string  =   releving_array.join(', ');
        } else {
          // console.log($scope.v.symptoms_log_input.related_symptom_ids);
          angular.forEach($scope.v.releving_factors,function(item){
            if(item.is_checked && $scope.v.symptoms_log_input.relieving_ids.indexOf(parseInt(item.id)) == -1){
              item.is_checked = false;
            }
          });
        }
      }
    }


    $scope.show_enviorment  =   function(status){
      if(status){
          $scope.is_main = false;
          $scope.is_enviorment = true;
      } else {
        $scope.is_main = true;
        $scope.is_enviorment = false;
      }
    }

    $scope.show_timing  =   function(status){
      if(status){
          $scope.is_main = false;
          $scope.is_timing = true;
      } else {
        $scope.is_main = true;
        $scope.is_timing = false;
      }
    }

    $scope.notes_popup  =   null;
    $scope.notes  =   '';
    $scope.open_note_popup  =   function(){
      $scope.notes_popup = $ionicPopup.show({
        templateUrl: 'note_popup.html',
        title: 'Notes',
        scope: $scope,
        'cssClass':'note_popup'
      });
    }

    $scope.close_notes_popup  =  function(notes){
      $scope.v.symptoms_log_input.notes =  notes;
      $scope.notes_popup.close();
    }
    $scope.cancel_notes_popup  =  function(){
      $scope.notes_popup.close();
    }

    $scope.choose_enviorment_type   =   function(type){
      angular.forEach($scope.v.enviorments,function(i_type){
          if(i_type.id == type.id){
            i_type.is_checked   = true;
            $scope.v.symptoms_log_input.enviorment_id   = type.id;
            $scope.v.symptom.enviorment_string  =   type.name;

          } else {
            i_type.is_checked   = false;
          }
      });
    }

    $scope.open_startdatepicker  =   function(){
      // alert(111);
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.symptoms_log_input.start_date   =   moment(val).format('YYYY-MM-DD');
          // console.log(val)
        },
        templateType: 'popup',
        to:new Date( moment().format('YYYY-MM-DD')),
        inputDate:new Date(moment(v.symptoms_log_input.start_date))
      });
    }

    $scope.open_enddatepicker  =   function(){
      // alert(111);
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.symptoms_log_input.end_date   =   moment(val).format('YYYY-MM-DD');
          // console.log(val)
        },
        templateType: 'popup',
        from:new Date( moment(v.symptoms_log_input.start_date).format('YYYY-MM-DD')),
        to:new Date( moment().format('YYYY-MM-DD')),
        inputDate:new Date(moment(v.symptoms_log_input.end_date))
      });
    }

    $scope.open_starttimepicker  =   function(){
        var v   =   $scope.v;
        var ipObj1 = {
          callback: function (val) {      //Mandatory
            if (typeof (val) === 'undefined') {
              console.log('Time not selected');
            } else {
              var selectedTime = new Date(val * 1000);
              v.symptoms_log_input.start_time = moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('HH:mm');
            }
          },
          inputTime: $scope.get_e_time(v.symptoms_log_input.start_time),   //Optional
          format: 12,         //Optional
          step: 1,           //Optional
          setLabel: 'Set'    //Optional
        };
        ionicTimePicker.openTimePicker(ipObj1);
      }
      $scope.open_endtimepicker  =   function(){
        var v   =   $scope.v;
        var ipObj1 = {
          callback: function (val) {      //Mandatory
            if (typeof (val) === 'undefined') {
              console.log('Time not selected');
            } else {
              var selectedTime = new Date(val * 1000);
              v.symptoms_log_input.end_time = moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('HH:mm');
            }
          },
          inputTime: $scope.get_e_time(v.symptoms_log_input.end_time),   //Optional
          format: 12,         //Optional
          step: 1,           //Optional
          setLabel: 'Set'    //Optional
        };
      ionicTimePicker.openTimePicker(ipObj1);
    }

    $scope.init();
  })


  /*
   |-------------------------------------------------------------------
   | ReportController - Controller for log of symptoms
   |-------------------------------------------------------------------
   |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
   |  @date:  2016-09-05
   */

  .controller('ReportController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$ionicLoading,$cordovaFileOpener2){

      $scope.init   =   function(){
        $timeout(function () {
            $rootScope.is_big_bar   =   false;
        }, 10);
        $scope.variables();
        $scope.v.archive_reports  =   [];
        var data = Core.user_report().get_archive_reports();
        for(var i=0;i<data.length;i++){
          $scope.v.archive_reports.push(data[data.length-i-1]);
        }
      }
      $scope.v  =   {};
      $scope.variables   =   function(){
          var v   = $scope.v;
          v.report_expand   =   false;
          v.archive_reports = [];
          v.log_expand   =   false;
          v.schedule_expand   =   false;

          v.num_days  =   '7';

          v.reports  =   {
            'summary':true,
            'graph':true,
            'detail':true
          };

          v.log  =   {
            'bgl':true,
            'a1c':false,
            'bp':false,
            'activity':false,
            'symptoms':true,
            'weight':false,
            'food':false,
            'medication':false,
          };

          v.schedule  =   {
            'doctor':false,
            'lab':false,
            'activity':false,
            'walk_jog':false,
            'medication':true,
          };
      }

      $scope.toggle_report   =   function(){
        var v   = $scope.v;
        v.report_expand   =  !v.report_expand;
      }

      $scope.toggle_log   =   function(){
        var v   = $scope.v;
        v.log_expand   =  !v.log_expand;
      }

      $scope.toggle_schedule   =   function(){
        var v   = $scope.v;
        v.schedule_expand   =  !v.schedule_expand;
      }


      $scope.file_generated   =   null;
      $scope.generate_report  =   function(){
          var v   = $scope.v;
          var data  =   {
            duration:parseInt(v.num_days),
            'report_type':v.reports,
            'logs':v.log,
            'schedule':v.schedule
          };


          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });

          Core.user_report().generate_report(data).then(function(data){
            $ionicLoading.hide();
            console.log(data);
            $scope.file_generated   =   data;
            $scope.report_popup = $ionicPopup.show({
              templateUrl: 'popup_report.html',
              title: 'Successfully Generated',
              scope: $scope,
              'cssClass':'report_popup'
            });

          },function(data){
            $ionicLoading.hide();
            $ionicLoading.show({
                template: '<span>Failed to Generate Report</span>'
            });
            $timeout(function(){
              $ionicLoading.hide();
            },2000);
          });
      }

      $scope.view_file    =   function(file){
        window.open(file,'_system');
      }

      $scope.share_file   =   function(file){
        window.plugins.socialsharing.share(null, null, file);
      }
      $scope.close_popup  =   function(){
        // $scope.report_popup.hide();
        $scope.report_popup.close();
      }

      $scope.init();
  })

  .controller('DashboardController',function($scope,$rootScope,$state,$timeout,Core,$ionicHistory){
      $scope.$state   =   $state;

      $scope.analytics   =  {};

      $scope.bgl_data   =   {};
      $scope.bp_data   =   {};
      $scope.food_data   =  {};
      $scope.a1c_data   =  {};
      $scope.weight_data  =   {};
      $scope.symptom_data   = {};
      $scope.medicine_data   = {};
      $scope.acticity_data   = {};

      $scope.init   =   function(){
        $timeout(function () {
            $rootScope.is_big_bar   =   false;
        }, 10);
        $scope.fetch_analytics();
        // $ionicHistory.clearHistory();
        var user = Core.user_profile().getActiveProfile();
        if(user!=null){
          $scope.username = user.name;
          if($scope.username == null || $scope.username.length == 0){
            $scope.username = user.email;
            if($scope.username == null || $scope.username.length == 0){
              $scope.username = user.phonenumber;
            }
          }
          console.log(user.profile_media);
          if(user.profile_media != null){
            $scope.profile_image    =   user.profile_media.uri;
          }
        }
      }

      function process_data(data){
        $scope.analytics = data.analytics;
        angular.forEach($scope.analytics,function(item){
          if(item.card_name == 'BGL_STAT'){
            $scope.bgl_data   =   item.state;
          }

          if(item.card_name == 'BP_STAT'){
            $scope.bp_data   =   item.state;
          }

          if(item.card_name == 'FOOD_STAT'){
            $scope.food_data   =   item.state;
          }

          if(item.card_name == 'A1C_STAT'){
            $scope.a1c_data   =   item.state;
            $scope.a1c_data.average   =   parseFloat($scope.a1c_data.average);
            $scope.a1c_data.log_count   =   parseFloat($scope.a1c_data.log_count);
          }

          if(item.card_name == 'SYMPTOM_STAT'){
            $scope.symptom_data   =   item.state;
          }

          if(item.card_name == 'MEDICINE_STAT'){
            $scope.medicine_data   =   item.state;
          }


          if(item.card_name == 'ACTIVITY_STAT'){
            $scope.activity_data   =   item.state;
            angular.forEach($scope.activity_data,function(d){
              // console.log(d.number);
              d.number   =  sprintf('%0.1f',parseFloat(d.number)/3600);
            });
            // console.log(item);
          }

          if(item.card_name == 'WEIGHT_FULL_HORIZONTAL'){
            $scope.weight_data   =   item.weight;
            $scope.weight_data.current   =   parseFloat($scope.weight_data.current);
            $scope.weight_data.target   =   parseFloat($scope.weight_data.target);
            if($scope.weight_data.target > $scope.weight_data.current){
              $scope.weight_data.away   =   $scope.weight_data.target - $scope.weight_data.current;
            } else {
              $scope.weight_data.away   =   $scope.weight_data.current - $scope.weight_data.target;
            }
            // $scope.weight_data.away   =   parseFloat($scope.a1c_data.target);
          }
          });
      }
      $scope.fetch_analytics  =   function(){

        Core.user_analytics().get().then(function(data){
          process_data(data);
        },function(error){

        },function(update){
          // console.log(update);
          process_data(update);
        });
      }



      $scope.init();
  })
  .controller('UserProfileController',function($scope,$rootScope,$state,$timeout,Core,ionicDatePicker,$cordovaToast,$ionicHistory){
    $scope.$state   =   $state;

    $scope.v  =   {};
    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();
      $scope.fetch_user_info();
    }

    $scope.go_back  =   function(){
      // alert(1111);
      $ionicHistory.goBack();
    }

    $scope.variables    =   function(){
      var v = $scope.v;
      v.user_info   =   {
        'name':'',
        'email':'',
        'dob':'',
        'gender':'',
        'gender_view':'',
        'height':'',
        'height_unit':'',
        'weight':'',
        'weight_unit':''
      }

      v.user_life_style   =   {
        'alergies':'',
        'smoking':'NEVER',
        'smoking_freq':'0',
        'driking':'NEVER',
        'drinking_freq':'0',
        'tabacco':'NEVER',
        'tabacco_freq':'0',
        'excerise':'NEVER',
        'excerise_freq':'0',
        'job_type':'NEVER',
        'job_travel':'RARELY',
        'eating_habit':'NEVER',
        'eating_habit_freq':'DONT_KNOW',
        'last_travel':'LAST_15_DAYS',
        'where_loc':'Mumbai'
      };

      v.contact_info    =   {
        'phonr_number':'',
        'address_1':'',
        'address_2':'',
        'city':'',
        'state':'',
        'zip':''
      };
    }

    $scope.$watch('v.user_info.gender',function(){
      // console.log(111);
      if($scope.v.user_info.gender == 'MALE'){
        $scope.v.user_info.gender_view  =   'Male';
      } else {
        $scope.v.user_info.gender_view  =   'Female';
      }
    });

    $scope.fetch_user_info  = function(){
      var v   =   $scope.v;

      var user = Core.user().logged_in_user();


      v.user_info   =   {
        'name':user.name,
        'email':user.email,
        'dob':moment(user.dob).format('Do MMM,YYYY'),
        'gender':user.gender,
        'gender_view':'',
        'height':user.height,
        'height_unit':user.height_unit,
        'weight':user.weight,
        'weight_unit':user.weight_unit
      }

      v.contact_info    =   {
        'contact_num':user.contact_num,
        'address_1':user.address_1,
        'address_2':user.address_2,
        'city':user.city,
        'state':user.state,
        'zip':user.zip
      };

      v.user_life_style   =   {
        'alergies':user.alergies,
        'smoking':user.smoking==undefined||user.smoking==null?"NEVER":user.smoking,
        'smoking_freq':user.smoking_freq,
        'driking':user.drinking==undefined||user.drinking==null?"NEVER":user.drinking,
        'drinking_freq':user.drinking_freq,
        'tabacco':user.tabacco==undefined||user.drinking==null?"NEVER":user.tabacco,
        'tabacco_freq':user.tabacco_freq,
        'excerise':user.excerise==undefined||user.excerise==null?"NEVER":user.excerise,
        'excerise_freq':user.excerise_freq,
        'job_type':user.job_type==undefined||user.job_type==null?"NEVER":user.job_type,
        'job_travel':user.job_travel,
        'eating_habit':user.eating_habit==undefined||user.eating_habit==null?"NEVER":user.eating_habit,
        'eating_habit_freq':user.eating_habit_freq,
        'last_travel':user.last_travel==undefined||user.last_travel==null?"":user.last_travel,
        'where_loc':user.where_loc
      };

      if($scope.v.user_info.gender == 'MALE'){
        $scope.v.user_info.gender_view  =   'Male';
      } else {
        $scope.v.user_info.gender_view  =   'Female';
      }
      console.log(user);
    }

    $scope.update_user_info   =   function(){
        var v   =   $scope.v;
        Core.user().update_user_info(v.user_info).then(function(data){
          $cordovaToast.show("User Updated",'long','center');

        },function(error){
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }

    $scope.update_contact_info   =   function(){
        var v   =   $scope.v;
        Core.user().update_user_other_info(v.contact_info).then(function(data){
          $cordovaToast.show("User Updated",'long','center');
        },function(error){
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }

    $scope.update_lifestyle_info   =   function(){
        var v   =   $scope.v;
        Core.user().update_user_other_info(v.user_life_style).then(function(data){
          $cordovaToast.show("User Updated",'long','center');
        },function(error){
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }

    $scope.trigger_click  =   function(id){
      var event = new MouseEvent('mousedown', {
         // 'view': window,
         // 'bubbles': true,
         'cancelable': true
     });
    //  alert(id);
     var cb = document.getElementById(id);
     // cb.size=4;
     cb.dispatchEvent(event);
    }

    $scope.open_date_picker    =  function(){
      // alert(111);
      var v   =   $scope.v;
      var default_date  = '';
      if(v.user_info.dob != ''){
          default_date  =   moment(v.user_info.dob, 'Do MMM,YYYY');
      } else  {
        default_date  =   moment('1991-01-01');
      }


      ionicDatePicker.openDatePicker({
        callback:function(val){
          v.user_info.dob   =   moment(val).format('Do MMM,YYYY');
        },
        templateType: 'popup',
        to:new Date( moment().subtract(3,'years').format('YYYY-MM-DD')),
        inputDate:new Date(default_date)
      });
    }



    $scope.init();
  })

  .controller('ScheduleController',function($scope,$state){
    $scope.$state = $state;
      $scope.init   =   function(){}
  })

  /*
   |-------------------------------------------------------------------
   | ScheduleMedicationController - Controller for Schedule Medication
   |-------------------------------------------------------------------
   |  @author:  Shekh Rizwan<rizwan@hirarky.com>
   |  @date:  2016-10-07
   */
  .controller('ScheduleMedicationController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$cordovaCalendar,DateTimePickerServices){
      $scope.$state    =  $state;
      $scope.v  =   {};

      $scope.init   =   function(){
        $timeout(function () {
            $rootScope.is_big_bar   =   false;
        }, 10);
        $scope.variables();
      }

      $scope.variables   =  function(){
          var v   =   $scope.v;
          v.is_check_enabled  =   false;
          v.med_sch_input  =  {

              'sch_medicine':[],
              'medicine_name':'',
              'doss_type':'',
              'doss':'',
              'sch_date':'',
              'sch_date_view':'',
              'sch_time':'',
              'sch_time_view':'',
              'sch_time_time':'',
              'sch_repeat':'',
              'notes':'',
              'repeat_on':'',
              'is_forever':'true',
              'until':'',
              'event_count':'',
              'years':'',
              'months':'',
              'days':'',
              'hours':'',
              'minutes':'',
              'togle_on_off':false,
              'toggle_disable':false
          };

          v.repeat  =   {
            'sun':false,
            'mon':false,
            'tue':false,
            'wed':false,
            'thu':false,
            'fri':false,
            'sat':false,
            'is_forever':'',
            'popup_date_view':'',
            'until_date':'',
            'num_event':''
          };

          v.med_sch_input.sch_date     =   moment().format('YYYY-MM-DD');
          $scope.revalidate_date();

          var time      =   moment().format('YYYY-MM-DD hh:mm A');
          $scope.revalidate_time(time);
      }

      $scope.open_datepicker  =   function(){

        var v   =   $scope.v;
        ionicDatePicker.openDatePicker({
          callback:function(val){

            v.med_sch_input.sch_date   =   moment(val).format('YYYY-MM-DD');
            $scope.revalidate_date();
            // console.log(val)
          },
          templateType: 'popup',
          to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
          inputDate:new Date(moment())
        });
      }

      $scope.open_timepicker  =   function(){
        var v   =   $scope.v;
        var ipObj1 = {
          callback: function (val) {      //Mandatory
            if (typeof (val) === 'undefined') {
              console.log('Time not selected');
            } else {
              var selectedTime = new Date(val * 1000);
              $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
            }
          },
          inputTime: v.med_sch_input.sch_time_time,   //Optional
          format: 12,         //Optional
          step: 1,           //Optional
          setLabel: 'Set'    //Optional
        };

        ionicTimePicker.openTimePicker(ipObj1);
      }

      $scope.revalidate_date =  function(){
          var v   =   $scope.v;
          v.med_sch_input.sch_date_view     =   moment(v.med_sch_input.sch_date).format('Do MMM,YYYY');
      }

      $scope.revalidate_time =  function(time){
          var v   =   $scope.v;
          v.med_sch_input.sch_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
          v.med_sch_input.sch_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
          v.med_sch_input.sch_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
      }
      $scope.notes_popup  =   null;
      $scope.notes  =   '';
      $scope.open_note_popup  =   function(){
        $scope.notes_popup = $ionicPopup.show({
          templateUrl: 'note_popup.html',
          title: 'Notes',
          scope: $scope,
          'cssClass':'note_popup'
        });
      }

      $scope.close_notes_popup  =  function(notes){
        $scope.v.med_sch_input.notes =  notes;
        $scope.notes_popup.close();
      }
      $scope.cancel_notes_popup  =  function(){
        $scope.notes_popup.close();
      }


      // display repeat days popup
      $scope.repeat_days_popup  =   null;
      $scope.sch_repeat  =   '';
      $scope.open_repeat_days_popup  =   function(){
        var v   =   $scope.v;
        v.med_sch_input.togle_on_off = true;
        v.med_sch_input.toggle_disable = true;
        $scope.repeat_days_popup = $ionicPopup.show({
          templateUrl: 'repeat_days_popup.html',
          title: 'Repeat',
          scope: $scope,
          'cssClass':'repeat_days_popup'
        });
      }

      $scope.save_repeat_days_popup  =  function(){
        var v   =   $scope.v;
        if((v.repeat.sun === false)&&(v.repeat.mon === false)&&(v.repeat.tue === false)&&(v.repeat.wed === false)
          &&(v.repeat.thu === false)&&(v.repeat.fri === false)&&(v.repeat.sat === false)){
          $cordovaToast.show("Please Select Atleast One day",'short','center');
        }
        else{

        var sun;
        var mon;
        var tue;
        var wed;
        var thu;
        var fri;
        var sat;
        if(v.repeat.sun === true){
          sun = "sun,";
        }else {
          sun = "";
        }
        if(v.repeat.mon === true){
          mon = "mon,";
        }else {
          mon = "";
        }
        if(v.repeat.tue === true){
          tue = "tue,";
        }else {
          tue = "";
        }
        if(v.repeat.wed === true){
          wed = "wed,";
        }else {
          wed = "";
        }if(v.repeat.thu === true){
          thu = "thu,";
        }else {
          thu = "";
        }
        if(v.repeat.fri === true){
          fri = "fri,";
        }else {
          fri = "";
        }if(v.repeat.sat === true){
          sat = "sat,";
        }else {
          sat = "";
        }
        if(v.repeat.is_forever == 0){
          v.med_sch_input.is_forever = true;
        }

        if(v.repeat.is_forever == 1){
          v.med_sch_input.is_forever = false;
          v.med_sch_input.until = DateTimePickerServices.getDate();
        }

        if(v.repeat.is_forever == 2){
          v.med_sch_input.is_forever = false;
          v.med_sch_input.event_count = v.repeat.num_event;
        }

        $scope.repeat_days_popup.close();
        v.med_sch_input.toggle_disable = false;
        v.med_sch_input.repeat_on = sun + mon + tue + wed + thu + fri + sat;
        console.log(v.med_sch_input.repeat_on);

        if(v.med_sch_input.repeat_on != null && v.med_sch_input.repeat_on != ""){
            v.med_sch_input.togle_on_off = true;
        }else{
          v.med_sch_input.togle_on_off = false;
        }
      }
    }

      $scope.cancel_repeat_days_popup  =  function(){
        var v   =   $scope.v;
        v.med_sch_input.toggle_disable = false;
        $scope.repeat_days_popup.close();
        if(v.med_sch_input.repeat_on != null && v.med_sch_input.repeat_on != ""){
            v.med_sch_input.togle_on_off = true;
        }else{
          v.med_sch_input.togle_on_off = false;
        }
      }

      // display medicine select popup
      $scope.select_medicine_popup  =   null;
      $scope.open_medicine_popup  =   function(){
        $scope.select_medicine_popup = $ionicPopup.show({
          templateUrl: 'select_medicine_popup.html',
          title: 'Medicine 1',
          scope: $scope,
          'cssClass':'repeat_days_popup'
        });
      }

      $scope.save_medicine_popup  =  function(){
        $scope.select_medicine_popup.close();
      }

      $scope.cancel_medicine_popup  =  function(){
        $scope.select_medicine_popup.close();
      }


      // API Calling Method for Schedule Medicine
      $scope.sch_medication  =   function(){
        var v   =   $scope.v;
        v.med_sch_input.sch_medicine = [];
        v.med_sch_input.sch_medicine.push({
          'name':v.med_sch_input.medicine_name,
          'doss_type': v.med_sch_input.doss_type,
          'doss': v.med_sch_input.doss,
        })
          Core.scheduledata().schedule_medication().commit(v.med_sch_input).then(function(data){
            $cordovaToast.show("Schedule Medication Details Logged",'long','center');
            $timeout(function(){
              $state.go('main_tabs.home_widget_tab.schedule_main');
              $scope.createEvent(); // add Clender Events
            },2000);
          },function(error){
            console.log(error.message);
            $cordovaToast.show(error.message,'short','center');
          });

      }


      // add  Schedule Medicine event to the Calender
      $scope.createEvent = function() {
          var v   =   $scope.v;

          $scope.formatDate(v.med_sch_input.sch_date);
          $scope.formatTime(v.med_sch_input.sch_time);

          $cordovaCalendar.createEvent({
              title: 'Schedule Medication',
              location: 'At Home',
              notes: 'Medicine Name : '+ v.med_sch_input.medicine_name +'\nMedicine Type : '+ v.med_sch_input.doss_type +'\nDoss Value : '+ v.med_sch_input.doss,
              startDate: new Date(v.med_sch_input.years, v.med_sch_input.months-1, v.med_sch_input.days, v.med_sch_input.hours, v.med_sch_input.minutes, 0, 0, 0),
              endDate: new Date(moment(new Date(v.med_sch_input.years, v.med_sch_input.months-1, v.med_sch_input.days, v.med_sch_input.hours, v.med_sch_input.minutes, 0, 0, 0)).add(15, 'minutes').toDate())
          }).then(function (result) {
              $cordovaToast.show("Schedule Medication Event Created",'long','center');
              console.log("Event created successfully " +result);
          }, function (err) {
              console.error("There was an error: " + err);
          });
      }

      $scope.formatDate = function(date) {
        var v   =   $scope.v;
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            _month = parseInt(month);
            _day = parseInt(day);
            _year = parseInt(year);

            v.med_sch_input.years = _year;
            v.med_sch_input.months = _month;
            v.med_sch_input.days = _day;
        }

          $scope.formatTime = function(time) {
              var v   =   $scope.v;
              var parts = time.split(':');
              v.med_sch_input.hours = parts[0];
              v.med_sch_input.minutes = parts[1];
          }

          // Go back confirmation popup
           $scope.showConfirm = function() {
             var v   =   $scope.v;
             if(v.med_sch_input.medicine_name == null || v.med_sch_input.medicine_name.length <= 0){
               $state.go('main_tabs.home_widget_tab.schedule_main');
             }else {
               var confirmPopup = $ionicPopup.confirm({
                 title: 'Medication',
                 template: 'Are you sure you want to go back?'
               });
               confirmPopup.then(function(res) {
                 if(res) {
                   $state.go('main_tabs.home_widget_tab.schedule_main');
                 } else {
                   console.log('You are not sure');
                 }
               });
             }
           };

      $scope.init();
  })

  /*
   |-------------------------------------------------------------------
   | ScheduleDoctorAppoController - Controller for Schedule Doctor Appointment
   |-------------------------------------------------------------------
   |  @author:  Shekh Rizwan<rizwan@hirarky.com>
   |  @date:  2016-10-07
   */
  .controller('ScheduleDoctorAppoController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$cordovaCalendar,DateTimePickerServices){
      $scope.$state    =  $state;
      $scope.v  =   {};

      $scope.init   =   function(){
        $timeout(function () {
            $rootScope.is_big_bar   =   false;
        }, 10);
        $scope.variables();
      }

      $scope.variables   =  function(){
          var v   =   $scope.v;
          v.is_check_enabled  =   false;
          v.doc_sch_input  =  {
              'hospital_name':'',
              'doctor_name':'',
              'doc_date':'',
              'doc_date_view':'',
              'doc_time':'',
              'notify_me':'Hour_1',
              'doc_time_view':'',
              'doc_time_time':'',
              'notes':'',
              'years':'',
              'months':'',
              'days':'',
              'hours':'',
              'minutes':'',
              'follow_ups':[],
              'schedule_date':'',
              'schedule_time':'',
              'notify_before':'Hour_1',
              'follow_flag':false
          };

          v.doc_sch_input.doc_date     =   moment().format('YYYY-MM-DD');

          $scope.revalidate_date();

          var time      =   moment().format('YYYY-MM-DD hh:mm A');
          $scope.revalidate_time(time);
      }

      $scope.open_datepicker  =   function(){
        // alert(111);
        var v   =   $scope.v;
        ionicDatePicker.openDatePicker({
          callback:function(val){

            v.doc_sch_input.doc_date   =   moment(val).format('YYYY-MM-DD');
            $scope.revalidate_date();
            // console.log(val)
          },
          templateType: 'popup',
          to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
          inputDate:new Date(moment())
        });
      }

      $scope.open_timepicker  =   function(){
        var v   =   $scope.v;
        var ipObj1 = {
          callback: function (val) {      //Mandatory
            if (typeof (val) === 'undefined') {
              console.log('Time not selected');
            } else {
              var selectedTime = new Date(val * 1000);
              $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
            }
          },
          inputTime: v.doc_sch_input.doc_time_time,   //Optional
          format: 12,         //Optional
          step: 1,           //Optional
          setLabel: 'Set'    //Optional
        };

        ionicTimePicker.openTimePicker(ipObj1);
      }

      $scope.revalidate_date =  function(){
          var v   =   $scope.v;
          v.doc_sch_input.doc_date_view     =   moment(v.doc_sch_input.doc_date).format('Do MMM,YYYY');
      }

      $scope.revalidate_time =  function(time){
          var v   =   $scope.v;
          v.doc_sch_input.doc_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
          // console.log(v.a1c_log_input.log_time);
          v.doc_sch_input.doc_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
          v.doc_sch_input.doc_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
      }
      $scope.notes_popup  =   null;
      $scope.notes  =   '';
      $scope.open_note_popup  =   function(){
        $scope.notes_popup = $ionicPopup.show({
          templateUrl: 'note_popup.html',
          title: 'Notes',
          scope: $scope,
          'cssClass':'note_popup'
        });
      }

      $scope.close_notes_popup  =  function(notes){
        $scope.v.doc_sch_input.notes =  notes;
        $scope.notes_popup.close();
      }
      $scope.cancel_notes_popup  =  function(){
        $scope.notes_popup.close();
      }

      // display follow up popup
      $scope.follow_up_popup  =   null;
      $scope.open_follow_up_popup  =   function(){
        var v   =   $scope.v;
        if(v.doc_sch_input.doctor_name == null || v.doc_sch_input.doctor_name.length <= 0 ){
            $cordovaToast.show("Please Enter Doctor Name",'long','center');
            return;
        }
        $scope.follow_up_popup = $ionicPopup.show({
          templateUrl: 'follow_up_popup.html',
          title: 'First Follow Up',
          scope: $scope,
          'cssClass':'follow_ups_popup'
        });
      }

      $scope.save_follow_up_popup  =  function(){
        var v   =   $scope.v;
        v.doc_sch_input.schedule_date = DateTimePickerServices.getDate();
        v.doc_sch_input.schedule_time = DateTimePickerServices.getTime();
        v.doc_sch_input.notify_before = v.doc_sch_input.notify_before;
        console.log(v.doc_sch_input.schedule_date);
        console.log(v.doc_sch_input.schedule_time);
        console.log(v.doc_sch_input.notify_before);
        $scope.v.doc_sch_input.follow_flag = true;

        $scope.follow_up_popup.close();
      }
      $scope.cancel_follow_up_popup  =  function(){
        $scope.follow_up_popup.close();
      }

        // API Calling Method for Schedule Doctor Appointments
      $scope.sch_doctor_appointment  =   function(){
        var v   =   $scope.v;
        v.doc_sch_input.follow_ups = [];
        v.doc_sch_input.follow_ups.push({
          'schedule_date':v.doc_sch_input.schedule_date,
          'schedule_time': v.doc_sch_input.schedule_time,
          'notify_before': v.doc_sch_input.notify_before,
        })
        console.log(v.doc_sch_input.follow_ups);
          Core.scheduledata().schedule_doc_appo().commit(v.doc_sch_input).then(function(data){
            $cordovaToast.show("Doctor Appointment Details Logged",'long','center');
            $timeout(function(){
              $state.go('main_tabs.home_widget_tab.schedule_main');
              $scope.createEvent(); // add Clender Events
            },2000);
          },function(error){
            console.log(error.message);
            $cordovaToast.show(error.message,'short','center');
          });
      }

      // add  Schedule Doctor Appointments event to the Calender
      $scope.createEvent = function() {
        var v   =   $scope.v;
        $scope.formatDate(v.doc_sch_input.doc_date);
        $scope.formatTime(v.doc_sch_input.doc_time);
          $cordovaCalendar.createEvent({
              title: 'Schedule Doctor Appointments',
              location: 'Total mall',
              notes: 'Eating sandwiches',
              startDate: new Date(v.doc_sch_input.years, v.doc_sch_input.months-1, v.doc_sch_input.days, v.doc_sch_input.hours, v.doc_sch_input.minutes, 0, 0, 0),
              endDate: new Date(moment(new Date(v.doc_sch_input.years, v.doc_sch_input.months-1, v.doc_sch_input.days, v.doc_sch_input.hours, v.doc_sch_input.minutes, 0, 0, 0)).add(60, 'minutes').toDate())
          }).then(function (result) {
              $cordovaToast.show("Schedule Doctor Appointments Event Created",'long','center');
              console.log("Event created successfully " +result);
          }, function (err) {
              console.error("There was an error: " + err);
          });
      }
      $scope.formatDate = function(date) {
        var v   =   $scope.v;
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            _month = parseInt(month);
            _day = parseInt(day);
            _year = parseInt(year);

            v.doc_sch_input.years = _year;
            v.doc_sch_input.months = _month;
            v.doc_sch_input.days = _day;
        }

          $scope.formatTime = function(time) {
              var v   =   $scope.v;
              var parts = time.split(':');
              v.doc_sch_input.hours = parts[0];
              v.doc_sch_input.minutes = parts[1];
          }

          // Go back confirmation popup
           $scope.showConfirm = function() {
             var v   =   $scope.v;
             if(v.doc_sch_input.hospital_name == null || v.doc_sch_input.hospital_name.length <= 0){
               $state.go('main_tabs.home_widget_tab.schedule_main');
             }else {
               var confirmPopup = $ionicPopup.confirm({
                 title: 'Doctors Appointment',
                 template: 'Are you sure you want to go back?'
               });
               confirmPopup.then(function(res) {
                 if(res) {
                   $state.go('main_tabs.home_widget_tab.schedule_main');
                 } else {
                   console.log('You are not sure');
                 }
               });
             }
           };

      $scope.init();
  })

  /*
   |-------------------------------------------------------------------
   | ScheduleLabTestController - Controller for Schedule Lab Test
   |-------------------------------------------------------------------
   |  @author:  Shekh Rizwan<rizwan@hirarky.com>
   |  @date:  2016-10-07
   */
  .controller('ScheduleLabTestController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$cordovaCalendar){
      $scope.$state    =  $state;
      $scope.v  =   {};

      $scope.init   =   function(){
        $timeout(function () {
            $rootScope.is_big_bar   =   false;
        }, 10);
        $scope.variables();
      }

      $scope.variables   =  function(){
          var v   =   $scope.v;
          v.is_check_enabled  =   false;
          v.labtest_sch_input  =  {
              'lab_name':'',
              'test_type':'',
              'labtest_date':'',
              'labtest_date_view':'',
              'labtest_time':'',
              'labtest_time_view':'',
              'labtest_time_time':'',
              'notify_before':'Hour_1',
              'notes':'',
              'years':'',
              'months':'',
              'days':'',
              'hours':'',
              'minutes':''
          };

          v.labtest_sch_input.labtest_date     =   moment().format('YYYY-MM-DD');

          $scope.revalidate_date();

          var time      =   moment().format('YYYY-MM-DD hh:mm A');
          $scope.revalidate_time(time);
      }

      $scope.open_datepicker  =   function(){
        // alert(111);
        var v   =   $scope.v;
        ionicDatePicker.openDatePicker({
          callback:function(val){

            v.labtest_sch_input.labtest_date   =   moment(val).format('YYYY-MM-DD');
            $scope.revalidate_date();
            // console.log(val)
          },
          templateType: 'popup',
          to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
          inputDate:new Date(moment())
        });
      }

      $scope.open_timepicker  =   function(){
        var v   =   $scope.v;
        var ipObj1 = {
          callback: function (val) {      //Mandatory
            if (typeof (val) === 'undefined') {
              console.log('Time not selected');
            } else {
              var selectedTime = new Date(val * 1000);
              $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
            }
          },
          inputTime: v.labtest_sch_input.labtest_time_time,   //Optional
          format: 12,         //Optional
          step: 1,           //Optional
          setLabel: 'Set'    //Optional
        };

        ionicTimePicker.openTimePicker(ipObj1);
      }

      $scope.revalidate_date =  function(){
          var v   =   $scope.v;
          v.labtest_sch_input.labtest_date_view     =   moment(v.labtest_sch_input.labtest_date).format('Do MMM,YYYY');
      }

      $scope.revalidate_time =  function(time){
          var v   =   $scope.v;
          v.labtest_sch_input.labtest_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
          // console.log(v.a1c_log_input.log_time);
          v.labtest_sch_input.labtest_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
          v.labtest_sch_input.labtest_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
      }
      $scope.notes_popup  =   null;
      $scope.notes  =   '';
      $scope.open_note_popup  =   function(){
        $scope.notes_popup = $ionicPopup.show({
          templateUrl: 'note_popup.html',
          title: 'Notes',
          scope: $scope,
          'cssClass':'note_popup'
        });
      }

      $scope.close_notes_popup  =  function(notes){
        $scope.v.labtest_sch_input.notes =  notes;
        $scope.notes_popup.close();
      }
      $scope.cancel_notes_popup  =  function(){
        $scope.notes_popup.close();
      }

        // Schedule LabTest API Calling Method
      $scope.sch_labtest   =   function(){
        var v   =   $scope.v;
          Core.scheduledata().schedule_labtest().commit(v.labtest_sch_input).then(function(data){
            $cordovaToast.show("Lab Test Details Logged",'long','center');
            $timeout(function(){
              $state.go('main_tabs.home_widget_tab.schedule_main');
              $scope.createEvent(); // add Clender Events
            },2000);
          },function(error){
            console.log(error.message);
            $cordovaToast.show(error.message,'short','center');
          });
      }

      // add  Schedule LabTest event to the Calender
      $scope.createEvent = function() {
        var v   =   $scope.v;
        $scope.formatDate(v.labtest_sch_input.labtest_date);
        $scope.formatTime(v.labtest_sch_input.labtest_time);
          $cordovaCalendar.createEvent({
              title: 'Schedule LabTest',
              location: 'Total mall',
              notes: 'Eating sandwiches',
              startDate: new Date(v.labtest_sch_input.years, v.labtest_sch_input.months-1, v.labtest_sch_input.days, v.labtest_sch_input.hours, v.labtest_sch_input.minutes, 0, 0, 0),
              endDate: new Date(moment(new Date(v.labtest_sch_input.years, v.labtest_sch_input.months-1, v.labtest_sch_input.days, v.labtest_sch_input.hours, v.labtest_sch_input.minutes, 0, 0, 0)).add(60, 'minutes').toDate())
          }).then(function (result) {
              $cordovaToast.show("Schedule LabTest Event Created",'long','center');
              console.log("Event created successfully " +result);
          }, function (err) {
              console.error("There was an error: " + err);
          });
      }

      $scope.formatDate = function(date) {
        var v   =   $scope.v;
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            _month = parseInt(month);
            _day = parseInt(day);
            _year = parseInt(year);

            v.labtest_sch_input.years = _year;
            v.labtest_sch_input.months = _month;
            v.labtest_sch_input.days = _day;
        }

          $scope.formatTime = function(time) {
              var v   =   $scope.v;
              var parts = time.split(':');
              v.labtest_sch_input.hours = parts[0];
              v.labtest_sch_input.minutes = parts[1];
          }

          // Go back confirmation popup
           $scope.showConfirm = function() {
             var v   =   $scope.v;
             if(v.labtest_sch_input.lab_name == null || v.labtest_sch_input.lab_name.length <= 0){
               $state.go('main_tabs.home_widget_tab.schedule_main');
             }else {
               var confirmPopup = $ionicPopup.confirm({
                 title: 'Lab Test',
                 template: 'Are you sure you want to go back?'
               });
               confirmPopup.then(function(res) {
                 if(res) {
                   $state.go('main_tabs.home_widget_tab.schedule_main');
                 } else {
                   console.log('You are not sure');
                 }
               });
             }
           };

      $scope.init();
  })

  /*
   |-------------------------------------------------------------------
   | ScheduleWalkJogController - Controller for Schedule Walking Jogging
   |-------------------------------------------------------------------
   |  @author:  Shekh Rizwan<rizwan@hirarky.com>
   |  @date:  2016-10-07
   */
  .controller('ScheduleWalkJogController',function($rootScope,$scope,$state,$ionicPlatform,Core,
    $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$cordovaCalendar,DateTimePickerServices){
      $scope.$state    =  $state;
      $scope.v  =   {};

      $scope.init   =   function(){
        $timeout(function () {
            $rootScope.is_big_bar   =   false;
        }, 10);
        $scope.variables();
      }

      $scope.variables   =  function(){
          var v   =   $scope.v;
          v.is_check_enabled  =   false;
          v.walkjog_sch_input  =  {
              'activity_name':'',
              'walkjog_date':'',
              'walkjog_date_view':'',
              'walkjog_time':'',
              'walkjog_time_view':'',
              'walkjog_time_time':'',
              'notify_before':'10_minutes',
              'years':'',
              'months':'',
              'days':'',
              'hours':'',
              'minutes':'',
              'togle_on_off':false,
              'repeat_on':'',
              'is_forever':'true',
              'until':'',
              'event_count':'',
              'toggle_disable':false
          };

          v.repeat  =   {
            'sun':false,
            'mon':false,
            'tue':false,
            'wed':false,
            'thu':false,
            'fri':false,
            'sat':false,
            'is_forever':'',
            'popup_date_view':'',
            'until_date':'',
            'num_event':''
          };

          v.walkjog_sch_input.walkjog_date     =   moment().format('YYYY-MM-DD');
          v.walkjog_sch_input.activity_name = 'walk';

          $scope.revalidate_date();

          var time      =   moment().format('YYYY-MM-DD hh:mm A');
          $scope.revalidate_time(time);
      }

      $scope.open_datepicker  =   function(){
        // alert(111);
        var v   =   $scope.v;
        ionicDatePicker.openDatePicker({
          callback:function(val){

            v.walkjog_sch_input.walkjog_date   =   moment(val).format('YYYY-MM-DD');
            $scope.revalidate_date();
            // console.log(val)
          },
          templateType: 'popup',
          to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
          inputDate:new Date(moment())
        });
      }

      $scope.open_timepicker  =   function(){
        var v   =   $scope.v;
        var ipObj1 = {
          callback: function (val) {      //Mandatory
            if (typeof (val) === 'undefined') {
              console.log('Time not selected');
            } else {
              var selectedTime = new Date(val * 1000);
              $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
            }
          },
          inputTime: v.walkjog_sch_input.walkjog_time_time,   //Optional
          format: 12,         //Optional
          step: 1,           //Optional
          setLabel: 'Set'    //Optional
        };
        ionicTimePicker.openTimePicker(ipObj1);
      }

      $scope.revalidate_date =  function(){
          var v   =   $scope.v;
          v.walkjog_sch_input.walkjog_date_view     =   moment(v.walkjog_sch_input.walkjog_date).format('Do MMM,YYYY');
      }

      $scope.revalidate_time =  function(time){
          var v   =   $scope.v;
          v.walkjog_sch_input.walkjog_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
          v.walkjog_sch_input.walkjog_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
          v.walkjog_sch_input.walkjog_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
      }

      // display repeat days popup
      $scope.repeat_days_popup  =   null;
      $scope.sch_repeat  =   '';
      $scope.open_repeat_days_popup  =   function(){
        var v   =   $scope.v;
        v.walkjog_sch_input.togle_on_off = true;
        v.walkjog_sch_input.toggle_disable = true;
        $scope.repeat_days_popup = $ionicPopup.show({
          templateUrl: 'repeat_days_popup.html',
          title: 'Repeat',
          scope: $scope,
          'cssClass':'repeat_days_popup'
        });
      }

      $scope.save_repeat_days_popup  =  function(){
        var v   =   $scope.v;
        if((v.repeat.sun === false)&&(v.repeat.mon === false)&&(v.repeat.tue === false)&&(v.repeat.wed === false)
          &&(v.repeat.thu === false)&&(v.repeat.fri === false)&&(v.repeat.sat === false)){
          $cordovaToast.show("Please Select Atleast One day",'short','center');
        }
        else{

        var sun;
        var mon;
        var tue;
        var wed;
        var thu;
        var fri;
        var sat;
        if(v.repeat.sun === true){
          sun = "sun,";
        }else {
          sun = "";
        }
        if(v.repeat.mon === true){
          mon = "mon,";
        }else {
          mon = "";
        }
        if(v.repeat.tue === true){
          tue = "tue,";
        }else {
          tue = "";
        }
        if(v.repeat.wed === true){
          wed = "wed,";
        }else {
          wed = "";
        }if(v.repeat.thu === true){
          thu = "thu,";
        }else {
          thu = "";
        }
        if(v.repeat.fri === true){
          fri = "fri,";
        }else {
          fri = "";
        }if(v.repeat.sat === true){
          sat = "sat,";
        }else {
          sat = "";
        }

        if(v.repeat.is_forever == 0){
          v.walkjog_sch_input.is_forever = true;
        }

        if(v.repeat.is_forever == 1){
          v.walkjog_sch_input.is_forever = false;
          v.walkjog_sch_input.until = DateTimePickerServices.getDate();
        }

        if(v.repeat.is_forever == 2){
          v.walkjog_sch_input.is_forever = false;
          v.walkjog_sch_input.event_count = v.repeat.num_event;
        }

        $scope.repeat_days_popup.close();
        v.walkjog_sch_input.toggle_disable = false;
        v.walkjog_sch_input.repeat_on = sun + mon + tue + wed + thu + fri + sat;
        console.log(v.walkjog_sch_input.repeat_on);
        if(v.walkjog_sch_input.repeat_on != null && v.walkjog_sch_input.repeat_on != ""){
            v.walkjog_sch_input.togle_on_off = true;
        }else{
          v.walkjog_sch_input.togle_on_off = false;
        }
    }
  }

      $scope.cancel_repeat_days_popup  =  function(){
        var v   =   $scope.v;
        v.walkjog_sch_input.toggle_disable = false;
        $scope.repeat_days_popup.close();
        if(v.walkjog_sch_input.repeat_on != null && v.walkjog_sch_input.repeat_on != ""){
            v.walkjog_sch_input.togle_on_off = true;
        }else{
          v.walkjog_sch_input.togle_on_off = false;
        }
      }

        // Schedule WalkJog API Calling Method
          $scope.sch_walkjog   =   function(){
            var v   =   $scope.v;
              Core.scheduledata().schedule_walkjog().commit(v.walkjog_sch_input).then(function(data){
                $cordovaToast.show("Activity Logged",'long','center');
                $timeout(function(){
                  $state.go('main_tabs.home_widget_tab.schedule_main');
                  $scope.createEvent(); // add Clender Events
                },2000);
              },function(error){
                console.log(error.message);
                $cordovaToast.show(error.message,'short','center');
              });
          }

          // add  Schedule WalkJog event to the Calender
          $scope.createEvent = function() {
            var v   =   $scope.v;
            $scope.formatDate(v.walkjog_sch_input.walkjog_date);
            $scope.formatTime(v.walkjog_sch_input.walkjog_time);
              $cordovaCalendar.createEvent({
                  title: 'Schedule WalkJog',
                  location: 'Total mall',
                  notes: 'Eating sandwiches',
                  startDate: new Date(v.walkjog_sch_input.years, v.walkjog_sch_input.months-1, v.walkjog_sch_input.days, v.walkjog_sch_input.hours, v.walkjog_sch_input.minutes, 0, 0, 0),
                  endDate: new Date(moment(new Date(v.walkjog_sch_input.years, v.walkjog_sch_input.months-1, v.walkjog_sch_input.days, v.walkjog_sch_input.hours, v.walkjog_sch_input.minutes, 0, 0, 0)).add(60, 'minutes').toDate())
              }).then(function (result) {
                  $cordovaToast.show("Schedule WalkJog Event Created",'long','center');
                  console.log("Event created successfully " +result);
              }, function (err) {
                  console.error("There was an error: " + err);
              });
          }

          $scope.formatDate = function(date) {
            var v   =   $scope.v;
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                _month = parseInt(month);
                _day = parseInt(day);
                _year = parseInt(year);

                v.walkjog_sch_input.years = _year;
                v.walkjog_sch_input.months = _month;
                v.walkjog_sch_input.days = _day;
            }

              $scope.formatTime = function(time) {
                  var v   =   $scope.v;
                  var parts = time.split(':');
                  v.walkjog_sch_input.hours = parts[0];
                  v.walkjog_sch_input.minutes = parts[1];
              }
      $scope.init();
  })

    /*
     |-------------------------------------------------------------------
     | ScheduleActivityController - Controller for Schedule Activity
     |-------------------------------------------------------------------
     |  @author:  Shekh Rizwan<rizwan@hirarky.com>
     |  @date:  2016-10-07
     */
    .controller('ScheduleActivityController',function($rootScope,$scope,$state,$ionicPlatform,Core,
      $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$cordovaCalendar,DateTimePickerServices){
        $scope.$state    =  $state;
        $scope.v  =   {};

        $scope.init   =   function(){
          $timeout(function () {
              $rootScope.is_big_bar   =   false;
          }, 10);
          $scope.variables();
        }

        $scope.variables   =  function(){

            var v   =   $scope.v;
            v.activity_search_text  =   '';
            v.is_searching  =   false;
            v.search_results    =   [];

            v.is_check_enabled  =   false;
            v.repeat  =   {
              'sun':false,
              'mon':false,
              'tue':false,
              'wed':false,
              'thu':false,
              'fri':false,
              'sat':false,
              'is_forever':'',
              'popup_date_view':'',
              'until_date':'',
              'num_event':''
            };

            v.activity_sch_input  =  {

                'activity_id':'',
                'activity_name':'',
                'activity_date':'',
                'activity_date_view':'',
                'activity_time':'',
                'activity_time_view':'',
                'activity_time_time':'',
                'notify_before':'10_minutes',
                'years':'',
                'months':'',
                'days':'',
                'hours':'',
                'minutes':'',
                'repeat_on':'',
                'togle_on_off':false,
                'is_forever':'true',
                'until':'',
                'event_count':'',
                'toggle_disable':false
            };

            v.activity_sch_input.activity_date     =   moment().format('YYYY-MM-DD');

            $scope.revalidate_date();

            var time      =   moment().format('YYYY-MM-DD hh:mm A');
            $scope.revalidate_time(time);
        }

        $scope.open_datepicker  =   function(){
          // alert(111);
          var v   =   $scope.v;
          ionicDatePicker.openDatePicker({
            callback:function(val){

              v.activity_sch_input.activity_date   =   moment(val).format('YYYY-MM-DD');
              $scope.revalidate_date();
              // console.log(val)
            },
            templateType: 'popup',
            to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
            inputDate:new Date(moment())
          });
        }

        $scope.open_timepicker  =   function(){
          var v   =   $scope.v;
          var ipObj1 = {
            callback: function (val) {      //Mandatory
              if (typeof (val) === 'undefined') {
                console.log('Time not selected');
              } else {
                var selectedTime = new Date(val * 1000);
                $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
              }
            },
            inputTime: v.activity_sch_input.activity_time_time,   //Optional
            format: 12,         //Optional
            step: 1,           //Optional
            setLabel: 'Set'    //Optional
          };

          ionicTimePicker.openTimePicker(ipObj1);
        }

        $scope.revalidate_date =  function(){
            var v   =   $scope.v;
            v.activity_sch_input.activity_date_view     =   moment(v.activity_sch_input.activity_date).format('Do MMM,YYYY');
        }

        $scope.revalidate_time =  function(time){
            var v   =   $scope.v;
            v.activity_sch_input.activity_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
            v.activity_sch_input.activity_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
            v.activity_sch_input.activity_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
        }

        // display repeat days popup
        $scope.repeat_days_popup  =   null;
        $scope.open_repeat_days_popup  =   function(){
          var v   =   $scope.v;
          v.activity_sch_input.togle_on_off = true;
          v.activity_sch_input.toggle_disable = true;
          $scope.repeat_days_popup = $ionicPopup.show({
            templateUrl: 'repeat_days_popup.html',
            title: 'Repeat',
            scope: $scope,
            'cssClass':'repeat_days_popup'
          });
        }

        $scope.save_repeat_days_popup  =  function(){
          var v   =   $scope.v;
          if((v.repeat.sun === false)&&(v.repeat.mon === false)&&(v.repeat.tue === false)&&(v.repeat.wed === false)
            &&(v.repeat.thu === false)&&(v.repeat.fri === false)&&(v.repeat.sat === false)){
            $cordovaToast.show("Please Select Atleast One day",'short','center');
          }
          else{

          var sun;
          var mon;
          var tue;
          var wed;
          var thu;
          var fri;
          var sat;
          if(v.repeat.sun === true){
            sun = "sun,";
          }else {
            sun = "";
          }
          if(v.repeat.mon === true){
            mon = "mon,";
          }else {
            mon = "";
          }
          if(v.repeat.tue === true){
            tue = "tue,";
          }else {
            tue = "";
          }
          if(v.repeat.wed === true){
            wed = "wed,";
          }else {
            wed = "";
          }if(v.repeat.thu === true){
            thu = "thu,";
          }else {
            thu = "";
          }
          if(v.repeat.fri === true){
            fri = "fri,";
          }else {
            fri = "";
          }if(v.repeat.sat === true){
            sat = "sat,";
          }else {
            sat = "";
          }

          if(v.repeat.is_forever == 0){
            v.activity_sch_input.is_forever = true;
          }

          if(v.repeat.is_forever == 1){
            v.activity_sch_input.is_forever = false;
            v.activity_sch_input.until = DateTimePickerServices.getDate();
          }

          if(v.repeat.is_forever == 2){
            v.activity_sch_input.is_forever = false;
            v.activity_sch_input.event_count = v.repeat.num_event;
          }

          $scope.repeat_days_popup.close();
          v.activity_sch_input.toggle_disable = false;
          v.activity_sch_input.repeat_on = sun + mon + tue + wed + thu + fri + sat;
          console.log(v.activity_sch_input.repeat_on);

          if(v.activity_sch_input.repeat_on != null && v.activity_sch_input.repeat_on != ""){
              v.activity_sch_input.togle_on_off = true;
          }else{
            v.activity_sch_input.togle_on_off = false;
          }
        }
      }

        $scope.cancel_repeat_days_popup  =  function(){
          var v   =   $scope.v;
          v.activity_sch_input.toggle_disable = false;
          $scope.repeat_days_popup.close();

          if(v.activity_sch_input.repeat_on != null && v.activity_sch_input.repeat_on != ""){
              v.activity_sch_input.togle_on_off = true;
          }else{
            v.activity_sch_input.togle_on_off = false;
          }
        }

        $scope.search_activity  =   function(){
            var v   =   $scope.v;
            v.is_searching  =   true;
          Core.scheduledata().schedule_activity().search(v.activity_search_text).then(function(data){
                 v.search_results   =   data.message;
                 v.is_searching  =   false;
            },function(data){
                v.is_searching  =   false;
            });
        }

        // Schedule Activity API Calling Method
        $scope.sch_activity   =   function(){
          var v   =   $scope.v;
            Core.scheduledata().schedule_activity().commit(v.activity_sch_input).then(function(data){
              $cordovaToast.show("Activity Logged",'long','center');
              $timeout(function(){
                $state.go('main_tabs.home_widget_tab.schedule_main');
                $scope.createEvent(); // add Clender Events
              },2000);
            },function(error){
              console.log(error.message);
              $cordovaToast.show(error.message,'short','center');
            });
        }

        $scope.choose_activity  =   function(item){
          var v   =   $scope.v;
          v.activity_sch_input.activity_id   = item.id+'';
          v.activity_search_text   = item.activity_name;
          v.activity_sch_input.activity_name   = item.activity_name;
          v.search_results   =   [];

        }

        // add  Schedule Activity event to the Calender
        $scope.createEvent = function() {
          var v   =   $scope.v;
          $scope.formatDate(v.activity_sch_input.activity_date);
          $scope.formatTime(v.activity_sch_input.activity_time);
            $cordovaCalendar.createEvent({
                title: 'Schedule Activity',
                location: 'Total mall',
                notes: 'Eating sandwiches',
                startDate: new Date(v.activity_sch_input.years, v.activity_sch_input.months-1, v.activity_sch_input.days, v.activity_sch_input.hours, v.activity_sch_input.minutes, 0, 0, 0),
                endDate: new Date(moment(new Date(v.activity_sch_input.years, v.activity_sch_input.months-1, v.activity_sch_input.days, v.activity_sch_input.hours, v.activity_sch_input.minutes, 0, 0, 0)).add(60, 'minutes').toDate())
            }).then(function (result) {
                $cordovaToast.show("Schedule Activity Event Created",'long','center');
                console.log("Event created successfully " +result);
            }, function (err) {
                console.error("There was an error: " + err);
            });
        }

        $scope.formatDate = function(date) {
          var v   =   $scope.v;
          var d = new Date(date),
              month = '' + (d.getMonth() + 1),
              day = '' + d.getDate(),
              year = d.getFullYear();

              if (month.length < 2) month = '0' + month;
              if (day.length < 2) day = '0' + day;

              _month = parseInt(month);
              _day = parseInt(day);
              _year = parseInt(year);

              v.activity_sch_input.years = _year;
              v.activity_sch_input.months = _month;
              v.activity_sch_input.days = _day;
          }

            $scope.formatTime = function(time) {
                var v   =   $scope.v;
                var parts = time.split(':');
                v.activity_sch_input.hours = parts[0];
                v.activity_sch_input.minutes = parts[1];
            }
            // Go back confirmation popup
             $scope.showConfirm = function() {
               var v   =   $scope.v;
               if(v.activity_search_text == null || v.activity_search_text.length <= 0){
                 $state.go('main_tabs.home_widget_tab.schedule_main');
               }else {
                 var confirmPopup = $ionicPopup.confirm({
                   title: 'Activity',
                   template: 'Are you sure you want to go back?'
                 });
                 confirmPopup.then(function(res) {
                   if(res) {
                     $state.go('main_tabs.home_widget_tab.schedule_main');
                   } else {
                     console.log('You are not sure');
                   }
                 });
               }
             };

        $scope.init();
    })

    /*
     |-------------------------------------------------------------------
     | DateTimePickerController - Controller for set Date and Time
     |-------------------------------------------------------------------
     |  @author:  Shekh Rizwan<rizwan@hirarky.com>
     |  @date:  2016-10-12
     */
    .controller('DateTimePickerController',function($rootScope,$scope,$state,$ionicPlatform,Core,
      $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,DateTimePickerServices){
        $scope.$state    =  $state;
        $scope.v  =   {};

        $scope.init   =   function(){
          $timeout(function () {
              $rootScope.is_big_bar   =   false;
          }, 10);
          $scope.variables();
        }

        $scope.variables   =  function(){
            var v   =   $scope.v;
            v.is_check_enabled  =   false;

            v.repeat  =   {
              'popup_date_view':'',
              'popup_time_view':'',
              'popup_date':'',
              'popup_time':''
            };

            v.repeat.popup_date     =   moment().format('YYYY-MM-DD');
            $scope.revalidate_date();

            var time      =   moment().format('YYYY-MM-DD hh:mm A');
            $scope.revalidate_time(time);
        }

        $scope.open_datepicker  =   function(){
          var v   =   $scope.v;
          ionicDatePicker.openDatePicker({
            callback:function(val){

              v.repeat.popup_date   =   moment(val).format('YYYY-MM-DD');
              $scope.revalidate_date();
            },
            templateType: 'popup',
            to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
            inputDate:new Date(moment())
          });
        }

        $scope.open_timepicker  =   function(){
          var v   =   $scope.v;
          var ipObj1 = {
            callback: function (val) {      //Mandatory
              if (typeof (val) === 'undefined') {
                console.log('Time not selected');
              } else {
                var selectedTime = new Date(val * 1000);
                $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
              }
            },
            inputTime: v.repeat.popup_time_time,   //Optional
            format: 12,         //Optional
            step: 1,           //Optional
            setLabel: 'Set'    //Optional
          };

          ionicTimePicker.openTimePicker(ipObj1);
        }

        $scope.revalidate_date =  function(){
            var v   =   $scope.v;
            v.repeat.popup_date_view     =   moment(v.repeat.popup_date).format('Do MMM,YYYY');
              DateTimePickerServices.setDate(v.repeat.popup_date);
        }

        $scope.revalidate_time =  function(time){
            var v   =   $scope.v;
            v.repeat.popup_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
            v.repeat.popup_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
            v.repeat.popup_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
            DateTimePickerServices.setTime(v.repeat.popup_time);
        }
        $scope.init();
    })
    /*
     |-------------------------------------------------------------------
     | SosFeatureController - Controller for SOS Feature
     |-------------------------------------------------------------------
     |  @author:  Shekh Rizwan<rizwan@hirarky.com>
     |  @date:  2016-10-12
     */
    .controller('SosFeatureController',function($rootScope,$scope,$state,$cordovaSms,$cordovaToast ,Core,$ionicLoading){
        $scope.$state    =  $state;

        $scope.contact_numbers  =   [];
        $scope.primary_contact  =   null;

        $scope.init   =   function(){

          $scope.contacts = Core.emergency_contact().get_contacts_cached();
          console.log($scope.contacts);
          angular.forEach($scope.contacts.emergency_contacts,function(contact){
            if(contact.is_primary){
              $scope.primary_contact  =   contact.phone_number;
            }
            $scope.contact_numbers.push(contact.phone_number);
          });
        }

        // send SMS method
        $scope.sendSms = function(){
          $ionicLoading.show({
                    template: '<ion-spinner icon="android"></ion-spinner>'
                  });

          var options = {
          replaceLineBreaks: false, // true to replace \n by a new line, false by default
          android: {
            // intent: 'INTENT'  // send SMS with the default SMS app
            intent: ''        // send SMS without open any other app
          }
          };
          var count   =   0;
          angular.forEach($scope.contact_numbers,function(number){
            $cordovaSms.send(number, 'In Emergency, Please Help', options)
                  .then(function() {
                      // Success! SMS was sent
                      count++;
                      if(count == $scope.contact_numbers.length){
                        $ionicLoading.hide();
                      }
                  $cordovaToast.show('Message Sent Successfully to '+number ,'short','center');

                  }, function(error) {
                  // An error occurred
                  count++;
                  if(count == $scope.contact_numbers.length){
                    $ionicLoading.hide();
                  }
                  $cordovaToast.show('Message Not Sent','short','center');
                });//then
              });
            };

        // Phone Calling method
        $scope.makeCall = function(){
          function onSuccess(result){
              console.log("Success:"+result);
            }
          function onError(result) {
            console.log("Error:"+result);
            }
              window.plugins.CallNumber.callNumber(onSuccess, onError,$scope.primary_contact);
          };

          $scope.sos_cancel = function(){
            $state.go('main_tabs.home_widget_tab.dashboard');
          };
          $scope.init();
       })


 .controller('UserMedProfileController',function($ionicLoading,$rootScope,$scope,$state,$stateParams,$ionicHistory,Core,$cordovaToast,$timeout,$ionicPopup,$cordovaSms){

   $scope.v   =   {};

   $scope.init  =   function(){
     $scope.v   =   {
       'name':'',
       'phone_number':'',
       'relation':'',
       'profile_image':null,
       'is_new':true,
       'profile_id':''
     };

     $scope.v.name  =   $stateParams.name;
     $scope.v.phone_number  =   $stateParams.phone_number;
     $scope.fetch_med_profile($scope.v.phone_number);
   }

   $scope.fetch_med_profile  =   function(phone_number){
     Core.med_social().social_profile().show({
       'phone_number':phone_number
     },function(data){
       $scope.v.is_new  =   false;
       if(data.profile != undefined && data.profile != null){
         $scope.v.profile_id  =   data.profile.id;
         if(data.profile.profile_media != null){
            $scope.v.profile_image   =   data.profile.profile_media.uri;
         }
       }
     },function(error){
       if(error.status == 404){
         $scope.v.is_new  =   true;
       }
     });
   }

   $scope.add_request   =   function(){
     if($scope.v.profile_id == null || $scope.v.profile_id.length <= 0){
       $cordovaToast.show('Profile does not exists with us, please invite.','short','center');
       return;
     }

     if($scope.v.relation == null || $scope.v.relation.length <= 0){
       $cordovaToast.show('Please choose relation with '+$scope.v.name+'.','short','center');
       return;
     }

     Core.med_social().sent_request().commit({
       'dest_id':$scope.v.profile_id
     },{
       'action':'ADD',
       'relation':$scope.v.relation,
     },function(data){
       $cordovaToast.show('Request to monitor is sent to user','short','center');
      //  console.log(data);
      $timeout(function(){
        $state.go('med_dashboard');
      },1000);
     },function(error){
       $cordovaToast.show('You have already sent Request to '+$scope.v.phone_number,'short','center');
       if(error.data.errors.already_request != undefined){
          $cordovaToast.show(error.data.errors.already_request,'short','center');
       }
     });
   }

   $scope.invite_request   =   function(){
     if($scope.v.phone_number == null || $scope.v.phone_number.length <= 0){
       $state.go('med_dashboard');
       return;
     }

     if($scope.v.relation == null || $scope.v.relation.length <= 0){
       $cordovaToast.show('Please choose relation with '+$scope.v.name+'.','short','center');
       return;
     }

     $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
      });

     Core.med_social().send_invite().commit({
       'dest_phone_number':$scope.v.phone_number
     },{
       'action':'ADD',
       'relation':$scope.v.relation,
     },function(data){
      $timeout(function(){
        $ionicLoading.hide();
        $scope.user_invite_popup();
      },1000);
     },function(error){
       $ionicLoading.hide();
       $cordovaToast.show('You have already sent Invitation to '+$scope.v.phone_number,'short','center');
       $scope.user_invite_popup();
       if(error.data.errors.already_request != undefined){
         $cordovaToast.show(error.data.errors.already_request,'short','center');
         $cordovaToast.show('Already sent Invitation','short','center');
       }
     });
   }

   $scope.goBack  =   function(){
     $ionicHistory.goBack();
   }

   // popup for share HCNCare join invitation
   $scope.invite_popup  =   null;
   $scope.user_invite_popup  =   function(){
     $scope.invite_popup = $ionicPopup.show({
       templateUrl: 'user_invite_popup.html',
       title: 'HCNCare Invitation',
       scope: $scope,
       'cssClass':'user_invite_popup'
     });
   }

   $scope.send_msg_popup  =  function(){
     $scope.invite_popup.close();
     $timeout(function(){
      $scope.sendSms();
     },100);
   }

   $scope.share_msg_popup  =  function(){
     $scope.invite_popup.close();
     $timeout(function(){
      window.plugins.socialsharing.share('I Would like to invite you to user HCNCare', null, null, 'https://www.google.in/');
    },1000);
   }

   // send SMS method
   $scope.sendSms = function(){

     var options = {
     replaceLineBreaks: false, // true to replace \n by a new line, false by default
     android: {
       intent: 'INTENT'  // send SMS with the default SMS app
      //  intent: ''        // send SMS without open any other app
     }
     };
     var number = $scope.v.phone_number;
       $cordovaSms.send(number, 'I Would like to invite you to user HCNCare https://www.google.in/', options)
             .then(function() {
                 // Success! SMS was sent
                console.log("Successfully sent message " +number);
             }, function(error) {
             // An error occurred
             console.log("Message Not Sent");
           });//then
       };

   $scope.init();
 })

 .controller('UserMedRequestController',function($rootScope,$scope,$state,$stateParams,$ionicHistory,Core,$cordovaToast,$timeout){

   $scope.v   =   {};

   $scope.init  =   function(){
     $scope.v   =   {
       'name':'',
       'phone_number':'',
       'relation':'',
       'profile_image':null,
       'is_new':true,
       'profile_id':'',
       'profile_user_id':'',
     };

     $scope.v.name  =   $stateParams.name;
     $scope.v.phone_number  =   $stateParams.phone_number;
     $scope.v.relation  =   $stateParams.relation;
     $scope.fetch_med_profile($scope.v.phone_number);
   }

   $scope.fetch_med_profile  =   function(phone_number){
     Core.med_social().social_profile().show({
       'phone_number':phone_number
     },function(data){
       $scope.v.is_new  =   false;
       if(data.profile != undefined && data.profile != null){
         $scope.v.profile_id  =   data.profile.id;
         $scope.v.profile_user_id  =   data.profile.user_id;
         if(data.profile.profile_media != null){
            $scope.v.profile_image   =   data.profile.profile_media.uri;
            console.log($scope.v.profile_image);
         }
       }
     },function(error){
       if(error.status == 404){
         $scope.v.is_new  =   true;
       }
     });
   }

   $scope.status_update   =   function(status){
     if($scope.v.profile_id == null || $scope.v.profile_id.length <= 0){
       $cordovaToast.show('Profile does not exists with us, please invite.','short','center');
       return;
     }

     Core.med_social().status_request().commit({
       'src_id':$scope.v.profile_user_id
     },{
       'action':'STATUS_UPDATE',
       'status':status,
     },function(data){
       if(status == 'ACCEPT'){
          $cordovaToast.show($scope.v.name+' is now in your Med network','short','center');
       }
      //  console.log(data);
      $timeout(function(){
        $state.go('med_dashboard');
      },1000);
     },function(error){
       console.log("request fail");
       if(error.data.errors.already_request != undefined){
          $cordovaToast.show(error.data.errors.already_request,'short','center');
       }
     });
   }

   $scope.goBack  =   function(){
     $ionicHistory.goBack();
   }

   $scope.init();
 })

     /*
      |-------------------------------------------------------------------
      | PhrUploadController - Controller for PHR Upload Feature
      |-------------------------------------------------------------------
      |  @author:  Shekh Rizwan<rizwan@hirarky.com>
      |  @date:  2016-11-01
      */
     .controller('PhrUploadController',function($rootScope,$scope,$state,$cordovaToast ,Core,$ionicLoading,$ionicPopup,
           $cordovaImagePicker,$ionicPlatform,$cordovaCamera,$timeout,ionicDatePicker,ionicTimePicker,Media,$cordovaFileTransfer){
             $scope.$state    =  $state;
             $scope.v  =   {};
             var count = 0;
             var v   =   $scope.v;
             v.search_results    =   [];

             $scope.type_array = ["Insurance card","Laboratory Reports","Medical Prescription","Doctor Consultation"
                                  ,"Discharge Summary","Identification Records","Operation/Surgery Report",
                                  "Health Progress Report","X Ray Report","MRI Reports","CT Scan","Imaging Reports",
                                  "Medicine Bill","Hospital Bill","Other Bills","Immunization Records",
                                  "Vaccination Reports","Hospital Admission Records","Hospital Records","Hospital Card"];


             $scope.init   =   function(){
               $timeout(function () {
                   $rootScope.is_big_bar   =   false;
               }, 10);
               $scope.variables();
             }
             $scope.variables   =  function(){
                 var v   =   $scope.v;
                 v.search_results    =   [];
                 v.selected_file    =   null;
                 v.phr_upload_input  =  {
                     'event_name':'',
                     'create_event':'',
                     'event_type':'',
                     'phr_date_view':'',
                     'phr_date':'',
                     'phr_time_view':'',
                     'phr_time':'',
                     'phr_time_time':'',
                     'notes':'',
                     'media_id':[],
                     'attach_docs':false
                 };

                 v.phr_upload_input.phr_date     =   moment().format('YYYY-MM-DD');
                 $scope.revalidate_date();

                 var time      =   moment().format('YYYY-MM-DD hh:mm A');
                 $scope.revalidate_time(time);
                 $scope.phr_show_event();
             }

             $scope.open_datepicker  =   function(){
               var v   =   $scope.v;
               ionicDatePicker.openDatePicker({
                 callback:function(val){

                   v.phr_upload_input.phr_date   =   moment(val).format('YYYY-MM-DD');
                   $scope.revalidate_date();
                   // console.log(val)
                 },
                 templateType: 'popup',
                 to:new Date( moment().format('YYYY-MM-DD')),
                 inputDate:new Date(moment())
               });
             }

             $scope.open_timepicker  =   function(){
               var v   =   $scope.v;
               var ipObj1 = {
                 callback: function (val) {      //Mandatory
                   if (typeof (val) === 'undefined') {
                     console.log('Time not selected');
                   } else {
                     var selectedTime = new Date(val * 1000);
                     $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
                   }
                 },
                 inputTime: v.phr_upload_input.phr_time_time,   //Optional
                 format: 12,         //Optional
                 step: 1,           //Optional
                 setLabel: 'Set'    //Optional
               };

               ionicTimePicker.openTimePicker(ipObj1);
             }

             $scope.revalidate_date =  function(){
                 var v   =   $scope.v;
                 v.phr_upload_input.phr_date_view     =   moment(v.phr_upload_input.phr_date).format('Do MMM,YYYY');
             }

             $scope.revalidate_time =  function(time){
                 var v   =   $scope.v;
                 v.phr_upload_input.phr_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
                 v.phr_upload_input.phr_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
                 v.phr_upload_input.phr_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
             }

             // notes popup
             $scope.notes_popup  =   null;
             $scope.notes  =   '';
             $scope.open_note_popup  =   function(){
               $scope.notes_popup = $ionicPopup.show({
                 templateUrl: 'note_popup.html',
                 title: 'Notes',
                 scope: $scope,
                 'cssClass':'note_popup'
               });
             }

             $scope.close_notes_popup  =  function(notes){
               $scope.v.phr_upload_input.notes =  notes;
               $scope.notes_popup.close();
             }
             $scope.cancel_notes_popup  =  function(){
               $scope.notes_popup.close();
             }

             // Create new Event popup
             $scope.create_event_popup  =   null;
             $scope.v.new_event_name  =   '';
             $scope.open_event_popup  =   function(){
               $scope.create_event_popup = $ionicPopup.show({
                 templateUrl: 'create_event_popup.html',
                 title: 'Create a New Event',
                 scope: $scope,
                 'cssClass':'create_event_popup'
               });
             }

             $scope.save_event_popup  =  function(){
               if( $scope.event_array.indexOf($scope.v.new_event_name)  !== false){
                 $scope.event_array.push({
                  'event_name': $scope.v.new_event_name
                 });
               }
               $scope.v.phr_upload_input.event_name   =   $scope.v.new_event_name;
               $scope.create_event_popup.close();
             }
             $scope.cancel_event_popup  =  function(){
               $scope.create_event_popup.close();
             }

             // Attach Document popup
             $scope.attach_doc_popup  =   null;
             $scope.open_attach_doc_popup  =   function(){
               $scope.attach_doc_popup = $ionicPopup.show({
                 templateUrl: 'attach_doc_popup.html',
                 title: 'Attach a Document',
                 scope: $scope,
                 'cssClass':'report_popup'
               });
             }
             $scope.close_attach_doc_popup  =  function(){
               $scope.attach_doc_popup.close(); // close popup while image selection from gallery
             }

             // upload Image from Gallery
             $scope.select_file_to_upload = function(){
               $timeout(function() {
                 document.getElementById('uploaded_file').click();
               });
             }

             $scope.get_image_from_gallery   =   function(){
               $scope.close_attach_doc_popup();
                 var upload_files    =   document.getElementById('uploaded_file').files;
                 console.log(upload_files);
                 if(upload_files.length > 0){
                   if(!Media.validate_for_upload(document.getElementById('uploaded_file'))){
                     $scope.error  =   'File to be upload should be document or image.';
                     return;
                   }
                   $scope.$digest();
                   $scope.upload_media(upload_files);
                 }
             }

             // upload media method from gallery
                $scope.upload_media = function(upload_files) {
                  angular.forEach(upload_files,function(file,k){
                    Media.upload(upload_files[0],'SUBMIT_FILE',function(data){
                        console.log(['suc',data]);
                        $ionicLoading.hide();
                        $scope.$digest();
                        var response  =   JSON.parse(data.currentTarget.response);
                        var media   =   response.media;
                        count++;
                        v.selected_file  =   media.path;
                        v.search_results.push(count +" "+ v.selected_file.split('/').pop());
                        v.phr_upload_input.media_id.push(media.id);
                        v.phr_upload_input.attach_docs = true;

                        console.log( v.selected_file);
                        console.log(v.phr_upload_input.media_id);
                        $scope.$digest();
                    },function(data){
                        console.log(['error',data]);
                        $ionicLoading.hide();
                        $scope.$digest();
                        $ionicLoading.show({
                          template: '<div style="text-align:center;width:200px;" class="tf_loader" style="">Failed to Add</div>'
                        });
                        $scope.$digest();
                        $timeout(function(){
                            $ionicLoading.hide();
                            $scope.$digest();
                        },3000);

                        $scope.$digest();
                    },function(){

                    },function(progress){

                      console.log(progress);

                      var progress  =   parseInt((progress.loaded/progress.total)*100);
                      $scope.$digest();
                      $ionicLoading.show({
                        template: '<div style="text-align:center;width:200px;" class="tf_loader" style="">('+(k+1)+'/'+upload_files.length+') Uploading ('+progress+'%)... </div>'
                      });

                      $scope.$digest();
                      $timeout(function(){
                          $scope.$digest();
                      },3000);

                    });
                  });
                }

              // get Picture/Image from Camera
                $scope.get_image_from_camera   =   function(){
                $scope.close_attach_doc_popup(); // close popup while image selection from gallery
                $ionicPlatform.ready(function() {
                  var options = {
                        quality: 50,
                        destinationType: Camera.DestinationType.FILE_URI,
                        sourceType: Camera.PictureSourceType.CAMERA,
                        encodingType: Camera.EncodingType.JPEG,
                        popoverOptions: CameraPopoverOptions,
                        saveToPhotoAlbum: true,
                        correctOrientation:true
                      };

                  $cordovaCamera.getPicture(options).then(function (imageURI) {
                      count++;
                          console.log(imageURI);
                            $scope.upload_file_to_server(imageURI);

                  }, function(error) {
                      console.log('Error: ' + JSON.stringify(error));    // In case of error
                  });
                });
              }

              $scope.upload_file_to_server  =   function(path){

                var options = {
                    fileKey: "file",
                    filename: "camera_image",
                    chunkedMode: true,
                    mimeType: "image/png"
                    };

                var server_url = Core.network().api_url+'/media?type=PHR';
                console.log(server_url);
                $cordovaFileTransfer.upload(
                  server_url,path, options)
                  .then(function(result){
                    console.log("success");
                    var response  =   JSON.parse(result.response);

                    var media   =   response.media;
                    v.selected_file  =   media.path;
                    v.search_results.push(count +" "+ v.selected_file.split('/').pop());
                    v.phr_upload_input.media_id.push(media.id);
                    v.phr_upload_input.attach_docs = true;
                    }, function(err) {
                      // Error
                      console.log(err);
                    }, function (progress) {
                      // constant progress updates
                      console.log(progress);
                    });
              }

              // Phr show API Calling Method for getting Event
              $scope.event_array =[];
               $scope.phr_show_event   =   function(){
                 $ionicLoading.show({
                     template: '<ion-spinner icon="android"></ion-spinner>'
                 });

                 Core.phrdata().phr_event().get().then(function(data){
                   angular.forEach(data.data,function(data){
                           $scope.event_array.push(data);
                   });
                   $ionicLoading.hide();

                 },function(error){
                   console.log("error "+error);
                   $ionicLoading.hide();
                 },function(update){
                   console.log("update "+update);
                   $ionicLoading.hide();
                 });
               }

               // Phr Upload API Calling Method
                $scope.phr_dataupload   =   function(){
                  $ionicLoading.show({
                      template: '<ion-spinner icon="android"></ion-spinner>'
                  });

                  var v   =   $scope.v;
                    Core.phrdata().phr_upload().commit(v.phr_upload_input).then(function(data){
                        $scope.cancel_upload();

                      $ionicLoading.hide();
                      $cordovaToast.show("Successfully Uploaded",'long','center');

                      $timeout(function(){
                      },2000);

                    },function(error){
                      $ionicLoading.hide();
                      console.log(error.message);
                      $cordovaToast.show(error.message,'short','center');
                    });
                }

                $scope.cancel_upload = function() {
                  var v   =   $scope.v;
                  count = 0;
                  v.search_results = [];
                  v.phr_upload_input.attach_docs = false;
                  v.phr_upload_input  =  {
                      'event_name':'',
                      'create_event':'',
                      'event_type':'',
                      'phr_date_view':'',
                      'phr_date':'',
                      'phr_time_view':'',
                      'phr_time':'',
                      'phr_time_time':'',
                      'notes':'',
                      'media_id':[],
                      'attach_docs':false
                  };
                  v.phr_upload_input.phr_date     =   moment().format('YYYY-MM-DD');
                  $scope.revalidate_date();

                  var time      =   moment().format('YYYY-MM-DD hh:mm A');
                  $scope.revalidate_time(time);
                }

               $scope.init();
          })

      /*
       |-------------------------------------------------------------------
       | PhrRecordController - Controller for PHR Record Feature
       |-------------------------------------------------------------------
       |  @author:  Shekh Rizwan<rizwan@hirarky.com>
       |  @date:  2016-11-01
       */
      .controller('PhrRecordController',function($rootScope,$scope,$stateParams,$ionicPopup,$cordovaFileOpener2,$timeout,$state,$cordovaToast ,Core,$ionicLoading){

        $scope.$state    =  $state;
        $scope.v  =   {};
        var v   =   $scope.v;

        $scope.init   =   function(){
          if($state.current.name  == 'main_tabs.phr_widget_tab.event'){
            console.log($stateParams.event_name);
          } else {

          }
          $timeout(function () {
              $rootScope.is_big_bar   =   false;
              $scope.phr_show_event();
          }, 10);
          $scope.variables();
        }
        $scope.variables   =  function(){
            var v   =   $scope.v;
            v.phr_record_input  =  {
              'head1':'',
              'head2':'',
              'file_path':''
            };
        }
        // Phr show API Calling Method for getting Event
        $scope.event_array =[];
         $scope.phr_show_event   =   function(){
           $ionicLoading.show({
               template: '<ion-spinner icon="android"></ion-spinner>'
           });

           Core.phrdata().phr_event().get().then(function(data){
             angular.forEach(data.data,function(data){
                     $scope.event_array.push(data);
             });
             $ionicLoading.hide();

           },function(error){
             console.log("error "+error);
             $ionicLoading.hide();
           },function(update){
             console.log("update "+update);
             $ionicLoading.hide();
           });
         }

         $scope.navi_toEtype = function(para1 , para2){
           $state.go('main_tabs.phr_widget_tab.event',{'event_name':para1,'file_count':para2});

           v.phr_record_input.head1 = para1;
           v.phr_record_input.head2 = para2;
           console.log(v.phr_record_input.head1);
           console.log(v.phr_record_input.head2);
         }

        $scope.init();
       })

     /*
      |-------------------------------------------------------------------
      | PhrEventTypeController - Controller for PHR get Event Type Feature
      |-------------------------------------------------------------------
      |  @author:  Shekh Rizwan<rizwan@hirarky.com>
      |  @date:  2016-11-01
      */
      .controller('PhrEventTypeController',function($rootScope,$scope,$state,$stateParams,$timeout,$state,Core,$ionicLoading,$ionicHistory){
            $scope.$state    =  $state;
            $scope.v  =   {};

            $scope.init   =   function(){
              var v   =   $scope.v;

              $timeout(function () {
                  $rootScope.is_big_bar   =   false;
                  v.event_name =  $stateParams.event_name;
                  v.count =  $stateParams.file_count;
                   $scope.phr_show_event_type();
              }, 10);
            }

            $scope.go_back = function(){
              $ionicHistory.goBack();
            }
            $scope.eType_array =[];
             $scope.phr_show_event_type   =   function(){
               $ionicLoading.show({
                   template: '<ion-spinner icon="android"></ion-spinner>'
               });

               Core.phrdata().phr_event_type().get($scope.v.event_name).then(function(data){
                 angular.forEach(data.data,function(data){
                         $scope.eType_array.push(data);
                         console.log($scope.eType_array);
                         $ionicLoading.hide();
                 });
               },function(error){
                 console.log("error "+error);
                 $ionicLoading.hide();
               },function(update){
                 console.log("update "+update);
                 $ionicLoading.hide();
               });
             }

             $scope.navigate_to_type  =   function(type){
               $state.go('main_tabs.phr_widget_tab.event_type',{'event_name':$scope.v.event_name,'type_name':type.type});
             }
            $scope.init();

          })
        /*
         |-------------------------------------------------------------------
         | PhrEventTypeRecordController - Controller for get PHR Upload Record Feature
         |-------------------------------------------------------------------
         |  @author:  Shekh Rizwan<rizwan@hirarky.com>
         |  @date:  2016-11-01
         */
        .controller('PhrEventTypeRecordController',function($rootScope,$scope,$state,$stateParams,$timeout,$state,Core,$ionicLoading,$ionicPopup,$ionicHistory){
            $scope.$state    =  $state;
            $scope.v  =   {};

            $scope.init   =   function(){
              var v   =   $scope.v;

              $timeout(function () {
                  $rootScope.is_big_bar   =   false;
                  v.event_name =  $stateParams.event_name;
                  v.type_name =  $stateParams.type_name;
                  v.type_count =  $stateParams.type_count;
                  $scope.phr_show_media();
              }, 10);
            }

            // Phr show API Calling Method
            $scope.media_array = [];
            $scope.media_record = [];
             $scope.phr_show_media   =   function(){
               $ionicLoading.show({
                   template: '<ion-spinner icon="android"></ion-spinner>'
               });

               // API calling for getting phr media files
               Core.phrdata().phr_event_type_media().get($scope.v.event_name,$scope.v.type_name).then(function(data){

                 angular.forEach(data.phr,function(data){
                   console.log(data);
                   $scope.media_record.push(data);

                 });
                 console.log($scope.media_record);
                 $ionicLoading.hide();
               },function(error){
                 console.log("error "+error);
                  $ionicLoading.hide();
               },function(update){
                 console.log("update "+update);
                  $ionicLoading.hide();
               });
             }


             // popup for get Image/Doc details
             $scope.create_doc_details_popup  =   null;
             $scope.v.current_record_to_see   =   {};
             $scope.show_doc_details_popup  =   function(record){
                $scope.v.current_record_to_see = record;
               $scope.create_doc_details_popup = $ionicPopup.show({
                 templateUrl: 'show_doc_details_popup.html',
                 title: 'PHR DETAILS',
                 scope: $scope,
                 'cssClass':'create_event_popup'
               });
             }

             $scope.view_doc_details_popup  =  function(media_uri){
               $scope.create_doc_details_popup.close();
               $scope.view_file(media_uri);
             }
             $scope.close_doc_details_popup  =  function(){
               $scope.create_doc_details_popup.close();
             }

             // file opner method
             $scope.view_file    =   function(file){
               console.log(file);
               window.open(file,'_system');
             }

             $scope.navigate_to_back = function(){
               $ionicHistory.goBack();
             }

            $scope.init();

          })

   /*
      |-------------------------------------------------------------------
      | PhrSearchController - Controller for PHR Search Feature
      |-------------------------------------------------------------------
      |  @author:  Shekh Rizwan<rizwan@hirarky.com>
      |  @date:  2016-11-01
      */
    .controller('PhrSearchController',function($rootScope,$scope,$ionicPopup,$state,$cordovaToast,$timeout,Core,$ionicLoading,$timeout,ionicDatePicker){
       $scope.$state    =  $state;
       $scope.v  =   {};
       $scope.type_array = ["Insurance card","Laboratory Reports","Medical Prescription","Doctor Consultation"
                            ,"Discharge Summary","Identification Records","Operation/Surgery Report",
                            "Health Progress Report","X Ray Report","MRI Reports","CT Scan","Imaging Reports",
                            "Medicine Bill","Hospital Bill","Other Bills","Immunization Records",
                            "Vaccination Reports","Hospital Admission Records","Hospital Records","Hospital Card"];


       $scope.init   =   function(){
         $timeout(function () {
             $rootScope.is_big_bar   =   false;
         }, 10);
         $scope.variables();
         $scope.phr_show_event();
       }
       $scope.variables   =  function(){
           var v   =   $scope.v;
           v.phr_search_input  =  {
               'event_name':'',
               'type':'',
               'to_date_view':'',
               'to_date':'',
               'from_date_view':'',
               'from_date':'',
               'modify_data':false,
               'page_size':300
           };

           v.phr_search_input.to_date     =   moment().format('YYYY-MM-DD');
           v.phr_search_input.from_date     =   moment().subtract(7,'days').format('YYYY-MM-DD');
           $scope.revalidate_date();
       }

       $scope.event_array =[];
        $scope.phr_show_event   =   function(){
          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });
          Core.phrdata().phr_event().get().then(function(data){
            angular.forEach(data.data,function(data){
                    $scope.event_array.push(data);
            });
            $ionicLoading.hide();

          },function(error){
            console.log("error "+error);
            $ionicLoading.hide();
          },function(update){
            console.log("update "+update);
            $ionicLoading.hide();
          });
        }

       $scope.open_datepicker_to  =   function(){
         var v   =   $scope.v;
         ionicDatePicker.openDatePicker({
           callback:function(val){

             v.phr_search_input.to_date   =   moment(val).format('YYYY-MM-DD');
             $scope.revalidate_date();
             // console.log(val)
           },
           templateType: 'popup',
           to:new Date( moment().format('YYYY-MM-DD')),
           inputDate:new Date(moment())
         });
       }

       $scope.open_datepicker_from  =   function(){
         var v   =   $scope.v;
         ionicDatePicker.openDatePicker({
           callback:function(val){

             v.phr_search_input.from_date   =   moment(val).format('YYYY-MM-DD');
             $scope.revalidate_date();
             // console.log(val)
           },
           templateType: 'popup',
           to:new Date( moment().format('YYYY-MM-DD')),
           inputDate:new Date(moment())
         });
       }

       $scope.revalidate_date =  function(){
           var v   =   $scope.v;
           v.phr_search_input.to_date_view     =   moment(v.phr_search_input.to_date).format('Do MMM,YYYY');
           v.phr_search_input.from_date_view     =   moment(v.phr_search_input.from_date).format('Do MMM,YYYY');
       }

       $scope.modify_event = function(){
          var v   =   $scope.v;
          $scope.media_array = [];
          v.phr_search_input.modify_data = false;
       }

       $scope.search_event = function(){
          var v   =   $scope.v;
          v.phr_search_input.modify_data = true;
            $scope.phr_show_data();
       }

       // Phr show API Calling Method
       $scope.media_array = [];
       $scope.media_record = [];
        $scope.phr_show_data   =   function(){

          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });
          $scope.media_record     =   [];
          Core.phrdata().phr_search().get($scope.v.phr_search_input).then(function(data){
            angular.forEach(data.data,function(data){
              console.log(data);
              $scope.media_record.push(data);

            });
            console.log($scope.media_array);
              $ionicLoading.hide();
          },function(error){
            console.log("error "+error);
            $ionicLoading.hide();
          },function(update){
            console.log("update "+update);
            $ionicLoading.hide();
          });
        }

        // popup for get Image/Doc details
        $scope.create_doc_details_popup  =   null;
        $scope.v.current_record_to_see   =   {};
        $scope.show_doc_details_popup  =   function(record){
           $scope.v.current_record_to_see = record;
          $scope.create_doc_details_popup = $ionicPopup.show({
            templateUrl: 'show_doc_details_popup.html',
            title: 'PHR DETAILS',
            scope: $scope,
            'cssClass':'create_event_popup'
          });
        }

        $scope.view_doc_details_popup  =  function(media_uri){
          $scope.create_doc_details_popup.close();
          $scope.view_file(media_uri);
        }
        $scope.close_doc_details_popup  =  function(){
          $scope.create_doc_details_popup.close();
        }

        // file opner method
        $scope.view_file    =   function(file){
          console.log(file);
          window.open(file,'_system');
        }

        $scope.cleardata = function(){
          var v   =   $scope.v;
          v.phr_search_input  =  {
              'event_name':'',
              'type':'',
              'to_date_view':'',
              'to_date':'',
              'from_date_view':'',
              'from_date':'',
              'modify_data':false,
              'page_size':300
          };

          v.phr_search_input.to_date     =   moment().format('YYYY-MM-DD');
          v.phr_search_input.from_date     =   moment().subtract(7,'days').format('YYYY-MM-DD');
          $scope.revalidate_date();
        }

       $scope.init();
      })


  /*
     |-------------------------------------------------------------------
     | CommonControllerForProfileHeader - Controller for fetch USERNAME and PROFILE
     |-------------------------------------------------------------------
     |  @author:  Shekh Rizwan<rizwan@hirarky.com>
     |  @date:  2016-11-09
     */
     .controller('CommonControllerForProfile',function($scope,$rootScope,$ionicPopover,$state,$cordovaToast,$timeout,Core,$ionicLoading,$timeout){
       $scope.$state    =  $state;
       $scope.v  =   {};

       $scope.init   =   function(){
         var user = Core.user_profile().getActiveProfile();
         if(user!=null){
           $scope.username = user.name;
           if($scope.username == null || $scope.username.length == 0){
             $scope.username = user.email;
             if($scope.username == null || $scope.username.length == 0){
               $scope.username = user.phonenumber;
             }
           }
           if(user.profile_media != null){
             $scope.profile_image    =   user.profile_media.uri;
           }
         }
       }

      $ionicPopover.fromTemplateUrl('popover.html', {
      scope: $scope
       }).then(function(popover) {
          $scope.popover = popover;
       });

       $scope.openPopover = function($event) {
          $scope.popover.show($event);
       };

       $scope.closePopover = function() {
          $scope.popover.hide();
       };

       //Cleanup the popover when we're done with it!
       $scope.$on('$destroy', function() {
          $scope.popover.remove();
       });

       // Execute action on hide popover
       $scope.$on('popover.hidden', function() {
          // Execute action
       });

       // Execute action on remove popover
       $scope.$on('popover.removed', function() {
          // Execute action
       });

       $scope.goto_aboutus_page = function(){
         $state.go('about_us');
         $scope.closePopover();
       }
       $scope.goto_feedback_page = function(){
         $state.go('feedback');
         $scope.closePopover();
       }
       $scope.goto_rate_review_page = function(){
         $scope.closePopover();
       }
       $scope.goto_share_page = function(){
         $state.go('share');
         $scope.closePopover();
       }
       $scope.goto_settings_page = function(){
         $state.go('settings');
         $scope.closePopover();
       }

       $scope.init();
     })

     .controller('AboutUsController',function($scope,$rootScope,$ionicPopover,$state,$cordovaToast,$timeout,Core,$ionicLoading,$timeout){
       $scope.$state    =  $state;
     })
     .controller('FeedbackController',function($scope,$rootScope,$ionicPopover,$state,$cordovaToast,$timeout,Core,$ionicLoading,$timeout){
       $scope.$state    =  $state;
     })
     .controller('ShareAppController',function($scope,$rootScope,$ionicPopover,$state,$cordovaToast,$timeout,Core,$ionicLoading,$timeout){
       $scope.$state    =  $state;
     })

    /*
    |-------------------------------------------------------------------
    | AccountSettingsController - Controller for user account setting
    |-------------------------------------------------------------------
    |  @author:  Shekh Rizwan<rizwan@hirarky.com>
    |  @date:  2016-11-10
    */
   .controller('AccountSettingsController',function($scope,$rootScope,$ionicPopover,$state,$cordovaToast,$timeout,Core,$ionicLoading,$timeout){
     $scope.$state    =  $state;

     $scope.init   =   function(){
       $timeout(function () {
           $rootScope.is_big_bar   =   false;
       }, 10);
     }

     $scope.logout   =   function(){
       Core.user().logout();
       $ionicLoading.show({
           template: '<ion-spinner icon="android"></ion-spinner>'
       });

       $timeout(function(){
         $ionicLoading.hide();
         $timeout(function(){
           $state.go('credential');
         },10);
       },1000);
     };

     $scope.init();
   })

   /*
   |-------------------------------------------------------------------
   | NewTimelineController - Controller for user Timeline
   |-------------------------------------------------------------------
   |  @author:  Shekh Rizwan<rizwan@hirarky.com>
   |  @date:  2016-11-16
   */
  .controller('NewTimelineController',function($scope,$rootScope,$ionicPopover,$state,$cordovaToast,$timeout,Core,$ionicLoading,$timeout){
    $scope.$state    =  $state;

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
    }

    // API calling for getting user timeline
    $scope.result_data = [];
    $scope.result_dates = [];
    $scope.today = new Date();
    $scope.start_date =  $scope.today.getFullYear() + '-' + ($scope.today.getMonth() + 1) + '-' + $scope.today.getDate();
    $scope.lenght = 5;
    $scope.last_date = null;
    $scope.noMoreItemsAvailable = false;
    $scope.yesterday = moment($scope.start_date).subtract(1,'days').format('YYYY-MM-DD');
    $scope.count = 0;

    $scope.loadMore = function(){

      // $ionicLoading.show({
      //     template: '<ion-spinner icon="android"></ion-spinner>'
      // });

      if($scope.last_date   ==  null){
        $scope.last_date    =   moment().format('YYYY-MM-DD');
      }

      Core.timeline().get( $scope.last_date,  $scope.lenght ).then(function(data){
        angular.forEach(data.result , function(result){
          $scope.result_data.push(result);
          $scope.result_dates.push(result.date);
          $scope.count++;
          $scope.$broadcast('scroll.infiniteScrollComplete');

        });

          if( $scope.count == 0 ){
             $scope.noMoreItemsAvailable = true;
           }

        $scope.last_date = moment($scope.result_dates[$scope.count-1]).subtract(1,'days').format('YYYY-MM-DD');
        $scope.result_dates = [];
        $scope.count = 0;

        $ionicLoading.hide();
      },function(error){
        $ionicLoading.hide();
        console.log(error);
      },function(update){
        $ionicLoading.hide();
        console.log(update);
      });
    }

    // $scope.$on('$stateChangeSuccess', function() {
    // $scope.loadMore();
    // });

    $scope.init();
  })
    ;
