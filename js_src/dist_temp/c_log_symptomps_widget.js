/*
 |-------------------------------------------------------------------
 | LogSymptomsWidgetController - Controller for log of symptoms
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
angular.module('hcncore.controllers')
.controller('LogSymptomsWidgetController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "ionicTimePicker", "ionicDatePicker", "$cordovaToast", "$ionicLoading", "$ionicPopup", "$ionicHistory", function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicLoading,$ionicPopup,$ionicHistory){

  $scope.$state    =  $state;
  $scope.v  =   {};

  $scope.init   =   function(){
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
    }, 10);
    $scope.variables();
  }

  $scope.go_back  = function(){
    $ionicHistory.goBack();
  }

  $scope.is_main  =  true;
  $scope.is_location  =  false;
  $scope.is_symptoms  =   false;
  $scope.is_nature_symptoms   =   false;
  $scope.is_provoking_factors   =   false;
  $scope.is_relieving_factors   =   false;
  $scope.is_timing   =   false;
  $scope.is_enviorment   =   false;

  $scope.variables   =  function(){
      var v   =   $scope.v;

      v.symptom_search_text  =   '';
      v.is_searching  =   false;
      v.search_results    =   [];

      v.is_check_enabled  =   false;
      v.symptoms_log_input  =  {
          'symptom_id':'',
          'severity':5,
          'location_ids':[],
          'related_symptom_ids':[],
          'pain_nature_ids':[],
          'provoking_ids':[],
          'relieving_ids':[],
          'log_date':'',
          'log_date_view':'',
          'log_time':'',
          'log_time_view':'',
          'start_date':'',
          'start_time':'',
          'end_date':'',
          'end_time':'',
          'notes':'',
          'enviorment_id':'',
          'frequency':'',
          'duration':''
      };

      v.symptom = {
        'location_string':'',
        'symptom_string':'',
        'provoking_string':'',
        'enviorment_string':'',
        'nature_string':'',
        'releving_string':'',
        'timing_string':''
      };

      v.symptom.timing_string =  v.symptoms_log_input.duration;
      v.locations     =   [];
      v.symptoms    =   [];
      v.pain_natures   =   [];
      v.provoking_factors   =   [];
      v.releving_factors   =   [];
      v.enviorments   =   [];
      v.symptoms_timing_saved   = false;
      v.symptoms_log_input.log_date     =   moment().format('YYYY-MM-DD');

      $scope.revalidate_date();

      var time      =   moment().format('YYYY-MM-DD hh:mm A');

      v.symptoms_log_input.start_date     =   moment().format('YYYY-MM-DD');
      v.symptoms_log_input.start_time     =   moment().format('HH:mm');
      v.symptoms_log_input.end_date     =   moment().format('YYYY-MM-DD');
      v.symptoms_log_input.end_time     =   moment().format('HH:mm');


      $scope.revalidate_time(time);
      $scope.fetch_locations();
      $scope.fetch_symptoms();
      $scope.fetch_pain_nature();
      $scope.fetch_provoking_factors();
      $scope.fetch_releving_factors();
      $scope.fetch_enviorments();

      // $scope.init_handler();
  }

  // $scope.init_handler    =  function(){
  //   $scope.$watch('v.locations',function(){
  //     console.log($scope.v.locations);
  //   });
  // }

  // $scope.$watch('is_main',function(){
  //   console.log($scope.is_main?"Hello":"name");
  // });

  $scope.fetch_locations  =   function(){
    Core.logdata().symptoms_log().symptoms_location().then(function(data){
         $scope.v.locations   =   data.message;
        //  v.is_searching  =   false;
    },function(data){
        // v.is_searching  =   false;
    });
  }

  $scope.fetch_symptoms  =   function(){
    Core.logdata().symptoms_log().symptoms().then(function(data){
         $scope.v.symptoms   =   data.message;
        //  v.is_searching  =   false;
    },function(data){
        // v.is_searching  =   false;
    });
  }

  $scope.fetch_pain_nature  =   function(){
    Core.logdata().symptoms_log().nature_pain().then(function(data){
         $scope.v.pain_natures   =   data.message;
        //  v.is_searching  =   false;
    },function(data){
        // v.is_searching  =   false;
    });
  }

  $scope.fetch_provoking_factors  =   function(){
    Core.logdata().symptoms_log().provoking_factors().then(function(data){
         $scope.v.provoking_factors   =   data.message;
        //  v.is_searching  =   false;
    },function(data){
        // v.is_searching  =   false;
    });
  }

  $scope.fetch_releving_factors  =   function(){
    Core.logdata().symptoms_log().releving_factors().then(function(data){
         $scope.v.releving_factors   =   data.message;
        //  v.is_searching  =   false;
    },function(data){
        // v.is_searching  =   false;
    });
  }

  $scope.fetch_enviorments  =   function(){
    Core.logdata().symptoms_log().enviorment().then(function(data){
         $scope.v.enviorments   =   data.message;
        //  v.is_searching  =   false;
    },function(data){
        // v.is_searching  =   false;
    });
  }

  $scope.validate_for_check   =   function(){
    var v   =   $scope.v;
    // console.log(111);
    if(v.symptoms_log_input.symptom_id == null || v.symptoms_log_input.symptom_id.length == 0){
      v.is_check_enabled  =   false;
      return;
    }

    v.is_check_enabled  =   true;
  }

  $scope.revalidate_date =  function(){
      var v   =   $scope.v;
      v.symptoms_log_input.log_date_view     =   moment(v.symptoms_log_input.log_date).format('Do MMM,YYYY');
  }

  $scope.revalidate_time =  function(time){
      var v   =   $scope.v;
      v.symptoms_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
      console.log(v.symptoms_log_input.log_time);
      v.symptoms_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
      v.symptoms_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
  }

  $scope.get_e_time   = function(time){
    var time_split    =   time.split(':');
    // console.log(time_split);
    // console.log(parseInt(time_split[0])*3600 + parseInt(time_split[1])*60);
    return parseInt(time_split[0])*3600 + parseInt(time_split[1])*60;

  }

  $scope.search_symptoms  =   function(){
      var v   =   $scope.v;
      v.is_searching  =   true;
      v.symptoms_log_input.symptom_id = '';
      Core.logdata().symptoms_log().symptoms_search(v.symptom_search_text).then(function(data){
           v.search_results   =   data.message;
           angular.forEach(data.message,function(result){
             result.name =  result.name.charAt(0).toUpperCase() + result.name.slice(1);
           });
           v.is_searching  =   false;
      },function(data){
          v.is_searching  =   false;
      });
      $scope.validate_for_check();
  }

  $scope.symptoms  =   function(){
      var v   =   $scope.v;
      v.is_searching  =   true;
      v.symptoms_log_input.symptom_id = '';
      Core.logdata().symptoms_log().symptoms_search(v.symptom_search_text).then(function(data){
           v.search_results   =   data.message;
           v.is_searching  =   false;
      },function(data){
          v.is_searching  =   false;
      });
      $scope.validate_for_check();
  }

  $scope.log_symptoms   =   function(){
    $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
            });
    var v   =   $scope.v;
      Core.logdata().symptoms_log().commit(v.symptoms_log_input).then(function(data){
        $ionicLoading.hide();
        $scope.log_success_popup("Symptoms Logged");
        // $cordovaToast.show("Symptoms Logged",'long','center');
        // $timeout(function(){
        //   $state.go('main_tabs.log_widget');
        // },2000);
      },function(error){
        $ionicLoading.hide();
        console.log(error.message);
        $cordovaToast.show(error.message,'short','center');
      });
  }

  $scope.log_pop_message  =   null;
  $scope.log_popup  =   null;
  $scope.log_success_popup  =   function(message){
    $scope.log_pop_message  =   message;
    $scope.log_popup = $ionicPopup.show({
      templateUrl: 'log_sucess_popup.html',
      title: 'Successfully Logged',
      scope: $scope,
      'cssClass':'log_success_popup'
    });
  }

  $scope.close_log_success_popup  =  function(notes){
    $scope.log_popup.close();
    $timeout(function(){
      $state.go('main_tabs.log_widget');
      $rootScope.$broadcast('data:loggged');
    },100);
  }

  $scope.choose_symptom  =   function(item){
    // alert(1111);
    var v   =   $scope.v;
    v.symptoms_log_input.symptom_id   = item.id;
    v.symptom_search_text   = item.name;
    // v.activity_search_text  =   '';
    v.search_results   =   [];
    $scope.validate_for_check();
  }

  $scope.show_location    =   function(status,final){
    if(status){
        $scope.is_main = false;
        $scope.is_location = true;
    } else {
      $scope.is_main = true;
      $scope.is_location = false;
      if(final != undefined){
        var location_array   =  [];
        $scope.v.symptoms_log_input.location_ids  =   [];
        angular.forEach($scope.v.locations,function(item){
          if(item.is_checked){
            $scope.v.symptoms_log_input.location_ids.push(parseInt(item.id));
            location_array.push(item.name);
          }
        });
        $scope.v.symptom.location_string  =   location_array.join(', ');
        // console.log($scope.v.symptom.location_string);
      } else {
        console.log($scope.v.symptoms_log_input.location_ids);
        angular.forEach($scope.v.locations,function(item){
          if(item.is_checked && $scope.v.symptoms_log_input.location_ids.indexOf(parseInt(item.id)) == -1){
            item.is_checked = false;
          }
        });
      }

    }
  }

  $scope.show_symptoms   =  function(status,final){
    if(status){
        $scope.is_main = false;
        $scope.is_symptoms = true;
    } else {
      $scope.is_main = true;
      $scope.is_symptoms = false;
      if(final != undefined){
        var symptom_array   =  [];
        $scope.v.symptoms_log_input.related_symptom_ids  =   [];
        angular.forEach($scope.v.symptoms,function(item){
          if(item.is_checked){
            $scope.v.symptoms_log_input.related_symptom_ids.push(parseInt(item.id));
            symptom_array.push(item.name);
          }
        });
        $scope.v.symptom.symptom_string  =   symptom_array.join(', ');
      } else {
        // console.log($scope.v.symptoms_log_input.related_symptom_ids);
        angular.forEach($scope.v.symptoms,function(item){
          if(item.is_checked && $scope.v.symptoms_log_input.related_symptom_ids.indexOf(parseInt(item.id)) == -1){
            item.is_checked = false;
          }
        });
      }
    }
  }

  $scope.show_nature_symptoms   =   function(status,final){
    if(status){
        $scope.is_main = false;
        $scope.is_nature_symptoms = true;
    } else {
      $scope.is_main = true;
      $scope.is_nature_symptoms = false;

      if(final != undefined){
        var nature_array   =  [];
        $scope.v.symptoms_log_input.pain_nature_ids  =   [];
        angular.forEach($scope.v.pain_natures,function(item){
          if(item.is_checked){
            $scope.v.symptoms_log_input.pain_nature_ids.push(parseInt(item.id));
            nature_array.push(item.name);
          }
        });
        $scope.v.symptom.nature_string  =   nature_array.join(', ');
      } else {
        // console.log($scope.v.symptoms_log_input.related_symptom_ids);
        angular.forEach($scope.v.pain_natures,function(item){
          if(item.is_checked && $scope.v.symptoms_log_input.pain_nature_ids.indexOf(parseInt(item.id)) == -1){
            item.is_checked = false;
          }
        });
      }

    }
  }

  $scope.show_provoking_factors   =   function(status,final){
    if(status){
        $scope.is_main = false;
        $scope.is_provoking_factors = true;
    } else {
      $scope.is_main = true;
      $scope.is_provoking_factors = false;

      if(final != undefined){
        var provoking_array   =  [];
        $scope.v.symptoms_log_input.provoking_ids  =   [];
        angular.forEach($scope.v.provoking_factors,function(item){
          if(item.is_checked){
            $scope.v.symptoms_log_input.provoking_ids.push(parseInt(item.id));
            provoking_array.push(item.name);
          }
        });
        $scope.v.symptom.provoking_string  =   provoking_array.join(', ');
      } else {
        // console.log($scope.v.symptoms_log_input.related_symptom_ids);
        angular.forEach($scope.v.provoking_factors,function(item){
          if(item.is_checked && $scope.v.symptoms_log_input.provoking_ids.indexOf(parseInt(item.id)) == -1){
            item.is_checked = false;
          }
        });
      }
    }
  }

  $scope.show_relieving_factors   =   function(status,final){
    if(status){
        $scope.is_main = false;
        $scope.is_relieving_factors = true;
    } else {
      $scope.is_main = true;
      $scope.is_relieving_factors = false;

      if(final != undefined){
        var releving_array   =  [];
        $scope.v.symptoms_log_input.relieving_ids  =   [];
        angular.forEach($scope.v.releving_factors,function(item){
          if(item.is_checked){
            $scope.v.symptoms_log_input.relieving_ids.push(parseInt(item.id));
            releving_array.push(item.name);
          }
        });
        $scope.v.symptom.releving_string  =   releving_array.join(', ');
      } else {
        // console.log($scope.v.symptoms_log_input.related_symptom_ids);
        angular.forEach($scope.v.releving_factors,function(item){
          if(item.is_checked && $scope.v.symptoms_log_input.relieving_ids.indexOf(parseInt(item.id)) == -1){
            item.is_checked = false;
          }
        });
      }
    }
  }


  $scope.show_enviorment  =   function(status){
    if(status){
        $scope.is_main = false;
        $scope.is_enviorment = true;
    } else {
      $scope.is_main = true;
      $scope.is_enviorment = false;
    }
  }

  $scope.show_timing  =   function(status){
    $scope.v.symptom.timing_string =  $scope.v.symptoms_log_input.duration.replace('_', ' ');
    if(status){
        $scope.is_main = false;
        $scope.is_timing = true;
    } else {
      $scope.is_main = true;
      $scope.is_timing = false;
    }
  }

  $scope.notes_popup  =   null;
  $scope.notes  =   '';
  $scope.open_note_popup  =   function(){
    $scope.notes_popup = $ionicPopup.show({
      templateUrl: 'note_popup.html',
      title: 'Notes',
      scope: $scope,
      'cssClass':'note_popup'
    });
  }

  $scope.close_notes_popup  =  function(notes){
    $scope.v.symptoms_log_input.notes =  notes;
    $scope.notes_popup.close();
  }
  $scope.cancel_notes_popup  =  function(){
    $scope.notes_popup.close();
  }

  $scope.choose_enviorment_type   =   function(type){
    angular.forEach($scope.v.enviorments,function(i_type){
        if(i_type.id == type.id){
          i_type.is_checked   = true;
          $scope.v.symptoms_log_input.enviorment_id   = type.id;
          $scope.v.symptom.enviorment_string  =   type.name;

        } else {
          i_type.is_checked   = false;
        }
    });
  }

  $scope.open_startdatepicker  =   function(){
    // alert(111);
    var v   =   $scope.v;
    ionicDatePicker.openDatePicker({
      callback:function(val){

        v.symptoms_log_input.start_date   =   moment(val).format('YYYY-MM-DD');
        // console.log(val)
      },
      templateType: 'popup',
      to:new Date( moment().format('YYYY-MM-DD')),
      inputDate:new Date(moment(v.symptoms_log_input.start_date))
    });
  }

  $scope.open_enddatepicker  =   function(){
    // alert(111);
    var v   =   $scope.v;
    ionicDatePicker.openDatePicker({
      callback:function(val){

        v.symptoms_log_input.end_date   =   moment(val).format('YYYY-MM-DD');
        // console.log(val)
      },
      templateType: 'popup',
      from:new Date( moment(v.symptoms_log_input.start_date).format('YYYY-MM-DD')),
      to:new Date( moment().format('YYYY-MM-DD')),
      inputDate:new Date(moment(v.symptoms_log_input.end_date))
    });
  }

  $scope.open_starttimepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            v.symptoms_log_input.start_time = moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('HH:mm');
          }
        },
        inputTime: $scope.get_e_time(v.symptoms_log_input.start_time),   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };
      ionicTimePicker.openTimePicker(ipObj1);
    }
    $scope.open_endtimepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            v.symptoms_log_input.end_time = moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('HH:mm');
          }
        },
        inputTime: $scope.get_e_time(v.symptoms_log_input.end_time),   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };
    ionicTimePicker.openTimePicker(ipObj1);
  }

  // Go back confirmation popup
   $scope.showConfirm = function() {
     var v   =   $scope.v;
     if(v.symptom_search_text == null || v.symptom_search_text.length <= 0){
       $state.go("main_tabs.log_widget");
     }else {
       var confirmPopup = $ionicPopup.confirm({
         title: 'Record Symptoms',
         template: 'Are you sure you want to go back?'
       });
       confirmPopup.then(function(res) {
         if(res) {
           $state.go("main_tabs.log_widget");
         } else {
           console.log('You are not sure');
         }
       });
     }
   };

  $scope.init();
}])
