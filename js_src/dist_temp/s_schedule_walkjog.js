/*
|------------------------------------------------------------------------------
|  Schedule Walking/Jogging - Service to Schedule the Info from Walking/Jogging
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-10-17
*/
angular.module('hcncore.services')
.factory('ScheduleWalkJog',["Log", "Network", "$q", "$resource", "locker", "User", "UserProfile", function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};
  function resource(){


    var url   =   Network.api_url+'/schedule/activity';

    return  $resource(url + '/:id', {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  factory.commit  =   function(data){

    var deferred  =   $q.defer();

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }

    var profile  =   UserProfile.getActiveProfile();
    if(profile ==  null){
      return null;
    }

    data.profile_id   =  profile.id;

    if(data.notify_before == null || data.notify_before.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please choose Notify time'
      });
      return deferred.promise;
    }

    var user_data   =   data;
    console.log(data);
    resource().add({
      'profile_id':data.profile_id,
      'activity_name':data.activity_name,
      'schedule_date':data.walkjog_date,
      'schedule_time':data.walkjog_time,
      'notify_before':data.notify_before,
      'repeat_on':data.repeat_on,
      'is_forever':data.is_forever,
      'event_count':data.event_count,
      'until':data.until,
      'duration':data.duration,

    },function(data){
        Log.info('ScheduleActivity:commit',data);
        deferred.resolve(data);
    },function(data){
        Log.error('ScheduleActivity:commit',data);
        deferred.reject(error);
    });
    return deferred.promise;
  }
  return factory;
}])
