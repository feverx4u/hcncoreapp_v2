angular.module('hcncore.filters')
.filter('med_tostring',function(){
  return function(input){
    var str   =   '';
    if(input == null){
      return str;
    }

    if(input.length <= 0){
      return str;
    }
    var str_arr   =   [];
    angular.forEach(input,function(item){
      str_arr.push(item.name+'('+item.doss+')');
    });
    return str_arr.join(', ');
  }
})
