/*
 |-------------------------------------------------------------------
 | DiabetesRangeController - Controller for user diabater range
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
 angular.module('hcncore.controllers')
 .controller('DiabetesRangeController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "$ionicLoading", "$cordovaToast", "ionicDatePicker", "$ionicHistory", function($rootScope,$scope,$state,$ionicPlatform,Core,
   $timeout,$ionicLoading,$cordovaToast,ionicDatePicker,$ionicHistory){

     $scope.v   =   {};
      //Method to initialize the controller
      $scope.init   =   function(){
        $timeout(function () {
            $rootScope.is_big_bar   =   true;
        }, 10);
        $scope.variable();
        $scope.get_diabates_type();
      }

      $scope.set_range = function(is_set){
        var v   =   $scope.v;
        v.range_start_ar = [];
        v.range_end_ar = [];

        if(v.range_unit == 'mg_dl'){
          if(is_set == undefined){
            v.choosen_start   =   parseInt(sprintf('%0.1f',parseFloat(v.choosen_start)*18))+'';
            v.choosen_end   =   parseInt(sprintf('%0.1f',parseFloat(v.choosen_end)*18))+'';
          }


          for(var i=v.range_start;i<parseInt(v.choosen_end);i++){
              v.range_start_ar.push(i);
          }

          for(var i=parseInt(v.choosen_start);i<=v.range_end;i++){
              v.range_end_ar.push(i);
          }
        } else {
          v.choosen_start   =   sprintf('%0.1f',parseFloat(v.choosen_start)/18)+'';
          v.choosen_end   =   sprintf('%0.1f',parseFloat(v.choosen_end)/18)+'';

          for(var i=v.range_start/18;i<parseFloat(v.choosen_end);i = i+0.1){
              v.range_start_ar.push(sprintf('%0.1f',i));
          }

          for(var i=parseFloat(v.choosen_start);i<=v.range_end/18;i = i+0.1){
              v.range_end_ar.push(sprintf('%0.1f',i));
          }
          //
        }

        console.log([v.range_start_ar,v.range_end_ar]);
      }

      $scope.refactor_range = function(is_set){
        var v   =   $scope.v;
        v.range_start_ar = [];
        v.range_end_ar = [];

        if(v.range_unit == 'mg_dl'){
          if(is_set == undefined){
            v.choosen_start   =   parseInt(sprintf('%0.1f',parseFloat(v.choosen_start)))+'';
            v.choosen_end   =   parseInt(sprintf('%0.1f',parseFloat(v.choosen_end)))+'';
          }


          for(var i=v.range_start;i<parseInt(v.choosen_end);i++){
              v.range_start_ar.push(i);
          }

          for(var i=parseInt(v.choosen_start)+1;i<=v.range_end;i++){
              v.range_end_ar.push(i);
          }
        } else {
          v.choosen_start   =   sprintf('%0.1f',parseFloat(v.choosen_start))+'';
          v.choosen_end   =   sprintf('%0.1f',parseFloat(v.choosen_end))+'';

          for(var i=v.range_start/18;i<parseFloat(v.choosen_end);i = i+0.1){
              v.range_start_ar.push(sprintf('%0.1f',i));
          }

          for(var i=(parseFloat(v.choosen_start)+0.1);i<=v.range_end/18;i = i+0.1){
              v.range_end_ar.push(sprintf('%0.1f',i));
          }
          //
        }

        console.log([v.range_start_ar,v.range_end_ar]);
      }

      //variable
      $scope.variable   = function(){
        var v   =   $scope.v;
        // $scope.v.diabetes_type    =   [];
        v.range_start       =   60;
        v.range_end       =   220;

        v.choosen_start   =   '82';
        v.choosen_end   =   '109';
        v.range_start_ar = [];
        v.range_end_ar = [];
        v.range_unit  =   'mg_dl';
      }



      $scope.get_diabates_type  =   function(){
        var v   =   $scope.v;
        var diabetes_type  = Core.diabetes().get_user_diabetes_type();
        if(diabetes_type == 'gestational_diabetes'){
          v.range_start       =   80;
          v.range_end       =   140;

          v.choosen_start   =   '82';
          v.choosen_end   =   '109';
          $scope.set_range(true);
        } else {
          $scope.set_range(true);
        }
      }


      $scope.set_target_range =   function(){
        var v   =   $scope.v;

        Core.diabetes().update_target_range(v.range_unit,v.choosen_start,v.choosen_end).then(function(){
          // $cordovaToast.show('Updated','short','bottom');
          $state.go('therephy_type');
        },function(error){
          $cordovaToast.show(error.message,'short','center');
        });
      }

      $scope.go_back  =   function(){
        $ionicHistory.goBack();
        // $state.go('diabetes_type');
      }



      $scope.init();
  }])
