angular.module('hcncore.directives')
.directive('selectpopup', ["$timeout", "$ionicPopup", function ($timeout,$ionicPopup) {
  return {
    restrict:'AE',
    require:"ngModel",
    scope: {
      gender: '=ngModel',
      dataSrc: '=data'
    },
    link: function (scope, element, attrs,ngModel) {
      // console.log(attrs);

      scope.model  =   ngModel;
      scope.attrs   =   {};
      for (var a in attrs) {
        attr = a.replace('attr', '').toLowerCase();
        // add attribute overriding defaults
        // and preventing duplication
        if (a.indexOf('attr') === 0) {
          scope.attrs[attr] = attrs[a];
        }
      }

      scope.title     = attrs.title;
      var gender_popup = null;
      element.bind('click', function() {
        gender_popup   =  $ionicPopup.show({
            template: '<div class="sel-item">'+
              '<ion-list >'+
                '<ion-item ng-repeat="(key,value) in dataSrc" ng-click="select_item(key)">'+
                  "<span>{{ value }}</span>"+
                "</ion-item>"+
              "</ion-list>"+
            "</div>",
            scope:scope,
            title: scope.attrs.title,
            cssClass:'selectpopup'
          });
      });

      scope.select_item  =   function(key){
        console.log(key);
        gender_popup.close();
        // defer.resolve(key);
        ngModel.$setViewValue(key);
        ngModel.$render();
      }

      // if (attrs.focusMeDisable === "true") {
      //   return;
      // }
      // $timeout(function () {
      //   element[0].focus();
      //   if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      //     cordova.plugins.Keyboard.show(); //open keyboard manually
      //   }
      // }, 350);
    }
  };
}])
