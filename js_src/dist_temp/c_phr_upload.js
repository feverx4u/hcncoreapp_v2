/*
 |-------------------------------------------------------------------
 | PhrUploadController - Controller for PHR Upload Feature
 |-------------------------------------------------------------------
 |  @author:  Shekh Rizwan<rizwan@hirarky.com>
 |  @date:  2016-11-01
 */
 angular.module('hcncore.controllers')
.controller('PhrUploadController',["$rootScope", "$scope", "$state", "$cordovaToast", "Core", "$ionicLoading", "$ionicPopup", "$cordovaImagePicker", "$ionicPlatform", "$cordovaCamera", "$timeout", "ionicDatePicker", "ionicTimePicker", "Media", "$cordovaFileTransfer", "$ionicHistory", function($rootScope,$scope,$state,$cordovaToast ,Core,$ionicLoading,$ionicPopup,
      $cordovaImagePicker,$ionicPlatform,$cordovaCamera,$timeout,ionicDatePicker,ionicTimePicker,Media,$cordovaFileTransfer,$ionicHistory){

        $scope.$state    =  $state;
        $scope.$history   =   $ionicHistory;
        $scope.v  =   {};
        var count = 0;
        var v   =   $scope.v;
        v.search_results    =   [];

        $scope.type_array = ["Insurance card","Laboratory Reports","Medical Prescription","Doctor Consultation"
                             ,"Discharge Summary","Identification Records","Operation/Surgery Report",
                             "Health Progress Report","X Ray Report","MRI Reports","CT Scan","Imaging Reports",
                             "Medicine Bill","Hospital Bill","Other Bills","Immunization Records"
                             ,"Hospital Admission Records","Hospital Records","Hospital Card"];


        $scope.init   =   function(){
          $timeout(function () {
              $rootScope.is_big_bar   =   false;
          }, 10);
          $scope.variables();
        }
        $scope.variables   =  function(){
            var v   =   $scope.v;
            v.search_results    =   [];
            v.selected_file    =   null;
            v.phr_upload_input  =  {
                'event_name':'',
                'create_event':'',
                'event_type':'',
                'phr_date_view':'',
                'phr_date':'',
                'phr_time_view':'',
                'phr_time':'',
                'phr_time_time':'',
                'notes':'',
                'media_id':[],
                'attach_docs':false
            };

            v.phr_upload_input.phr_date     =   moment().format('YYYY-MM-DD');
            $scope.revalidate_date();

            var time      =   moment().format('YYYY-MM-DD hh:mm A');
            $scope.revalidate_time(time);
            $scope.phr_show_event();
        }

        $scope.open_datepicker  =   function(){
          var v   =   $scope.v;
          ionicDatePicker.openDatePicker({
            callback:function(val){

              v.phr_upload_input.phr_date   =   moment(val).format('YYYY-MM-DD');
              $scope.revalidate_date();
              // console.log(val)
            },
            templateType: 'popup',
            to:new Date( moment().format('YYYY-MM-DD')),
            inputDate:new Date(moment())
          });
        }

        $scope.open_timepicker  =   function(){
          var v   =   $scope.v;
          var ipObj1 = {
            callback: function (val) {      //Mandatory
              if (typeof (val) === 'undefined') {
                console.log('Time not selected');
              } else {
                var selectedTime = new Date(val * 1000);
                $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
              }
            },
            inputTime: v.phr_upload_input.phr_time_time,   //Optional
            format: 12,         //Optional
            step: 1,           //Optional
            setLabel: 'Set'    //Optional
          };

          ionicTimePicker.openTimePicker(ipObj1);
        }

        $scope.revalidate_date =  function(){
            var v   =   $scope.v;
            v.phr_upload_input.phr_date_view     =   moment(v.phr_upload_input.phr_date).format('Do MMM,YYYY');
        }

        $scope.revalidate_time =  function(time){
            var v   =   $scope.v;
            v.phr_upload_input.phr_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
            v.phr_upload_input.phr_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
            v.phr_upload_input.phr_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
        }

        // notes popup
        $scope.notes_popup  =   null;
        $scope.notes  =   '';
        $scope.open_note_popup  =   function(){
          $scope.notes_popup = $ionicPopup.show({
            templateUrl: 'note_popup.html',
            title: 'Notes',
            scope: $scope,
            'cssClass':'note_popup'
          });
        }

        $scope.close_notes_popup  =  function(notes){
          $scope.v.phr_upload_input.notes =  notes;
          $scope.notes_popup.close();
        }
        $scope.cancel_notes_popup  =  function(){
          $scope.notes_popup.close();
        }

        // Create new Event popup
        $scope.create_event_popup  =   null;
        $scope.v.new_event_name  =   '';
        $scope.open_event_popup  =   function(){
          $scope.create_event_popup = $ionicPopup.show({
            templateUrl: 'create_event_popup.html',
            title: 'Create a New Event',
            scope: $scope,
            'cssClass':'create_event_popup'
          });
        }

        $scope.save_event_popup  =  function(){
          if( $scope.event_array.indexOf($scope.v.new_event_name)  !== false){
            $scope.event_array.push({
             'event_name': $scope.v.new_event_name
            });
          }
          $scope.v.phr_upload_input.event_name   =   $scope.v.new_event_name;
          $scope.create_event_popup.close();
        }
        $scope.cancel_event_popup  =  function(){
          $scope.create_event_popup.close();
        }

        // Attach Document popup
        $scope.attach_doc_popup  =   null;
        $scope.open_attach_doc_popup  =   function(){
          $scope.attach_doc_popup = $ionicPopup.show({
            templateUrl: 'attach_doc_popup.html',
            title: 'Attach a Document',
            scope: $scope,
            'cssClass':'report_popup record_popup'
          });
        }
        $scope.close_attach_doc_popup  =  function(){
          $scope.attach_doc_popup.close(); // close popup while image selection from gallery
        }

        // upload Image from Gallery
        $scope.select_file_to_upload = function(){
          $timeout(function() {
            document.getElementById('uploaded_file').click();
          });
        }

        $scope.get_image_from_gallery   =   function(){
          $scope.close_attach_doc_popup();
            var upload_files    =   document.getElementById('uploaded_file').files;
            console.log(upload_files);
            if(upload_files.length > 0){
              if(!Media.validate_for_upload(document.getElementById('uploaded_file'))){
                $scope.error  =   'File to be upload should be document or image.';
                return;
              }
              $scope.$digest();
              $scope.upload_media(upload_files);
            }
        }

        // upload media method from gallery
           $scope.upload_media = function(upload_files) {
             angular.forEach(upload_files,function(file,k){
               Media.upload(upload_files[0],'SUBMIT_FILE',function(data){
                   console.log(['suc',data]);
                   $ionicLoading.hide();
                   $scope.$digest();
                   var response  =   JSON.parse(data.currentTarget.response);
                   var media   =   response.media;
                   count++;
                   v.selected_file  =   media.path;
                   v.search_results.push(count +" "+ v.selected_file.split('/').pop());
                   v.phr_upload_input.media_id.push(media.id);
                   v.phr_upload_input.attach_docs = true;

                   console.log( v.selected_file);
                   console.log(v.phr_upload_input.media_id);
                   $scope.$digest();
               },function(data){
                   console.log(['error',data]);
                   $ionicLoading.hide();
                   $scope.$digest();
                   $ionicLoading.show({
                     template: '<div style="text-align:center;width:200px;" class="tf_loader" style="">Failed to Add</div>'
                   });
                   $scope.$digest();
                   $timeout(function(){
                       $ionicLoading.hide();
                       $scope.$digest();
                   },3000);

                   $scope.$digest();
               },function(){

               },function(progress){

                 console.log(progress);

                 var progress  =   parseInt((progress.loaded/progress.total)*100);
                 $scope.$digest();
                 $ionicLoading.show({
                   template: '<div style="text-align:center;width:200px;" class="tf_loader" style="">('+(k+1)+'/'+upload_files.length+') Uploading ('+progress+'%)... </div>'
                 });

                 $scope.$digest();
                 $timeout(function(){
                     $scope.$digest();
                 },3000);

               });
             });
           }

         // get Picture/Image from Camera
           $scope.get_image_from_camera   =   function(){
           $scope.close_attach_doc_popup(); // close popup while image selection from gallery
           $ionicPlatform.ready(function() {
             var options = {
                   quality: 50,
                   destinationType: Camera.DestinationType.FILE_URI,
                   sourceType: Camera.PictureSourceType.CAMERA,
                   encodingType: Camera.EncodingType.JPEG,
                   popoverOptions: CameraPopoverOptions,
                   saveToPhotoAlbum: true,
                   correctOrientation:true
                 };

             $cordovaCamera.getPicture(options).then(function (imageURI) {
                 count++;
                     console.log(imageURI);
                       $scope.upload_file_to_server(imageURI);

             }, function(error) {
                 console.log('Error: ' + JSON.stringify(error));    // In case of error
             });
           });
         }

         $scope.upload_file_to_server  =   function(path){

           var options = {
               fileKey: "file",
               filename: "camera_image",
               chunkedMode: true,
               mimeType: "image/png"
               };

           var server_url = Core.network().api_url+'/media?type=PHR';
           console.log(server_url);
           $cordovaFileTransfer.upload(
             server_url,path, options)
             .then(function(result){
               console.log("success");
               var response  =   JSON.parse(result.response);

               var media   =   response.media;
               v.selected_file  =   media.path;
               v.search_results.push(count +" "+ v.selected_file.split('/').pop());
               v.phr_upload_input.media_id.push(media.id);
               v.phr_upload_input.attach_docs = true;
               }, function(err) {
                 // Error
                 console.log(err);
               }, function (progress) {
                 // constant progress updates
                 console.log(progress);
               });
         }

         // Phr show API Calling Method for getting Event
         $scope.event_array =[];
          $scope.phr_show_event   =   function(){
            $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
            });

            Core.phrdata().phr_event().get().then(function(data){
              angular.forEach(data.data,function(data){
                      $scope.event_array.push(data);
              });
              $ionicLoading.hide();

            },function(error){
              console.log("error "+error);
              $ionicLoading.hide();
            },function(update){
              console.log("update "+update);
              $ionicLoading.hide();
            });
          }

          // Phr Upload API Calling Method
           $scope.phr_dataupload   =   function(){
             $ionicLoading.show({
                 template: '<ion-spinner icon="android"></ion-spinner>'
             });

             var v   =   $scope.v;
               Core.phrdata().phr_upload().commit(v.phr_upload_input).then(function(data){
                   $scope.cancel_upload();

                 $ionicLoading.hide();
                 $cordovaToast.show("Successfully Uploaded",'long','center');

                 $timeout(function(){
                   $state.go('main_tabs.phr_record',{'chache':false});
                 },2000);

               },function(error){
                 $ionicLoading.hide();
                 console.log(error.message);
                 $cordovaToast.show(error.message,'short','center');
               });
           }

           $scope.cancel_upload = function() {
             var v   =   $scope.v;
             count = 0;
             v.search_results = [];
             v.phr_upload_input.attach_docs = false;
             v.phr_upload_input  =  {
                 'event_name':'',
                 'create_event':'',
                 'event_type':'',
                 'phr_date_view':'',
                 'phr_date':'',
                 'phr_time_view':'',
                 'phr_time':'',
                 'phr_time_time':'',
                 'notes':'',
                 'media_id':[],
                 'attach_docs':false
             };
             v.phr_upload_input.phr_date     =   moment().format('YYYY-MM-DD');
             $scope.revalidate_date();

             var time      =   moment().format('YYYY-MM-DD hh:mm A');
             $scope.revalidate_time(time);
           }

           $scope.delete_uploaded_document  = function(k){
             var v   =   $scope.v;
             v.search_results.splice(k,1);
             if(v.search_results.length == 0){
               v.phr_upload_input.attach_docs = false;
             }
           }

           $scope.delete_doc_confirm = function(k){

             var confirmPopup = $ionicPopup.confirm({
               title: 'PHR',
               template: 'Are you sure you want to delete this document?'
             });
             confirmPopup.then(function(res) {
               if(res) {
                 $scope.delete_uploaded_document(k);
               } else {
                 console.log('You are not sure');
               }
             });
           }
          $scope.init();
     }])
