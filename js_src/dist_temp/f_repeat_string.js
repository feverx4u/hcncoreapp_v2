angular.module('hcncore.filters')
.filter('repeat_string',function(){
  return function(input){
    var temp  = input.split(',');

    var result_array  =   [];
    angular.forEach(temp,function(item){
      if(item.trim().length == 0){
        return;
      }
      item  = item.toUpperCase();
      result_array.push(item);
    });

    return result_array.join(', ');
  }
})
