/*
 |-------------------------------------------------------------------
 | DateTimePickerController - Controller for set Date and Time
 |-------------------------------------------------------------------
 |  @author:  Shekh Rizwan<rizwan@hirarky.com>
 |  @date:  2016-10-12
 */
 angular.module('hcncore.controllers')
.controller('DateTimePickerController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "ionicTimePicker", "ionicDatePicker", "$cordovaToast", "$ionicPopup", "DateTimePickerServices", function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,DateTimePickerServices){
    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();
    }

    $scope.variables   =  function(){
        var v   =   $scope.v;
        v.is_check_enabled  =   false;

        v.repeat  =   {
          'popup_date_view':'',
          'popup_time_view':'',
          'popup_date':'',
          'popup_time':''
        };

        v.repeat.popup_date     =   moment().format('YYYY-MM-DD');
        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');
        $scope.revalidate_time(time);
    }

    $scope.open_datepicker  =   function(){
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.repeat.popup_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
        },
        templateType: 'popup',
        to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
        inputDate:new Date(moment())
      });
    }

    $scope.open_timepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
          }
        },
        inputTime: v.repeat.popup_time_time,   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };

      ionicTimePicker.openTimePicker(ipObj1);
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.repeat.popup_date_view     =   moment(v.repeat.popup_date).format('Do MMM,YYYY');
          DateTimePickerServices.setDate(v.repeat.popup_date);
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.repeat.popup_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        v.repeat.popup_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.repeat.popup_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
        DateTimePickerServices.setTime(v.repeat.popup_time);
    }
    $scope.init();
}])
