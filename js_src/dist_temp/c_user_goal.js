/*
 |-------------------------------------------------------------------
 | UserGoalController - Controller for user goal
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
 angular.module('hcncore.controllers')
 .controller('UserGoalController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "$ionicLoading", "$cordovaToast", "ionicDatePicker", "$ionicHistory", function($rootScope,$scope,$state,$ionicPlatform,Core,
   $timeout,$ionicLoading,$cordovaToast,ionicDatePicker,$ionicHistory){

     $scope.v   =   {};
      //Method to initialize the controller
      $scope.init   =   function(){
        $timeout(function () {
            $rootScope.is_big_bar   =   true;
        }, 10);
        $scope.variable();
        $scope.fetch_goal();
      }

      //variable
      $scope.variable   = function(){
        var v   =   $scope.v;
        $scope.v.goals    =   [];
      }

      //fetching the goal
      $scope.fetch_goal  =   function(){
        // alert(111);
        Core.goal().fetch_goals().then(function(data){
          $scope.v.goals  =   data.message;
          // console.log(data);
        });
      }

      $scope.go_back  =   function(){
        $ionicHistory.goBack();
        // $state.go('user_info');
      }

      //Method is used to choose the goal
      $scope.choose_goal  =   function(goal){
        if(goal.status == 'COMING_SOON'){
            $cordovaToast.show('Goal yet to be released','short','center');
            return;
        }
        goal.is_checked   = !goal.is_checked;

      }

      //Method is used to select the goal
      $scope.select_goal  =   function(){
        var goals   =   $scope.v.goals;
        var choosen_goal  = [];
        angular.forEach(goals,function(goal){
            if(goal.is_checked){
              choosen_goal.push(goal.slug);
            }
        });

        // if(choosen_goal.length <= 0){
        //   $cordovaToast.show('Please choose the goal.','short','center');
        //   return;
        // }
        console.log(choosen_goal);
        Core.goal().set_goal(choosen_goal).then(function(){
          if(choosen_goal.indexOf('diabetes_mgmt') >= 0){
            $state.go('diabetes_type');
          } else if(choosen_goal.indexOf('hypertension') >= 0){
            $state.go('hypertension');
          } else {
            $state.go('emergency_contact');
          }
          // $cordovaToast.show('Goal updated','short','bottom');
        },function(error){
          console.log('here');
          // $cordovaToast.show(error.message,'short','center');
        });
      }

      $scope.init();
  }])
