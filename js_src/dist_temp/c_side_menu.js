/*
 |---------------------------------------------------------------
 |  RouteController - Controller is place where application is
 |                    is initialized and taken to proper state.
 |---------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-01
 */
 angular.module('hcncore.controllers')

.controller('SideMenuController',["$scope", "$state", "$rootScope", "$ionicHistory", "Core", "$timeout", "$ionicLoading", function($scope,$state,$rootScope,$ionicHistory,Core,$timeout,$ionicLoading){
  $scope.setting_status   =   false;

  $scope.setting_style  =   {};
  $scope.$state   =   $state;

  $scope.is_show  =   true;
  $scope.username = '';
  $scope.profile_image    = null  ;

  $scope.init   =   function(){
    if($state.current.name     ==  'credential'){
        $scope.is_show  =   false;
    }
    var user = Core.user_profile().getActiveProfile();
    if(user!=null){
      $scope.username = user.name;
      if($scope.username == null || $scope.username.length == 0){
        $scope.username = user.email;
        if($scope.username == null || $scope.username.length == 0){
          $scope.username = user.phonenumber;
        }
      }
      console.log(user.profile_media);
      if(user.profile_media != null){
        $scope.profile_image    =   user.profile_media.uri;
      }
    }

  }
  $scope.init();
  $scope.toggle_setting   =   function(){
      $scope.setting_status   =   !$scope.setting_status;
      if($scope.setting_status === true){
          $scope.setting_style  =   {'height':'252px'};
      } else {
        $scope.setting_style  =   {'height':'0'};
      }
  };

  $scope.go   =   function(state){
    $state.go(state);
  };

  $scope.logout   =   function(){
    Core.user().logout();
    $ionicLoading.show({
        template: '<ion-spinner icon="android"></ion-spinner>'
    });

    $timeout(function(){
      $ionicLoading.hide();
      $timeout(function(){
        $state.go('credential');
      },10);
    },1000);
    // User.logout().then(function(){
    //   $state.go('intro');
    // });
  };

}])
