/*
 |-------------------------------------------------------------------
 | LogWidgetController - Controller for log
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
angular.module('hcncore.controllers')
.controller('MessengerController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "$stateParams", "$ionicScrollDelegate", function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,$stateParams,$ionicScrollDelegate){

  $scope.$state    =  $state;
  $scope.v  =   {};
  var v   =   $scope.v;

  v.message   =   '';
  v.other_profile   =   null;
  v.reder_profile   =   null;
  v.loader  =   true;

  v.messages  =   [];
  v.existing_message_id   =   [];
  $scope.init   =   function(){
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
    }, 10);
    v.dest_id   =   $stateParams.dest_id;
    // console.log($scope.v.dest_id);
    $scope.fetch_parties_profile();
    v.existing_message_id   =   [];

  }

  //function to the other party profile
  $scope.fetch_parties_profile  =   function(){
    var profile   = Core.user_profile().getActiveProfile();
    // console.log(profile);
    if(profile.profile_media == null){
      profile.profile_media_url   =   './img/user_account.png';
    } else {
      profile.profile_media_url   =   profile.profile_media.uri;
    }
    v.other_profile     =   profile;

    var logged_in_user  =   Core.user().logged_in_user();

    angular.forEach(logged_in_user.profiles,function(profile){
      if(parseInt(profile.is_virtual) == 0){
        v.reder_profile   =   profile;
      }
    });

    $scope.initialize_messages();
  }

  //Function to send message to party
  $scope.send_message   =   function(){
    Core.social_messages().messages().add({
      'reader_id':v.reder_profile.id,
      'other_id':v.other_profile.id,
    },{'message':v.message},function(data){
      v.messages.push(data.message);
      v.message   =   '';
      $ionicScrollDelegate.resize();
      $ionicScrollDelegate.scrollBottom(true);
    },function(err){
      // v.loader  =   false;
      console.log(err);
    });
  }

  //Function to fetch the new message
  $scope.fetch_new_message = function(init){
    if(init == undefined){
      v.loader  =   false;
    } else {
      v.loader  =   true;
    }
    Core.social_messages().messages().get({
      'reader_id':v.reder_profile.id,
      'other_id':v.other_profile.id,
      'new_messages':'1'
    },function(data){
      v.loader  =   false;
      var readed_messages =   [];
      angular.forEach(data.messages,function(m){
        if(m.src_id != v.reder_profile.id){
            readed_messages.push(m.id);
        }
        if(v.existing_message_id.indexOf(m.id) == -1){

            v.messages.push(m);
            v.existing_message_id.push(m.id);
        }
      });
      if(readed_messages.length > 0){

        Core.social_messages().messages().update({
          'reader_id':v.reder_profile.id
        },{'messages_id':readed_messages.join(',')});
      }
      $ionicScrollDelegate.resize();
      $ionicScrollDelegate.scrollBottom(true);
      // if(init != undefined){
      //   $scope.fetch_old_message();
      // }
    },function(err){
      v.loader  =   false;
      console.log(err);
    });
  }

  //Function to fetch the old message
  $scope.fetch_old_message    =   function(init){
    v.loader  =   true;
    Core.social_messages().messages().get({
      'reader_id':v.reder_profile.id,
      'other_id':v.other_profile.id,
      // 'new_messages':'1'
    },function(data){
      v.loader  =   false;
      angular.forEach(data.messages,function(m){
        // console.log(v.existing_message_id);
        if(v.existing_message_id.indexOf(m.id) == -1){
            v.messages.push(m);
            v.existing_message_id.push(m.id);
        }
          // v.messages.push(m);
          $ionicScrollDelegate.resize();
      });
      if(init != undefined){
        $scope.fetch_new_message(true);
      }
    },function(err){
      v.loader  =   false;
      console.log(err);
    });
  }
  $scope.new_message_thread  =  0;
  $scope.initialize_messages  =   function(){
    v.messages  =   [];
    $scope.fetch_old_message(true)


    $scope.new_message_thread   =   setInterval(function(){
      $scope.fetch_new_message();
    },5000);
  }


  $rootScope.$on('$stateChangeSuccess',
    function(event, toState, toParams, fromState, fromParams, options){
      if(toState.name == 'messennger'){
          $scope.init();
      }
    });

    $rootScope.$on('$stateChangeStart',
      function(event, toState, toParams, fromState, fromParams, options){
        if(toState.name != 'messennger' && fromState.name == 'messennger'){
            clearInterval($scope.new_message_thread);
        }
      });
  $scope.init();
}])
