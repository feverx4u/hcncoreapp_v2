/*
|------------------------------------------------------------------------------
|  DateTimePickerServices - Common Service for Schedule and DateTimePicker
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-10-20
*/
angular.module('hcncore.services')
.factory('DateTimePickerServices',function(){
   var date = '';
   var time = '';
   return {
       getDate: function () {
           return date;
       },
       setDate: function(datevalue) {
           date = datevalue;
       },

       getTime: function () {
           return time;
       },
       setTime: function(timevalue) {
           time = timevalue;
       }
   };
})
