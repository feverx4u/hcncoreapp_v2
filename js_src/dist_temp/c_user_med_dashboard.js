angular.module('hcncore.controllers')
.controller('UserMedDashabordController',["$rootScope", "$scope", "$state", "$timeout", "Core", "$ionicPopup", "$cordovaToast", "$ionicLoading", "$ionicHistory", "$ionicModal", function($rootScope,$scope,$state,$timeout,Core,$ionicPopup,
$cordovaToast,$ionicLoading,$ionicHistory,$ionicModal){

   $scope.init   =   function(){
     $timeout(function () {
         $rootScope.is_big_bar   =   false;
     }, 10);
    $ionicHistory.clearHistory();
    $scope.fetch_login_user();
    // $scope.search_contacts();
    // Core.user_profile().setActiveProfile(null);

    $scope.network_loading   = true;
    $scope.galleryOptions = {
        pagination: '.swiper-pagination',
        slidesPerView: 3,
        freeMode: true,
        spaceBetween: 10
      };
   }


   $scope.profiles     =   [];
   $scope.contacts   =   [];

   $scope.v  =   {};
   $scope.fetch_login_user   =   function(){
     var v   =   $scope.v;
     v.user_name   =   '';
     v.is_search   =   '';
     v.is_search_contact   =   false;

     var user  = Core.user().logged_in_user();
     if(user == null){
       return;
     }

     if(user.profiles != null){
       $scope.profiles = user.profiles;

      //  console.log(user);
       Core.user_profile().setActiveProfile(null);
       $scope.fetch_network();
     }
   }

   $scope.network  = {};
   $scope.fetch_network  =   function(){
     $scope.network_loading   = true;
     $scope.network  = {};
     Core.med_social().network().get({},function(data){
       $scope.network  =   data;
       $scope.network_loading   = false;
       angular.forEach($scope.network.monitoring,function(person){
         if(person.destination_profile.profile_media == null || person.destination_profile.profile_media.uri == null){
           person.destination_profile.profile_media_url   =   './img/user_account.png';

         } else {
           person.destination_profile.profile_media_url   =   person.destination_profile.profile_media.uri;
         }
       });


       angular.forEach($scope.network.monitored_by,function(person){
         if(person.source_profile.profile_media == null || person.source_profile.profile_media.uri == null){
           person.source_profile.profile_media_url   =   './img/user_account.png';

         } else {
           person.source_profile.profile_media_url   =   person.source_profile.profile_media.uri;
         }
       });
       $scope.$broadcast('scroll.refreshComplete');
     },function(error){
       $scope.network_loading   = false;
       $scope.$broadcast('scroll.refreshComplete');
     });
   }

   $scope.take_to_med_request  =   function(person){
     $state.go('med_request',{'phone_number':person.source_profile.phone_number,'relation':person.relation,'name':person.source_profile.name});
   }

   $scope.take_to_med_profile  =   function(contact){
     $scope.search_modal.hide();
     $state.go('med_profile',{'phone_number':contact.phoneNumbers,'name':contact.name});
   }

   $scope.search_contacts  =   function(){
     var v   =   $scope.v;
     v.is_search_contact = true;
     Core.phone_contacts().getAllContacts().then(function(data){
       $scope.contacts   =   data;
       console.log(data);
       v.is_search_contact = false;
     });
   }

   $scope.take_to_internal_profile  =   function(profile,other){
     console.log(['profile',profile]);
     Core.user_profile().setActiveProfile(profile);
     if(other == undefined){
       if(profile.is_init_flow == 0){
         $state.go('user_info');
       } else {
        //  $ionicLoading.show({
        //      template: '<ion-spinner icon="android"></ion-spinner>'
        //  });
            $state.go('main_tabs.home_widget_tab.dashboard');

       }
     } else {
       if(parseInt(profile.is_init_flow) == 1){
            $state.go('main_tabs.home_widget_tab.dashboard');
       } else {
         $cordovaToast.show('User is yet complete his profile','short','center');
       }
     }

   }

   // modal for user search
   $scope.search_modal  =   null;
   $scope.search_data   =   null;
   $ionicModal.fromTemplateUrl('med_dash_search_modal.html', {
     scope: $scope,
     animation: 'slide-in-up'
   }).then(function(modal) {
     $scope.search_modal = modal;
     $scope.search_contacts();
   });

   //Method is used to open the user search modal
   $scope.open_search_modal   =   function(){
     $scope.search_modal.show();
   }

     $scope.goBack  =   function(){
       $scope.v.user_name = '';
       $scope.search_modal.hide();
     }

     $scope.user_pending_popup  =   null;
     $scope.open_user_pending_popup  =   function(){
       $scope.user_pending_popup = $ionicPopup.show({
         templateUrl: 'user_pending_popup.html',
         scope: $scope,
         'cssClass':'custom-class'
       });
     }


     $scope.take_to_messages  =   function(person){
       var profile  = person.source_profile;
       console.log(profile);
       Core.user_profile().setActiveProfile(profile);
       $state.go('messennger',{'dest_id':profile.id});
     }
   $scope.init();

   $rootScope.$on('$stateChangeSuccess',
     function(event, toState, toParams, fromState, fromParams, options){
       if(toState.name == 'med_dashboard'){
        //  console.log('I am at med dashboard');
           $scope.init();
       }
     });
}])
