/*
|------------------------------------------------------------------------------
|  PhrEventType - Service to Add Phr event type Info from app
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-11-04
*/
angular.module('hcncore.services')
.factory('PhrEventType',["Log", "Network", "$q", "$resource", "locker", "User", "UserProfile", "$timeout", function(Log,Network,$q,$resource,locker,User,UserProfile,$timeout){
  var factory   =   {};
  function resource(){

      var user  =   User.logged_in_user();
      if(user == null){
        return null;
      }

      var profile  =   UserProfile.getActiveProfile();
      if(profile ==  null){
        return null;
      }
      var url   =   Network.api_url+'/profile/'+profile.id+'/phr_event/:event_name/type';

      return  $resource(url, {},{
          'get': { method:'GET',params:{}},
          'show': { method:'GET',params:{}},
      });
    }

    factory.get  =   function(event_name){
      var deferred  =   $q.defer();

      var user  =   User.logged_in_user();
      if(user == null){
         deferred.reject(null);
         return deferred.promise;
      }

      var profile  =   UserProfile.getActiveProfile();
      if(profile ==  null){
        return null;
      }

      var user_id  =   profile.id;
      $timeout(function(){
        resource().get({
          'event_name' : event_name
        },function(data){
            Log.info('PhrEventType:get',data);
            deferred.resolve(data);
        },function(data){
            Log.error('PhrEventType:get',data);
            deferred.reject(data);
        });
      },500);

      return deferred.promise;
    }

  return factory;
}])
