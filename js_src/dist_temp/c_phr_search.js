/*
   |-------------------------------------------------------------------
   | PhrSearchController - Controller for PHR Search Feature
   |-------------------------------------------------------------------
   |  @author:  Shekh Rizwan<rizwan@hirarky.com>
   |  @date:  2016-11-01
   */
  angular.module('hcncore.controllers')
 .controller('PhrSearchController',["$rootScope", "$scope", "$ionicPopup", "$state", "$cordovaToast", "$timeout", "Core", "$ionicLoading", "$timeout", "ionicDatePicker", "$ionicHistory", function($rootScope,$scope,$ionicPopup,$state,$cordovaToast,$timeout,Core,$ionicLoading,$timeout,ionicDatePicker,$ionicHistory){
    $scope.$state    =  $state;
    $scope.$history    =  $ionicHistory;
    $scope.v  =   {};
    // $scope.type_array = ["Insurance card","Laboratory Reports","Medical Prescription","Doctor Consultation"
    //                      ,"Discharge Summary","Identification Records","Operation/Surgery Report",
    //                      "Health Progress Report","X Ray Report","MRI Reports","CT Scan","Imaging Reports",
    //                      "Medicine Bill","Hospital Bill","Other Bills","Immunization Records",
    //                      "Vaccination Reports","Hospital Admission Records","Hospital Records","Hospital Card"];


    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();
      $scope.phr_show_event();
    }
    $scope.variables   =  function(){
        var v   =   $scope.v;
        v.phr_search_input  =  {
            'event_name':'',
            'type':'',
            'to_date_view':'',
            'to_date':'',
            'from_date_view':'',
            'from_date':'',
            'modify_data':false,
            'page_size':300
        };

        v.phr_search_input.to_date     =   moment().format('YYYY-MM-DD');
        v.phr_search_input.from_date     =   moment().subtract(7,'days').format('YYYY-MM-DD');
        $scope.revalidate_date();
    }

    $scope.event_array =[];
     $scope.phr_show_event   =   function(){
       $ionicLoading.show({
           template: '<ion-spinner icon="android"></ion-spinner>'
       });
       Core.phrdata().phr_event().get().then(function(data){
         angular.forEach(data.data,function(data){
                 $scope.event_array.push(data);
                 console.log(data);
         });
         $ionicLoading.hide();
       },function(error){
         console.log(error);
         $ionicLoading.hide();
       },function(update){
         console.log(update);
         $ionicLoading.hide();
       });
     }

    $scope.open_datepicker_to  =   function(){
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.phr_search_input.to_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
          // console.log(val)
        },
        templateType: 'popup',
        to:new Date( moment().format('YYYY-MM-DD')),
        inputDate:new Date(moment())
      });
    }

    $scope.open_datepicker_from  =   function(){
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.phr_search_input.from_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
          // console.log(val)
        },
        templateType: 'popup',
        to:new Date( moment().format('YYYY-MM-DD')),
        inputDate:new Date(moment())
      });
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.phr_search_input.to_date_view     =   moment(v.phr_search_input.to_date).format('Do MMM,YYYY');
        v.phr_search_input.from_date_view     =   moment(v.phr_search_input.from_date).format('Do MMM,YYYY');
    }

    $scope.modify_event = function(){
       var v   =   $scope.v;
       $scope.media_array = [];
       v.phr_search_input.modify_data = false;
    }

    $scope.search_event = function(){
       var v   =   $scope.v;
       v.phr_search_input.modify_data = true;
         $scope.phr_show_data();
    }

    // Phr show API Calling Method
    $scope.media_array = [];
    $scope.media_record = [];
     $scope.phr_show_data   =   function(){

       $ionicLoading.show({
           template: '<ion-spinner icon="android"></ion-spinner>'
       });
       $scope.media_record     =   [];
       Core.phrdata().phr_search().get($scope.v.phr_search_input).then(function(data){
         angular.forEach(data.data,function(data){
           console.log(data);
           $scope.media_record.push(data);

         });
         console.log($scope.media_array);
           $ionicLoading.hide();
       },function(error){
         console.log("error "+error);
         $ionicLoading.hide();
       },function(update){
         console.log("update "+update);
         $ionicLoading.hide();
       });
     }

     // popup for get Image/Doc details
     $scope.create_doc_details_popup  =   null;
     $scope.v.current_record_to_see   =   {};
     $scope.show_doc_details_popup  =   function(record){
        $scope.v.current_record_to_see = record;
       $scope.create_doc_details_popup = $ionicPopup.show({
         templateUrl: 'show_doc_details_popup.html',
         title: 'PHR DETAILS',
         scope: $scope,
         'cssClass':'create_event_popup'
       });
     }

     $scope.view_doc_details_popup  =  function(media_uri){
       $scope.create_doc_details_popup.close();
       $scope.view_file(media_uri);
     }
     $scope.close_doc_details_popup  =  function(){
       $scope.create_doc_details_popup.close();
     }

     // file opner method
     $scope.view_file    =   function(file){
       console.log(file);
       window.open(file,'_system');
     }

     $scope.cleardata = function(){
       var v   =   $scope.v;
       v.phr_search_input  =  {
           'event_name':'',
           'type':'',
           'to_date_view':'',
           'to_date':'',
           'from_date_view':'',
           'from_date':'',
           'modify_data':false,
           'page_size':300
       };

       v.phr_search_input.to_date     =   moment().format('YYYY-MM-DD');
       v.phr_search_input.from_date     =   moment().subtract(7,'days').format('YYYY-MM-DD');
       $scope.revalidate_date();
     }

    $scope.init();
   }])
