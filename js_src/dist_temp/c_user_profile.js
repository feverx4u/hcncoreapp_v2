angular.module('hcncore.controllers')
.controller('UserProfileController',["$scope", "$rootScope", "$state", "$timeout", "Core", "ionicDatePicker", "$cordovaToast", "$ionicHistory", "$cordovaImagePicker", "$jrCrop", "$cordovaCamera", "Media", "$cordovaToast", "$ionicLoading", "$ionicHistory", "$ionicPopup", function($scope,$rootScope,$state,$timeout,Core,ionicDatePicker,$cordovaToast,$ionicHistory,
    $cordovaImagePicker,$jrCrop,$cordovaCamera,Media,$cordovaToast,$ionicLoading,$ionicHistory,$ionicPopup){
  $scope.$state   =   $state;

  $scope.v  =   {};

  $scope.v.gender_options   =   {
    'MALE':'Male',
    'FEMALE':'Female'
  };

  $scope.init   =   function(){
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
    }, 10);
    $scope.variables();
    $scope.fetch_user_info();
  }

  $scope.go_back  =   function(){
    // alert(1111);
    $ionicHistory.goBack();
  }

  $scope.variables    =   function(){
    var v = $scope.v;
    v.user_info   =   {
      'name':'',
      'email':'',
      'dob':'',
      'gender':'',
      'gender_view':'',
      'height':'',
      'height_unit':'',
      'weight':'',
      'weight_unit':'',
      'blood_group':''
    }

    v.user_life_style   =   {
      'alergies':'',
      'smoking':'NEVER',
      'smoking_freq':'0',
      'driking':'NEVER',
      'drinking_freq':'0',
      'tabacco':'NEVER',
      'tabacco_freq':'0',
      'excerise':'NEVER',
      'excerise_freq':'0',
      'job_type':'NEVER',
      'job_travel':'RARELY',
      'eating_habit':'NEVER',
      'eating_habit_freq':'DONT_KNOW',
      'last_travel':'LAST_15_DAYS',
      'where_loc':'Mumbai'
    };

    v.contact_info    =   {
      'phonr_number':'',
      'address_1':'',
      'address_2':'',
      'city':'',
      'state':'',
      'zip':''
    };
  }

  $scope.$watch('v.user_info.gender',function(){
    // console.log(111);
    if($scope.v.user_info.gender == 'MALE'){
      $scope.v.user_info.gender_view  =   'Male';
    } else {
      $scope.v.user_info.gender_view  =   'Female';
    }
  });

  $scope.fetch_user_info  = function(){
    var v   =   $scope.v;

    var user = Core.user_profile().getActiveProfile();
    console.log(user);
    v.user_info   =   {
      'name':user.name,
      'email':user.email,
      'dob':moment(user.dob).format('Do MMM,YYYY'),
      'gender':user.gender,
      'gender_view':'',
      'height':user.height,
      'height_unit':user.height_unit,
      'weight':user.weight,
      'weight_unit':user.weight_unit,
      'blood_group':user.blood_group,
      'profile_pic_media':user.profile_pic_media,
      'profile_image':'',
    };

    if(user.profile_media != null){
      v.user_info.profile_image   =   Core.network().web+'/'+user.profile_media.path;
    }

    // console.log(user.profile_media);
    v.contact_info    =   {
      'contact_num':user.phone_number,
      'address_1':user.address_1,
      'address_2':user.address_2,
      'city':user.city,
      'state':user.state,
      'zip':user.zip
    };

    v.user_life_style   =   {
      'alergies':user.alergies,
      'smoking':user.smoking==undefined||user.smoking==null?"NEVER":user.smoking,
      'smoking_freq':user.smoking_freq,
      'driking':user.drinking==undefined||user.drinking==null?"NEVER":user.drinking,
      'drinking_freq':user.drinking_freq,
      'tabacco':user.tabacco==undefined||user.drinking==null?"NEVER":user.tabacco,
      'tabacco_freq':user.tabacco_freq,
      'excerise':user.excerise==undefined||user.excerise==null?"NEVER":user.excerise,
      'excerise_freq':user.excerise_freq,
      'job_type':user.job_type==undefined||user.job_type==null?"NEVER":user.job_type,
      'job_travel':user.job_travel,
      'eating_habit':user.eating_habit==undefined||user.eating_habit==null?"NEVER":user.eating_habit,
      'eating_habit_freq':user.eating_habit_freq,
      'last_travel':user.last_travel==undefined||user.last_travel==null?"":user.last_travel,
      'where_loc':user.where_loc
    };

    if($scope.v.user_info.gender == 'MALE'){
      $scope.v.user_info.gender_view  =   'Male';
    } else {
      $scope.v.user_info.gender_view  =   'Female';
    }
    // console.log(user);
  }

  $scope.update_user_info   =   function(){
      var v   =   $scope.v;
      Core.user_profile().update_user_info(v.user_info).then(function(data){
        console.log(data);
        // Core.user_profile().setActiveProfile(data);
        $cordovaToast.show("User Updated",'long','center');
      },function(error){
        console.log(error.message);
        $cordovaToast.show(error.message,'short','center');
      });
  }

  $scope.update_contact_info   =   function(){
      var v   =   $scope.v;
      Core.user().update_user_other_info(v.contact_info).then(function(data){
        $cordovaToast.show("User Updated",'long','center');
      },function(error){
        console.log(error.message);
        $cordovaToast.show(error.message,'short','center');
      });
  }

  $scope.update_lifestyle_info   =   function(){
      var v   =   $scope.v;
      Core.user().update_user_other_info(v.user_life_style).then(function(data){
        $cordovaToast.show("User Updated",'long','center');
      },function(error){
        console.log(error.message);
        $cordovaToast.show(error.message,'short','center');
      });
  }

  $scope.trigger_click  =   function(id){
    var event = new MouseEvent('mousedown', {
       // 'view': window,
       // 'bubbles': true,
       'cancelable': true
   });
  //  alert(id);
   var cb = document.getElementById(id);
   // cb.size=4;
   cb.dispatchEvent(event);
  }

  $scope.open_date_picker    =  function(){
    // alert(111);
    var v   =   $scope.v;
    var default_date  = '';
    if(v.user_info.dob != ''){
        default_date  =   moment(v.user_info.dob, 'Do MMM,YYYY');
    } else  {
      default_date  =   moment('1991-01-01');
    }


    ionicDatePicker.openDatePicker({
      callback:function(val){
        v.user_info.dob   =   moment(val).format('Do MMM,YYYY');
      },
      templateType: 'popup',
      to:new Date( moment().subtract(3,'years').format('YYYY-MM-DD')),
      inputDate:new Date(default_date)
    });
  }


  $scope.upload_popup  =   null;
  $scope.upload_options   =   function(){
    $scope.upload_popup = $ionicPopup.show({
      templateUrl: 'file_upload_popup.html',
      title: 'Upload',
      scope: $scope,
      'cssClass':'report_popup'
    });
  }

  $scope.close_upload_popup    =   function(){
    $scope.upload_popup.close();
  }

  function take_me_to_croper(uri){
    $jrCrop.crop({
         url: uri,
         width: 200,
         height: 200,
         // title: 'Move and Scale'
     }).then(function(canvas) {
         // success!
         var image = canvas.toDataURL();

         Media.upload(image,'BASE_64_IMAGE',function(data){
           var response  =   JSON.parse(data.currentTarget.response);
           var media   =   response.media;
           $scope.v.user_info.profile_pic_media   =   media.id;
         });
         $scope.v.user_info.profile_image   = image;
         $scope.v.image_changed   = true;
         // console.log(image);
     }, function(error) {
         console.log(error);
         // User canceled or couldn't load image.
     });
  }


  $scope.open_image_picker   =   function(){
    var options = {
      maximumImagesCount: 1,
      width: 800,
      height: 800,
     };

     $scope.close_upload_popup();
    $cordovaImagePicker.getPictures(options)
    .then(function (results) {
       for (var i = 0; i < results.length; i++) {
         console.log('Image URI: ' + results[i]);
          take_me_to_croper(results[i]);
       }
     }, function(error) {
       // error getting photos
     });
  }

  $scope.open_camera_for_image   =   function(){
       $scope.close_upload_popup();
        var options = {
          quality: 50,
          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.CAMERA,
          encodingType: Camera.EncodingType.PNG,
          targetWidth:800,
          popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false,
          correctOrientation:true
       };

       $cordovaCamera.getPicture(options).then(function(imageURI) {
         take_me_to_croper(imageURI);
       }, function(err) {
         // error
       });
  }

  $scope.remove_image  =   function(){
     $scope.v.profile_image   = null;
     $scope.close_upload_popup();
     $scope.v.image_changed  = true;
  }

  $scope.init();
}])
