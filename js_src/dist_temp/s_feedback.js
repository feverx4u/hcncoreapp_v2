/*
|------------------------------------------------------------------------------
|  Schedule Activity - Service to Schedule the Info from activity
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('Feedback',["Log", "Network", "$q", "$resource", "locker", "User", "UserProfile", function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};

  function resource(){


    var url   =   Network.api_url+'/feedback';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  factory.commit  =   function(data){

    var deferred  =   $q.defer();

          if(data.phone_number == null || data.phone_number.length <= 0 ){
            deferred.reject({
              'status':400,
              'message':'Phone number not found'
            });
            return deferred.promise;
          }

          var user_data   =   data;
          resource().add({
            'phone_number':data.phone_number,
            'name':data.name,
            'email':data.email,
            'message':data.message,
          },function(data){
              Log.info('Feedback:commit',data);
              deferred.resolve(data);
          },function(data){
              Log.error('Feedback:commit',data);
              deferred.reject(error);
          });
          return deferred.promise;
        }
        return factory;
      }])
