/*
|------------------------------------------------------------------------------
|  BGLLog - Service to Log the Info from food
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('BGLLog',["Log", "Network", "$q", "$resource", "locker", "User", "UserProfile", function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};

  function resource(){
    var url   =   Network.api_url+'/log/blood_glucose';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  function events(){
    var url   =   Network.api_url+'/day_event';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
    });
  }

  factory.event  =   function(){
      var deferred  =   $q.defer();
            events().get({
            },function(data){
              Log.info('BGLLog:event',data.events);
                deferred.resolve({
                  'staut':200,
                  'message':data.events
                });
              // if(data.foods != null && data.foods.food != undefined){
              //   deferred.resolve({
              //     'staut':200,
              //     'message':data.foods.food
              //   });
              // } else {
              //   deferred.resolve({
              //     'staut':200,
              //     'message':[]
              //   });
              // }
            });

      return deferred.promise;
  }


  factory.commit  =   function(data){

    var deferred  =   $q.defer();

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }

    var profile  =   UserProfile.getActiveProfile();
    if(profile ==  null){
      return null;
    }

    data.profile_id   =  profile.id;


    // if(data.activity_id == null || data.activity_id.length <= 0 ){
    //   deferred.reject({
    //     'staut':400,
    //     'message':'Please choose activity'
    //   });
    //   return deferred.promise;
    // }
    if(data.blood_glucose_level == null || data.blood_glucose_level.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please enter blood glucose level'
      });
      return deferred.promise;
    }
    var bgl_val   =   parseFloat(data.blood_glucose_level);
    if(data.unit  ==  'mg_dl'){
      if(bgl_val > 999 || bgl_val < 0){
        deferred.reject({
          'staut':400,
          'message':'Glucose level should be less than 999'
        });
        return deferred.promise;
      }
    } else {
      if(bgl_val > 99 || bgl_val < 0){
        deferred.reject({
          'staut':400,
          'message':'Glucose level should be less than 99'
        });
        return deferred.promise;
      }
    }

    if(data.event_id == null || data.event_id.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please enter event'
      });
      return deferred.promise;
    }

    var user_data   =   data;
    resource().add(data,function(data){
        Log.info('BGLLog:commit',data);
        deferred.resolve(data);
    },function(data){
        Log.error('BGLLog:commit',data);
        deferred.reject(error);
    });
    return deferred.promise;
  }
  return factory;
}])
