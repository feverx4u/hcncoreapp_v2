/*
 | Factory to manage the Reports for User
 |
 |
 */
 angular.module('hcncore.services')
 .factory('UserReports',["Network", "$q", "$resource", "locker", "User", "Log", "$timeout", "FileOp", "UserProfile", function(Network,$q,$resource,locker,User,Log,$timeout,FileOp,UserProfile){

   var factory   =   {};

   function resource(){

     var user  =   User.logged_in_user();
     if(user == null){
       return null;
     }

     var profile  =   UserProfile.getActiveProfile();
     if(profile ==  null){
       return null;
     }

     var url   =   Network.api_url+'/profile/'+profile.id+'/report';

     return  $resource(url, {},{
         'get': { method:'POST',params:{}},
     });
   }

   factory.generate_report  =   function(data){
     var deferred  =   $q.defer();

     var user  =   User.logged_in_user();
     if(user == null){
        deferred.reject(null);
        return deferred.promise;
     }
     var user_id  =   user.id;

     var profile  =   UserProfile.getActiveProfile();
     if(profile ==  null){
       return null;
     }


     resource().get(data,function(data){
         Log.info('UserReports:generate_report',data);

         // FileOp.createDir('hcncore');
         FileOp.checkDir('reports').then(function(){

         },function(){
           FileOp.createDir('reports').then(function(data){
             console.log(data);
           },function(data){
             console.log(data);
           });
         });

         FileOp.download_file(data.report.path,'reports/HCNCORE_'+moment().format("YYYY-MM-DD_HHmmss")+'.pdf').then(function(data){
           var _reports = locker.namespace('hcncore_'+profile.id).get('rerports',[]);
           _reports.push(data);
           locker.namespace('hcncore_'+profile.id).put('rerports',_reports);
           deferred.resolve(data);
         },function(error){
           deferred.reject(error);
         },function(progress){
           deferred.notify(progress);
         });
     },function(data){
         Log.error('UserReports:generate_report',data);
         deferred.reject(data);
     });

     return deferred.promise;
   }

   factory.get_archive_reports    = function(){
     var user  =   User.logged_in_user();
     if(user == null){
        deferred.reject(null);
        return deferred.promise;
     }

     var profile  =   UserProfile.getActiveProfile();
     if(profile ==  null){
       return null;
     }


     var user_id  =   user.id;
     console.log(locker.namespace('hcncore_'+profile.id).get('rerports',[]));
     return locker.namespace('hcncore_'+profile.id).get('rerports',[]);
   }

   factory.delete_report   =   function(i){

     var profile  =   UserProfile.getActiveProfile();
     if(profile ==  null){
       return null;
     }

     var reports   =   locker.namespace('hcncore_'+profile.id).get('rerports',[]);

     reports.splice(i,1);
     cosole.log(reports);
     locker.namespace('hcncore_'+profile.id).put('rerports',reports);
   }
   return factory;
 }])
