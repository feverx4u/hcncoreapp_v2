/*
   |-------------------------------------------------------------------
   | CommonControllerForProfileHeader - Controller for fetch USERNAME and PROFILE
   |-------------------------------------------------------------------
   |  @author:  Shekh Rizwan<rizwan@hirarky.com>
   |  @date:  2016-11-09
   */
   angular.module('hcncore.controllers')
   .controller('CommonControllerForProfile',["$scope", "$rootScope", "$ionicPopup", "$ionicPopover", "$state", "$cordovaToast", "$timeout", "Core", "$ionicLoading", "$timeout", function($scope,$rootScope,$ionicPopup,$ionicPopover,$state,$cordovaToast,$timeout,Core,$ionicLoading,$timeout){
     $scope.$state    =  $state;
     $scope.v  =   {};

     $scope.profile_id   =   null;
     $scope.init   =   function(){
       var user = Core.user_profile().getActiveProfile();
       var profile_type   =   Core.user_profile().getActiveProfileType();
       $scope.active_flag = profile_type;
       console.log(['sdahgjhdsgjhgsadsad',profile_type]);
       if(user!=null){
         $scope.username = user.name;
         $scope.profile_id   =   user.id;
         if($scope.username == null || $scope.username.length == 0){
           $scope.username = user.email;
           if($scope.username == null || $scope.username.length == 0){
             $scope.username = user.phonenumber;
           }
         }
         if(user.profile_media != null){
           $scope.profile_image    =   user.profile_media.uri;
         }
       }

      //  $scope.user_profile_type = user.active_flag;
     }

    $ionicPopover.fromTemplateUrl('popover.html', {
    scope: $scope,
    cssClass:'hcnmenu'
     }).then(function(popover) {
        $scope.popover = popover;
     });

     $scope.openPopover = function($event) {
        $scope.popover.show($event);
     };

     $scope.closePopover = function() {
        $scope.popover.hide();
     };

     //Cleanup the popover when we're done with it!
     $scope.$on('$destroy', function() {
       if($scope.popover == undefined){
         return;
       }
        $scope.popover.remove();
     });

     // Execute action on hide popover
     $scope.$on('popover.hidden', function() {
        // Execute action
     });

     // Execute action on remove popover
     $scope.$on('popover.removed', function() {
        // Execute action
     });

     $scope.goto_aboutus_page = function(){
       $state.go('about_us');
       $scope.closePopover();
     }
     $scope.goto_notification_page = function(){
       $state.go('notification');
       $scope.closePopover();
     }
     $scope.goto_helth_asses_page = function(){
       $state.go('health');
       $scope.closePopover();
     }
     $scope.goto_feedback_page = function(){
       $state.go('feedback');
       $scope.closePopover();
     }
     $scope.goto_rate_review_page = function(){
       $scope.open_appreview_popup();
       $scope.closePopover();
     }
     $scope.goto_share_page = function(){
       $state.go('share');
       $scope.closePopover();
     }
     $scope.goto_settings_page = function(){
       $state.go('settings');
       $scope.closePopover();
     }


     $scope.v.is_select = "YES";
     $scope.v.user_rating   =   0;

     $scope.appreview_popup  =   null;
     $scope.is_event_bind   =   false;
     $scope.open_appreview_popup  =   function(){
       $scope.closePopover();
       $scope.v.user_rating   =   0;
       $scope.appreview_popup = $ionicPopup.show({
         templateUrl: 'app_reviews_popup.html',
         scope: $scope,
         'cssClass':'custom-class'
       })
         if(!$scope.is_event_bind){
           console.log(res);
            $scope.is_event_bind   =   true;
           var htmlEl = angular.element(document.querySelector('html'));
              htmlEl.on('click', function (event) {
                       $scope.appreview_popup.close();
              });
         }
     }

     $scope.close_appreview_popup  =  function(){
       $scope.appreview_popup.close();
     }

     $scope.review_final_popup  =   null;
     $scope.set_rating = function(value){
       if(value   < 0){
         $scope.v.user_rating   =   0;
       } else if(value > 5) {
         $scope.v.user_rating   =   5;
       } else {
         $scope.v.user_rating   =   value;
       }

       $timeout(function(){
         var template_url =   'for_5_app_review.html';
         if($scope.v.user_rating <  5){
           template_url =   'for_less5_app_review.html';
         }
         $scope.appreview_popup.close();
         $scope.review_final_popup = $ionicPopup.show({
           templateUrl: template_url,
           scope: $scope,
           'cssClass':'custom-class'
         });
       },1000);
     }

     $scope.final_review_popup_close =function(){
       $scope.review_final_popup.close();
       if($scope.v.user_rating == 5){
         window.open('https://play.google.com','_system');
       } else {
         window.open('https://goo.gl/forms/rLJ68chjF9l6apxi2','_system');
       }

     }

     $scope.take_to_mail  =   function(profile_id){
       console.log(profile_id);
        $state.go('messennger',{'dest_id':profile_id});
        // alert('takking you yo mail');
     }

     $scope.init();
   }])
