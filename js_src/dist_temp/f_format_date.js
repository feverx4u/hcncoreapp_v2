angular.module('hcncore.filters')
.filter('format_date',function(){
  return function(date,format,format_to){
    return moment(date, format).format(format_to);
  }
})
