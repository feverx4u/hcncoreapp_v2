/*
 |-------------------------------------------------------------------
 | LogWeightWidgetController - Controller for log
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
angular.module('hcncore.controllers')
.controller('LogWeightWidgetController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "ionicTimePicker", "ionicDatePicker", "$cordovaToast", "$ionicLoading", "$ionicPopup", function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicLoading,$ionicPopup){

  $scope.$state    =  $state;
  $scope.v  =   {};

  $scope.init   =   function(){
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
    }, 10);
    $scope.variables();


  }

  $scope.variables   =  function(){
      var v   =   $scope.v;
      v.is_check_enabled  =   false;
      v.weight_log_input  =  {
          'existing_weight':'',
          'weight':'',
          'weight_unit':'KG',
          'log_date':'',
          'log_date_view':'',
          'log_time':'',
          'log_time_view':'',
      };

      v.weight_log_input.log_date     =   moment().format('YYYY-MM-DD');

      $scope.revalidate_date();

      var time      =   moment().format('YYYY-MM-DD hh:mm A');
      $scope.revalidate_time(time);

      var existing_weight   = Core.user_profile().get_user_weight();
      // cons
      if(existing_weight != null){
        v.weight_log_input.existing_weight   =   existing_weight.weight;
        v.weight_log_input.weight_unit   =   existing_weight.weight_unit;
      }

  }

  $scope.validate_for_check   =   function(){
    var v   =   $scope.v;
    if(v.weight_log_input.weight == null || v.weight_log_input.weight.length == 0){
      v.is_check_enabled  =   false;
      return;
    }
    v.is_check_enabled  =   true;
  }

  $scope.open_datepicker  =   function(){
    // alert(111);
    var v   =   $scope.v;
    ionicDatePicker.openDatePicker({
      callback:function(val){
        v.weight_log_input.log_date   =   moment(val).format('YYYY-MM-DD');
        $scope.revalidate_date();
        // console.log(val)
      },
      templateType: 'popup',
      to:new Date( moment().format('YYYY-MM-DD')),
      inputDate:new Date(moment())
    });
  }

  $scope.open_timepicker  =   function(){
    var v   =   $scope.v;
    var ipObj1 = {
      callback: function (val) {      //Mandatory
        if (typeof (val) === 'undefined') {
          console.log('Time not selected');
        } else {
          var selectedTime = new Date(val * 1000);
          $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
        }
      },
      inputTime: v.weight_log_input.log_time_time,   //Optional
      format: 12,         //Optional
      step: 1,           //Optional
      setLabel: 'Set'    //Optional
    };

    ionicTimePicker.openTimePicker(ipObj1);
  }
  $scope.revalidate_date =  function(){
      var v   =   $scope.v;
      v.weight_log_input.log_date_view     =   moment(v.weight_log_input.log_date).format('Do MMM,YYYY');
  }

  $scope.revalidate_time =  function(time){
      var v   =   $scope.v;
      v.weight_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
      console.log(v.weight_log_input.log_time);
      v.weight_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
      v.weight_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
  }

  $scope.log_weight   =   function(){
    $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
            });
    var v   =   $scope.v;
      Core.logdata().weight_log().commit(v.weight_log_input).then(function(data){
        $ionicLoading.hide();
        $scope.log_success_popup('Weight Logged');
        // $cordovaToast.show("Weight Logged",'long','center');
      },function(error){
        $ionicLoading.hide();
        console.log(error.message);
        $cordovaToast.show(error.message,'short','center');
      });
  }

  $scope.log_pop_message  =   null;
  $scope.log_popup  =   null;
  $scope.log_success_popup  =   function(message){
    $scope.log_pop_message  =   message;
    $scope.log_popup = $ionicPopup.show({
      templateUrl: 'log_sucess_popup.html',
      title: 'Successfully Logged',
      scope: $scope,
      'cssClass':'log_success_popup'
    });
  }

  $scope.close_log_success_popup  =  function(notes){
    $scope.log_popup.close();
    $timeout(function(){
      $state.go('main_tabs.log_widget');
      $rootScope.$broadcast('data:loggged');
    },100);
  }

  // Go back confirmation popup
   $scope.showConfirm = function() {
     var v   =   $scope.v;
     if(v.weight_log_input.weight == null || v.weight_log_input.weight.length <= 0){
       $state.go("main_tabs.log_widget");
     }else {
       var confirmPopup = $ionicPopup.confirm({
         title: 'Log Weight',
         template: 'Are you sure you want to go back?'
       });
       confirmPopup.then(function(res) {
         if(res) {
           $state.go("main_tabs.log_widget");
         } else {
           console.log('You are not sure');
         }
       });
     }
   };

  $scope.init();
}])
