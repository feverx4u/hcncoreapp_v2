/*
|------------------------------------------------------------------------------
|  LogInfo - Service to Log the Info from app
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('LogInfo',["WeightLog", "A1cLog", "BpLog", "MedicineLog", "ActivityLog", "FoodLog", "BGLLog", "SymptomsLog", function(WeightLog,A1cLog,BpLog,MedicineLog,ActivityLog,FoodLog,BGLLog,SymptomsLog){
  var factory   =   {};

  factory.weight_log  =   function(){
      return WeightLog;
  }

  factory.a1c_log   =   function(){
    return A1cLog;
  }

  factory.bp_log  =   function(){
    return BpLog;
  }

  factory.medicine_log  =   function(){
    return MedicineLog;
  }

  factory.activity_log  =   function(){
    return ActivityLog;
  }

  factory.food_log  =   function(){
    return FoodLog;
  }

  factory.bgl_log   =   function(){
    return BGLLog;
  }
  factory.symptoms_log  =   function(){
    return SymptomsLog;
  }

  return factory;
}])
