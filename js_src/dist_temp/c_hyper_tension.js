/*
 |-------------------------------------------------------------------
 | HyperTenstionController - Controller for hypertension
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
 angular.module('hcncore.controllers')
 .controller('HyperTenstionController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "$ionicLoading", "$cordovaToast", "ionicDatePicker", "$ionicHistory", function($rootScope,$scope,$state,$ionicPlatform,Core,
   $timeout,$ionicLoading,$cordovaToast,ionicDatePicker,$ionicHistory){

     $scope.v    =   {};

     //Method to initialize the variable
     $scope.variable   =   function(){
       var v   =   $scope.v;
       v.info   =   {
         'systolic_unit':'MMHG',
         'systolic':'',
         'diastolic_unit':'MMHG',
         'diastolic':'',
         'pulse_unit':'BPM',
         'pulse':''
       };
     }



     //Fecthing the loged in user
     $scope.fetch_user_details   =   function(){
       var v   =   $scope.v;
       var user  = Core.user().logged_in_user();
       if(user == null){
         return;
       }
      //  v.user_info.name  = user.name;
      //  v.user_info.dob    = moment(user.dob).format( 'Do MMM,YYYY');
      //  v.user_info.gender   = user.gender;
      //  v.user_info.height   = user.height;
      //  v.user_info.weight   = user.weight;
      //  v.user_info.height_unit   = user.height_unit;
      //  v.user_info.weight_unit   = user.weight_unit;
     }

     //Initialisig the init
     $scope.init    =  function(){
       $timeout(function () {
           $rootScope.is_big_bar   =   true;
       }, 10);
       $scope.variable();
       $scope.fetch_user_details();
     }

     //Inititalizing the updated user
     $scope.update_info   =   function(){
       var info   =   $scope.v.info;
       Core.hypertension().update_hypertension(info).then(function(data){
         // $cordovaToast.show(error.message,'short','bottom');
         // $cordovaToast.show('User updated','short','bottom');

         $state.go('emergency_contact');

       },function(error){
         // login_data.in_prog   =   false;
         console.log(error.message);
         $cordovaToast.show(error.message,'short','center');
       });
     }

     $scope.go_back  =   function(){
       $ionicHistory.goBack();
      //  var user_goal   =   Core.goal().fetch_user_goal();
      //  if(user_goal.indexOf('diabetes_mgmt') >= 0){
      //    $state.go('therephy_type');
      //  } else {
      //     $state.go('user_goal');
      //  }

     }

     $scope.init();
 }])
