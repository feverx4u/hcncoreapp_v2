/*
 |
 |
 |
 |
 |
 */
 angular.module('hcncore.services')
.factory('Media',["$log", "Network", function($log,Network){
   var factory   =   {};

   factory.upload  =   function(file,type,success,failed,abort,progress){
     var fd = new FormData()
     fd.append("file", file)
     fd.append("type", type)

     var xhr = new XMLHttpRequest()
     xhr.upload.addEventListener("progress", progress, false)
     xhr.addEventListener("load", success, false)
     xhr.addEventListener("error", failed, false)
     xhr.addEventListener("abort", abort, false)
     xhr.open("POST", Network.api_url+"/media")
     xhr.send(fd)
   }

   factory.validate_for_upload   =   function(oInput,ext){
       if(ext == undefined){
         ext   =   [
           'JPG','JPEG','PDF','PPT','PPTX','DOC','DOCX','PNG'
         ];
       }
       if (oInput.type == "file") {
         var sFileName = oInput.value;
          if (sFileName.length > 0) {
             var blnValid = false;
             for (var j = 0; j < ext.length; j++) {
                 var sCurExtension = ext[j];
                 if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                     blnValid = true;
                     break;
                 }
             }

             if (!blnValid) {
                 oInput.value = "";
                 return false;
             }
         }
     }
     return true;
   }

   return factory;
}])
