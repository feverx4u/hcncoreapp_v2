/*
 |-------------------------------------------------------------------
 | ScheduleActivityController - Controller for Schedule Activity
 |-------------------------------------------------------------------
 |  @author:  Shekh Rizwan<rizwan@hirarky.com>
 |  @date:  2016-10-07
 */
 angular.module('hcncore.controllers')
.controller('ScheduleActivityController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "ionicTimePicker", "ionicDatePicker", "$cordovaToast", "$ionicLoading", "$ionicPopup", "$cordovaCalendar", "DateTimePickerServices", "$ionicHistory", function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicLoading,$ionicPopup,$cordovaCalendar,DateTimePickerServices,$ionicHistory){
    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();
    }

    $scope.variables   =  function(){

        var v   =   $scope.v;
        v.activity_search_text  =   '';
        v.is_searching  =   false;
        v.search_results    =   [];

        v.is_check_enabled  =   false;
        v.repeat  =   {
          'sun':false,
          'mon':false,
          'tue':false,
          'wed':false,
          'thu':false,
          'fri':false,
          'sat':false,
          'is_forever':'1',
          'popup_date_view':'',
          'until_date':'',
          'num_event':''
        };

        v.activity_sch_input  =  {

            'activity_id':'',
            'activity_name':'',
            'activity_date':'',
            'activity_date_view':'',
            'activity_time':'',
            'activity_time_view':'',
            'activity_time_time':'',
            'notify_before':'10_minutes',
            'years':'',
            'months':'',
            'days':'',
            'hours':'',
            'minutes':'',
            'repeat_on':'',
            'togle_on_off':false,
            'is_forever':'',
            'until':'',
            'duration':'',
            'event_count':'',
            'toggle_disable':false
        };

        v.activity_sch_input.activity_date     =   moment().format('YYYY-MM-DD');

        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');
        $scope.revalidate_time(time);

        $scope.$watchGroup([
          'v.repeat.sun',
          'v.repeat.mon',
          'v.repeat.tue',
          'v.repeat.wed',
          'v.repeat.thu',
          'v.repeat.fri',
          'v.repeat.sat',
        ],function(){
          var v   =   $scope.v;
          var until   = (new Date());
          console.log(v.repeat.sun);
          if(v.repeat.sun == true){
            var temp  = get_next_date(0);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }

          if(v.repeat.mon  == true){
            var temp  = get_next_date(1);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }

          if(v.repeat.tue  == true){
            var temp  = get_next_date(2);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }

          if(v.repeat.wed  == true){
            var temp  = get_next_date(3);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }

          if(v.repeat.thu  == true){
            var temp  = get_next_date(4);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }

          if(v.repeat.fri  == true){
            var temp  = get_next_date(5);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }

          if(v.repeat.sat  == true){
            var temp  = get_next_date(6);
            if(temp.getTime() > until.getTime()){
              until   =   temp;
            }
          }
          v.repeat.until_date   = moment(until).format('YYYY-MM-DD');
          v.repeat.popup_date_view   = moment(until).format('Do MMM,YYYY');
          console.log(v.repeat.until_date);
        });
    }

    function get_next_date(index){
      var today_date  =   new Date();
      var current_week_day  = today_date.getDay();

      var different_days  =   index - current_week_day;
      if(different_days <= 0){
        different_days = 7 + different_days;
      }

      return moment(today_date).add('days',different_days).toDate();
    }

    $scope.open_datepicker  =   function(){

      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.activity_sch_input.activity_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
          // console.log(val)
        },
        templateType: 'popup',
        from:new Date(),
        // to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
        inputDate:new Date(moment(v.activity_sch_input.activity_date))
      });
    }

    $scope.open_untildatepicker  =   function(){

      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.repeat.until_date   =   moment(val).format('YYYY-MM-DD');
          v.repeat.popup_date_view     =   moment(v.repeat.until_date).format('Do MMM,YYYY');
          // console.log(val)
        },
        templateType: 'popup',
        from:new Date(),
        // to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
        inputDate:new Date(moment(v.repeat.until_date,'YYYY-MM-DD').format('YYYY-MM-DD'))
      });
    }

    $scope.open_timepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
          }
        },
        inputTime: v.activity_sch_input.activity_time_time,   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };

      ionicTimePicker.openTimePicker(ipObj1);
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.activity_sch_input.activity_date_view     =   moment(v.activity_sch_input.activity_date).format('Do MMM,YYYY');
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.activity_sch_input.activity_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        v.activity_sch_input.activity_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.activity_sch_input.activity_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
    }

    // display repeat days popup
    $scope.repeat_days_popup  =   null;
    $scope.open_repeat_days_popup  =   function(){
      var v   =   $scope.v;
      v.activity_sch_input.togle_on_off = true;
      v.activity_sch_input.toggle_disable = true;
      $scope.repeat_days_popup = $ionicPopup.show({
        templateUrl: 'repeat_days_popup.html',
        title: 'Repeat',
        scope: $scope,
        'cssClass':'repeat_days_popup'
      });
    }

    $scope.save_repeat_days_popup  =  function(){
      var v   =   $scope.v;
      if((v.repeat.sun === false)&&(v.repeat.mon === false)&&(v.repeat.tue === false)&&(v.repeat.wed === false)
        &&(v.repeat.thu === false)&&(v.repeat.fri === false)&&(v.repeat.sat === false)){
        $cordovaToast.show("Please Select Atleast One day",'short','center');
      }
      else{

      var sun;
      var mon;
      var tue;
      var wed;
      var thu;
      var fri;
      var sat;
      var week_array  =   [];
      if(v.repeat.sun === true){
        sun = "Sun";
        week_array.push(sun);
      }else {
        sun = "";
      }
      if(v.repeat.mon === true){
        mon = "Mon";
        week_array.push(mon);
      }else {
        mon = "";
      }
      if(v.repeat.tue === true){
        tue = "Tue";
        week_array.push(tue);
      }else {
        tue = "";
      }
      if(v.repeat.wed === true){
        wed = "Wed";
        week_array.push(wed);
      }else {
        wed = "";
      }if(v.repeat.thu === true){
        thu = "Thu";
        week_array.push(thu);
      }else {
        thu = "";
      }
      if(v.repeat.fri === true){
        fri = "Fri";
        week_array.push(fri);
      }else {
        fri = "";
      }if(v.repeat.sat === true){
        sat = "Sat";
        week_array.push(sat);
      }else {
        sat = "";
      }

      v.activity_sch_input.is_forever = false;
      v.activity_sch_input.until = null;
      v.activity_sch_input.event_count = null;

      if(v.repeat.is_forever == 0){
        v.activity_sch_input.is_forever = true;
      }

      if(v.repeat.is_forever == 1){
        v.activity_sch_input.is_forever = false;
        v.activity_sch_input.until = v.repeat.until_date;
      }

      if(v.repeat.is_forever == 2){
        v.activity_sch_input.is_forever = false;
        v.activity_sch_input.event_count = v.repeat.num_event;
      }

      $scope.repeat_days_popup.close();
      v.activity_sch_input.toggle_disable = false;
      v.activity_sch_input.repeat_on = week_array.join(', ');

      v.activity_sch_input.repeat_on    =   v.walkjog_sch_input.repeat_on.trim();
      console.log(v.activity_sch_input.repeat_on);

      if(v.activity_sch_input.repeat_on != null && v.activity_sch_input.repeat_on != ""){
          v.activity_sch_input.togle_on_off = true;
      }else{
        v.activity_sch_input.togle_on_off = false;
      }
    }
  }

    $scope.cancel_repeat_days_popup  =  function(){
      var v   =   $scope.v;
      v.activity_sch_input.toggle_disable = false;
      $scope.repeat_days_popup.close();

      if(v.activity_sch_input.repeat_on != null && v.activity_sch_input.repeat_on != ""){
          v.activity_sch_input.togle_on_off = true;
      }else{
        v.activity_sch_input.togle_on_off = false;
      }
    }

    $scope.search_activity  =   function(){
        var v   =   $scope.v;
        v.is_searching  =   true;
      Core.scheduledata().schedule_activity().search(v.activity_search_text).then(function(data){
             v.search_results   =   data.message;
             v.is_searching  =   false;
        },function(data){
            v.is_searching  =   false;
        });
    }

    // Schedule Activity API Calling Method
    $scope.sch_activity   =   function(){
      $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
              });
      var v   =   $scope.v;
        Core.scheduledata().schedule_activity().commit(v.activity_sch_input).then(function(data){
          $ionicLoading.hide();
          Core.calender().commit_event(data.schedule,'ACTIVITY');
          $cordovaToast.show("Activity scheduled",'long','center');
          $timeout(function(){
            $state.go('main_tabs.schedule_main');
            // $scope.createEvent(); // add Clender Events
          },2000);

        },function(error){
          $ionicLoading.hide();
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }

    $scope.choose_activity  =   function(item){
      var v   =   $scope.v;
      v.activity_sch_input.activity_id   = item.id+'';
      v.activity_search_text   = item.activity_name;
      v.activity_sch_input.activity_name   = item.activity_name;
      v.search_results   =   [];

    }

    // add  Schedule Activity event to the Calender
    $scope.createEvent = function() {
      var v   =   $scope.v;
      $scope.formatDate(v.activity_sch_input.activity_date);
      $scope.formatTime(v.activity_sch_input.activity_time);

        $cordovaCalendar.createEvent({
            title: v.activity_search_text,
            location: '',
            notes: '',
            startDate: new Date(v.activity_sch_input.years, v.activity_sch_input.months-1, v.activity_sch_input.days, v.activity_sch_input.hours, v.activity_sch_input.minutes, 0, 0, 0),
            endDate: new Date(moment(new Date(v.activity_sch_input.years, v.activity_sch_input.months-1, v.activity_sch_input.days, v.activity_sch_input.hours, v.activity_sch_input.minutes, 0, 0, 0)).add(60, 'minutes').toDate())
        }).then(function (result) {
            $cordovaToast.show("Schedule Activity Event Created",'long','center');
            console.log("Event created successfully " +result);
        }, function (err) {
            console.error("There was an error: " + err);
        });
    }

    $scope.formatDate = function(date) {
      var v   =   $scope.v;
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

          if (month.length < 2) month = '0' + month;
          if (day.length < 2) day = '0' + day;

          _month = parseInt(month);
          _day = parseInt(day);
          _year = parseInt(year);

          v.activity_sch_input.years = _year;
          v.activity_sch_input.months = _month;
          v.activity_sch_input.days = _day;
      }

        $scope.formatTime = function(time) {
            var v   =   $scope.v;
            var parts = time.split(':');
            v.activity_sch_input.hours = parts[0];
            v.activity_sch_input.minutes = parts[1];
        }
        // Go back confirmation popup
         $scope.showConfirm = function() {
           var v   =   $scope.v;
           if(v.activity_search_text == null || v.activity_search_text.length <= 0){
             $ionicHistory.goBack();
           }else {
             var confirmPopup = $ionicPopup.confirm({
               title: 'Activity',
               template: 'Are you sure you want to go back?'
             });
             confirmPopup.then(function(res) {
               if(res) {
                 $ionicHistory.goBack();
               } else {
                 console.log('You are not sure');
               }
             });
           }
         };

    $scope.init();
}])
