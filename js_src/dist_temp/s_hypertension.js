angular.module('hcncore.services')
.factory('Hypertenstion',["Log", "Network", "$q", "$resource", "locker", "User", "UserProfile", function(Log,Network,$q,$resource,locker,User,UserProfile){

    var factory = {};
    function resource(){
      var url   =   Network.api_url+'/profile/:profile_id/goal/hypertension';

      return  $resource(url, {},{
          'update': { method:'put',params:{}},
      });
    }

    factory.update_hypertension = function(data){
      var deferred  =   $q.defer();
      var user  =   User.logged_in_user();
      if(user == null){
        deferred.reject({
          'staut':400,
          'message':'User not logged in'
        });
        return deferred.promise;
      }

      if(data.systolic.length <= 0){
        deferred.reject({
          'staut':400,
          'message':'Please type systolic'
        });
        return deferred.promise;
      }

      if(data.diastolic.length <= 0){
        deferred.reject({
          'staut':400,
          'message':'Please type diastolic'
        });
        return deferred.promise;
      }

      // if(data.pulse.length <= 0){
      //   deferred.reject({
      //     'staut':400,
      //     'message':'Please type pulse'
      //   });
      //   return deferred.promise;
      // }

      data.weight   =   user.weight;
      data.weight_unit   =   user.weight_unit;

      var profile   =   UserProfile.getActiveProfile();
      if(profile == null){
        return null;
      }

      resource().update({
        'profile_id':profile.id
      },data,function(data){
        deferred.resolve({
          'status':200,
          'data':'Updated successfully'
        });
      },function(error){
        deferred.reject({
          'staut':400,
          'message':'Failed to update'
        });
      });
      return deferred.promise;
    }

    return factory;
}])
