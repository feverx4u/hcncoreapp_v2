angular.module('hcncore.filters')
.filter('php_datetime_format',function(){
  return function(date,format){
    return moment(date, "YYYY-MM-DD HH:mm:ss").format(format);
  }
})
