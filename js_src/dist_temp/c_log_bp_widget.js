/*
 |-------------------------------------------------------------------
 | LogBPWidgetController - Controller for log of BP Unit
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
angular.module('hcncore.controllers')
.controller('LogBPWidgetController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "ionicTimePicker", "ionicDatePicker", "$ionicLoading", "$cordovaToast", "$ionicPopup", function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,ionicTimePicker,ionicDatePicker,$ionicLoading,$cordovaToast,$ionicPopup){

  $scope.$state    =  $state;
  $scope.v  =   {};

  $scope.init   =   function(){
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
    }, 10);
    $scope.variables();


  }


  $scope.variables   =  function(){
      var v   =   $scope.v;
      v.is_check_enabled  =   false;
      v.bp_log_input  =  {
          'systolic_unit':'MMHG',
          'systolic':'',
          'diastolic_unit':'MMHG',
          'diastolic':'',
          'log_date':'',
          'log_date_view':'',
          'log_time':'',
          'log_time_view':'',
          'weight_unit':'KG',
          'weight':'',
          'pulse':'',
          'pulse_unit':'BPM',
          'mood':'',
          'notes':''
      };

      v.bp_log_input.log_date     =   moment().format('YYYY-MM-DD');

      $scope.revalidate_date();

      var time      =   moment().format('YYYY-MM-DD hh:mm A');
      $scope.revalidate_time(time);

      var existing_weight   = Core.user_profile().get_user_weight();
      // cons
      // v.a1c_log_input.existing_weight   =   existing_weight.weight;
      if(existing_weight == null){
        v.bp_log_input.weight_unit   =   'KG';
      } else {
        v.bp_log_input.weight_unit   =   existing_weight.weight_unit;
      }
  }

  $scope.validate_for_check   =   function(){
    var v   =   $scope.v;
    // console.log(111);
    if(v.bp_log_input.systolic == null || v.bp_log_input.systolic.length == 0){
      v.is_check_enabled  =   false;
      return;
    }

    if(v.bp_log_input.diastolic == null || v.bp_log_input.diastolic.length == 0){
      v.is_check_enabled  =   false;
      return;
    }

    v.is_check_enabled  =   true;
  }

  $scope.open_datepicker  =   function(){
    // alert(111);
    var v   =   $scope.v;
    ionicDatePicker.openDatePicker({
      callback:function(val){

        v.bp_log_input.log_date   =   moment(val).format('YYYY-MM-DD');
        $scope.revalidate_date();
        // console.log(val)
      },
      templateType: 'popup',
      to:new Date( moment().format('YYYY-MM-DD')),
      inputDate:new Date(moment())
    });
  }

  $scope.open_timepicker  =   function(){
    var v   =   $scope.v;
    var ipObj1 = {
      callback: function (val) {      //Mandatory
        if (typeof (val) === 'undefined') {
          console.log('Time not selected');
        } else {
          var selectedTime = new Date(val * 1000);
          $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
        }
      },
      inputTime: v.bp_log_input.log_time_time,   //Optional
      format: 12,         //Optional
      step: 1,           //Optional
      setLabel: 'Set'    //Optional
    };

    ionicTimePicker.openTimePicker(ipObj1);
  }

  $scope.revalidate_date =  function(){
      var v   =   $scope.v;
      v.bp_log_input.log_date_view     =   moment(v.bp_log_input.log_date).format('Do MMM,YYYY');
  }

  $scope.revalidate_time =  function(time){
      var v   =   $scope.v;
      v.bp_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
      console.log(v.bp_log_input.log_time);
      v.bp_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
      v.bp_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
  }

  $scope.log_bp   =   function(){

    $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
            });
    var v   =   $scope.v;
      Core.logdata().bp_log().commit(v.bp_log_input).then(function(data){
          $ionicLoading.hide();
        $scope.log_success_popup('Blood Pressure Logged');
        $scope.variables();
        // $cordovaToast.show("Blood Pressure Logged",'long','center');
        // $timeout(function(){
        //   $state.go('main_tabs.log_widget');
        // },2000);
      },function(error){
        $ionicLoading.hide();
        console.log(error.message);
        $cordovaToast.show(error.message,'short','center');
      });
  }

  $scope.notes_popup  =   null;

  $scope.notes  =   '';
  $scope.open_note_popup  =   function(){
    $scope.notes_popup = $ionicPopup.show({
      templateUrl: 'note_popup.html',
      title: 'Notes',
      scope: $scope,
      'cssClass':'note_popup'
    });
  }

  $scope.close_notes_popup  =  function(notes){
    $scope.v.bp_log_input.notes =  notes;
    console.log(notes);
    $scope.notes_popup.close();
  }


  $scope.log_pop_message  =   null;
  $scope.log_popup  =   null;
  $scope.log_success_popup  =   function(message){
    $scope.log_pop_message  =   message;
    $scope.log_popup = $ionicPopup.show({
      templateUrl: 'log_sucess_popup.html',
      title: 'Successfully Logged',
      scope: $scope,
      'cssClass':'log_success_popup'
    });
  }

  $scope.close_log_success_popup  =  function(notes){
    $scope.log_popup.close();
    $timeout(function(){
      $state.go('main_tabs.log_widget');
      $rootScope.$broadcast('data:loggged');
    },100);
  }

  $scope.cancel_notes_popup  =  function(){
      $scope.notes_popup.close();
    }

    // Go back confirmation popup
     $scope.showConfirm = function() {
       var v   =   $scope.v;
       if(v.bp_log_input.systolic == null || v.bp_log_input.systolic.length <= 0){
         $state.go("main_tabs.log_widget");
       }else {
         var confirmPopup = $ionicPopup.confirm({
           title: 'Log BP',
           template: 'Are you sure you want to go back?'
         });
         confirmPopup.then(function(res) {
           if(res) {
             $state.go("main_tabs.log_widget");
           } else {
             console.log('You are not sure');
           }
         });
       }
     };
  $scope.init();
}])
