angular.module('hcncore.services')
.factory('PhoneContacts',["$log", "$q", "$ionicPlatform", "$cordovaContacts", "$cordovaSQLite", function($log,$q,$ionicPlatform,$cordovaContacts,$cordovaSQLite){
  var factory   =   {};



  factory.invalidatePhoneContact  =   function(){
    //Funcation is used to fetch the contact number from device and strore it to sqllite
    var defer     =   $q.defer();
    $ionicPlatform.ready(function(){
        var db = $cordovaSQLite.openDB({ name: "hcncare_manual.db", location: 'default' });
        var query   =   'CREATE TABLE IF NOT EXISTS hcncontact_table (contact_num, name,image,is_medsocial)';
        $cordovaSQLite.execute(db, query, []).then(function(res) {
        }, function (err) {
        });

        $cordovaContacts.find({}).then(function(allContacts) { //omitting parameter to .find() causes all contacts to be returned
          var contact_num_array   =   [];
          angular.forEach(allContacts,function(contact){
            if(contact.phoneNumbers != null && contact.phoneNumbers.length > 0){
                var displayName    = contact.displayName;
                var photo   =   null;
                if(contact.photos != null){
                  photo   =   '';
                }
                // console.log(photo);
                // console.log(contact);
                angular.forEach(contact.phoneNumbers,function(phoneNumber){
                  var phone_number  =   phoneNumber.value;
                  phone_number = phone_number.replace(/\s/g, '');

                  contact_num_array.push(phone_number);
                  var query   =   "Select * from hcncontact_table where contact_num=?";
                  $cordovaSQLite.execute(db, query, [phone_number]).then(function(res) {
                    if(res.rows.length > 0){
                      // var up_query  =   "Update hcncontact_table set name=?,image=? where contact_num=?";
                      //
                      // $cordovaSQLite.execute(db, up_query, [displayName,photo,phone_number]).then(function(res) {
                      //   // console.log(res);
                      // },function(err){
                      //   // console.log(error);
                      // });
                    } else {
                      var query   = 'INSERT INTO hcncontact_table VALUES (?,?,?,?)';
                      $cordovaSQLite.execute(db, query, [phone_number,displayName,photo,'NO']).then(function(res) {
                      }, function (err) {
                      });
                    }
                  }, function (err) {
                    console.log(err);
                  });
                });
                factory.invalidateMedsocial(contact_num_array);
            }
          });
        });
    });

    return defer.promise
  }

  factory.invalidateMedsocial   =   function(phoneNumbers){
    //Funcation is used to check whether phone numbers are medsocial avaible or not based on that it will set the flog
  }

  factory.getAllContacts   =   function(){
    var defer     =   $q.defer();
    console.log(11111);
    $ionicPlatform.ready(function(){
      $cordovaContacts.find({
        hasPhoneNumber:true,
      }).then(function(allContacts) {
        var result  =   [];
        angular.forEach(allContacts,function(item){
          if(item.phoneNumbers != null && item.phoneNumbers.length > 0){
            var name  =   '';
            if(item.displayName != null && item.displayName != undefined && item.displayName.length > 0){
                name  =   item.displayName;
            }

            var photo   =   '';
            if(item.photos != null && item.photos != undefined && item.photos.length > 0){
              photo   =   item.photos[0].value;

              // var final_path    =   '';
              // window.resolveLocalFileSystemURL(photo, function(ee){
              //   console.log(ee);
              // },function(dd){
              //   console.log(dd);
              // });
              // console.log(final_path);
            }

            angular.forEach(item.phoneNumbers,function(number){
              var contact   =   {
                'name':name,
                'phoneNumbers':number.value,
                'image111':photo,
                'image':''
              };
              result.push(contact);
            });
          }
        });

        var cSort = function(a, b) {
            aName = a.name;
            bName = b.name;
            return aName < bName ? -1 : (aName == bName ? 0 : 1);
        }
        result  = result.sort(cSort);
        // console.log(allContacts);
        defer.resolve(result);
      },function(error){
          defer.reject(error);
        console.log(error);
      });
    });
    return defer.promise;
  }

  factory.searchContact   = function(searc_query){
    //Will search the data from sqlite and return the contact

    factory.invalidatePhoneContact();
    var defer     =   $q.defer();
    var db = $cordovaSQLite.openDB({ name: "hcncare_manual.db", location: 'default' });
    var query   =   '';
    if(searc_query.trim().length > 0){
        query   =   "Select * from hcncontact_table where name like '%"+searc_query.trim()+"%' order by name asc";
    } else{
      query   =   "Select * from hcncontact_table order by name asc";
    }

    $cordovaSQLite.execute(db, query, []).then(function(res) {
      var details   =   [];
      for(var i=0;i<res.rows.length;i++){
        details.push(res.rows.item(i));
      }
      defer.resolve(details);
    },function(des){
      defer.reject({});
    });
    return defer.promise
  }

  return factory;
}])
