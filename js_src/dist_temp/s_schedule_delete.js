/*
|------------------------------------------------------------------------------
|  Schedule Walking/Jogging - Service to Schedule the Info from Walking/Jogging
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-10-17
*/
angular.module('hcncore.services')
.factory('ScheduleDelete',["Log", "Network", "$q", "$resource", "locker", "User", "UserProfile", function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};
  function resource(){


    var url   =   Network.api_url+'/schedule/activity/:_id';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  factory.delete_activity  =   function(_id){
    var deferred  =   $q.defer();
    resource().delete({
      'id':_id
    },function(data){
        Log.info('ScheduleActivity:deleted',data);
        deferred.resolve(data);
    },function(data){
        Log.error('ScheduleActivity:deleted',data);
        deferred.reject(error);
    });
    return deferred.promise;
  }
  return factory;
}])
