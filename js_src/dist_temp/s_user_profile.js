angular.module('hcncore.services')
.factory('UserProfile',["Log", "Network", "$q", "$resource", "locker", "User", "$state", "$rootScope", function(Log,Network,$q,$resource,locker,User,$state,$rootScope){

  var factory  =   {};

  factory.user_id  =   null;

  function resource(){
    getLoggedinUser();

    var url   =   Network.api_url+'/user/'+factory.user_id+'/profile/:profile_id';

    return  $resource(url, {},{
        'get': { method:'GET',params:{'profile_id':''}},
        'add': { method:'POST',params:{'profile_id':''}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }


  function getLoggedinUser(){
    var user   =   User.logged_in_user();
    if(user == null){
      $state.go('credential');
      return;
    }
    console.log(user);
    factory.user_id   = user.id;
  }



  factory.get_profiles   =   function(){

  }

   factory.resync_active_profile  =   function(){
     var user   =   User.logged_in_user();
     if(user == null){
       return;
     }

     var profile = factory.getActiveProfile();
     if(profile == null){
       return;
     }

     var profile_id  =   profile.id;
     angular.forEach(user.profiles,function(p){
         if(p.id == profile_id){
           factory.setActiveProfile(p);
         }
     });
   }
  factory.setActiveProfile   =   function(profile){
    getLoggedinUser();
     var login_user = User.logged_in_user();
    // console.log(login_user);
    if(profile == null){
      // console.log(login_user.id+'dskjkasjhdkhkdakjhds');
      locker.namespace('hcncore_'+login_user.id).pull('active_profile',null);
      locker.namespace('hcncore_'+login_user.id).pull('active_profile_type',null);
      $rootScope.active_profile_flag  =   null;
    } else {

      //Check against logged in user profile
      var is_found  =   false;
      angular.forEach(login_user.profiles,function(prof){
          if(is_found){
            return;
          }

          if(prof.id == profile.id){
            is_found  = true;
          }
      });

      locker.namespace('hcncore_'+login_user.id).put('active_profile',profile);
      console.log(login_user.id+'askgdsahgdsajhgasdjhgsa');
      if(is_found){
        //your own
        locker.namespace('hcncore_'+login_user.id).put('active_profile_type','OWN_USER');

      } else {
        //other
        locker.namespace('hcncore_'+login_user.id).put('active_profile_type','OTHER_USER');

      }
    }
  }

  factory.getActiveProfile   =   function(){
    return locker.namespace('hcncore_'+factory.user_id).get('active_profile',null);
  }

  factory.getActiveProfileType   =   function(){
    console.log(factory.user_id);
    return locker.namespace('hcncore_'+factory.user_id).get('active_profile_type',null);
  }

  factory.add_profile  =   function(data){
    var deferred  =   $q.defer();
    if(data.name == undefined || data.name == null || data.name.length == 0){
      deferred.reject({
        'status':500,
        'message':'Something went wrong try again latter.'
      });
    }

    resource().add(data,function(data){
        Log.info('UserProfile:add_profile',data);
        User.resync_logged_in_user().then(function(){
          deferred.resolve({
            'status':200,
            'message':'Successfully.'
          });
        },function(){
          deferred.resolve({
            'status':200,
            'message':'Successfully.'
          });
        });


    },function(error){
      Log.error('UserProfile:add_profile',error);
      deferred.reject({
        'status':500,
        'message':'Something went wrong.'
      });
    });

    return deferred.promise;
  }

  factory.update_profile   =   function(profile_id,data){
    var deferred  =   $q.defer();
    if(data.name == undefined || data.name == null){
      deferred.reject({
        'status':500,
        'message':'Something went wrong try again latter.'
      });
    }


    if(user_data.height !=null && user_data.height.length >= 0 ){
      if(isNaN(user_data.height)){
        deferred.reject({
          'status':400,
          'message':'Height should be numeric.'
        });
        return deferred.promise;
      }  else if((user_data.height < 1 || user_data.height > 10) && user_data.height_unit == 'FEET'){
        deferred.reject({
          'status':400,
          'message':'Please enter a valid height.'
        });
        return deferred.promise;
      } else if((user_data.height < 0.30 || user_data.height > 3.0) && user_data.height_unit == 'METER'){
        deferred.reject({
          'status':400,
          'message':'Please enter a valid height.'
        });
        return deferred.promise;
      }
    }

    if(user_data.weight !=null || user_data.weight.length >= 0){
      if((user_data.weight < 20 || user_data.weight > 200) && user_data.weight_unit == 'KG'){
        deferred.reject({
          'status':400,
          'message':'Please enter a valid weight.'
        });
        return deferred.promise;
      } else if((user_data.weight < 44 || user_data.weight > 445) && user_data.weight_unit == 'LBS'){
        deferred.reject({
          'status':400,
          'message':'Please enter a valid weight.'
        });
        return deferred.promise;
      }
    } 

    resource().update({
      'profile_id':profile_id
    },data,function(data){
        Log.info('UserProfile:update_profile',data);
        User.resync_logged_in_user().then(function(){
          deferred.resolve({
            'status':200,
            'message':'Successfully.'
          });
        },function(){
          deferred.resolve({
            'status':200,
            'message':'Successfully.'
          });
        });
    },function(error){
      Log.error('UserProfile:update_profile',error);
      deferred.reject({
        'status':500,
        'message':'Something went wrong.'
      });
    });

    return deferred.promise;
  }

  factory.delete_profile   =   function(){

  }

  factory.set_user_weight   =  function(weight,unit){
    var profile   =   factory.getActiveProfile();
    if(profile == null){
      return;
    }
    locker.namespace('hcncore_'+profile.id).put('last_user_weight',{
      'weight':weight,
      'weight_unit':unit
    });
  }

  factory.get_user_weight   =  function(weight,unit){
    var profile   =   factory.getActiveProfile();
    if(profile == null){
      return;
    }
    return locker.namespace('hcncore_'+profile.id).get('last_user_weight',null);
  }


  //Method is used to update the information of currently logged in user
  factory.update_user_info   =   function(user_data){
    var deferred  =   $q.defer();
    var user    =  factory.getActiveProfile();
    console.log(['Active Profile',user]);
    if(user == null){
      Log.error(['UserProfile:update_user_info','No current active profile.']);
      deferred.reject({
        'status':500,
        'message':'User not logged in'
      });
      return deferred.promise;
    }

    if(user_data.name ==null || user_data.name.length <= 0){
      deferred.reject({
        'status':400,
        'message':'Name is empty.'
      });
      return deferred.promise;
    }

    if(user_data.dob ==null || user_data.dob.length <= 0){
      deferred.reject({
        'status':400,
        'message':'Dob is empty.'
      });
      return deferred.promise;
    }

    if(user_data.gender ==null || user_data.gender.length <= 0){
      deferred.reject({
        'status':400,
        'message':'gender is empty.'
      });
      return deferred.promise;
    }

    if(user_data.height ==null || user_data.height.length <= 0 ){
      deferred.reject({
        'status':400,
        'message':'height is empty.'
      });
      return deferred.promise;
    } else if(isNaN(user_data.height)){
      deferred.reject({
        'status':400,
        'message':'Height should be numeric.'
      });
      return deferred.promise;
    }  else if((user_data.height < 1 || user_data.height > 10) && user_data.height_unit == 'FEET'){
      deferred.reject({
        'status':400,
        'message':'Please enter a valid height.'
      });
      return deferred.promise;
    } else if((user_data.height < 0.30 || user_data.height > 3.0) && user_data.height_unit == 'METER'){
      deferred.reject({
        'status':400,
        'message':'Please enter a valid height.'
      });
      return deferred.promise;
    }

    if(user_data.weight ==null || user_data.weight.length <= 0){
      deferred.reject({
        'status':400,
        'message':'weight is empty.'
      });
      return deferred.promise;
    } else if((user_data.weight < 20 || user_data.weight > 200) && user_data.weight_unit == 'KG'){
      deferred.reject({
        'status':400,
        'message':'Please enter a valid weight.'
      });
      return deferred.promise;
    } else if((user_data.weight < 44 || user_data.weight > 445) && user_data.weight_unit == 'LBS'){
      deferred.reject({
        'status':400,
        'message':'Please enter a valid weight.'
      });
      return deferred.promise;
    }
    if(user_data.email != undefined && user_data.email != null && user_data.email.length >0){
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(!re.test(user_data.email)){
        deferred.reject({
          'status':400,
          'message':'Invalid Email ID.'
        });
        return deferred.promise;
      }
    }

    user_data.dob   =   moment(user_data.dob,'Do MMM,YYYY').format('YYYY-MM-DD');
   //  dob    =   data.dob;
    resource().update({
      'profile_id':user.id
    },user_data,function(data){
      factory.setActiveProfile(data.profile);
     //  locker.namespace('hcncore_'+data.user.id).put('user',data.user);
      factory.set_user_weight(user_data.weight,user_data.weight_unit);
      Log.info('UserProfile:update_user_info',data);
      user_data.dob   =   moment(user_data.dob,'YYYY-MM-DD').format('Do MMM,YYYY');
      User.resync_logged_in_user();
      deferred.resolve({
        'status':200,
        'message':'User updated successfully'
      });
    },function(error){
      user_data.dob   =   moment(user_data.dob,'YYYY-MM-DD').format('Do MMM,YYYY');
      Log.info('UserProfile:update_user_info',user_data.dob);
      Log.error('UserProfile:update_user_info',error);
      deferred.reject({
        'status':500,
        'message':'Something went wrong'
      });
    });

    return deferred.promise;
  }

  return factory;
}])
