/*
 |-------------------------------------------------------------------
 | ScheduleDoctorAppoController - Controller for Schedule Doctor Appointment
 |-------------------------------------------------------------------
 |  @author:  Shekh Rizwan<rizwan@hirarky.com>
 |  @date:  2016-10-07
 */
 angular.module('hcncore.controllers')
.controller('ScheduleDoctorAppoController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "ionicTimePicker", "ionicDatePicker", "$cordovaToast", "$ionicPopup", "$ionicLoading", "$cordovaCalendar", "DateTimePickerServices", "$ionicHistory", function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$ionicLoading,$cordovaCalendar,DateTimePickerServices,$ionicHistory){
    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();
    }

    $scope.variables   =  function(){
        var v   =   $scope.v;
        v.is_check_enabled  =   false;
        v.doc_sch_input  =  {
            'hospital_name':'',
            'doctor_name':'',
            'doc_date':'',
            'doc_date_view':'',
            'doc_time':'',
            'notify_me':'Hour_1',
            'doc_time_view':'',
            'doc_time_time':'',
            'notes':'',
            'years':'',
            'months':'',
            'days':'',
            'hours':'',
            'minutes':'',
            'follow_ups':[],
            'schedule_date':'',
            'schedule_time':'',
            'notify_before':'Hour_1',
            'follow_flag':false
        };

        v.doc_sch_input.doc_date     =   moment().format('YYYY-MM-DD');

        $scope.revalidate_date();

        var time      =   moment().format('YYYY-MM-DD hh:mm A');
        $scope.revalidate_time(time);
    }

    $scope.open_datepicker  =   function(){
      // alert(111);
      var v   =   $scope.v;
      ionicDatePicker.openDatePicker({
        callback:function(val){

          v.doc_sch_input.doc_date   =   moment(val).format('YYYY-MM-DD');
          $scope.revalidate_date();
          // console.log(val)
        },
        templateType: 'popup',
        from:new Date(),
        to:new Date( moment().add(3,'years').format('YYYY-MM-DD')),
        inputDate:new Date(moment())
      });
    }

    $scope.open_timepicker  =   function(){
      var v   =   $scope.v;
      var ipObj1 = {
        callback: function (val) {      //Mandatory
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            var selectedTime = new Date(val * 1000);
            $scope.revalidate_time(moment(moment().format('YYYY-MM-DD')+' '+selectedTime.getUTCHours()+":"+sprintf('%02d',selectedTime.getUTCMinutes()),'YYYY-MM-DD HH:mm').format('YYYY-MM-DD hh:mm a'));
          }
        },
        inputTime: v.doc_sch_input.doc_time_time,   //Optional
        format: 12,         //Optional
        step: 1,           //Optional
        setLabel: 'Set'    //Optional
      };

      ionicTimePicker.openTimePicker(ipObj1);
    }

    $scope.revalidate_date =  function(){
        var v   =   $scope.v;
        v.doc_sch_input.doc_date_view     =   moment(v.doc_sch_input.doc_date).format('Do MMM,YYYY');
    }

    $scope.revalidate_time =  function(time){
        var v   =   $scope.v;
        v.doc_sch_input.doc_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
        // console.log(v.a1c_log_input.log_time);
        v.doc_sch_input.doc_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
        v.doc_sch_input.doc_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
    }
    $scope.notes_popup  =   null;
    $scope.notes  =   '';
    $scope.open_note_popup  =   function(){
      $scope.notes_popup = $ionicPopup.show({
        templateUrl: 'note_popup.html',
        title: 'Notes',
        scope: $scope,
        'cssClass':'note_popup'
      });
    }

    $scope.close_notes_popup  =  function(notes){
      $scope.v.doc_sch_input.notes =  notes;
      $scope.notes_popup.close();
    }
    $scope.cancel_notes_popup  =  function(){
      $scope.notes_popup.close();
    }

    // display follow up popup
    $scope.follow_up_popup  =   null;
    $scope.open_follow_up_popup  =   function(){
      var v   =   $scope.v;
      if(v.doc_sch_input.doctor_name == null || v.doc_sch_input.doctor_name.length <= 0 ){
          $cordovaToast.show("Please Enter Doctor Name",'long','center');
          return;
      }
      $scope.follow_up_popup = $ionicPopup.show({
        templateUrl: 'follow_up_popup.html',
        title: 'First Follow Up',
        scope: $scope,
        'cssClass':'follow_ups_popup'
      });
    }

    $scope.save_follow_up_popup  =  function(){
      var v   =   $scope.v;
      v.doc_sch_input.schedule_date = DateTimePickerServices.getDate();
      v.doc_sch_input.schedule_time = DateTimePickerServices.getTime();
      v.doc_sch_input.notify_before = v.doc_sch_input.notify_before;
      console.log(v.doc_sch_input.schedule_date);
      console.log(v.doc_sch_input.schedule_time);
      console.log(v.doc_sch_input.notify_before);
      $scope.v.doc_sch_input.follow_flag = true;

      $scope.follow_up_popup.close();
    }
    $scope.cancel_follow_up_popup  =  function(){
      $scope.follow_up_popup.close();
    }

      // API Calling Method for Schedule Doctor Appointments
    $scope.sch_doctor_appointment  =   function(){
      $ionicLoading.show({
                template: '<ion-spinner icon="android"></ion-spinner>'
              });
      var v   =   $scope.v;
      v.doc_sch_input.follow_ups = [];
      v.doc_sch_input.follow_ups.push({
        'schedule_date':v.doc_sch_input.schedule_date,
        'schedule_time': v.doc_sch_input.schedule_time,
        'notify_before': v.doc_sch_input.notify_before,
      })
      console.log(v.doc_sch_input.follow_ups);
        Core.scheduledata().schedule_doc_appo().commit(v.doc_sch_input).then(function(data){
          $ionicLoading.hide();
          Core.calender().commit_event(data.schedule,'DOCTOR_APPOINTMENT');
          $cordovaToast.show("Doctor Appointment Details scheduled",'long','center');
          $timeout(function(){
            $state.go('main_tabs.schedule_main');
            // $scope.createEvent(); // add Clender Events
          },2000);
        },function(error){
          $ionicLoading.hide();
          console.log(error.message);
          $cordovaToast.show(error.message,'short','center');
        });
    }

    // add  Schedule Doctor Appointments event to the Calender
    $scope.createEvent = function() {
      var v   =   $scope.v;
      $scope.formatDate(v.doc_sch_input.doc_date);
      $scope.formatTime(v.doc_sch_input.doc_time);
        $cordovaCalendar.createEvent({
            title: 'Doctor('+v.doc_sch_input.doctor_name+') Appointments',
            location: v.doc_sch_input.hospital_name,
            notes: v.doc_sch_input.notes,
            startDate: new Date(v.doc_sch_input.years, v.doc_sch_input.months-1, v.doc_sch_input.days, v.doc_sch_input.hours, v.doc_sch_input.minutes, 0, 0, 0),
            endDate: new Date(moment(new Date(v.doc_sch_input.years, v.doc_sch_input.months-1, v.doc_sch_input.days, v.doc_sch_input.hours, v.doc_sch_input.minutes, 0, 0, 0)).add(60, 'minutes').toDate())
        }).then(function (result) {
            $cordovaToast.show("Schedule Doctor Appointments Event Created",'long','center');
            console.log("Event created successfully " +result);
        }, function (err) {
            console.error("There was an error: " + err);
        });
    }
    $scope.formatDate = function(date) {
      var v   =   $scope.v;
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

          if (month.length < 2) month = '0' + month;
          if (day.length < 2) day = '0' + day;

          _month = parseInt(month);
          _day = parseInt(day);
          _year = parseInt(year);

          v.doc_sch_input.years = _year;
          v.doc_sch_input.months = _month;
          v.doc_sch_input.days = _day;
      }

        $scope.formatTime = function(time) {
            var v   =   $scope.v;
            var parts = time.split(':');
            v.doc_sch_input.hours = parts[0];
            v.doc_sch_input.minutes = parts[1];
        }

        // Go back confirmation popup
         $scope.showConfirm = function() {
           var v   =   $scope.v;
           if(v.doc_sch_input.hospital_name == null || v.doc_sch_input.hospital_name.length <= 0){
             $ionicHistory.goBack();
           }else {
             var confirmPopup = $ionicPopup.confirm({
               title: 'Doctors Appointment',
               template: 'Are you sure you want to go back?'
             });
             confirmPopup.then(function(res) {
               if(res) {
                 $ionicHistory.goBack();
               } else {
                 console.log('You are not sure');
               }
             });
           }
         };

    $scope.init();
}])
