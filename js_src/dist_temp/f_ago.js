angular.module('hcncore.filters')
.filter('ago',function(){
    return function(date){
      var now = moment();
      var then = moment(date,'YYYY-MM-DD HH:mm:ss');
      return moment.duration(now.diff(then))._data.days;
    }
})
