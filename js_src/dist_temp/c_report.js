/*
 |-------------------------------------------------------------------
 | ReportController - Controller for log of symptoms
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
angular.module('hcncore.controllers')
.controller('ReportController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "ionicTimePicker", "ionicDatePicker", "$cordovaToast", "$ionicPopup", "$ionicLoading", "$cordovaFileOpener2", function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$ionicLoading,$cordovaFileOpener2){

    $scope.init   =   function(){
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      $scope.variables();
      $scope.v.archive_reports  =   [];
      var data = Core.user_report().get_archive_reports();
      for(var i=0;i<data.length;i++){
        $scope.v.archive_reports.push(data[data.length-i-1]);
      }
    }
    $scope.v  =   {};
    $scope.variables   =   function(){
        var v   = $scope.v;
        v.report_expand   =   false;
        v.archive_reports = [];
        v.log_expand   =   false;
        v.schedule_expand   =   false;

        v.num_days  =   '7';

        v.reports  =   {
          'summary':true,
          'graph':true,
          'detail':true
        };

        v.log  =   {
          'bgl':true,
          'a1c':false,
          'bp':false,
          'activity':false,
          'symptoms':true,
          'weight':false,
          'food':false,
          'medication':false,
        };

        v.schedule  =   {
          'doctor':false,
          'lab':false,
          'activity':false,
          'walk_jog':false,
          'medication':true,
        };
    }

    $scope.toggle_report   =   function(){
      var v   = $scope.v;
      v.report_expand   =  !v.report_expand;
    }

    $scope.toggle_log   =   function(){
      var v   = $scope.v;
      v.log_expand   =  !v.log_expand;
    }

    $scope.toggle_schedule   =   function(){
      var v   = $scope.v;
      v.schedule_expand   =  !v.schedule_expand;
    }


    $scope.file_generated   =   null;
    $scope.generate_report  =   function(){
        var v   = $scope.v;
        var data  =   {
          duration:parseInt(v.num_days),
          'report_type':v.reports,
          'logs':v.log,
          'schedule':v.schedule
        };


        $ionicLoading.show({
            template: '<ion-spinner icon="android"></ion-spinner>'
        });

        Core.user_report().generate_report(data).then(function(data){
          $ionicLoading.hide();
          console.log(data);
          $scope.file_generated   =   data;
          $scope.report_popup = $ionicPopup.show({
            templateUrl: 'popup_report.html',
            title: 'Successfully Generated',
            scope: $scope,
            'cssClass':'report_popup'
          });

        },function(data){
          $ionicLoading.hide();
          $ionicLoading.show({
              template: '<span>Failed to Generate Report</span>'
          });
          $timeout(function(){
            $ionicLoading.hide();
          },2000);
        });
    }

    $scope.view_file    =   function(file){
      window.open(file,'_system');
    }

    $scope.share_file   =   function(file){
      window.plugins.socialsharing.share(null, null, file);
    }
    $scope.close_popup  =   function(){
      // $scope.report_popup.hide();
      $scope.report_popup.close();
    }
    $scope.goBack = function(){
      $state.go("med_dashboard");
    }

    $scope.goto_sos_feature = function(){
      $state.go("sos_feature_tab.sos");
    }

    $scope.init();
}])
