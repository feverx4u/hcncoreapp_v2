angular.module('hcncore.filters')
.filter('php_date_format',function(){
  return function(date,format){
    return moment(date, "YYYY-MM-DD").format(format);
  }
})
