/*
|------------------------------------------------------------------------------
|  WeightLog - Service to Log the Info from weight
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('WeightLog',["Log", "Network", "$q", "$resource", "locker", "User", "UserProfile", function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};

  function resource(){


    var url   =   Network.api_url+'/log/weight';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  factory.commit  =   function(data){

    var deferred  =   $q.defer();

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }

    var profile  =   UserProfile.getActiveProfile();
    if(profile ==  null){
      return null;
    }

    data.profile_id   =  profile.id;


    if(data.weight_unit == null || data.weight_unit.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please pass weight unit'
      });
      return deferred.promise;
    }

    if(data.weight == null || data.weight.length <= 0 || isNaN(data.weight)){
      deferred.reject({
        'staut':400,
        'message':'Please enter weight'
      });
      return deferred.promise;
    }

    if(data.log_date == null || data.log_date.length <= 0){
      deferred.reject({
        'staut':400,
        'message':'Please enter date'
      });
      return deferred.promise;
    }

    if(data.log_time == null || data.log_time.length <= 0){
      deferred.reject({
        'staut':400,
        'message':'Please enter time'
      });
      return deferred.promise;
    }

    // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
    var user_data   =   data;
    resource().add(data,function(data){
      Log.info('WeightLog:commit',data);
      UserProfile.set_user_weight(user_data.weight,user_data.weight_unit);
      deferred.resolve(data);
    },function(data){
        Log.error('WeightLog:commit',data);
        deferred.reject(error);
    });
    return deferred.promise;
  }
  return factory;
}])
