/*
 |-------------------------------------------------------------------
 | DiabetesTherephyController - Controller for user goal
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
 angular.module('hcncore.controllers')
 .controller('DiabetesTherephyController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "$ionicLoading", "$cordovaToast", "ionicDatePicker", "$ionicHistory", function($rootScope,$scope,$state,$ionicPlatform,Core,
   $timeout,$ionicLoading,$cordovaToast,ionicDatePicker,$ionicHistory){

     $scope.v   =   {};
      //Method to initialize the controller
      $scope.init   =   function(){
        $scope.variable();
        $scope.fetch_therephy();
      }

      //variable
      $scope.variable   = function(){
        var v   =   $scope.v;
        $scope.v.therephy_type    =   [];
      }

      //fetching the goal
      $scope.fetch_therephy  =   function(){
        // alert(111);
        Core.diabetes().fetch_therephy_type().then(function(data){
          $scope.v.therephy_type  =   data.message;
          // console.log(data);
        });
      }

      $scope.go_back  =   function(){
        $ionicHistory.goBack();
        // $state.go('user_goal');
      }


      //Method is used to select the goal
      $scope.select_type  =   function(){
        var therephy_type   =   $scope.v.therephy_type;
        var choosen_types  = [];
        angular.forEach(therephy_type,function(type){
            if(type.is_checked){
              choosen_types.push(type.id);
            }
        });

        if(choosen_types.length <= 0){
          // $cordovaToast.show('Please choose the therephy type.','short','center');
          var user_goal   =   Core.goal().fetch_user_goal();
          if(user_goal.indexOf('hypertension') >= 0){
            $state.go('hypertension');
          } else {
            $state.go('emergency_contact');
          }
          return;
        }

        Core.diabetes().update_therephy_type(choosen_types).then(function(){
          // $cordovaToast.show('Updated','short','bottom');

          var user_goal   =   Core.goal().fetch_user_goal();
          if(user_goal.indexOf('hypertension') >= 0){
            $state.go('hypertension');
          } else {
            $state.go('emergency_contact');
          }
          // $state.go('diabetes_range');
        },function(error){
          $cordovaToast.show(error.message,'short','center');
        });
      }

      $scope.init();
  }])
