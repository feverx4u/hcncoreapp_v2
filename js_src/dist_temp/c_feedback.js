angular.module('hcncore.controllers')
.controller('FeedbackController',["$scope", "$rootScope", "$ionicPopover", "$state", "$cordovaToast", "$timeout", "Core", "$ionicLoading", "$timeout", "$ionicHistory", function($scope,$rootScope,$ionicPopover,
  $state,$cordovaToast,$timeout,Core,$ionicLoading,$timeout,$ionicHistory){
  $scope.$state    =  $state;

  $scope.init   =   function(){
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
    }, 10);
    $scope.variables();
  }

  $scope.variables   =  function(){

       $scope.feedback_data  =  {
          'phone_number':'',
          'name':'',
          'email':'',
          'message':''
      };

      var user  =   Core.user().logged_in_user();
      console.log(user);
      $scope.feedback_data.phone_number = user.phone_number;
      $scope.feedback_data.email        = user.email;
      $scope.feedback_data.name         = user.name;
  }

   $scope.send_feedback = function(){

     if($scope.feedback_data.message == null || $scope.feedback_data.message == "" ){
       $cordovaToast.show("Please type your Feedback  ",'long','center');
     }else{

       $ionicLoading.show({
         template: '<ion-spinner icon="android"></ion-spinner>'
       });

       Core.user_feedback().commit($scope.feedback_data).then(function(data){
         $timeout(function(){
           $ionicLoading.hide();
           $scope.feedback_data.message = '';
           $cordovaToast.show("Feedback sent successfully  ",'long','center');
         },200);
       },function(error){
         $ionicLoading.hide();
         console.log(error);
       });
      }
    }

  $scope.$history   =   $ionicHistory;
  $scope.open_in_app_browser  =   function(url){
    window.open(Core.network().web+encodeURI(url), '_blank', 'location=yes');
  }

  $scope.init();
}])
