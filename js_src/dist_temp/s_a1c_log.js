/*
|------------------------------------------------------------------------------
|  A1cLog - Service to Log the Info from a1c
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('A1cLog',["Log", "Network", "$q", "$resource", "locker", "User", "UserProfile", function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};

  function resource(){


    var url   =   Network.api_url+'/log/a1c';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  factory.commit  =   function(data){

    var deferred  =   $q.defer();

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }

    var profile  =   UserProfile.getActiveProfile();
    if(profile ==  null){
      return null;
    }

    data.profile_id   =  profile.id;


    if(data.a1c == null || data.a1c.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please enter a1c'
      });
      return deferred.promise;
    }
    data.a1c   =  parseFloat(data.a1c);
    if(data.a1c > 20){
      deferred.reject({
        'staut':400,
        'message':'A1c should be less than 20'
      });
      return deferred.promise;
    }

    // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
    var user_data   =   data;
    resource().add(data,function(data){
      Log.info('A1cLog:commit',data);
      deferred.resolve(data);
    },function(data){
        Log.error('A1cLog:commit',data);
        deferred.reject(error);
    });
    return deferred.promise;
  }
  return factory;
}])
