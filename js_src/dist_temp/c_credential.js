/*
 |---------------------------------------------------------------
 |  CredentialController - Controller is used to manage the credential view
 |---------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-02
 */
 angular.module('hcncore.controllers')
 .controller('CredentialController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "$ionicLoading", "$cordovaToast", "$ionicNavBarDelegate", "$ionicHistory", "$ionicScrollDelegate", function($rootScope,$scope,$state,$ionicPlatform,Core,$timeout,
   $ionicLoading,$cordovaToast,$ionicNavBarDelegate,$ionicHistory,$ionicScrollDelegate){

   $scope.v   =   {};

   //Initializing class
   $scope.init   =  function(){
     $ionicNavBarDelegate.showBar(false);
     $ionicHistory.clearHistory();
     $scope.variables();
     $scope.set_active_tab_handler();
   }

   $scope.take_screen_to_top  =   function(id){
     var elem   =   document.getElementById(id);
     var top =  elem.getBoundingClientRect().top - 10;
     console.log(top);
      $ionicScrollDelegate.$getByHandle('mainScroll').scrollTo(0,top,true);
      console.log($ionicScrollDelegate.$getByHandle('mainScroll').getScrollPosition());
   }


   $scope.variables =   function(){
     var v  =   $scope.v;

    //  if($state.current.name == 'credential.register'){
       v.active_tab   =   'REGISTER';
    //  } else {
      //  v.active_tab   =   'LOGIN';
    //  }

     v.login_data   =   {
       'username':'',
       'username_err':'',
       'password':'',
       'password_err':'',
       'is_pass_visible':false,
       'is_otp':false,
       'country_code':'91',
       'country_name':'India',
       'in_prog':false
     };

     v.register_data   =   {
       'username':'',
       'username_err':'',
       'password':'',
       'password_err':'',
       'is_pass_visible':false,
       'repeat_password':'',
       'password_con_err':'',
       'is_pass_con_visible':false,
       'is_otp':false,
       'country_code':'91',
       'country_name':'India',
       'in_prog':false
     };

     v.country_code   = country_code.items;
    //  console.log(v.country_code);
   }

   $scope.$watch('v.login_data.country_code',function(){
     var code = $scope.v.login_data.country_code;
     angular.forEach($scope.v.country_code,function(item){
       if(item.code   == code){
         $scope.v.login_data.country_name   = item.country;
       }
     });
   });
   $scope.open_country_list    =  function(id){

     var event = new MouseEvent('mousedown', {
        // 'view': window,
        // 'bubbles': true,
        'cancelable': true
    });
    var cb;
    if(id == undefined){
        cb = document.getElementById('country_list');
    } else {
      cb = document.getElementById(id);
    }

    // cb.size=4;
    cb.dispatchEvent(event);

    //  document.getElementById('country_list').click();
   }
   //Method is used to change the tab
   $scope.change_tab  =   function(tab_name){
      var v  =   $scope.v;
      if(tab_name == 'LOGIN'){
        // $state.go('credential.login');
        v.active_tab   =   'LOGIN';
      } else if(tab_name == 'REGISTER'){
        // $state.go('credential.register');
        v.active_tab   =   'REGISTER';
      }
   }

   //Set the Active Tab handler
   $scope.set_active_tab_handler  =   function(){
     var v  =   $scope.v;
     $rootScope.$on('$stateChangeSuccess',
     function(event, toState, toParams, fromState, fromParams){
       if(toState.name == 'credential.register'){
         v.active_tab   =   'REGISTER';
       } else if(toState.name == 'credential.login'){
         v.active_tab   =   'LOGIN';
       }
     });
   }


   //Authenticating the User.
   $scope.authenticate_user   =   function(){
     var login_data  =   $scope.v.login_data;
     login_data.username_err = '';
     login_data.password_err = '';
     var login_data_username = login_data.username+'';
     if(login_data.username.length == 0){
       login_data.username_err = 'Phone Number should not be empty.';
       return;
     }

     if(login_data_username.length < 10){
       login_data.username_err = 'Phone Number should be of 10 digits.';
       return;
     }

     if(!login_data.is_otp){
       if(login_data.password.length < 6){
         login_data.password_err = 'Password should be of 6 charactes.';
         return;
       }
     }

     login_data.in_prog   =   false;
     $ionicLoading.show({
         template: '<ion-spinner icon="android"></ion-spinner>'
     });

     Core.user().authenticate('+'+login_data.country_code+''+login_data.username,login_data.password,login_data.is_otp).then(function(data){
       console.log("Login Success");
       $timeout(function () {
         login_data.in_prog   =   false;
         console.log("Login Success");

         var user_data  =   data.user_data;
        //  var initial_flow   =   parseInt(data.is_init_flow);
         if(user_data.profiles != null && user_data.profiles.length > 0){
           console.log("Login Success");

           $state.go('med_dashboard');
         } else {
           console.log("Login Success222");

           $state.go('user_info_med');
         }
         $ionicLoading.hide();

       }, 2000);
     },function(error){
       console.log("Login Success222222222");

       login_data.in_prog   =   false;
       $cordovaToast.show(error.message,'short','center');
       $ionicLoading.hide();
     },function(data){
       console.log(data);
       if(data.message == 'HIDE_LOADER'){
         login_data.in_prog   =   false;
         $ionicLoading.hide();
       } else if(data.message == 'SHOW_LOADER') {
         login_data.in_prog   =   true;
         $ionicLoading.show({
             template: '<ion-spinner icon="android"></ion-spinner>'
         });
       }
     });

   }

   $scope.register_user    =  function(){
     var register_data  =   $scope.v.register_data;
     register_data.username_err = '';
     register_data.password_err = '';
     register_data.password_con_err = '';
     var register_data_username = register_data.username+'';
     if(register_data.username.length == 0){
       register_data.username_err = 'Phonenumber should not be empty.';
       return;
     }

     if(register_data_username.length < 10){
       register_data.username_err = 'Phonenumber should be of 10 digits.';
       return;
     }

     if(register_data.password.length < 6){
       register_data.password_err = 'Password should be of 6 charactes.';
       return;
     }

     if(register_data.password != register_data.repeat_password){
       register_data.password_con_err = 'Password does not match.';
       return;
     }

     register_data.in_prog   =   false;

     $ionicLoading.show({
         template: '<ion-spinner icon="android"></ion-spinner>'
     });

     Core.user().register('+'+register_data.country_code+''+register_data.username,register_data.password).then(function(data){
       $timeout(function () {
         register_data.in_prog   =   false;
         $ionicLoading.hide();
        //  $ionicLoading.show({
        //      template: 'User Registration Successfully Finished.'
        //  });
        //  $timeout(function () {
        //    $ionicLoading.hide();
        //  },2000);
        $state.go('user_info_med');

       }, 2000);
     },function(error){
       $cordovaToast.show(error.message,'short','center');
       register_data.in_prog   =   false;
       $ionicLoading.hide();
     });


   }

   $scope.init();
 }])
