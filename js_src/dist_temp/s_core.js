/*
 |---------------------------------------------------------------
 |  Core - Single point access to access the all the application Feature
 |---------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-01
 */
 angular.module('hcncore.services')
.factory('Core',["Log", "Network", "User", "Goal", "Diabetes", "Hypertenstion", "EmergencyContact", "LogInfo", "ScheduleInfo", "UserAnalytics", "FileOp", "UserReports", "UserProfile", "PhoneContacts", "MedSocial", "PhrInfo", "TimelineData", "Feedback", "Calender", "Insights", "SocialMessages", function(Log,Network,User,Goal,Diabetes,Hypertenstion,EmergencyContact,LogInfo,ScheduleInfo,
  UserAnalytics,FileOp,UserReports,UserProfile,PhoneContacts,MedSocial,PhrInfo,TimelineData, Feedback,Calender,Insights,SocialMessages){
  var factory   =   {}

  //Method will return the user object to handle all user
  //related funcationality
  factory.user  =  function(){
    return User;
  }

  factory.user_profile  =   function(){
    return UserProfile;
  }

  //Method will return the goal object to handle all goal
  //related funcationality
  factory.goal  =  function(){
    return Goal;
  }

  //Method will return the goal object to handle all goal
  //related funcationality
  factory.diabetes  =  function(){
    return Diabetes;
  }

  factory.hypertension  =   function(){
    return Hypertenstion;
  }

  factory.emergency_contact  =   function(){
    return EmergencyContact;
  }

  factory.logdata   =   function(){
    return LogInfo;
  }

  factory.phrdata   =   function(){
    return PhrInfo;
  }

  factory.scheduledata   =   function(){
    return ScheduleInfo;
  }
  //Method will return the log object to handle the loging the log
  //For the application.
  factory.log   =   function(){
    return Log;
  }

  factory.schedule   =   function(){
    return Schedule;
  }

  //Method will return the log object to handle the network status call
  factory.network   =   function(){
    return Network;
  }

  //Method will return the log object to handle the network status call
  factory.user_analytics   =   function(){
    return UserAnalytics;
  }

  factory.file  =   function(){
    return FileOp;
  }

  factory.user_report  =   function(){
    return UserReports;
  }


  factory.phone_contacts  =   function(){
    return PhoneContacts;
  }

  factory.med_social  =   function(){
    return MedSocial;
  }

  factory.timeline = function(){
    return TimelineData;
  }

  factory.insights = function(){
    return Insights;
  }

  factory.user_feedback = function(){
    return Feedback;
  }

  factory.calender = function(){
    return Calender;
  }

  factory.social_messages = function(){
    return SocialMessages;
  }
  return factory;
}])
