angular.module('hcncore.controllers')
.controller('UserMedinfoController',["$rootScope", "$scope", "$state", "$timeout", "Core", "$ionicPopup", "$cordovaImagePicker", "$jrCrop", "$cordovaCamera", "Media", "$cordovaToast", "$ionicLoading", "$ionicHistory", function($rootScope,$scope,$state,$timeout,Core,$ionicPopup,
  $cordovaImagePicker,$jrCrop,$cordovaCamera,Media,$cordovaToast,$ionicLoading,$ionicHistory){

    $scope.init    =  function(){
      // $ionicNavBarDelegate.showBar(false);
      $timeout(function () {
          $rootScope.is_big_bar   =   false;
      }, 10);
      // document.querySelect
      $scope.variable();

       $scope.fetch_login_user();
       $ionicHistory.clearHistory();
    }


    $scope.v     =   {};

    $scope.user  =   null;
    $scope.variable  =   function(){
       var v   =   $scope.v;
       v.name  =   '';
       v.profile_image  =   null;
       v.image_changed   =   false;

       v.profile_id    = null;
    }

    //Fecthing the loged in user
    $scope.fetch_login_user   =   function(){
      var v   =   $scope.v;
      var user  = Core.user().logged_in_user();
      console.log(user);
      if(user == null){
        return;
      }

      $scope.user    =   user;

      if(user.profiles != null){
        angular.forEach(user.profiles,function(profile){
          if(profile.is_virtual == 0){
            v.name  = profile.name;
            v.profile_id   =   profile.id;
            if(profile.profile_media != null){
               v.profile_image  = profile.profile_media.uri;
            }
          }
        });
      }

    }

    $scope.update_user_info   =   function(){

      var v   =   $scope.v;

      if(v.name == ""){
        $cordovaToast.show("Please Enter Your Name",'short','center');
        return;
      }

      if(v.profile_id == null){
        if(v.image_changed && v.profile_image != null){
          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });

          Media.upload(v.profile_image,'BASE_64_IMAGE',function(data){
            console.log(data);
            var response  =   JSON.parse(data.currentTarget.response);
            var media   =   response.media;
            Core.user_profile().add_profile({
               'name':v.name,
               'profile_pic_media':media.id,
               'is_virtual':'FALSE',
               'phone_number':$scope.user.phone_number
            }).then(function(data){
              $state.go('med_dashboard');
              console.log(data);
            },function(error){
              console.log(error);
              $cordovaToast.show(error.message,'short','center');
            });

            $ionicLoading.hide();
          },function(data){
            console.log(data);
            $cordovaToast.show('Failed uploading image.','short','center');
             $ionicLoading.hide();

          },function(data){
            console.log(data);
            $cordovaToast.show('Image upload aborted','short','center');
             $ionicLoading.hide();
          },function(data){
            console.log(data);
          });

        } else {
          var data    =  {
            'name':v.name,
            'is_virtual':'FALSE',
            'phone_number':$scope.user.phone_number
          };

          if(v.profile_image == null){
            data['profile_pic_media']    = 'null';
          }

          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });

          Core.user_profile().add_profile(data).then(function(data){
            $state.go('med_dashboard');
            $ionicLoading.hide();

          },function(error){
            $cordovaToast.show(error.message,'short','center');
          });
        }
      } else {
        if(v.image_changed && v.profile_image != null){
          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });

          Media.upload(v.profile_image,'BASE_64_IMAGE',function(data){
            console.log(data);
            var response  =   JSON.parse(data.currentTarget.response);
            var media   =   response.media;
            Core.user_profile().update_profile(v.profile_id,{
               'name':v.name,
               'profile_pic_media':media.id,
               'is_virtual':'FALSE'
            }).then(function(data){
              $state.go('med_dashboard');
              console.log(data);
            },function(error){
              console.log(error);
              $cordovaToast.show(error.message,'short','center');
            });

            $ionicLoading.hide();
          },function(data){
            console.log(data);
            $cordovaToast.show('Failed uploading image.','short','center');
             $ionicLoading.hide();

          },function(data){
            console.log(data);
            $cordovaToast.show('Image upload aborted','short','center');
             $ionicLoading.hide();
          },function(data){
            console.log(data);
          });

        } else{
          var data    =  {
            'name':v.name,
            'is_virtual':'FALSE'
          };

          if(v.profile_image == null){
            data['profile_pic_media']    = 'null';
          }

          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });

          Core.user_profile().update_profile(v.profile_id,data).then(function(data){
            $state.go('med_dashboard');
            $ionicLoading.hide();

          },function(error){
            $cordovaToast.show(error.message,'short','center');
          });
        }
      }


    }

    $scope.upload_popup  =   null;
    $scope.upload_options   =   function(){
      $scope.upload_popup = $ionicPopup.show({
        templateUrl: 'file_upload_popup.html',
        title: 'Upload',
        scope: $scope,
        'cssClass':'report_popup'
      });
    }

    $scope.close_upload_popup    =   function(){
      $scope.upload_popup.close();
    }

    function take_me_to_croper(uri){
      $jrCrop.crop({
           url: uri,
           width: 200,
           height: 200,
           // title: 'Move and Scale'
       }).then(function(canvas) {
           // success!
           var image = canvas.toDataURL();
           $scope.v.profile_image   = image;
           $scope.v.image_changed   = true;
           // console.log(image);
       }, function(error) {
           console.log(error);
           // User canceled or couldn't load image.
       });
    }


    $scope.open_image_picker   =   function(){
      var options = {
        maximumImagesCount: 1,
        width: 800,
        height: 800,
       };

       $scope.close_upload_popup();
      $cordovaImagePicker.getPictures(options)
      .then(function (results) {
         for (var i = 0; i < results.length; i++) {
           console.log('Image URI: ' + results[i]);
            take_me_to_croper(results[i]);
         }
       }, function(error) {
         // error getting photos
       });
    }

    $scope.open_camera_for_image   =   function(){
         $scope.close_upload_popup();
          var options = {
            quality: 50,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA,
            encodingType: Camera.EncodingType.PNG,
            targetWidth:800,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation:true
         };

         $cordovaCamera.getPicture(options).then(function(imageURI) {
           take_me_to_croper(imageURI);
         }, function(err) {
           // error
         });
    }

    $scope.remove_image  =   function(){
       $scope.v.profile_image   = null;
       $scope.close_upload_popup();
       $scope.v.image_changed  = true;
    }

    $scope.init();
}])
