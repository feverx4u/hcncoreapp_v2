/*
 |-------------------------------------------------------------------
 | PhrEventTypeRecordController - Controller for get PHR Upload Record Feature
 |-------------------------------------------------------------------
 |  @author:  Shekh Rizwan<rizwan@hirarky.com>
 |  @date:  2016-11-01
 */
 angular.module('hcncore.controllers')
.controller('PhrEventTypeRecordController',["$rootScope", "$scope", "$state", "$stateParams", "$timeout", "$state", "Core", "$ionicLoading", "$ionicPopup", "$ionicHistory", function($rootScope,$scope,$state,$stateParams,$timeout,$state,Core,$ionicLoading,$ionicPopup,$ionicHistory){
    $scope.$state    =  $state;
    $scope.v  =   {};

    $scope.init   =   function(){
      var v   =   $scope.v;

      $timeout(function () {
          $rootScope.is_big_bar   =   false;
          v.event_name =  $stateParams.event_name;
          v.type_name =  $stateParams.type_name;
          v.type_count =  $stateParams.type_count;
          $scope.phr_show_media();
      }, 10);
    }

    // Phr show API Calling Method
    $scope.media_array = [];
    $scope.media_record = [];
     $scope.phr_show_media   =   function(){
       $ionicLoading.show({
           template: '<ion-spinner icon="android"></ion-spinner>'
       });

       // API calling for getting phr media files
       Core.phrdata().phr_event_type_media().get($scope.v.event_name,$scope.v.type_name).then(function(data){

         angular.forEach(data.phr,function(data){
           console.log(data);
           $scope.media_record.push(data);

         });
         console.log($scope.media_record);
         $ionicLoading.hide();
       },function(error){
         console.log("error "+error);
          $ionicLoading.hide();
       },function(update){
         console.log("update "+update);
          $ionicLoading.hide();
       });
     }


     // popup for get Image/Doc details
     $scope.create_doc_details_popup  =   null;
     $scope.v.current_record_to_see   =   {};
     $scope.show_doc_details_popup  =   function(record){
        $scope.v.current_record_to_see = record;
       $scope.create_doc_details_popup = $ionicPopup.show({
         templateUrl: 'show_doc_details_popup.html',
         title: 'PHR DETAILS',
         scope: $scope,
         'cssClass':'create_event_popup'
       });
     }

     $scope.view_doc_details_popup  =  function(media_uri){
       $scope.create_doc_details_popup.close();
       $scope.view_file(media_uri);
     }
     $scope.close_doc_details_popup  =  function(){
       $scope.create_doc_details_popup.close();
     }

     // file opner method
     $scope.view_file    =   function(file){
       console.log(file);
       window.open(file,'_system');
     }

     $scope.navigate_to_back = function(){
       $ionicHistory.goBack();
     }

    $scope.init();

  }])
