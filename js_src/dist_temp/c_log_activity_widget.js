/*
 |-------------------------------------------------------------------
 | LogActivityWidgetController - Controller for log of activity
 |-------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-05
 */
angular.module('hcncore.controllers')
.controller('LogActivityWidgetController',["$rootScope", "$scope", "$state", "$ionicPlatform", "Core", "$timeout", "ionicTimePicker", "ionicDatePicker", "$cordovaToast", "$ionicPopup", "$ionicLoading", "$ionicHistory", function($rootScope,$scope,$state,$ionicPlatform,Core,
  $timeout,ionicTimePicker,ionicDatePicker,$cordovaToast,$ionicPopup,$ionicLoading,$ionicHistory){

  $scope.$state    =  $state;
  $scope.v  =   {};

  $scope.init   =   function(){
    $timeout(function () {
        $rootScope.is_big_bar   =   false;
    }, 10);
    $scope.variables();


  }


  $scope.variables   =  function(){
      var v   =   $scope.v;
      v.activity_search_text  =   '';
      v.is_searching  =   false;
      v.search_results    =   [];

      v.is_check_enabled  =   false;
      v.activity_log_input  =  {
          'activity_id':'',
          'activity_name':'',
          'duration':'',
          'log_date':'',
          'log_date_view':'',
          'log_time':'',
          'log_time_view':'',
      };

      v.activity_log_input.log_date     =   moment().format('YYYY-MM-DD');

      $scope.revalidate_date();

      var time      =   moment().format('YYYY-MM-DD hh:mm A');
      $scope.revalidate_time(time);
  }



  $scope.validate_for_check   =   function(){
    var v   =   $scope.v;
    // console.log(111);
    if(v.activity_log_input.duration == null || v.activity_log_input.duration.length == 0){
      v.is_check_enabled  =   false;
      return;
    }

    if(v.activity_log_input.activity_id == null || v.activity_log_input.activity_id.length == 0){
      v.is_check_enabled  =   false;
      return;
    }

    v.is_check_enabled  =   true;
  }

  $scope.revalidate_date =  function(){
      var v   =   $scope.v;
      v.activity_log_input.log_date_view     =   moment(v.activity_log_input.log_date).format('Do MMM,YYYY');
  }

  $scope.revalidate_time =  function(time){
      var v   =   $scope.v;
      v.activity_log_input.log_time     =   moment(time,'YYYY-MM-DD hh:mm A').format('HH:mm');
      console.log(v.activity_log_input.log_time);
      v.activity_log_input.log_time_view     =   moment(time,'YYYY-MM-DD hh:mm A').format('hh:mm A');
      v.activity_log_input.log_time_time     =   parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('HH'))*3600 + parseInt(moment(time,'YYYY-MM-DD hh:mm A').format('mm'))*60;
  }

  $scope.search_activity  =   function(){
      var v   =   $scope.v;
      v.is_searching  =   true;
      Core.logdata().activity_log().search(v.activity_search_text).then(function(data){
           v.search_results   =   data.message;
           v.is_searching  =   false;
      },function(data){
          v.is_searching  =   false;
      });
  }

  $scope.log_activity   =   function(){

    $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
            });

    var v   =   $scope.v;
      Core.logdata().activity_log().commit(v.activity_log_input).then(function(data){
          $ionicLoading.hide();
        $scope.log_success_popup("Activity Logged");

      },function(error){
          $ionicLoading.hide();
        console.log(error.message);
        $cordovaToast.show(error.message,'short','center');
      });
  }

  $scope.choose_activity  =   function(item){
    // alert(1111);
    var v   =   $scope.v;
    v.activity_log_input.activity_id   = item.id+'';
    v.activity_log_input.activity_name   = item.activity_name;
    v.activity_search_text  =   '';
    v.search_results   =   [];
    // console.log(v.activity_log_input.activity_id);
    // console.log(v.is_searching);
  }

  $scope.log_pop_message  =   null;
  $scope.log_popup  =   null;
  $scope.log_success_popup  =   function(message){
    $scope.log_pop_message  =   message;
    $scope.log_popup = $ionicPopup.show({
      templateUrl: 'log_sucess_popup.html',
      title: 'Successfully Logged',
      scope: $scope,
      'cssClass':'log_success_popup'
    });
  }

  $scope.close_log_success_popup  =  function(notes){
    $scope.log_popup.close();
    $timeout(function(){
      $state.go('main_tabs.log_widget');
      $rootScope.$broadcast('data:loggged');
    },100);
  }

  // Go back confirmation popup
   $scope.showConfirm = function() {
     var v   =   $scope.v;
     if(!v.is_check_enabled){
       $ionicHistory.goBack();
     }else {
       var confirmPopup = $ionicPopup.confirm({
         title: 'Log Activity',
         template: 'Are you sure you want to go back?'
       });
       confirmPopup.then(function(res) {
         if(res) {
           v.is_check_enabled  =   false;
           $state.go("main_tabs.log_widget");
         } else {
           console.log('You are not sure');
         }
       });
     }
   };

  $scope.init();
}])
