/*
|------------------------------------------------------------------------------
|  SymptomsLog - Service to Log the Info from Symptoms
|------------------------------------------------------------------------------
|  @author:  Lokesh Kumawat<lokesh@hirarky.com>
|  @date:  2016-09-05
*/
angular.module('hcncore.services')
.factory('SymptomsLog',["Log", "Network", "$q", "$resource", "locker", "User", "UserProfile", function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};

  function resource(){
    var url   =   Network.api_url+'/log/symptom_log';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  function symptom(){
    var url   =   Network.api_url+'/symptom';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
    });
  }

  function locations(){
    var url   =   Network.api_url+'/symptom_location';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
    });
  }

  function nature_pain(){
    var url   =   Network.api_url+'/symptom_pain_nature';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
    });
  }

  function provoking_factors(){
    var url   =   Network.api_url+'/symptom_provoking';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
    });
  }

  function releving_factors(){
    var url   =   Network.api_url+'/symptom_relieving';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
    });
  }

  function enviorments(){
    var url   =   Network.api_url+'/enviorment';

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
    });
  }

  factory.symptoms_location   = function(){
    var deferred  =   $q.defer();

          locations().get({
          },function(data){
            Log.info('SymptomsLog:symptoms_location',data.symptom_locations);
              deferred.resolve({
                'staut':200,
                'message':data.symptom_locations
              });
          });
    return deferred.promise;
  }

  factory.symptoms   = function(){
    var deferred  =   $q.defer();

          symptom().get({
          },function(data){
            Log.info('SymptomsLog:symptom',data.symptoms);
              deferred.resolve({
                'staut':200,
                'message':data.symptoms
              });
          });
    return deferred.promise;
  }

  factory.nature_pain   = function(){
    var deferred  =   $q.defer();

          nature_pain().get({
          },function(data){
            Log.info('SymptomsLog:nature_pain',data.symptom_pain_natures);
              deferred.resolve({
                'staut':200,
                'message':data.symptom_pain_natures
              });
          });
    return deferred.promise;
  }

  factory.provoking_factors   = function(){
    var deferred  =   $q.defer();

          provoking_factors().get({
          },function(data){
            Log.info('SymptomsLog:provoking_factors',data.symptom_provokings);
              deferred.resolve({
                'staut':200,
                'message':data.symptom_provokings
              });
          });
    return deferred.promise;
  }


  factory.releving_factors   = function(){
    var deferred  =   $q.defer();

          releving_factors().get({
          },function(data){
            Log.info('SymptomsLog:releving_factors',data.symptom_relieving);
              deferred.resolve({
                'staut':200,
                'message':data.symptom_relieving
              });
          });
    return deferred.promise;
  }

  factory.enviorment   = function(){
    var deferred  =   $q.defer();

          enviorments().get({
          },function(data){
            Log.info('SymptomsLog:enviorment',data.enviorments);
              deferred.resolve({
                'staut':200,
                'message':data.enviorments
              });
          });
    return deferred.promise;
  }


  factory.symptoms_search  =   function(search_query){
      var deferred  =   $q.defer();
          if(search_query.length <= 2){
            deferred.resolve({
              'staut':200,
              'message':[]
            });
          } else {
            symptom().get({
              'sq':search_query
            },function(data){

              Log.info('SymptomsLog:event',data.symptoms);
                deferred.resolve({
                  'staut':200,
                  'message':data.symptoms
                });
            });
          }
      return deferred.promise;
  }


  factory.commit  =   function(data){

    var deferred  =   $q.defer();

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }

    var profile  =   UserProfile.getActiveProfile();
    if(profile ==  null){
      return null;
    }

    data.profile_id   =  profile.id;


    // if(data.activity_id == null || data.activity_id.length <= 0 ){
    //   deferred.reject({
    //     'staut':400,
    //     'message':'Please choose activity'
    //   });
    //   return deferred.promise;
    // }
    if(data.symptom_id == null || data.symptom_id.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please select symptom'
      });
      return deferred.promise;
    }


    if(data.severity == null || data.severity.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please select severity'
      });
      return deferred.promise;
    }

    if(data.location_ids != null){
        data.location_ids   =   data.location_ids.join(',');
    }

    if(data.pain_nature_ids != null){
      data.pain_nature_ids   =   data.pain_nature_ids.join(',');
    }

    if(data.provoking_ids != null){
      data.provoking_ids   =   data.provoking_ids.join(',');
    }

    if(data.related_symptom_ids != null){
      data.related_symptom_ids   =   data.related_symptom_ids.join(',');
    }

    if(data.relieving_ids != null){
      data.relieving_ids   =   data.relieving_ids.join(',');
    }

    var user_data   =   data;
    resource().add(data,function(data){
        Log.info('SymptomsLog:commit',data);
        deferred.resolve(data);
    },function(data){
        Log.error('SymptomsLog:commit',data);
        deferred.reject(error);
    });
    return deferred.promise;
  }
  return factory;
}])
