angular.module('hcncore.filters')
.filter('symptom_date_format',function(){
  return function(date){
    return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
  }
})
