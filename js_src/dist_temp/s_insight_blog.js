/*
|------------------------------------------------------------------------------
|  Insights - Service to get hcncare blog data
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-12-19
*/
angular.module('hcncore.services')
.factory('Insights',["Log", "Network", "$q", "$resource", "locker", "User", "UserProfile", "$timeout", function(Log,Network,$q,$resource,locker,User,UserProfile,$timeout){
  var factory   =   {};

  function resource(){

    var url   =   'http://blog.hcncare.com/wp-json/wp/v2/posts?categories=:id';
      // var url   =   'http://blog.hcncare.com/wp-json/wp/v2/categories'+'?slug='+':slug_name';


      return  $resource(url, {},{
          'get': { method:'GET',isArray: true}
      });
    }

    function media_resource(){

      var url   =   'http://blog.hcncare.com/wp-json/wp/v2/media/:id';
        // var url   =   'http://blog.hcncare.com/wp-json/wp/v2/categories'+'?slug='+':slug_name';


        return  $resource(url, {},{
            'get': { method:'GET'}
        });
      }

    factory.media_resource  =   media_resource;
    factory.get  =   function(id){
      var deferred  =   $q.defer();

      var user  =   User.logged_in_user();
      if(user == null){
         deferred.reject(null);
         return deferred.promise;
      }

      $timeout(function(){
        resource().get({
          'id': id
        },function(data){
            Log.info('Insights:get',data);
            deferred.resolve(data);
        },function(data){
            Log.error('Insights:get',data);
            deferred.reject(data);
        });
      },500);

      return deferred.promise;
    }

  return factory;
}])
