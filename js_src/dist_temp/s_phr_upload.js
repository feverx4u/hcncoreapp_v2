/*
|------------------------------------------------------------------------------
|  PhrUpload - Service to upload/add Phr Info from app
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-11-04
*/
angular.module('hcncore.services')
.factory('PhrUpload',["Log", "Network", "$q", "$resource", "locker", "User", "UserProfile", function(Log,Network,$q,$resource,locker,User,UserProfile){
  var factory   =   {};

  function resource(){

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }
    var profile  =   UserProfile.getActiveProfile();
    if(profile ==  null){
      return null;
    }


    var url   =   Network.api_url+'/profile/'+profile.id+'/phr';
    console.log(user.id);

    return  $resource(url, {},{
        'get': { method:'GET',params:{}},
        'add': { method:'POST',params:{}},
        'show': { method:'GET',params:{}},
        'update': { method:'PUT',params:{}},
        'delete': { method:'DELETE',params:{}}
    });
  }

  factory.commit  =   function(data){

    var deferred  =   $q.defer();

    var user  =   User.logged_in_user();
    if(user == null){
      return null;
    }

    var profile  =   UserProfile.getActiveProfile();
    if(profile ==  null){
      return null;
    }

    data.profile_id   =  profile.id;

    if(data.media_id == null || data.media_id.length <= 0){
      deferred.reject({
        'staut':400,
        'message':'Please Attach Media'
      });
      return deferred.promise;
    }

    if(data.event_name == null || data.event_name.length <= 0 ){
      deferred.reject({
        'staut':400,
        'message':'Please Select Event'
      });
      return deferred.promise;
    }

    if(data.event_type == null || data.event_type.length <= 0){
      deferred.reject({
        'staut':400,
        'message':'Please Select Type'
      });
      return deferred.promise;
    }

    if(data.phr_date == null || data.phr_date.length <= 0){
      deferred.reject({
        'staut':400,
        'message':'Please enter date'
      });
      return deferred.promise;
    }

    if(data.phr_time == null || data.phr_time.length <= 0){
      deferred.reject({
        'staut':400,
        'message':'Please enter time'
      });
      return deferred.promise;
    }

    // data.log_time   =   moment(data.log_date+' '+data.log_time,'YYYY-MM-DD hh:mm:ss');
    var user_data   =   data;
    console.log(data);
    resource().add({
      'event_name':data.event_name,
      'type':data.event_type,
      'phr_date':data.phr_date,
      'phr_time':data.phr_time,
      'notes':data.notes,
      'media_id':data.media_id
    },function(data){
        Log.info('PhrUpload:commit',data);
        deferred.resolve(data);
    },function(data){
        Log.error('PhrUpload:commit',data);
        deferred.reject(error);
    });
    return deferred.promise;
  }
  return factory;
}])
