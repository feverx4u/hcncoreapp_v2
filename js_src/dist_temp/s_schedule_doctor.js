/*
|  ScheduleDocAppointment - Service to Schedule the Doctor Appointment Info from app
|------------------------------------------------------------------------------
|  @author:  Shekh Rizwan<rizwan@hirarky.com>
|  @date:  2016-10-18
*/
angular.module('hcncore.services')
.factory('ScheduleDocAppointment',["Log", "Network", "$q", "$resource", "locker", "User", "UserProfile", function(Log,Network,$q,$resource,locker,User,UserProfile){
      var factory   =   {};
      function resource(){


        var url   =   Network.api_url+'/schedule/doctor';

        return  $resource(url + '/:id', {},{
            'get': { method:'GET',params:{}},
            'add': { method:'POST',params:{}},
            'show': { method:'GET',params:{}},
            'update': { method:'PUT',params:{}},
            'delete': { method:'DELETE',params:{}}
        });
      }

      factory.commit  =   function(data){

        var deferred  =   $q.defer();

        var user  =   User.logged_in_user();
        if(user == null){
          return null;
        }

        var profile  =   UserProfile.getActiveProfile();
        if(profile ==  null){
          return null;
        }

        data.profile_id   =  profile.id;

        if(data.hospital_name == null || data.hospital_name.length <= 0 ){
          deferred.reject({
            'staut':400,
            'message':'Please Enter Hospital Name'
          });
          return deferred.promise;
        }

        if(data.doctor_name == null || data.doctor_name.length <= 0 ){
          deferred.reject({
            'staut':400,
            'message':'Please Enter Doctor Name'
          });
          return deferred.promise;
        }

        if(data.notify_me == null || data.notify_me.length <= 0 ){
          deferred.reject({
            'staut':400,
            'message':'Please choose Notify time'
          });
          return deferred.promise;
        }

        var user_data   =   data;
        console.log(data);
        resource().add({
          'profile_id':data.profile_id,
          'hospital_name':data.hospital_name,
          'doctor_name':data.doctor_name,
          'schedule_date':data.doc_date,
          'schedule_time':data.doc_time,
          'notify_before':data.notify_me,
          'notes':data.notes,
          'follow_ups':data.follow_ups,

        },function(data){
            Log.info('ScheduleDocAppointment:commit',data);
            deferred.resolve(data);
        },function(data){
            Log.error('ScheduleDocAppointment:commit',data);
            deferred.reject(error);
        });
        return deferred.promise;
      }

      factory.resource  = resource;
      return factory;
    }])
