/*
 |------------------------------------------------------------------------------
 |  Network - Loging of  the Log
 |------------------------------------------------------------------------------
 |  @author:  Lokesh Kumawat<lokesh@hirarky.com>
 |  @date:  2016-09-01
 |
 */
 angular.module('hcncore.services')
 .factory('Network',["$log", "$cordovaNetwork", function($log,$cordovaNetwork){
   var factory   =   {};

   factory.web =  'http://hcncore.hirarky.com';
  // factory.web =  'http://192.168.0.11:8000';
  //  factory.web =  'http://localhost:8000';

   factory.api_url   =   factory.web+'/api/v1';

  //  factory.api_url   =   'http://192.168.0.9:8000/api/v1';
   // factory.api_url   =   'http://localhost:8000/api/v1';

   //Method is used to get network state
   factory.getNetwork  =   function(){
     /*
      |
      | Connection Type	Description
      | Connection.UNKNOWN	Unknown connection
      | Connection.ETHERNET	Ethernet connection
      | Connection.WIFI	WiFi connection
      | Connection.CELL_2G	Cell 2G connection
      | Connection.CELL_3G	Cell 3G connection
      | Connection.CELL_4G	Cell 4G connection
      | Connection.CELL	Cell generic connection
     */
     return $cordovaNetwork.getNetwork();
   }

   //Method is used to check whether network is online or not
   factory.isOnline  =   function(){
     return $cordovaNetwork.isOnline();
   }

   //Method is used to check whether network is online or not
   factory.isOffline  =   function(){
     return $cordovaNetwork.isOffline();
   }

   return factory;
 }])
