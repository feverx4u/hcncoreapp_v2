var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var templateCache = require('gulp-angular-templatecache');
var ngAnnotate = require('gulp-ng-annotate');
var useref = require('gulp-useref');
var uglify   =   require('gulp-uglify');


var paths = {
  sass: ['./scss/**/*.scss'],
  templateCache: ['./www/templates/**/*.html'],
  ng_annotate: ['./www/js/*.js'],
  useref: ['./www/*.html']
};

gulp.task('default', ['sass','templatecache', 'ng_annotate', 'useref']);

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('templatecache', function(done){
  gulp.src('./www/templates/**/*.html')
  .pipe(templateCache({
    standalone:true,
    module:'hcncore.templates'
  }))
  .pipe(gulp.dest('./www/js/'))
  .on('end', done);
});


var bundle_array  = [
  './js_src/controllers/*.js',
  './js_src/directives/*.js',
  './js_src/filters/*.js',
  './js_src/services/*.js'
];

gulp.task('make-bundel',['ng_annotate_service','ng_annotate_directive','ng_annotate_filters','ng_annotate_controller'],function(){
  return gulp.src('./js_src/dist_temp/*.js').
  pipe(uglify())
  .pipe(concat('hcncore.min.js'))
  .pipe(gulp.dest('./www/js/build/'));
});

gulp.task('ng_annotate_controller',function(done){
  return gulp.src('./js_src/controllers/*.js').
  pipe(ngAnnotate({single_qoutes:true}))
  .pipe(gulp.dest('./js_src/dist_temp/'));
});

gulp.task('ng_annotate_service',function(done){
  return gulp.src('./js_src/services/*.js').
  pipe(ngAnnotate({single_qoutes:true}))
  .pipe(gulp.dest('./js_src/dist_temp/'));
});

gulp.task('ng_annotate_directive',function(done){
  return gulp.src('./js_src/directives/*.js').
  pipe(ngAnnotate({single_qoutes:true}))
  .pipe(gulp.dest('./js_src/dist_temp/'));
});

gulp.task('ng_annotate_filters',function(done){
  return gulp.src('./js_src/filters/*.js').
  pipe(ngAnnotate({single_qoutes:true}))
  .pipe(gulp.dest('./js_src/dist_temp/'));
});

gulp.task('ng_annotate', function (done) {
  gulp.src('./www/js/*.js')
  .pipe(ngAnnotate({single_quotes: true}))
  .pipe(gulp.dest('./www/dist/dist_js/app'))
  .on('end', done);
});

gulp.task('useref', function (done) {

  return gulp.src('./www/*.html')
        .pipe(useref())
        .pipe(gulp.dest('./www/dist/'))
        .on('end', done);

});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.templateCache, ['templatecache']);
  gulp.watch(paths.ng_annotate, ['ng_annotate']);
  gulp.watch(paths.useref, ['useref']);
  gulp.watch(bundle_array, ['make-bundel']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
